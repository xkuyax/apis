package me.xkuyax.utils.bungee;

import me.xkuyax.utils.mc.MCPlatforms;
import net.md_5.bungee.api.plugin.Plugin;

public class ApiPluginMain extends Plugin {

    @Override
    public void onLoad() {
        ApiPlugin.setPlugin(this);
        MCPlatforms.setCurrentPlatform(new BungeePlatform());
    }
}
