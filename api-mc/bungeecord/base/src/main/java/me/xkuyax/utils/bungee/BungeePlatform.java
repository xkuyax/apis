package me.xkuyax.utils.bungee;

import me.xkuyax.utils.mc.MCPlatform;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.Title;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class BungeePlatform implements MCPlatform<ProxiedPlayer> {

    private final ProxyServer instance;

    public BungeePlatform() {
        instance = ProxyServer.getInstance();
    }

    @Override
    public void sendChatComponent(ProxiedPlayer player, BaseComponent[] components) {
        player.sendMessage(components);
    }

    @Override
    public void sendTitleTimes(ProxiedPlayer player, int fadeIn, int fadeOut, int stay) {
        Title title = instance.createTitle();
        title.fadeIn(fadeIn);
        title.fadeOut(fadeOut);
        title.stay(stay);
        title.send(player);
    }

    @Override
    public void sendTitle(ProxiedPlayer player, String titleText) {
        Title title = instance.createTitle();
        title.title(TextComponent.fromLegacyText(titleText));
        title.send(player);
    }

    @Override
    public void sendSubTitle(ProxiedPlayer player, String subtitle) {
        Title title = instance.createTitle();
        title.subTitle(TextComponent.fromLegacyText(subtitle));
        title.send(player);
    }

    @Override
    public void sendSound(ProxiedPlayer player, String sound, float volume, float pitch) {
        //not implemented
    }

    @Override
    public void sendActionBar(ProxiedPlayer player, String message) {
        //not implemented
    }
}
