package me.xkuyax.utils.bungee;

import lombok.Getter;
import lombok.Setter;
import net.md_5.bungee.api.plugin.Plugin;

public class ApiPlugin {

    @Getter
    @Setter
    public static Plugin plugin;

}