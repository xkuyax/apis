package me.xkuyax.utils.mc;

import net.md_5.bungee.api.chat.BaseComponent;

public interface MCPlatform<T> {

    void sendChatComponent(T player, BaseComponent[] components);

    void sendTitleTimes(T player, int fadeIn, int fadeOut, int stay);

    void sendTitle(T player, String title);

    void sendSubTitle(T player, String subtitle);

    void sendSound(T player, String sound, float volume, float pitch);

    void sendActionBar(T player, String message);

}
