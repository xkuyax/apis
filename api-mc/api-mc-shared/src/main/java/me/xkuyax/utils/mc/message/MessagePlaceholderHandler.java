package me.xkuyax.utils.mc.message;

import lombok.AllArgsConstructor;
import lombok.Data;
import me.xkuyax.utils.replace.ReplaceHandler;
import sun.reflect.Reflection;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.logging.Logger;

public class MessagePlaceholderHandler {

    private static final Map<String, MessagePlaceholder<?>> PLACEHOLDER_HANDLER_MAP = new HashMap<>();

    public static <T> void register(String javaPackage) {
        PLACEHOLDER_HANDLER_MAP.put(javaPackage, new MessagePlaceholder<T>(new ReplaceHandler<>(), new HashMap<>()));
    }

    public static Map<String, MessagePlaceholder<?>> getPlaceholderMap() {
        return PLACEHOLDER_HANDLER_MAP;
    }

    public static MessagePlaceholder<?> getMessagePlaceholder() {
        for (int i = 2; i < 30; i++) {
            Class<?> clazz = Reflection.getCallerClass(i);
            if (clazz != null) {
                String pack = clazz.getPackage().getName();
                for (Map.Entry<String, MessagePlaceholder<?>> entries : getPlaceholderMap().entrySet()) {
                    String key = entries.getKey();
                    if (pack.startsWith(key)) {
                        return entries.getValue();
                    }
                }
            } else {
                break;
            }
        }
        return null;
    }

    public static void with(String var, Supplier<String> supplier) {
        MessagePlaceholder<?> projektPlaceholder = getMessagePlaceholder();
        if (projektPlaceholder == null) {
            throw new UnsupportedOperationException("MessagePlaceholder not registered. Pls use MessagePlaceholderHandler#registerMessagePlaceholderHandler");
        }
        projektPlaceholder.getIndividualPlaceholder().put(var, supplier);
    }

    public static void with(String var, String o) {
        MessagePlaceholder<?> projektPlaceholder = getMessagePlaceholder();
        if (projektPlaceholder == null) {
            throw new UnsupportedOperationException("MessagePlaceholder not registered. Pls use MessagePlaceholderHandler#registerMessagePlaceholderHandler");
        }
        projektPlaceholder.getIndividualPlaceholder().put(var, () -> o);
    }

    public static <T> void with(String var, Function<T, String> render) {
        MessagePlaceholder projektPlaceholder = getMessagePlaceholder();
        ReplaceHandler<T> placeholderHandler = projektPlaceholder == null ? null : (ReplaceHandler<T>) projektPlaceholder.getTypePlaceholder();
        if (placeholderHandler == null) {
            throw new UnsupportedOperationException("MessagePlaceholder not registered. Pls use MessagePlaceholderHandler#registerMessagePlaceholderHandler");
        }
        placeholderHandler.with(var, render);
    }

    public static <T> String[] getVariables(T player) {
        MessagePlaceholderHandler.MessagePlaceholder<?> messagePlaceholder = MessagePlaceholderHandler.getMessagePlaceholder();
        if (messagePlaceholder == null) {
            return new String[0];
        }
        List<String> list = new ArrayList<>();
        messagePlaceholder.getIndividualPlaceholder().forEach((s, supplier) -> {
            list.add(s);
            list.add(supplier.get());
        });
        try {
            MessagePlaceholder<T> placeholder = (MessagePlaceholder<T>) messagePlaceholder;
            ReplaceHandler<T> typePlaceholder = placeholder.getTypePlaceholder();
            typePlaceholder.getVariables().forEach((s, function) -> {
                String apply = function.apply(player);
                list.add(s.pattern());
                list.add(apply);
            });
        }catch (ClassCastException exception){
            Logger.getGlobal().warning("Type T is not the same as in the MessagePlaceholderHandler");
        }
        return list.toArray(new String[0]);
    }

    @Data
    @AllArgsConstructor
    public  static class MessagePlaceholder<T> {
        private ReplaceHandler<T> typePlaceholder;
        private Map<String, Supplier<String>> individualPlaceholder;
    }

}
