package me.xkuyax.utils.mc.message;

import lombok.Data;
import me.xkuyax.utils.config.additions.ConfigOptional;
import me.xkuyax.utils.config.additions.LanguageCEntry;

@Data
public class ConfigMessageOptional {

    private ConfigOptional<Integer> repeats;
    private ConfigOptional<LanguageCEntry<String>> actionBar = ConfigOptional.withDefault(LanguageCEntry.withDefault("ActionBar"));
    private ConfigOptional<LanguageCEntry<String>> titleTitle = ConfigOptional.withDefault(LanguageCEntry.withDefault("Title"));
    private ConfigOptional<LanguageCEntry<String>> titleSubTitle = ConfigOptional.withDefault(LanguageCEntry.withDefault("SubTitle"));
    private ConfigOptional<Integer> titleFadeIn = ConfigOptional.withDefault(5);
    private ConfigOptional<Integer> titleFadeOut = ConfigOptional.withDefault(5);
    private ConfigOptional<Integer> titleStay = ConfigOptional.withDefault(20);
    private ConfigOptional<String> soundType = ConfigOptional.withDefault("CLICK");
    private ConfigOptional<Float> soundVolume = ConfigOptional.withDefault(1.0f);
    private ConfigOptional<Float> soundPitch = ConfigOptional.withDefault(1.0f);
    private ConfigOptional<Integer> chatComponentMessageAmount = ConfigOptional.withDefault(1);

}
