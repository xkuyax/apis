package me.xkuyax.utils.mc.message;

import com.google.common.base.Preconditions;
import lombok.Data;
import lombok.experimental.Accessors;
import me.xkuyax.utils.StringFormatting;
import me.xkuyax.utils.StringUtils;
import me.xkuyax.utils.config.additions.DefaultResolvers;
import me.xkuyax.utils.config.additions.DynamicCEntry;
import me.xkuyax.utils.config.additions.LanguageCEntry;
import me.xkuyax.utils.mc.MCPlatform;
import me.xkuyax.utils.mc.MCPlatforms;
import me.xkuyax.utils.replace.ReplaceHandler;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.apache.commons.lang3.ArrayUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

@SuppressWarnings("UnusedReturnValue")
@Data
public class ConfigMessage<P> {

    private ReplaceHandler<P> replaceHandler = new ReplaceHandler<>();
    private LanguageCEntry<List<String>> languageMessage;
    private DynamicCEntry<ConfigMessageChatComponent> components;
    private ConfigMessageOptional optional;
    @Accessors(fluent = true)
    private String defaultMessage;
    @Accessors(fluent = true)
    private String path;
    //1 bit = prefix
    //2 bit = normal message mode
    //3 bit = sendTitle
    //4 bit = playSound
    //5 bit = sendChatComponent
    @Accessors(fluent = true)
    private int mode;
    private String modes = "NONE";
    @Accessors(fluent = true)
    private LanguageCEntry<String> prefix;
    @Accessors(fluent = true)
    private Supplier<MCPlatform<P>> platform;

    static {
        DefaultResolvers.add(ConfigMessage.class, ConfigMessageCreator::create);
    }

    protected ConfigMessage() {
        platform = MCPlatforms::getCurrentPlatform;
    }

    public ConfigMessage<P> variable(String variable, Function<P, String> mapping) {
        replaceHandler.with(variable, mapping);
        return this;
    }

    public ConfigMessage<P> variables(String... variables) {
        Preconditions.checkArgument(variables.length % 2 == 0, "Variables must be an even number!");
        for (int i = 0, variablesLength = variables.length; i < variablesLength; i += 2) {
            String variable = variables[i];
            String next = variables[i + 1];
            replaceHandler.with(variable, player -> next);
        }
        return this;
    }

    public List<String> getFormatted(P player, String[] extraVariables) {
        List<String> message;
        if ((mode & 0x1) > 0 || modes.contains("PREFIX")) {
            String prefixStr = prefix.get(player);
            message = languageMessage.get(player).stream().map(m -> prefixStr + m).collect(Collectors.toList());
        } else {
            message = languageMessage.get(player);
        }
        return formatMessage(player, extraVariables, message);
    }

    public List<String> formatMessage(P player, String[] extraVariables, List<String> message) {
        return message.stream().map(s -> formatSingleMessage(player, extraVariables, s)).collect(Collectors.toList());
    }

    public String formatSingleMessage(P player, String[] extraVariables, String message) {
        String format = replaceHandler.format(message, player);
        if (extraVariables != null && extraVariables.length > 1) {
            format = StringUtils.format(format, extraVariables);
        }
        return StringFormatting.simpleColorFormat(format);
    }

    private String[] withGlobalPlaceholder(P player, String... variables) {
        String[] v = MessagePlaceholderHandler.getVariables(player);
        if (variables == null) {
            return v;
        }
        return ArrayUtils.addAll(variables, v);
    }

    public ConfigMessage<P> send(P player, boolean useGlobalPlaceholder, String... variables) {
        if (useGlobalPlaceholder) {
            variables = withGlobalPlaceholder(player, variables);
        }
        if ((mode & 0b10) > 0 || modes.contains("MESSAGE")) {
            sendNormalMessage(player, variables);
        }
        if ((mode & 0b100) > 0 || modes.contains("TITLE")) {
            sendTitle(player, variables);
        }
        if ((mode & 0b1000) > 0 || modes.contains("SOUND")) {
            playSound(player);
        }
        if ((mode & 0b10000) > 0 || modes.contains("CHAT")) {
            sendChatComponent(player, variables);
        }
        if ((mode & 0b100000) > 0 || modes.contains("ACTIONBAR")) {
            sendActionBar(player, variables);
        }
        return this;
    }

    public ConfigMessage<P> send(P player, String... variables) {
        return this.send(player, true, variables);
    }

    public void sendNormalMessage(P player, String... variables) {
        List<String> messages = getFormatted(player, variables);
        // player.sendMessage(format);
        messages.forEach(s -> platform.get().sendChatComponent(player, TextComponent.fromLegacyText(s)));

    }

    public void sendActionBar(P player, String... replace) {
        ConfigMessageOptional optional = getOptional();
        String actionBar = optional.getActionBar().getWithForce().get(player);
        platform.get().sendActionBar(player, formatSingleMessage(player, replace, actionBar));
    }

    public void sendChatComponent(P player, String... replace) {
        Integer amount = getOptional().getChatComponentMessageAmount().getWithForce();
        for (int i = 0; i < amount; i++) {
            ConfigMessageChatComponent component = getComponents().get("optional.componentMessage." + i);
            String text = formatSingleMessage(player, replace, component.getText().getWithForce().get(player));
            BaseComponent[] baseComponents = TextComponent.fromLegacyText(text);
            for (BaseComponent baseComponent : baseComponents) {
                if (component.getClickEventAction().isPresent()) {
                    ClickEvent.Action action = component.getClickEventAction().getWithForce();
                    String clickText = component.getClickEventText().getWithForce().get(player);
                    ClickEvent clickEvent = new ClickEvent(action, formatSingleMessage(player, replace, clickText));
                    baseComponent.setClickEvent(clickEvent);
                }
                if (component.getHoverEventAction().isPresent()) {
                    HoverEvent.Action action = component.getHoverEventAction().getWithForce();
                    String hoverText = component.getHoverEventText().getWithForce().get(player);
                    HoverEvent hoverEvent = new HoverEvent(action, TextComponent.fromLegacyText(formatSingleMessage(player, replace, hoverText)));
                    baseComponent.setHoverEvent(hoverEvent);
                }
            }
            platform.get().sendChatComponent(player, baseComponents);
        }
        //   player.spigot().sendMessage(baseComponents);
    }

    public void sendTitle(P player, String... replace) {
        ConfigMessageOptional optional = getOptional();
        String title = optional.getTitleTitle().getWithForce().get(player);
        String subTitle = optional.getTitleSubTitle().getWithForce().get(player);
        int fadeIn = optional.getTitleFadeIn().getWithForce();
        int fadeout = optional.getTitleFadeOut().getWithForce();
        int stay = optional.getTitleStay().getWithForce();

        //   Titles.sendTitle(player, EnumTitleAction.TIMES, fadeIn, fadeout, stay);
        //   Titles.sendTitle(player, EnumTitleAction.TITLE, formatMessage(player, replace, title));
        //  Titles.sendTitle(player, EnumTitleAction.SUBTITLE, formatMessage(player, replace, subTitle));
        platform.get().sendTitleTimes(player, fadeIn, fadeout, stay);
        platform.get().sendTitle(player, formatSingleMessage(player, replace, title));
        platform.get().sendSubTitle(player, formatSingleMessage(player, replace, subTitle));
    }

    public void playSound(P player) {
        String sound = optional.getSoundType().getWithForce();
        float loud = optional.getSoundVolume().getWithForce();
        float pitch = optional.getSoundPitch().getWithForce();
        platform.get().sendSound(player, sound, loud, pitch);
        // player.playSound(player.getLocation(), sound, loud, pitch);
    }

    public ConfigMessage<P> broadcast(Iterable<? extends P> players) {
        players.forEach(this::send);
        return this;
    }

    public ConfigMessage<P> broadcast(Iterable<? extends P> players, Function<P, String[]> mapping) {
        players.forEach(o -> send(o, mapping.apply(o)));
        return this;
    }

    public <B> ConfigMessage<P> broadcast(List<B> to, Function<B, String[]> mapping, Function<B, P> playerFunction) {
        to.forEach(p -> {
            String[] apply = mapping.apply(p);
            send(playerFunction.apply(p), apply);
        });
        return this;
    }

    public BaseComponent[] toComponent(P player, String... variables) {
        List<BaseComponent[]> baseComponents = new ArrayList<>();
        getFormatted(player, variables).forEach(s -> {
            BaseComponent[] components = TextComponent.fromLegacyText(s);
            baseComponents.add(components);
        });

        return baseComponents.stream().flatMap(Arrays::stream).toArray(BaseComponent[]::new);
    }

    public static String[] easyStrings(String... variables) {
        return variables;
    }

    public static <P> ConfigMessage<P> create(String path, String defaultMessage) {
        ConfigMessage<P> message = new ConfigMessage<>();
        message.defaultMessage(defaultMessage);
        message.path(path);
        return message;
    }
}
