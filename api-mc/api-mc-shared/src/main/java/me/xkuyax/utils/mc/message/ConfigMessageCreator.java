package me.xkuyax.utils.mc.message;

import com.google.gson.reflect.TypeToken;
import me.xkuyax.utils.config.Config;
import me.xkuyax.utils.config.ConfigGenericResolver;
import me.xkuyax.utils.config.additions.CETypeResolver.CETypeContext;
import me.xkuyax.utils.config.additions.DynamicCEntry;
import me.xkuyax.utils.config.additions.LanguageCEntry;

import java.lang.reflect.Type;
import java.util.Collections;
import java.util.List;

public class ConfigMessageCreator {

    private static final Type STRING_LIST_TYPE = new TypeToken<List<String>>() {
    }.getType();

    public static ConfigMessage create(CETypeContext ceTypeContext) {
        Object value = ceTypeContext.getDefaultValue();
        if (value == null) {
            value = new ConfigMessage<>();
        }
        if (!(value instanceof ConfigMessage)) {
            throw new RuntimeException("You need to create a default BukkitConfigMessage.create()!");
        }
        ConfigMessage<?> message = (ConfigMessage) value;
        Config config = ceTypeContext.getResolver().getConfig();
        String defaultMessage = getDefaultMessage(message);
        String path = message.path() == null ? ceTypeContext.getPath() : message.path();
        LanguageCEntry<List<String>> languageMessage = createDynamicLanguage(ceTypeContext, defaultMessage, path + ".message");
        LanguageCEntry<String> prefixMessage = createLanguageEntry(ceTypeContext, "Prefix", "Prefix");
        message.setLanguageMessage(languageMessage);
        message.mode(message.mode() == 0 ? config.getInt(path + ".mode", 0b11) : message.mode());
        message.setOptional(config.getGenericType(path + ".optional", ConfigMessageOptional.class));
        message.prefix(message.prefix() == null ? prefixMessage : message.prefix());
        message.setComponents(createDynamicChatComponents(ceTypeContext, path));

        loadDefaults(message.mode(), message);
        return message;
    }

    public static DynamicCEntry<ConfigMessageChatComponent> createDynamicChatComponents(CETypeContext ceTypeContext, String path) {
        ConfigGenericResolver<ConfigMessageChatComponent> resolver = (ConfigGenericResolver<ConfigMessageChatComponent>) ceTypeContext.getResolver();
        return new DynamicCEntry<>(path, ConfigMessageChatComponent.class, resolver);
    }

    public static LanguageCEntry<String> createLanguageEntry(CETypeContext ceTypeContext, String defaultMessage, String path) {
        ConfigGenericResolver<String> resolver = (ConfigGenericResolver<String>) ceTypeContext.getResolver();
        return new LanguageCEntry<>(path, String.class, resolver, defaultMessage);
    }

    public static LanguageCEntry<List<String>> createDynamicLanguage(CETypeContext ceTypeContext, String defaultMessage, String path) {
        ConfigGenericResolver<List<String>> resolver = (ConfigGenericResolver<List<String>>) ceTypeContext.getResolver();
        return new LanguageCEntry<>(path, STRING_LIST_TYPE, resolver, Collections.singletonList(defaultMessage));
    }

    public static String getDefaultMessage(ConfigMessage message) {
        String defaultMessage = message.defaultMessage();
        defaultMessage = defaultMessage == null ? "Default" : defaultMessage;
        return defaultMessage;
    }

    private static void loadDefaults(int mode, ConfigMessage message) {
        ConfigMessageOptional configMessageOptional = message.getOptional();
        if ((mode & 0b100) > 0) {
            configMessageOptional.getTitleFadeIn().getWithForce();
            configMessageOptional.getTitleFadeOut().getWithForce();
            configMessageOptional.getTitleStay().getWithForce();
            configMessageOptional.getTitleSubTitle().getWithForce().loadDefaults();
            configMessageOptional.getTitleTitle().getWithForce().loadDefaults();
        }
        if ((mode & 0b1000) > 0) {
            configMessageOptional.getSoundPitch().getWithForce();
            configMessageOptional.getSoundType().getWithForce();
            configMessageOptional.getSoundVolume().getWithForce();
        }
        if ((mode & 0b10000) > 0) {
            DynamicCEntry<ConfigMessageChatComponent> components = message.getComponents();
            for (int i = 0; i < configMessageOptional.getChatComponentMessageAmount().getWithForce(); i++) {
                ConfigMessageChatComponent configMessageChatComponent = components.get("optional.componentMessage." + i);
                configMessageChatComponent.getHoverEventAction().getWithForce();
                configMessageChatComponent.getClickEventAction().getWithForce();
                configMessageChatComponent.getText().getWithForce();
                configMessageChatComponent.getHoverEventText().getWithForce().loadDefaults();
                configMessageChatComponent.getClickEventText().getWithForce().loadDefaults();
            }

        }
        if ((mode & 0b100000) > 0) {
            configMessageOptional.getActionBar().getWithForce().loadDefaults();
        }

    }
}
