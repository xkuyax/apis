package me.xkuyax.utils.mc.message;

import lombok.Data;
import me.xkuyax.utils.config.additions.ConfigOptional;
import me.xkuyax.utils.config.additions.LanguageCEntry;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;

@Data
public class ConfigMessageChatComponent {

    private ConfigOptional<ClickEvent.Action> clickEventAction = ConfigOptional.withDefault(ClickEvent.Action.RUN_COMMAND);
    private ConfigOptional<LanguageCEntry<String>> clickEventText = ConfigOptional.withDefault(LanguageCEntry.withDefault("Click Text"));
    private ConfigOptional<HoverEvent.Action> hoverEventAction = ConfigOptional.withDefault(HoverEvent.Action.SHOW_TEXT);
    private ConfigOptional<LanguageCEntry<String>> hoverEventText = ConfigOptional.withDefault(LanguageCEntry.withDefault("Hover Text!"));
    private ConfigOptional<LanguageCEntry<String>> text;

}
