package me.xkuyax.utils.test;

import me.xkuyax.utils.config.BukkitConfig;
import me.xkuyax.utils.mc.message.ConfigMessage;
import me.xkuyax.utils.scoreboard.ConfigBoard;
import me.xkuyax.utils.scoreboard.ScoreboardContext;
import me.xkuyax.utils.scoreboard.ScoreboardReplaceHandler;
import me.xkuyax.utils.scoreboard.StateHandler;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.text.SimpleDateFormat;
import java.util.*;

public class TestPlugin extends JavaPlugin implements Listener {

    private final BukkitConfig config = new BukkitConfig("Test", "config.yml");
    private ConfigBoard board;
    private int length;

    @Override
    public void onEnable() {
        getServer().getPluginManager().registerEvents(this, this);
        ScoreboardReplaceHandler render = new ScoreboardReplaceHandler().with("%time%", (config1, context, locale) -> {
            System.out.println("render call #1");
            return new SimpleDateFormat("yyyy.MMMM.dd GGG hh:mm:ss").format(new Date());
        }).with("%name%", (config1, locale, context, player) -> {
            System.out.println("render call #2");
            return player.getName();
        }).with("%random%", (config1, context, locale) -> {
            System.out.println("render call #3 " + length);
            Random random = new Random();
            int l = (length++) % 64;
            StringBuilder sb = new StringBuilder(random.nextLong() + "");
            while (sb.length() < l) {
                sb.append(random.nextLong());
            }
            return sb.substring(0, l);
        });
        new StateHandler<ScoreboardContext>().with("goldVote", context -> 1);
        board = new ConfigBoard(config, render);
        board.setObjectiveNameRender(render);
        board.setPerLineRendering(true);
        Bukkit.getScheduler().runTaskTimer(this, () -> {
            for (Player player : Bukkit.getOnlinePlayers()) {
                board.renderPlayer(player, Locale.GERMANY);
            }
        }, 0, 20);
    }

    @EventHandler
    public void onPlayerJoinEvent(PlayerJoinEvent e) {
        board.setPlayerBoard(e.getPlayer(), Locale.GERMANY);
        System.out.println("set board");
        TestConfig config = new TestConfig();
        config.playerJoin.broadcast(Bukkit.getOnlinePlayers());
        config.playerJoin.send(Bukkit.getPlayer("asd"), "%player%", "hans");
        config.playerJoin.send(Bukkit.getPlayer(""));
        config.playerJoin.broadcast(Bukkit.getOnlinePlayers(), p -> new String[]{"%name%", p.getName()});
        config.playerJoin.broadcast(Collections.singletonList(new UUID(0, 0)), p -> new String[]{"%bits%", p.getLeastSignificantBits() + ""},
                p -> Bukkit.getPlayer(p));
    }

    public static class TestConfig {

        private final ConfigMessage<Player> playerJoin = ConfigMessage.<Player>create("Messages.join", "Der Spieler %player% ist gejoined!").variable("%player%", Player::getDisplayName);

    }

}
