package me.xkuyax.api.games.stats;

import com.google.common.base.Joiner;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

@RequiredArgsConstructor
public class MysqlGetter {
    
    @Getter
    private final MysqlConnection mysql;
    
    public Map<String, Object> getAllData(UUID uuid, TimeInterval time, String timeVal) {
        return getAllData(uuid, time, timeVal, A.a("*"));
    }
    
    public Map<String, Object> getAllData(UUID uuid, TimeInterval time, String timeVal, List<String> types) {
        Map<String, Object> a = new HashMap<>();
        String t = Joiner.on(",").join(types);
        String q = "SELECT " + t + " FROM `" + time.getMysql() + timeVal + "` where `uuid` like '" + uuid.toString() + "'";
        try (Connection con = mysql.getConnection(); Statement statement = con.createStatement(); ResultSet rs = statement.executeQuery(q)) {
            if (rs != null && rs.next()) {
                for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
                    a.put(rs.getMetaData().getColumnLabel(i), rs.getObject(i));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return a;
    }
    
    public List<PositionEntry> getPositionList(String type, TimeInterval time, String timeVal, int min, int max) {
        return getPositionList(type, time, timeVal, min, max, "");
    }
    
    public List<PositionEntry> getPositionList(String type, TimeInterval time, String timeVal, int min, int max, String depend) {
        String q = "SELECT uuid, " + type + " FROM `" + time.getMysql() + timeVal + "`" + depend + " ORDER BY " + type + " DESC limit " + min + "," + max;
        List<PositionEntry> a = new ArrayList<>();
        int i = min;
        try (Connection con = mysql.getConnection(); Statement statement = con.createStatement(); ResultSet rs = statement.executeQuery(q)) {
            while (rs != null && rs.next()) {
                i++;
                UUID u = UUID.fromString(rs.getString(1));
                PositionEntry pe = new PositionEntry(u, rs.getObject(2), i, getName(u));
                a.add(pe);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return a;
    }
    
    public PositionEntry getPositionEntry(UUID uuid, TimeInterval time, String timeVal, String type) {
        return getPositionEntry(uuid, time, timeVal, type, "");
    }
    
    public PositionEntry getPositionEntry(UUID uuid, TimeInterval time, String timeVal, String type, String depend) {
        String q = "SELECT `uuid`," + type + " FROM `" + time.getMysql() + timeVal + "`" + depend + " ORDER BY " + type + " DESC";
        try (Connection con = mysql.getConnection(); Statement statement = con.createStatement()) {
            ResultSet rs = statement.executeQuery(q);
            int pos = 1;
            String suuid = uuid.toString();
            while (rs != null && rs.next()) {
                if (rs.getString(1).equals(suuid)) {
                    UUID u = UUID.fromString(rs.getString(1));
                    return new PositionEntry(u, rs.getObject(2), pos, getName(u));
                }
                pos++;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    private String getName(UUID uuid) {
        Player p = Bukkit.getPlayer(uuid);
        if (p != null) {
            return p.getName();
        }
        Clan c = null;
        if (Clans.getClanManager() != null) {
            c = Clans.getClanManager().getClanStatsID(uuid);
        }
        return c == null ? UResolver.getPlayerFromUUID(uuid) : c.getName();
    }
}
