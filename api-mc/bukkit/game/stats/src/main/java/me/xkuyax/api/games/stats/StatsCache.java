package me.xkuyax.api.games.stats;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class StatsCache {
    
    private LoadingCache<PlayerQuery, Map<String, Object>> playerCache;
    private LoadingCache<PlayerTypesQuery, Map<String, Object>> playerTypeCache;
    private LoadingCache<Query, List<PositionEntry>> positionCache;
    private MysqlGetter getter;
    
    public StatsCache(MysqlGetter getter) {
        this.getter = getter;
        positionCache = CacheBuilder.newBuilder().maximumSize(1000).concurrencyLevel(16).expireAfterWrite(10, TimeUnit.MINUTES)
                .build(new CacheLoader<Query, List<PositionEntry>>() {
                    
                    @Override
                    public List<PositionEntry> load(Query q) throws Exception {
                        return getter.getPositionList(q.getType(), q.getTime(), q.getTimeVal(), q.getMin(), q.getMax(), q.getDepend());
                    }
                });
        playerCache = CacheBuilder.newBuilder().maximumSize(1000).concurrencyLevel(16).expireAfterWrite(10, TimeUnit.MINUTES)
                .build(new CacheLoader<PlayerQuery, Map<String, Object>>() {
                    
                    @Override
                    public Map<String, Object> load(PlayerQuery query) throws Exception {
                        return getter.getAllData(query.getUuid(), query.getTime(), query.getTimeVal());
                    }
                });
        playerTypeCache = CacheBuilder.newBuilder().maximumSize(1000).concurrencyLevel(16).expireAfterWrite(10, TimeUnit.MINUTES)
                .build(new CacheLoader<PlayerTypesQuery, Map<String, Object>>() {
                    
                    @Override
                    public Map<String, Object> load(PlayerTypesQuery query) throws Exception {
                        return getter.getAllData(query.getUuid(), query.getTime(), query.getTimeVal(), query.getTypes());
                    }
                });
    }
    
    public void clear() {
        playerCache.invalidateAll();
        positionCache.invalidateAll();
        playerTypeCache.invalidateAll();
    }
    
    public Object getData(UUID uuid, TimeInterval time, String timeVal, String type) {
        Map<String, Object> a;
        return (a = getAllData(uuid, time, timeVal)) != null ? a.get(type) : null;
    }
    
    public Map<String, Object> getAllData(UUID uuid, TimeInterval time, String timeVal) {
        return playerCache.getUnchecked(new PlayerQuery(uuid, time, timeVal));
    }
    
    public Map<String, Object> getCustomTypeData(UUID uuid, TimeInterval time, String timeVal, List<String> types) {
        return playerTypeCache.getUnchecked(new PlayerTypesQuery(uuid, time, timeVal, types));
    }
    
    public List<PositionEntry> getPositionList(String type, TimeInterval time, String timeVal, int min, int max, String depend, boolean clans) {
        try {
            return positionCache.get(new Query(type, time, timeVal, min, max, wrapDepend(depend, clans)));
        } catch (ExecutionException e) {
            return getter.getPositionList(type, time, timeVal, min, max);
        }
    }
    
    public static String wrapDepend(String depend, boolean clans) {
        if (depend != null && !depend.isEmpty()) {
            depend = depend.startsWith("where") ? depend.replaceFirst("where", "") : depend;
            depend = "where `clan` " + (clans ? " >= 1" : " = 0") + " and (" + depend + ")";
        } else {
            depend = "where `clan`  " + (clans ? " >= 1" : " = 0");
        }
        return depend;
    }
    
    @Data
    @AllArgsConstructor
    private static class Query {
        
        private String type;
        private TimeInterval time;
        private String timeVal;
        private int min;
        private int max;
        private String depend;
    }
    
    @Data
    @AllArgsConstructor
    private static class PlayerQuery {
        
        private UUID uuid;
        private TimeInterval time;
        private String timeVal;
    }
    
    @Data
    @AllArgsConstructor
    private static class PlayerTypesQuery {
        
        private UUID uuid;
        private TimeInterval time;
        private String timeVal;
        private List<String> types;
    }
}
