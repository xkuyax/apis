package me.xkuyax.api.games.stats;

import lombok.Getter;
import org.bukkit.event.HandlerList;

@Getter
public class ClanStatsEvent extends UUIDStatsEvent {
    
    @Getter
    private final static HandlerList handlerList = new HandlerList();
    private final Clan clan;
    
    public ClanStatsEvent(Clan who, Object val, StatType source) {
        super(who.getStatsUUID(), val, source);
        this.clan = who;
    }
    
    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }
}
