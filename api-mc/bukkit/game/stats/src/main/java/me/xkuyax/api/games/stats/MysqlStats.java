package me.xkuyax.api.games.stats;

import com.google.common.base.Preconditions;
import lombok.*;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.*;

@Data
public class MysqlStats {
    
    private static ClanManager clans;
    private static boolean useClans = false;
    @Setter
    @Getter
    private static PointMultiplicator multiplicator = (p -> 1.0d);
    private static DefaultStatsListener listener;
    private EnumMap<TimeInterval, StatValue> excludedValues = new EnumMap<>(TimeInterval.class);
    private List<StatValue> values;
    private List<StatType> exclude;
    private List<StatType> types;
    private String game;
    
    public static void checkClans() {
        if (clans == null) {
            if (Clans.isClansActive()) {
                clans = Clans.getClanManager();
                useClans = true;
            }
        }
    }
    
    //private StatsGetter		statsGetter;
    public MysqlStats(String game, StatEnum... enums) {
        if (listener == null) {
            listener = new DefaultStatsListener();
        }
        this.game = game;
        values = new ArrayList<>();
        types = new ArrayList<>();
        exclude = new ArrayList<>();
        for (StatEnum se : enums) {
            String n = se.toString().toLowerCase();
            Preconditions.checkArgument(!n.equalsIgnoreCase("points"), "Sorry, but you cant set the name to points!");
            Preconditions.checkArgument(!n.equalsIgnoreCase("uuid"), "Sorry, but you cant set the name to uuid!");
            Preconditions.checkArgument(!n.equalsIgnoreCase("nick"), "Sorry, but you cant set the name to nick!!");
            Preconditions.checkArgument(!n.equalsIgnoreCase("clan"), "Sorry, but you cant set the name to clan!!");
            types.add(new StatType(n, se.getType(), se.getType().raw(), se instanceof StatPointEnum ? ((StatPointEnum) se).getPoints() : 0));
        }
        types.add(new StatType("points", DataType.INT, 0, 0));
        types.add(new StatType("clan", DataType.BOOLEAN, 0, 0));
        /*if (GameApi.getCurrent() != null && GameApi.getCurrent().getOptions() != null && GameApi.getCurrent().getOptions().isDebug()) {
			statsGetter = new StatsGetter(this, false);
		}*/
        try {
            Plugin pl = General.a() == null ? GameApi.getCurrent() : General.a();
            if (pl != null) {
                Bukkit.getScheduler().runTaskAsynchronously(General.a(), () -> new MetaDataSaver(this).save());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public MysqlStats(String game, List<StatType> types) {
        this.game = game;
        this.values = new ArrayList<>();
        this.types = new ArrayList<>(types);
    }
    
    public void addExclude(String type) {
        getExclude().add(getType(type));
    }
    
    public void setValueExcluded(String name, Object val, String type, TimeInterval time) {
        StatType ty = getType(type);
        StatValue sv = new StatValue(UResolver.getUUIDFromPlayer(name), ty, val);
        excludedValues.put(time, sv);
    }
    
    public void save() {
        new MysqlSaver(this).save();
    }
    
    public void setValue(String name, Object val, String type) {
        setValue(name, val, getType(type));
    }
    
    public void setValue(String name, Object val, StatEnum se) {
        setValue(name, val, se.toString().toLowerCase());
    }
    
    public void setValue(String player, Object val, StatType type) {
        setValue(UResolver.getUUIDFromPlayer(player), val, type);
    }
    
    public void setValue(UUID uuid, Object val, StatType type) {
        setValue(uuid, val, type, true);
    }
    
    private void setValue(UUID uuid, Object val, StatType type, boolean b) {
        if (type.getPoints() != 0) {
            addPoints(uuid, type.getPoints());
        }
        for (StatValue sv : values) {
            if (sv.getName().equals(uuid) && sv.getType().equals(type)) {
                sv.setValue(val);
                callEvent(uuid, val, type);
                return;
            }
        }
        values.add(new StatValue(uuid, type, val));
        if (b && useClans) {
            SimpleClan clan = clans.getClanFast(uuid);
            if (clan != null) {
                if (type.getData() == DataType.INT || type.getData() == DataType.DOUBLE) {
                    add(clan.getStatsUUID(), val, type, false);
                    setValue(clan.getStatsUUID(), 1, getType("clan"));
                }
            }
        }
        callEvent(uuid, val, type);
    }
    
    private void callEvent(UUID uuid, Object val, StatType type) {
        Player p = Bukkit.getPlayer(uuid);
        if (p != null) {
            Bukkit.getPluginManager().callEvent(new PlayerStatsEvent(p, val, type));
        }
		/*//TODO Change to SimpleClan
		if (useClans) {
			Clan clan = clans.getClanStatsID(uuid);
			if (clan != null) {
				Bukkit.getPluginManager().callEvent(new ClanStatsEvent(clan, val, type));
			}
		}*/
        Bukkit.getPluginManager().callEvent(new UUIDStatsEvent(uuid, val, type));
    }
    
    public Object getValue(String name, StatEnum type) {
        return getValue(name, type.toString().toLowerCase());
    }
    
    public Object getValue(String name, String type) {
        return getValue(name, getType(type));
    }
    
    public Object getValue(String name, StatType type) {
        return getValue(UResolver.getUUIDFromPlayer(name), type);
    }
    
    public Object getValue(UUID u, StatType type) {
        for (StatValue sv : values) {
            if (sv.getName().equals(u) && sv.getType().equals(type)) {
                return sv.getValue();
            }
        }
        return type.getDefaul();
    }
    
    public void addPoints(String name, int points) {
        add(name, points, "points");
        Message.send(Bukkit.getPlayer(name), "idontwanttoseethismessage", "Messages.pointsGot", A.a("%points%"), A.a(points + ""));
    }
    
    public void addPoints(UUID uuid, int points) {
        add(uuid, points, getType("points"));
    }
    
    public void add(String name, Object val, String type) {
        add(name, val, getType(type));
    }
    
    public void add(String name, Object val, StatEnum type) {
        add(name, val, type.toString().toLowerCase());
    }
    
    public void add(String name, Object val, StatType type) {
        add(UResolver.getUUIDFromPlayer(name), val, type);
    }
    
    public void add(UUID uuid, Object val, StatType type) {
        add(uuid, val, type, true);
    }
    
    public void add(UUID uuid, Object val, StatEnum type) {
        add(uuid, val, type(type), true);
    }
    
    public void add(UUID uuid, Object val, StatType type, boolean checkclans) {
        if (type.getData().equals(DataType.DOUBLE) && val instanceof Double) {
            setValue(uuid, (double) getValue(uuid, type) + (double) val, type, checkclans);
            return;
        }
        if (type.getData().equals(DataType.INT) && val instanceof Integer) {
            Object v = getValue(uuid, type);
            setValue(uuid, (int) v + (int) val, type, checkclans);
            return;
        }
        throw new NullPointerException("You cant add a non number !");
    }
    
    public void i(String name, StatType type) {
        add(name, 1, type);
    }
    
    public void i(String name, StatPointEnum type) {
        add(name, 1, type);
    }
    
    public void d(String name, StatType type) {
        add(name, -1, type);
    }
    
    public StatType getType(String name) {
        for (StatType st : types) {
            if (st.getColumn().equals(name)) {
                return st;
            }
        }
        return null;
    }
    
    public List<GamePlayer> sortFromValue(StatPointEnum e, Game game) {
        List<StatValue> a = new ArrayList<>();
        for (GamePlayer gp : game.getPlayers()) {
            if (gp.isPlayed()) {
                StatValue val = getValue(gp.getName(), e);
                if (val != null) {
                    a.add(val);
                }
            }
        }
        a.sort(new StatValueIntComparator());
        List<GamePlayer> b = new ArrayList<>();
        for (StatValue c : a) {
            b.add(game.getPlayer(Bukkit.getPlayer(c.getName())));
        }
        return b;
    }
    
    public StatValue getValue(String name, StatPointEnum e) {
        UUID u = UResolver.getUUIDFromPlayer(name);
        for (StatValue val : getValues()) {
            if (val.getName().equals(u)) {
                if (equals(val, e)) {
                    return val;
                }
            }
        }
        return null;
    }
    
    public boolean equals(StatValue val, StatPointEnum e) {
        return (val.getType().column.equals(e.toString().toLowerCase()));
    }
    
    private StatType type(StatEnum e) {
        return getType(e.toString().toLowerCase());
    }
    
    public class StatValueIntComparator implements Comparator<StatValue> {
        
        @Override
        public int compare(StatValue a, StatValue b) {
            //	Preconditions.checkArgument((a.getType().data == DataType.INT && b.getType().data == DataType.INT), "Cant compare an non int type!");
            return -Integer.compare((int) a.getValue(), (int) b.getValue());
        }
    }
    
    public enum DataType {
        BOOLEAN("tinyint (1) ", "0"),
        DOUBLE("double", "0"),
        INT("int (11)", "0");
        
        @Getter
        private String mysql, defaul;
        
        DataType(String mysql, String defaul) {
            this.mysql = mysql;
            this.defaul = defaul;
        }
        
        public Object raw() {
            if (this == INT) {
                return 0;
            }
            if (this == BOOLEAN) {
                return false;
            }
            return 0.0;
        }
    }
    
    public interface StatEnum {
        
        DataType getType();
    }
    
    public interface StatPointEnum extends StatEnum {
        
        int getPoints();
    }
    
    @Data
    @AllArgsConstructor
    public static class StatType {
        
        private String column;
        private DataType data;
        private Object defaul;
        private int points;
    }
    
    @Data
    @AllArgsConstructor
    @EqualsAndHashCode(of = "name")
    public static class StatValue {
        
        private UUID name;
        private StatType type;
        private Object value;
    }
    
    public interface PointMultiplicator {
        
        double getMultiplicator(String name);
    }
}
