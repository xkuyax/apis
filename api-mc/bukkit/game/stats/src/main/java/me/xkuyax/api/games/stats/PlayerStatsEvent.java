package me.xkuyax.api.games.stats;

import lombok.Getter;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;

public class PlayerStatsEvent extends UUIDStatsEvent {
    
    @Getter
    private static final HandlerList handlerList = new HandlerList();
    @Getter
    private final Player player;
    
    public PlayerStatsEvent(Player who, Object val, StatType source) {
        super(who.getUniqueId(), val, source);
        this.player = who;
    }
    
    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }
}
