package me.xkuyax.api.games.stats;

import com.google.common.base.Preconditions;
import lombok.Data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import java.util.UUID;

@Data
public class MysqlSaver {
    
    private static Calendar calendar = Calendar.getInstance();
    private Connection create;
    private Config config;
    private final MysqlStats stats;
    private String game;
    
    public void save() {
        try {
            checkConnection();
            final Statement statement = create.createStatement();
            for (StatValue sv : stats.getValues()) {
                save(sv, "day_" + calendar.get(1) + "_" + calendar.get(6), statement);
                save(sv, "week_" + calendar.get(1) + "_" + calendar.get(3), statement);
                save(sv, "month_" + calendar.get(1) + "_" + calendar.get(2), statement);
                save(sv, "year_" + calendar.get(1), statement);
                save(sv, "global", statement);
            }
            stats.getExcludedValues().forEach((ti, sv) -> {
                try {
                    save(sv, ti.getCurrent(), statement);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
            statement.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            NUtils.tryandClose(create);
        }
    }
    
    private void checkConnection() throws SQLException {
        if (game == null) {
            game = stats.getGame();
        }
        if (create == null || create.isClosed()) {
            config = new Config("mysql-stats.yml", true);
            String user = config.getString("Mysql.user", "root");
            String pass = config.getString("Mysql.pass", "a");
            String host = config.getString("Mysql.host", "5.135.128.70");
            create = DriverManager.getConnection("jdbc:mysql://" + host + "/?user=" + user + "&password=" + pass);
        }
        checkAndCreate();
    }
    
    private void save(StatValue sv, String s, Statement statement) throws SQLException {
        executeInsert(sv, s, statement);
        executeUpdate(sv, s, statement);
    }
    
    private void executeInsert(StatValue sv, String when, Statement st) throws SQLException {
        StringBuilder sb = new StringBuilder();
        sb.append("INSERT IGNORE INTO `").append(when).append("` ");
        sb.append("(`uuid`) VALUES ('").append(sv.getName()).append("')");
        st.executeUpdate(sb.toString());
    }
    
    private void executeUpdate(StatValue sv, String when, Statement st) throws SQLException {
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE `").append(when).append("` SET `");
        sb.append(sv.getType().getColumn()).append("`= `").append(sv.getType().getColumn()).append("`");
        sb.append(" + '").append(sv.getValue()).append("'").append(" where `uuid` like '");
        sb.append(sv.getName()).append("'");
        st.executeUpdate(sb.toString());
    }
    
    private void checkAndCreate() {
        Statement statement = null;
        try {
            statement = create.createStatement();
            StringBuilder sb = new StringBuilder();
            sb.append("CREATE DATABASE IF NOT EXISTS ").append(config.getString("Mysql.data", "stats2")).append(game);
            statement.executeUpdate(sb.toString());
            statement.executeUpdate("USE " + config.getString("Mysql.data", "stats2") + game);
            checkTables(statement);
        } catch (Exception e) {
            e.printStackTrace();
            new RuntimeException().addSuppressed(e);
        } finally {
            close(statement);
        }
    }
    
    public void checkTables(Statement statement) throws SQLException {
        Preconditions.checkNotNull(statement, "Can not be null");
        Preconditions.checkArgument(!statement.isClosed(), "Statement cannot be closed");
        createTable("day_" + calendar.get(Calendar.YEAR) + "_" + calendar.get(Calendar.DAY_OF_YEAR), statement);
        createTable("week_" + calendar.get(Calendar.YEAR) + "_" + calendar.get(Calendar.WEEK_OF_YEAR), statement);
        createTable("month_" + calendar.get(Calendar.YEAR) + "_" + calendar.get(Calendar.MONTH), statement);
        createTable("year_" + calendar.get(Calendar.YEAR), statement);
        createTable("global", statement);
    }
    
    private void createTable(String name, Statement statement) throws SQLException {
        StringBuilder sb = new StringBuilder("CREATE TABLE IF NOT EXISTS `");
        sb.append(name).append("`");
        sb.append("(`uuid` varchar(255) NOT NULL,");
        for (StatType st : stats.getTypes()) {
            sb.append("`").append(st.getColumn()).append("` ");
            sb.append(st.getData().getMysql()).append(" NOT NULL");
            sb.append(" DEFAULT ");
            String val = "";
            if (st.getColumn().equalsIgnoreCase("elo")) {
                val = "1000";
            } else {
                val = st.getDefaul() + "";
            }
            sb.append("'").append(val).append("', ");
        }
        sb.append("PRIMARY KEY (`uuid`)) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
        statement.executeUpdate(sb.toString());
    }
    
    private void close(Statement st) {
        if (st != null) {
            try {
                st.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
    
    public void insertManual(TimeInterval ti, StatPointEnum type, UUID uuid, Object val) {
        insertManual(ti, type.toString().toLowerCase(), uuid, val);
    }
    
    public void insertManual(TimeInterval ti, String type, UUID uuid, Object val) {
        Statement statement = null;
        try {
            checkConnection();
            statement = create.createStatement();
            StatValue wrap = new StatValue(uuid, stats.getType(type), val);
            String when = "";
            if (ti == TimeInterval.GLOBAL) {
                when = "global";
            } else if (ti == TimeInterval.YEARLY) {
                when = "year_" + calendar.get(1);
            } else if (ti == TimeInterval.MONTHLY) {
                when = "month_" + calendar.get(1) + "_" + calendar.get(2);
            } else if (ti == TimeInterval.WEEKLY) {
                when = "week_" + calendar.get(1) + "_" + calendar.get(3);
            } else {
                when = "day_" + calendar.get(1) + "_" + calendar.get(6);
            }
            executeInsert(wrap, when, statement);
            executeUpdate(wrap, when, statement);
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            NUtils.tryandClose(statement);
        }
    }
}
