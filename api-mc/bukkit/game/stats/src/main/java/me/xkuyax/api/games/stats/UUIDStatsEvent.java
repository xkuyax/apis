package me.xkuyax.api.games.stats;

import lombok.Getter;
import me.xkuyax.api.games.stats.MysqlStats.StatType;
import org.apache.commons.lang.Validate;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import java.util.UUID;

@Getter
public class UUIDStatsEvent extends Event {
    
    @Getter
    private static final HandlerList handlerList = new HandlerList();
    private final UUID who;
    private final StatType source;
    private final Object value;
    
    public UUIDStatsEvent(UUID who, Object val, StatType source) {
        Validate.notNull(who, "No null uuids");
        Validate.notNull(val, "no null vals");
        Validate.notNull(source, "No null source!");
        this.value = val;
        this.source = source;
        this.who = who;
    }
    
    public String getStatsName() {
        return source.getColumn();
    }
    
    public boolean isPointsEvent() {
        return source.getColumn().equals("points");
    }
    
    /**
     * @return
     * @throws ClassCastException if the value is not a integer
     */
    public int getPoints() {
        return (int) value;
    }
    
    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }
}
