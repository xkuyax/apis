package me.xkuyax.api.games.stats;

import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.entity.Player;

import java.sql.Statement;
import java.util.*;

@Getter
public class StatsGetter {

    private static final GregorianCalendar calendar = new GregorianCalendar();
    private MysqlConnection connection;
    private MysqlStats stats;
    private StatsCache cache;
    private MysqlGetter mysqlGetter;
    private boolean cacheStuff;

    public StatsGetter(MysqlStats base, boolean cacheStuff, boolean checkTables) {
        try {
            this.stats = base;
            this.cacheStuff = cacheStuff;
            setupMysql();
            mysqlGetter = new MysqlGetter(connection);
            cache = new StatsCache(mysqlGetter);
            if (checkTables) {
                Statement statement = connection.getConnection().createStatement();
                new MysqlSaver(base).checkTables(statement);
                NUtils.tryandClose(statement);
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public StatsGetter(MysqlStats base, boolean cacheStuff) {
        this(base, cacheStuff, true);
    }

    public Map<String, Object> getAllData(UUID uuid, TimeInterval time, String timeVal, List<String> types) {
        return cache.getCustomTypeData(uuid, time, timeVal, types);
    }

    /**
     * @param uuid    - des spielers
     * @param timeVal - zeit value 2015_96  für global auf "" stellen
     * @param time    - zeit interval daily,weekly,yearly global
     * @param type    - kills,deaths
     * @return null wenn es nicht gefunden werden konnte
     */
    public Map<String, Object> getAllData(UUID uuid, TimeInterval time, String timeVal) {
        return cache.getAllData(uuid, time, timeVal);
    }

    /**
     * @param uuid    - des spielers
     * @param timeVal - zeit value day_2015_96  für global auf "" stellen
     * @param time    - zeit interval daily,weekly,yearly global
     * @param type    - kills,deaths
     * @return null wenn es nicht gefunden werden konnte
     */
    public Object getData(UUID uuid, String timeVal, TimeInterval time, String type) {
        return cache.getData(uuid, time, timeVal, type);
    }

    public int getPosition(UUID uuid, String timeVal, TimeInterval time, String type) {
        return getEntry(uuid, timeVal, time, type, false).getPosition();
    }

    public int getPosition(UUID uuid, String timeVal, TimeInterval time, String type, boolean clans) {
        return getEntry(uuid, timeVal, time, type, clans).getPosition();
    }

    public PositionEntry getEntry(UUID uuid, String timeVal, TimeInterval time, String type) {
        return getEntry(uuid, timeVal, time, type, false);
    }

    public PositionEntry getEntry(UUID uuid, String timeVal, TimeInterval time, String type, boolean clans) {
        return mysqlGetter.getPositionEntry(uuid, time, timeVal, type, StatsCache.wrapDepend("", clans));
    }

    public List<PositionEntry> getPosition(String type, TimeInterval time, String timeVal, int min, int max) {
        return getPosition(type, time, timeVal, min, max, false);
    }

    public List<PositionEntry> getPosition(String type, TimeInterval time, String timeVal, int min, int max, boolean clans) {
        return cache.getPositionList(type, time, timeVal, min, max, "", clans);
    }

    public List<PositionEntry> getPosition(String type, TimeInterval time, String timeVal, int min, int max, String depend) {
        return getPosition(type, time, timeVal, min, max, depend, false);
    }

    public List<PositionEntry> getPosition(String type, TimeInterval time, String timeVal, int min, int max, String depend, boolean clans) {
        return cache.getPositionList(type, time, timeVal, min, max, depend, clans);
    }

    @RequiredArgsConstructor
    @Data
    public static class PositionEntry {

        private final UUID uuid;
        private final Object val;
        private final int position;
        private final String name;
    }

    @Getter
    @RequiredArgsConstructor
    public enum TimeInterval {
        GLOBAL("global", "global", 0),
        DAILY("day_", "day", 6),
        WEEKLY("week_", "week", 3),
        MONTHLY("month_", "month", 2),
        YEARLY("year_", "year", 1);

        private final String mysql;
        private final String display;
        private final int calendarField;

        public String getCurrent() {
            if (this == GLOBAL) {
                return "";
            }
            if (this == YEARLY) {
                return calendar.get(calendarField) + "";
            }
            return calendar.get(Calendar.YEAR) + "_" + calendar.get(calendarField);
        }

        public String getDisplayName(Config config, Player p) {
            return config.getTranslated("Interval." + toString(), p, toString());
        }

        public TimeInterval next() {
            return getNext(this);
        }

        public static TimeInterval paste(String s) {
            for (TimeInterval ti : values()) {
                if (ti.toString().equalsIgnoreCase(s)) {
                    return ti;
                }
                if (ti.getMysql().startsWith(s, 3)) {
                    return ti;
                }
                if (ti.getDisplay().equalsIgnoreCase(s)) {
                    return ti;
                }
            }
            return null;
        }

        public static TimeInterval getNext(TimeInterval ti) {
            return TimeInterval.values()[Utils.next(TimeInterval.values(), Utils.find(ti, TimeInterval.values()))];
        }
    }

    private void setupMysql() {
        Config config = new Config(General.a(), "me.xkuyax.utils.mysql-stats.yml", true);
        String host = config.getString("Mysql.host", "localhost");
        String user = config.getString("Mysql.user", "root");
        String pass = config.getString("Mysql.pass", "a");
        String data = config.getString("Mysql.data", "viceviceapi");
        String port = config.getString("Mysql.port", "3306");
        connection = new PooledMysqlConnection(host, user, pass, port, data + stats.getGame());
    }
}
