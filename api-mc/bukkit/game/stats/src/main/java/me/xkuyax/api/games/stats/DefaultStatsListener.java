package me.xkuyax.api.games.stats;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class DefaultStatsListener implements Listener {
    
    public DefaultStatsListener() {
        if (General.a() != null) {
            Bukkit.getPluginManager().registerEvents(this, General.a());
        } else {
            System.out.println("could not load default stats listener");
        }
    }
    
    @EventHandler
    public void onPlayerStatsEvent(PlayerStatsEvent e) {
        if (GameApi.getCurrent() != null) {
            Player p = e.getPlayer();
            if (e.isPointsEvent()) {
                GameApi.getCurrent().getPlayer(p).updateItems();
            }
        }
    }
}
