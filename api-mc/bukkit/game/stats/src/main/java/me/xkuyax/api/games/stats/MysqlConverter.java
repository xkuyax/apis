package me.xkuyax.api.games.stats;

import java.sql.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * This class has to modify ALL databases with ALL tables and add a new coloum with clan type boolean
 */
public class MysqlConverter {

    private static final List<String> deleteFields = Collections.singletonList("elo");
    private Config config;
    private Connection connection;

    public MysqlConverter() throws SQLException {
        config = new Config("me.xkuyax.utils.mysql-stats.yml", true);
        String user = config.getString("Mysql.user", "root");
        String pass = config.getString("Mysql.pass", "a");
        connection = DriverManager.getConnection("jdbc:me.xkuyax.utils.mysql://localhost/?user=" + user + "&password=" + pass);
        convert();
    }

    private void convert() throws SQLException {
        for (String db : getDatabases()) {
            System.out.println("Using db " + db);
            connection.createStatement().execute("use " + db);
            try (ResultSet rs = connection.createStatement().executeQuery("show tables")) {
                String table = null;
                while (rs.next()) {
                    table = rs.getString(1);
                    System.out.println("Converting " + table);
                    checkAndConvertTable(table);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private void checkAndConvertTable(String table) {
        try (ResultSet rs = connection.createStatement().executeQuery("describe " + table)) {
            Statement s = connection.createStatement();
            List<String> a = new ArrayList<>();
            while (rs.next()) {
                String name = rs.getString("Field");
                if (deleteFields.contains(name)) {
                    s.execute("alter table " + table + " drop " + name);
                    System.out.println("Remove from table " + table + " " + name);
                } else {
                    a.add(name);
                }
            }
            if (!a.contains("clan")) {
                s.execute("ALTER table " + table + " ADD `clan` tinyint(1) NOT NULL DEFAULT '0'");
                System.out.println("Adding clan to table " + table);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private List<String> getDatabases() {
        List<String> a = new ArrayList<>();
        try (ResultSet rs = connection.createStatement().executeQuery("show DATABASES like 'stats_%'")) {
            while (rs.next()) {
                String s = rs.getString(1);
                if (s.startsWith("stats_")) {
                    a.add(rs.getString(1));
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return a;
    }
}
