package me.xkuyax.api.games.stats;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import me.xkuyax.api.games.stats.StatsCommand.StatsType;
import me.xkuyax.api.games.stats.StatsGetter.TimeInterval;

import java.util.Calendar;
import java.util.GregorianCalendar;

@RequiredArgsConstructor
public class StatsArgsParser {
    
    private static final Calendar calendar = new GregorianCalendar();
    private final String[] args;
    private final StatsCommand command;
    
    public BaseResult pasteInterval() {
        TimeInterval time = pasteTimeInterval(args[0]);
        String timeVal = args.length >= 2 ? pasteTimeValue(time, args[1]) : time.getCurrent();
        return new BaseResult(time, timeVal);
    }
    
    public TypeResult pasteTypeResult() {
        StatsType type = pasteStatType(args[0]);
        TimeInterval time = args.length >= 2 ? pasteTimeInterval(args[1]) : TimeInterval.GLOBAL;
        String timeVal = args.length >= 3 && args.length != 4 ? pasteTimeValue(time, args[2]) : time.getCurrent();
        int start = args.length >= 4 ? args.length >= 5 ? Utils.paste(args[3], 0) : Utils.paste(args[4], 0) : 0;
        int end = args.length >= 4 ? args.length >= 5 ? Utils.paste(args[4], 0) : Utils.paste(args[5], 0) : 19;
        return new TypeResult(time, timeVal, type, start, end);
    }
    
    public OtherResult pasteOtherResult() {
        String other = args[0];
        TimeInterval time = args.length >= 2 ? pasteTimeInterval(args[1]) : TimeInterval.GLOBAL;
        String timeVal = args.length >= 3 ? pasteTimeValue(time, args[2]) : time.getCurrent();
        return new OtherResult(time, timeVal, other);
    }
    
    public TimeInterval pasteTimeInterval(String s) {
        return TimeInterval.paste(s);
    }
    
    private String pasteTimeValue(TimeInterval time, String s) {
        return time == TimeInterval.GLOBAL ? "" : calendar.get(1) + "_" + (calendar.get(time.getCalendarField()) - Utils.paste(s, 0));
    }
    
    public StatsType pasteStatType(String s) {
        for (StatsType type : command.getTypes()) {
            if (type.getType().equalsIgnoreCase(s)) {
                return type;
            }
            if (type.getMysql().equalsIgnoreCase(s)) {
                return type;
            }
            for (String b : type.getSynonyms()) {
                if (b.equalsIgnoreCase(s)) {
                    return type;
                }
            }
        }
        return null;
    }
    
    //stats daily 7
    //deine stats letze woche
    @Data
    @RequiredArgsConstructor
    public static class BaseResult {
        
        private final TimeInterval interval;
        private final String timeVal;
    }
    
    //stats kills daily 7 0 25
    //kills letze woche platz 0 bis 25
    @Data
    @EqualsAndHashCode(callSuper = true)
    public static class TypeResult extends BaseResult {
        
        private final StatsType type;
        private final int start, end;
        
        public TypeResult(TimeInterval interval, String timeVal, StatsType type, int start, int end) {
            super(interval, timeVal);
            this.type = type;
            this.start = start;
            this.end = end;
        }
    }
    
    //stats hans daily 7
    //hans stats letze woche
    @Data
    @EqualsAndHashCode(callSuper = true)
    public static class OtherResult extends BaseResult {
        
        private final String other;
        
        public OtherResult(TimeInterval interval, String timeVal, String other) {
            super(interval, timeVal);
            this.other = other;
        }
    }
}
