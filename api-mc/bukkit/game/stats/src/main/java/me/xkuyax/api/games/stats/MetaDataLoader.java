package me.xkuyax.api.games.stats;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class MetaDataLoader extends MetaData {

    private String game;

    public MetaDataLoader(String game) {
        this.game = game;
        setupConnection();
    }

    public MysqlStats getStats() {
        return new MysqlStats(game, getTypes());
    }

    public List<StatType> getTypes() {
        ResultSet rs = me.xkuyax.utils.mysql.getResultSet("SELECT `json` from `stats_types` where `gamemode` like '" + game + "'");
        try {
            if (rs.next()) {
                String json = rs.getString(1);
                return gson.fromJson(json, token.getType());
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            MysqlConnection.close(rs);
        }
        return null;
    }
}
