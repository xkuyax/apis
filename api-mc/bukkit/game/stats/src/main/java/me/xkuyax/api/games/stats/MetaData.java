package me.xkuyax.api.games.stats;

import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;

public class MetaData {

    @SuppressWarnings("serial")
    protected static TypeToken<List<StatType>> token = new TypeToken<List<StatType>>() {};
    protected static Gson gson = new Gson();
    protected MysqlConnection mysql;

    protected void setupConnection() {
        Config config = new Config(General.a(), "me.xkuyax.utils.mysql-stats.yml", true);
        String path = "Mysql";
        String host = config.getString(path + ".host", "localhost");
        String user = config.getString(path + ".user", "root");
        String pass = config.getString(path + ".pass", "a");
        String data = config.getString(path + ".data", "viceviceapi") + "metadata";
        String port = config.getString(path + ".port", "3306");
        mysql = new MysqlConnection(host, user, pass, port, data);
    }
}
