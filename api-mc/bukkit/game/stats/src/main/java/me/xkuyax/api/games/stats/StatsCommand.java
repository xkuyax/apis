package me.xkuyax.api.games.stats;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ClickEvent.Action;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;

@Getter
public class StatsCommand extends GCommand {

    private List<StatsType> types = new ArrayList<>();
    private StatsGetter getter;
    private MysqlStats stats;
    private Config config;
    private boolean clans;
    private String command;

    public StatsCommand(MysqlStats stats, String cmd, boolean clans) {
        this(stats, GameApi.getCurrent(), cmd, clans);
    }

    public StatsCommand(MysqlStats stats, Plugin plugin, String cmd, boolean clans) {
        super(cmd);
        this.command = cmd;
        this.stats = stats;
        getter = new StatsGetter(stats, true);
        config = new Config(plugin, cmd + "command.yml");
        int i = 0;
        for (String s : config.getKeys("Types", "kills", "deaths")) {
            String display = config.getString("Types." + s + ".de", s);
            String desc = config.getString("Types." + s + ".description", s);
            String mysql = config.getString("Types." + s + ".me.xkuyax.utils.mysql", s);
            boolean depend = config.isSet("Types." + s + ".depend");
            String sdepend = depend ? config.getString("Types." + s + ".depend", "") : null;
            List<String> syn = config.getStringList("Types." + s + ".synonyms", true, s);
            types.add(new StatsType(s, syn, display, desc, mysql, depend, sdepend, i++));
        }
        Message.register(new ConfigProvider() {

            @Override
            public String getPack() {
                return "me.vicevice.general.api.games.stats";
            }

            @Override
            public Config getConfig() {
                return config;
            }
        });
    }

    @CmdHandler(sender = CSender.PLAYER_ONLY, nullargs = true, runAsyc = true)
    public void onNullArgs(Player p, String[] args) {
        onIgnore(p, new String[]{"global"});
        Message.send(p, "See /stats help for more commands", "Stats", false);
    }
    /*
    @CmdHandler(args = "advanced", sender = CSender.PLAYER_ONLY, minarg = 1)
	public void advanced(Player p, String[] args) {
		int i = 1;
		// @formatter:off
		Message.send(p, "/stats advanced- zeigt dir diesen Text", "Stats.advanced"+i++,false);
		Message.send(p, "/stats times - Zeigt dir alle möglichen Zeitintervalle an", "Stats.advanced"+i++,false);
		Message.send(p, "/stats types - Zeigt dir alle möglichen StatWerte an", "Stats.advanced"+i++,false);
		Message.send(p, "/stats <interval> - Zeigt dir deine Stats im jeweiligen Zeitinterval", "Stats.advanced"+i++,false);
		Message.send(p, "/stats <interval> <verschiebung> - Zeigt dir deine Stats im jeweiligen Zeitinterval", "Stats.advanced"+i++,false);
		Message.send(p, "/stats <type> - Zeigt dir eine Liste mit Platz 0-30 mit den besten des jeweiligen Typs", "Stats.advanced"+i++,false);
		Message.send(p, "/stats <type> <interval> - Zeigt dir die Liste in einem anderen Zeitinterval", "Stats.advanced"+i++,false);
		Message.send(p, "/stats <type> <interval> <verschiebung> - Zeigt dir die Liste in einem anderen Zeitinterval ", "Stats.advanced"+i++,false);
		Message.send(p, "/stats <type> <interval>  <start> <ende> - BSP: /stats kills global 0 30 60 - zeigt dir platz 30-60 abhängig von den kills an", "Stats.advanced"+i++,false);
		Message.send(p, "/stats <type> <interval> <verschiebung> <start> <ende> - BSP: /stats kills global 0 30 60 - zeigt dir platz 30-60 abhängig von den kills an", "Stats.advanced"+i++,false);
		Message.send(p, "/stats <spieler> - zeigt dir eine übersicht des anderen spielers ", "Stats.advanced"+i++,false);
		Message.send(p, "/stats <spieler> <interval> - zeigt dir eine übersicht des anderen spielers in einem anderen zeitintervall- ", "Stats.advanced"+i++,false);
		Message.send(p, "/stats <spieler> <interval> <verschiebung> - zeigt dir eine übersicht des anderen spielers in einem anderen zeitintervall- ", "Stats.advanced"+i++,false);
		Message.send(p, "/stats compare <spieler> - vergleicht dich mit einem anderen spieler ", "Stats.advanced"+i++,false);
		Message.send(p, "/stats compare <spieler> <interval>- vergleicht dich mit einem anderen spieler in einem anderen zeitinterval ", "Stats.advanced"+i++,false);
		Message.send(p, "/stats compare <spieler> <interval> <verschiebung> - vergleicht dich mit einem anderen spieler in einem anderen zeitinterval ", "Stats.advanced"+i++,false);
		Message.send(p, "/stats compare <spieler a> <spieler b> - vergleicht spieler a mit b ", "Stats.advanced"+i++,false);
		Message.send(p, "/stats compare <spieler a> <spieler b> <interval>- vergleicht spieler a mit b in einem anderen zeitinterval ", "Stats.advanced"+i++,false);
		Message.send(p, "/stats compare <spieler a> <spieler b> <interval> <verschiebung> - vergleicht spieler a mit b in einem anderen zeitinterval ", "Stats.advanced"+i++,false);
		// @formatter:on
	}*/

    @CmdHandler(args = "help", sender = CSender.PLAYER_ONLY)
    public void help(Player p, String[] args) {
        int i = 1;
        // @formatter:off
        Message.send(p, "/stats help- zeigt dir diesen Text", "Stats.help" + i++, false);
        Message.send(p, "/stats times - Zeigt dir alle möglichen Zeitintervalle an", "Stats.help" + i++, false);
        Message.send(p, "/stats types - Zeigt dir alle möglichen StatWerte an", "Stats.help" + i++, false);
        Message.send(p, "/stats <interval> - Zeigt dir deine Stats im jeweiligen Zeitinterval", "Stats.help" + i++, false);
        Message.send(p, "/stats <type> - Zeigt dir eine Liste mit Platz 0-30 mit den besten des jeweiligen Typs", "Stats.help" + i++, false);
        Message.send(p, "/stats <type> <interval> - Zeigt dir die Liste in einem anderen Zeitinterval", "Stats.help" + i++, false);
        Message.send(p, "/stats <type> <interval>  <start> <ende> - BSP: /stats kills global 30 60 - zeigt dir platz 30-60 abhängig von den kills an", "Stats.help" + i++, false);
        Message.send(p, "/stats <spieler> - zeigt dir eine übersicht des anderen spielers ", "Stats.help" + i++, false);
        Message.send(p, "/stats <spieler> <interval> - zeigt dir eine übersicht des anderen spielers in einem anderen zeitintervall- ", "Stats.help" + i++, false);
        Message.send(p, "/stats compare <spieler> - vergleicht dich mit einem anderen spieler ", "Stats.help" + i++, false);
        Message.send(p, "/stats compare <spieler> <interval>- vergleicht dich mit einem anderen spieler in einem anderen zeitinterval ", "Stats.help" + i++, false);
        Message.send(p, "/stats compare <spieler a> <spieler b> - vergleicht spieler a mit b ", "Stats.help" + i++, false);
        Message.send(p, "/stats compare <spieler a> <spieler b> <interval>- vergleicht spieler a mit b in einem anderen zeitinterval ", "Stats.help" + i++, false);
        // @formatter:on
    }

    @CmdHandler(args = "times", sender = CSender.PLAYER_ONLY, minarg = 1, runAsyc = true)
    public void times(Player p, String[] args) {
        Message.send(p, "Es gibt folgende Zeitintervalle: ", "Stats.times.info");
        for (TimeInterval ti : TimeInterval.values()) {
            Message.send(p, "%interval%", "Stats.times.interval", A.a("%interval%"), A.a(config.getTranslated("Intervals." + ti.toString(), p, ti.toString())));
        }
    }

    @CmdHandler(args = "types", sender = CSender.PLAYER_ONLY, minarg = 1, runAsyc = true)
    public void types(Player p, String[] args) {
        Message.send(p, "Es werden folgende Werte getracked: ", "Stats.types.info");
        for (StatsType type : getTypes()) {
            Message.send(p, "%type%", "Stats.types.type", A.a("%type%", "%desc%"), A.a(type.getType(), type.getDescription()));
        }
    }

    @CmdHandler(ignoreargs = true, sender = CSender.PLAYER_ONLY, minarg = 1, runAsyc = true)
    public void onIgnore(Player p, String[] args) {
        String s = args[0];
        StatsArgsParser parser = new StatsArgsParser(args, this);
        StatsType statType = parser.pasteStatType(s);
        TimeInterval time = parser.pasteTimeInterval(s);
        if (s.equalsIgnoreCase("compare")) {
            //TODO COMPARE
        } else if (time != null) {
            BaseResult br = parser.pasteInterval();
            onInterval(p, UResolver.getUUIDFromPlayer(p), br.getInterval(), br.getTimeVal());
        } else if (statType != null) {
            TypeResult tr = parser.pasteTypeResult();
            onType(p, tr.getInterval(), tr.getTimeVal(), tr.getType(), tr.getStart(), tr.getEnd());
        } else {
            OtherResult other = parser.pasteOtherResult();
            onInterval(p, UResolver.getUUIDFromPlayer(other.getOther()), other.getInterval(), other.getTimeVal());
        }
    }

    public void onInterval(Player p, UUID print, TimeInterval time, String timeVal) {
        printValues(p, print, time, timeVal);
    }

    public void onType(Player p, TimeInterval time, String timeVal, StatsType type, int start, int end) {
        if (end > start + 19) {
            end = start + 19;
        }
        Message.send(p, "Loading stats...", "Stats.loading");
        List<PositionEntry> pos = type.isDependSet() ? getter.getPosition(type.getMysql(), time, timeVal, start, end, type.getDepend(), clans) : getter
                .getPosition(type.getMysql(), time, timeVal, start, end, clans);
        if (pos == null || pos.isEmpty()) {
            Message.send(p, "Konnte keine Stats finden!", "Stats.error.noStatsFound");
            return;
        }
        Message.send(p, "Position          Spieler           %type%", "Stats.typeHeader", false, A.a("%type%"), A.a(type.getDisplayname()));
        for (PositionEntry pe : pos) {
            int po = pe.getPosition();
            Message.send(p, "%pos% name% %val%", "Stats.typeReal", A.a("%pos%", "%name%", "%val%"), A
                    .a(po < 10 ? "0" + po : po + "", pe.getName(), pe.getVal() + ""), new ClickEvent(Action.RUN_COMMAND, "/" + command + " " + pe.getName()), config
                    .getHoverEvent("Hover.type", "Hover", A.a("%name%"), A.a(pe.getName())));
        }
    }

    private void printValues(Player p, UUID player, TimeInterval time, String timeVal) {
        //Map<String, Object> data = getter.getAllData(player, time, timeVal, CollectionUtils.transformString(getTypes(), t -> t.me.xkuyax.utils.mysql));
        Message.send(p, "Loading stats...", "Stats.loading");
        String dname = ViceviceApi.getOfflineUser(player).getColoredName();
        Map<String, Object> data = getter.getAllData(player, time, timeVal, getTypes().stream().map(StatsType::getMysql).collect(Collectors.toList()));
        if (data == null || data.isEmpty()) {
            Message.send(p, "Konnte keine Stats finden!", "Stats.error.noStatsFound");
            return;
        }
        List<MessageData> msg = new ArrayList<>();
        List<String> blackList = config.getStringList("TypeBlacklist", true, "elo", "uuid");
        List<Thread> threads = new ArrayList<>();
        for (Entry<String, Object> entrys : data.entrySet()) {
            String type = entrys.getKey();
            String dtype = types.stream().filter(t -> t.getMysql().equalsIgnoreCase(type)).findFirst().get().displayname;
            if (!blackList.contains(type)) {
                Thread t = new Thread(() -> {
                    Object val = entrys.getValue();
                    int position = getter.getPosition(player, timeVal, time, type);
                    List<String> r = A.a("%type%", "%val%", "%pos%");
                    List<String> o = A.a(dtype, val + "", position + "");
                    msg.add(new MessageData(r, o, type));
                });
                threads.add(t);
                t.start();
            }
        }
        threads.forEach(t -> {
            try {
                t.join();
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        msg.sort(Comparator.comparingInt(o -> getType(o.type).position));
        Message.send(p, "Header Value Message", "Stats.valueMessageHeader", false, A.a("%name%"), A.a(dname));
        Message.send(p, "header 2", "Stats.header2", false);
        for (MessageData md : msg) {
            String dtype = types.stream().filter(t -> t.getMysql().equalsIgnoreCase(md.getType())).findFirst().get().displayname;
            Message.send(p, "%type% %val% %pos%", "Stats.valueReal", md.getReplace(), md.getObjects(), new ClickEvent(Action.RUN_COMMAND, "/" + command + " " + md
                    .getType()), config.getHoverEvent("Hover.value", "Hover 2", A.a("%player%", "%type%"), A.a(dname, dtype)));
        }
        Message.send(p, "Footer Value Message", "Stats.footer2", false);
    }

    private StatsType getType(String mysqlType) {
        return types.stream().filter(t -> t.getMysql().equalsIgnoreCase(mysqlType)).findFirst().orElse(null);
    }

    @Data
    static class StatsType {

        private final String type;
        private final List<String> synonyms;
        private final String displayname;
        private final String description;
        private final String mysql;
        private final boolean dependSet;
        private final String depend;
        private final int position;
    }

    @Data
    @AllArgsConstructor
    private static class MessageData {

        private List<String> replace, objects;
        private String type;
    }
}
