package me.xkuyax.api.games.stats;

public class MetaDataSaver extends MetaData {

    private MysqlStats stats;

    public MetaDataSaver(MysqlStats stats) {
        this.stats = stats;
        setupConnection();
    }

    public void save() {
        String g = stats.getGame();
        String j = getJson();
        StringBuilder sb = new StringBuilder("insert into `stats_types` (`gamemode`,`json`) VALUES ('");
        sb.append(g).append("','").append(j).append("')");
        sb.append("ON DUPLICATE KEY UPDATE `json` = '").append(j).append("'");
        me.xkuyax.utils.mysql.executeUpdate(sb.toString());
    }

    private String getJson() {
        return gson.toJson(stats.getTypes(), token.getType());
    }
}
