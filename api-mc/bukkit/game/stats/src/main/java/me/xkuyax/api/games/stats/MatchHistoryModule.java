package me.xkuyax.api.games.stats;

import lombok.Data;
import me.Bodoo.Load.NetAPI.Main.NetAPI;
import org.apache.commons.codec.binary.Base64OutputStream;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.zip.GZIPOutputStream;

public class MatchHistoryModule extends Module implements Listener {
    
    private PooledMysqlConnection mysqlConnection;
    private MatchHistory localGame;
    private AchievSQLBackend gameMetaData;
    
    @Override
    public boolean load() {
        Bukkit.getPluginManager().registerEvents(this, getGeneral());
        mysqlConnection = PooledMysqlConnection.get(new Config("mysql-matchHistory.yml"), "Mysql");
        gameMetaData = new AchievSQLBackend(mysqlConnection);
        return true;
    }
    
    @EventHandler
    public void onGameStartEvent(GameStartedEvent e) {
        ManageableGame game = e.getGame();
        Bukkit.getScheduler().runTaskAsynchronously(General.a(), () -> {
            System.out.println("created new match history");
            localGame = new MatchHistory();
            localGame.setStartTime(System.currentTimeMillis());
            localGame.setGameId(gameMetaData.getGameID(game.getMatchHistoryName()));
            localGame.setMapId(gameMetaData.getMapID(localGame.getGameId(), game.getCurrent()));
            localGame.setGameExecutorId(NetAPI.ID);
            localGame.setPlayers(Bukkit.getOnlinePlayers().stream().map(a -> {
                User user = ViceviceApi.getOnlineUser(a.getPlayer());
                MatchPlayer matchPlayer = new MatchPlayer();
                matchPlayer.setGroupId(gameMetaData.getGroupID(user.getGroup()));
                matchPlayer.setPlayerId(gameMetaData.getPlayerID(user.getUuid().toString()));
                matchPlayer.setParty(SocialModule.a().getParty().isInParty(user.getUuid()));
                matchPlayer.setTeamId(getTeamId(game.getPlayer(a)));
                matchPlayer.setUuid(a.getUniqueId());
                return matchPlayer;
            }).collect(Collectors.toList()));
        });
    }
    
    @EventHandler
    public void onGamePlayerSetSpectatorEvent(GamePlayerSetItemEvent e) {
        if (e.getType() == ItemType.SPECTATOR) {
            quitPlayer(e.getGamePlayer());
        }
    }
    
    @EventHandler
    public void onGamePlayerQuitEvent(GamePlayerQuitEvent e) {
        quitPlayer(e.getPlayer());
    }
    
    @EventHandler
    public void onGameStoppedEvent(GameStoppedEvent e) {
        ManageableGame game = e.getGame();
        Bukkit.getScheduler().runTaskAsynchronously(General.a(), () -> {
            System.out.println(localGame);
            localGame.setEndedTime(System.currentTimeMillis());
            localGame.setGameTime(localGame.getEndedTime() - localGame.getStartTime());
            localGame.setWinningTeamId(getWinningTeamID(game));
            localGame.setMatchId(insertLocalGame());
            localGame.getPlayers().forEach(matchPlayer -> {
                Player player = Bukkit.getPlayer(matchPlayer.getUuid());
                if (player != null) {
                    GamePlayer gamePlayer = game.getPlayer(player);
                    if (gamePlayer.isPlayed() && matchPlayer.getPlayTime() == 0) {
                        matchPlayer.setPlayTime(localGame.getGameTime());
                    }
                }
                matchPlayer.setMatchId(localGame.getMatchId());
            });
            insertPlayers();
            if (hasAACLog()) {
                insertAACLog();
            }
        });
    }
    
    private void quitPlayer(GamePlayer gamePlayer) {
        if (gamePlayer.getGame().isIngame()) {
            localGame.getPlayers().stream().
                    filter(matchPlayer -> matchPlayer.getUuid() != null).
                    filter(matchPlayer -> matchPlayer.getUuid().equals(gamePlayer.getUser().getUuid())).
                    forEach(matchPlayer -> {
                        try {
                            matchPlayer.setPlayTime(System.currentTimeMillis() - localGame.getStartTime());
                        } catch (Throwable t) {
                            t.printStackTrace();
                        }
                    });
        }
    }
    
    private void insertPlayers() {
        for (MatchPlayer matchPlayer : localGame.getPlayers()) {
            mysqlConnection.prepare("insert into `match_history_players` (`matchId`,`playerId`,`teamId`,`groupId`,`playTime`,`party`) values (?,?,?,?,?,?)", ps -> {
                ps.setInt(1, matchPlayer.getMatchId());
                ps.setInt(2, matchPlayer.getPlayerId());
                ps.setInt(3, matchPlayer.getTeamId());
                ps.setInt(4, matchPlayer.getGroupId());
                ps.setLong(5, matchPlayer.getPlayTime());
                ps.setBoolean(6, matchPlayer.isParty());
            });
        }
    }
    
    private int insertLocalGame() {
        return mysqlConnection
                .keyInsert("insert into `match_history` (`gameId`,`mapId`,`winningTeamid`,`startTime`,`gameTime`,`gameExecutorId`) values (?,?,?,?,?,?)", ps -> {
                    ps.setInt(1, localGame.getGameId());
                    ps.setInt(2, localGame.getMapId());
                    ps.setInt(3, localGame.getWinningTeamId());
                    ps.setTimestamp(4, new Timestamp(localGame.getStartTime()));
                    ps.setLong(5, localGame.getGameTime());
                    ps.setInt(6, localGame.getGameExecutorId());
                });
    }
    
    private int getTeamId(GamePlayer gamePlayer) {
        if (gamePlayer.getTeam() != null) {
            return gameMetaData.getGameTeamID(gamePlayer.getTeam().getName().toLowerCase());
        }
        return 0;
    }
    
    private int getWinningTeamID(ManageableGame game) {
        if (game instanceof ITeamGame) {
            ITeamGame<? extends Team> teamGame = (ITeamGame<? extends Team>) game;
            List<? extends Team> winning = teamGame.getAliveTeams();
            if (winning.size() > 0) {
                return gameMetaData.getGameTeamID(winning.get(0).getName().toLowerCase());
            }
        }
        return 0;
    }
    
    private boolean hasAACLog() {
        try {
            if (Files.exists(Paths.get("plugins/Spartan/logs"))) {
                return Files.newDirectoryStream(Paths.get("plugins/Spartan/logs")).iterator().hasNext();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }
    
    private void insertAACLog() {
        try {
            Path logFile = Files.newDirectoryStream(Paths.get("plugins/Spartan/logs")).iterator().next();
            byte[] data = Files.readAllBytes(logFile);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            Base64OutputStream base64OutputStream = new Base64OutputStream(byteArrayOutputStream);
            GZIPOutputStream gzipOutputStream = new GZIPOutputStream(base64OutputStream);
            gzipOutputStream.write(data);
            gzipOutputStream.close();
            base64OutputStream.close();
            byte[] compressed = byteArrayOutputStream.toByteArray();
            String compressedString = new String(compressed);
            mysqlConnection.prepare("insert into `match_history_aac` (`matchId`,`log`) values (?,?)", ps -> {
                ps.setInt(1, localGame.getMatchId());
                ps.setString(2, compressedString);
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    @Override
    public boolean disable() {
        return true;
    }
    
    @Override
    public String getName() {
        return "matchHistory";
    }
    
    @Data
    public static class MatchHistory {
        
        private int matchId;
        private int gameId;
        private int mapId;
        private int winningTeamId;
        private int gameExecutorId;
        private long startTime;
        private long gameTime;
        private long endedTime;
        private List<MatchPlayer> players;
        
    }
    
    @Data
    public static class MatchPlayer {
        
        private int matchId;
        private int playerId;
        private int groupId;
        private int teamId;
        private long playTime;
        private boolean party;
        private UUID uuid;
        
    }
}
