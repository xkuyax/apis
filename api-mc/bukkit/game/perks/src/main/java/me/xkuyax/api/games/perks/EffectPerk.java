package me.xkuyax.api.games.perks;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import java.util.*;
import java.util.Map.Entry;

/**
 * Beschreibt ein Perk welches aktiviert wird und nach long activeTime wieder entfernt und cooldown gesetzt wird
 *
 * @author xkuyax
 */
@Getter(value = AccessLevel.PUBLIC)
@Setter(value = AccessLevel.PUBLIC)
public abstract class EffectPerk extends ActivePerk {
    
    /**
     * Player, StartZeit
     */
    private Map<String, Long> remain;
    private long activeTime;
    private int update;
    
    public EffectPerk(String name, ManageableGame game) {
        super(name, game);
        remain = new HashMap<>();
        this.activeTime = getOptionConfig().getInt("Perk." + getName() + ".activeTime", 3) * 1000;
    }
    
    public EffectPerk(String name) {
        this(name, GameApi.getCurrent());
    }
    
    @Override
    public void onTick() {
        if (update >= 5) {
            updatePlayers();
            update = 0;
        }
        onTickTimer();
        update++;
    }
    
    @SuppressWarnings("deprecation")
    @EventHandler
    public void playerInteractEvent(PlayerInteractEvent e) {
        if (game.isIngame() && e.getAction() != Action.PHYSICAL) {
            Player p = e.getPlayer();
            if (hasPerkSelected(p)) {
                if (isActive(p)) {
                    if (hasEnoughWool(p)) {
                        if (hasCooldown(p)) {
                            onCooldownInteract(e);
                        } else if (!getRemain().containsKey(p.getName())) {
                            onNonCooldownInteract(e);
                            e.setCancelled(true);
                            e.getPlayer().updateInventory();
                        } else {
                            onActiveInteract(e);
                            e.setCancelled(true);
                            e.getPlayer().updateInventory();
                        }
                    } else {
                        playNoSound(p);
                        e.setCancelled(true);
                        e.getPlayer().updateInventory();
                    }
                }
            }
        }
    }
    
    public void updatePlayers() {
        for (Iterator<Entry<String, Long>> iterator = remain.entrySet().iterator(); iterator.hasNext(); ) {
            Entry<String, Long> type = iterator.next();
            String name = type.getKey();
            Player p = Bukkit.getPlayer(name);
            long old = type.getValue();
            long time = System.currentTimeMillis() - old;
            //int slot = getSlot(p);
            //Abgelaufen
            if (time > activeTime) {
                iterator.remove();
                onEffectEnd(name);
                setCooldownAndUpdate(p);
            } /*else {
                ItemStack is = p.getInventory().getItem(slot);
				int amount = (int) ((activeTime / 1000) - (time / 1000));
				is.setAmount(amount);
				is.setDurability((short) 0);
				updateSlot(slot + 36, is, p);
				}*/
            onUpdate(name, old);
        }
    }
    
    public void setRemainActive(Player p) {
        remain.put(p.getName(), System.currentTimeMillis());
    }
    
    public boolean isRemainActive(Player p) {
        if (remain.containsKey(p.getName())) {
            long old = remain.get(p.getName());
            return System.currentTimeMillis() - old < activeTime;
        } else {
            return false;
        }
    }
    
    public void setActiveItem(Player p) {
        int slot = getSlot(p);
        ItemStack is = setHiddenName(perksConfig.getItemStack("perks." + getName() + ".ingame.active", p));
        //ItemStack is = Utils.generateItemStackFromConfig("perks." + getName() + ".ingame.active", Utils.getConfigFile("perks.yml"), "perks.yml", p);
        p.getInventory().setItem(slot, is);
    }
    
    @EventHandler
    @Override
    public void onInventoryClickEvent(InventoryClickEvent e) {
        if (e.getWhoClicked() instanceof Player) {
            Player p = (Player) e.getWhoClicked();
            if (game.isIngame() && isActive(p)) {
                if (hasCooldown(p) || isRemainActive(p)) {
                    e.setCancelled(true);
                    p.updateInventory();
                    me.vicevice.general.api.games.utils.Message.send(p, "Move your items when they are noncooldown!", "Messages.moveInfo");
                }
            }
        }
    }
    
    @Override
    public List<String> addFormatingObjects() {
        return Collections.singletonList(update / 1000 + "");
    }
    
    @Override
    public List<String> addFormatingReplace() {
        return Collections.singletonList("%activetime%");
    }
    
    @Override
    public void setCooldownAndUpdate(Player p) {
        remain.remove(p.getName());
        super.setCooldownAndUpdate(p);
    }
    
    public void onActiveInteract(PlayerInteractEvent e) {
    }
    
    /**
     * Startet den Timer und setzt im das ActiveItem
     * Entfernt die benötigten Kosten
     *
     * @param p für den perk gestartet werden soll
     */
    public void start(Player p) {
        setRemainActive(p);
        removeWool(p);
        setActiveItem(p);
    }
    
    /**
     * Wird jeden Tick ausgeführt
     */
    public void onTickTimer() {
    }
    
    /**
     * Wird alle 5 Ticks ausgeführt
     *
     * @param name      Name des Spielers
     * @param startTime die zeit in ms wann er das perk aktiviert hat
     */
    public abstract void onUpdate(String name, long startTime);
    
    /**
     * Methode wenn der Effekt ausläuft
     *
     * @param name
     */
    public abstract void onEffectEnd(String name);
}
