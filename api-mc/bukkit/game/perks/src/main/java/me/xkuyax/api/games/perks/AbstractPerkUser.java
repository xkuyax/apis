package me.xkuyax.api.games.perks;

import com.google.common.collect.Multimap;
import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.*;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

@Getter
public class AbstractPerkUser implements PerkUser {
    
    private Multimap<PerkType, String> defaultPerks;
    private ManageableGame game;
    private ConcurrentHashMap<AbstractPerk, Long> cooldown;
    private HashMap<PerkType, List<AbstractPerk>> selected;
    private String name;
    private Player player;
    
    public AbstractPerkUser(String name, ManageableGame game) {
        this.game = game;
        this.defaultPerks = game.getDefaultPerks();
        this.name = name;
        this.player = Bukkit.getPlayer(name);
        cooldown = new ConcurrentHashMap<>();
        selected = new HashMap<>();
    }
    
    @Override
    public void activePerks() {
        for (List<AbstractPerk> lap : selected.values()) {
            for (AbstractPerk ap : lap) {
                ap.activate(player);
                ap.giveNonCoolDownItem(player);
            }
        }
    }
    
    @Override
    public void updatePerks() {
        Player p = getPlayer();
        long current = System.currentTimeMillis();
        if (game.isIngame()) {
            for (Iterator<Entry<AbstractPerk, Long>> iterator = cooldown.entrySet().iterator(); iterator.hasNext(); ) {
                Entry<AbstractPerk, Long> entry = iterator.next();
                AbstractPerk ap = entry.getKey();
                if (ap.isUpdate()) {
                    long l = entry.getValue();
                    long c = ap.getCooldown();
                    int b = (int) (current - l);
                    if (b >= c) {
                        ap.giveNonCoolDownItem(p);
                        iterator.remove();
                    } else {
                        int slot = ap.getSlot(p);
                        ItemStack is = p.getInventory().getItem(slot);
                        if (is != null) {
                            int amount = (ap.getCooldown() / 1000) - (b / 1000);
                            is.setAmount(amount);
                            is.setDurability((short) 0);
                            PerkUtils.updateSlot(slot + 36, is, p);
                        }
                    }
                }
            }
        }
    }
    
    @Override
    public void loadPerks() {
        Player p = player;
        User user = ViceviceApi.getOnlineUser(p);
        new PerkLoader(p, user, game.getOptions().getMaxActivePerks(), PerkType.ACTIVE).load();
        new PerkLoader(p, user, game.getOptions().getMaxPassivePerks(), PerkType.PASSIVE).load();
    }
    
    @Override
    public void savePerks() {
        User user = ViceviceApi.getOnlineUser(player);
        for (List<AbstractPerk> lap : selected.values()) {
            for (int i = 0; i < lap.size(); i++) {
                AbstractPerk ap = lap.get(i);
                user.setKey("perks:" + game.getName() + "_" + ap.getType().toString() + "_" + i, ap.getName());
            }
        }
        user.saveKeys();
    }
    
    public void setActive(AbstractPerk perk, int id) {
        List<AbstractPerk> existing = getSelected().get(perk.getType());
        if (existing == null) {
            existing = new ArrayList<>(Collections.singletonList(perk));
        } else {
            if (existing.size() <= id) {
                existing.add(perk);
            } else {
                existing.set(id, perk);
            }
        }
        getSelected().put(perk.getType(), existing);
    }
    
    @Data
    @RequiredArgsConstructor
    private class PerkLoader {
        
        private final Player p;
        private final User user;
        private final int max;
        private final PerkType type;
        
        public void load() {
            for (int i = 0; i < max; i++) {
                String old = user.getKey("perks:" + getName() + "_" + type.toString() + "_" + i);
                if (old == null) {
                    old = new ArrayList<>(defaultPerks.get(type)).get(i);
                }
                if (old == null) {
                    continue;
                }
                AbstractPerk ap = game.getPerkManager().getPerk(old);
                if (ap == null) {
                    continue;
                }
                if (ap.hasPurchased(user) || (game.getOptions().isTryPerks() && ap.hasFreeUses(user))) {
                    setActive(ap, i);
                }
            }
        }
        
        private String getName() {
            //			return game instanceof DuelsAbstractController ? "duels:" + game.getName() : game.getName();
            return game.getName();
        }
    }
}
