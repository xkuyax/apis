package me.xkuyax.api.games.perks;

import com.comphenix.packetwrapper.WrapperPlayServerSetSlot;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.Metadatable;
import org.bukkit.util.BlockIterator;

public class PerkUtils {
    
    protected static BlockFace[] axis = {BlockFace.SOUTH,
            BlockFace.WEST,
            BlockFace.NORTH,
            BlockFace.EAST};
    protected static Side[] multiplier = {new Side(0, 1),
            new Side(-1, 0),
            new Side(0, -1),
            new Side(1, 0)};
    
    protected boolean areSameTeam(Player p, Player c) {
        Game game = GameApi.getCurrent();
        GamePlayer gu = game.getPlayer(p);
        GamePlayer gc = game.getPlayer(c);
        if (gu.isSpectator() || gc.isSpectator()) {
            return true;
        }
        if (gu.getTeam() == null || gc.getTeam() == null) {
            return true;
        }
        return gu.getTeam().equals(gc.getTeam());
    }
    
    protected void playNoSound(Player p) {
        Location l = p.getLocation();
        l.getWorld().playSound(l, Sound.VILLAGER_NO, 1F, 1F);
    }
    
    protected void playSound(Player p, Sound sound) {
        p.playSound(p.getLocation(), sound, 1f, 1f);
    }
    
    @SuppressWarnings("deprecation")
    protected void removeWool(Player p, int toRemove) {
        Inventory inv = p.getInventory();
        int removed = 0;
        for (ItemStack is : inv) {
            if (is == null || !is.getType().equals(Material.WOOL)) {
                continue;
            }
            int a = is.getAmount();
            while (a > 0 && removed < toRemove) {
                removed++;
                a--;
            }
            if (a == 0) {
                inv.remove(is);
            } else {
                is.setAmount(a);
            }
        }
        p.updateInventory();
    }
    
    protected int getWool(Player p) {
        return getAmount(p, Material.WOOL);
    }
    
    protected int getAmount(Player p, Material material) {
        int r = 0;
        Inventory inv = p.getInventory();
        for (ItemStack is : inv.getContents()) {
            if (is != null && is.getType().equals(material)) r += is.getAmount();
        }
        return r;
    }
    
    //	protected PerkUser getUser(Player p) {
    //		return PerkManager.a().getUser(p);
    //	}
    public static BlockFace yawToFace(float yaw) {
        return axis[(Math.round(yaw / 90.0F) & 0x3)];
    }
    
    public static Side yawToSide(float yaw) {
        return multiplier[(Math.round(yaw / 90.0F) & 0x3)];
    }
    
    public static Side yawToSide(float yaw, int i) {
        int c = (Math.round(yaw / 90.0F) & 0x3);
        int index = c + i >= multiplier.length ? (c + i) - multiplier.length : c + i;
        return multiplier[index];
    }
    
    public static Block getArrowLocation(ProjectileHitEvent e) {
        Block hit = null;
        BlockIterator iterator = new BlockIterator(e.getEntity().getWorld(), e.getEntity().getLocation().toVector(), e.getEntity().getVelocity().normalize(), 0.0D, 4);
        while (iterator.hasNext()) {
            hit = iterator.next();
            if (hit.getType() != Material.AIR) {
                break;
            }
        }
        return hit;
    }
    
    protected <T extends Projectile> T launchProjectile(Player p, Class<? extends T> a, String metaData) {
        T sb = p.launchProjectile(a);
        saveMetaData(sb, metaData, true);
        return sb;
    }
    
    protected void saveMetaData(Metadatable meta, String key, Object value) {
        meta.setMetadata(key, new FixedMetadataValue(General.a(), value));
    }
    
    protected static void updateSlot(int i, ItemStack is, Player p) {
        WrapperPlayServerSetSlot wps = new WrapperPlayServerSetSlot();
        wps.setSlot((short) i);
        wps.setSlotData(is);
        wps.setWindowId((byte) 0);
        wps.sendPacket(p);
    }
    
    protected boolean isHeadShot(Entity hit, Projectile proj) {
        double y = proj.getLocation().getY();
        double shotY = hit.getLocation().getY();
        return y - shotY > 1.35d;
    }
    
    @AllArgsConstructor
    @Data
    public static class Side {
        
        private int x, z;
    }
    
    public enum PlayerSight {
        FRONT(1),
        LEFT(3),
        BEHIND(4),
        RIGHT(2);
        
        @Getter
        private int side;
        
        PlayerSight(int side) {
            this.side = side;
        }
    }
}
