package me.xkuyax.api.games.perks;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public abstract class HoldingPerk extends AbstractPerk {
    
    private int ticks;
    private Map<String, HoldData> holdCounter = new HashMap<>();
    
    public HoldingPerk(String name, ManageableGame game) {
        super(name, PerkType.ACTIVE, game);
        new BukkitRunnable() {
            
            @Override
            public void run() {
                ticks++;
                for (Entry<String, HoldData> entry : holdCounter.entrySet()) {
                    HoldData hd = entry.getValue();
                    String name = entry.getKey();
                    Player p = Bukkit.getPlayer(name);
                    if (hd.getLastTick() >= ticks) {
                        hd.setAmount(hd.getAmount() + 1);
                        onTick(p, hd.getAmount());
                    } else {
                        if (hd.getAmount() > 0) {
                            onEnd(p, hd.getAmount());
                        }
                        hd.setAmount(0);
                    }
                }
            }
        }.runTaskTimer(General.a(), 1L, 1L);
    }
    
    @EventHandler
    public void onPlayerInteractEvent(PlayerInteractEvent e) {
        Player p = e.getPlayer();
        if (isActive(p)) {
            if (!hasCooldown(p)) {
                if (hasPerkSelected(p)) {
                    HoldData hd = holdCounter.get(p.getName());
                    if (hd == null) {
                        hd = new HoldData(0, 0);
                    }
                    if (hd.getAmount() == 0) {
                        if (hasEnoughWool(p)) {
                            onStart(p, 0);
                            hd.setLastTick(ticks + 5);
                            holdCounter.put(p.getName(), hd);
                        }
                    } else {
                        //TODO Nur für Woolbattle gut :(
                        if (ItemUtils.count(p, Material.WOOL) > 0) {
                            hd.setLastTick(ticks + 5);
                            holdCounter.put(p.getName(), hd);
                        } else {
                            onEnd(p, hd.getAmount());
                        }
                    }
                }
            }
        }
    }
    
    public abstract void onTick(Player p, int ticks);
    
    public abstract void onStart(Player p, int ticks);
    
    public abstract void onEnd(Player p, int ticks);
    
    @Data
    @AllArgsConstructor
    static class HoldData {
        
        private int lastTick;
        private int amount;
    }
}
