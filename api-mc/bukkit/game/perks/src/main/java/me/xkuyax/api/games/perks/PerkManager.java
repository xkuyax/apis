package me.xkuyax.api.games.perks;

import lombok.Getter;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;

@Getter
public class PerkManager extends BukkitRunnable {
    
    private ManageableGame game;
    private Config perkConfig;
    private List<AbstractPerk> perks;
    
    public PerkManager(ManageableGame game) {
        this.game = game;
        perks = new ArrayList<>();
        perkConfig = new Config(game, "perks.yml", true);
        runTaskTimer(General.a(), 1, 1);
    }
    
    public AbstractPerk getPerk(String name) {
        for (AbstractPerk ap : perks) {
            if (ap.getName().equalsIgnoreCase(name)) {
                return ap;
            }
        }
        return null;
    }
    
    public int getMaximumPerks(PerkType type) {
        return type == PerkType.ACTIVE ? game.getOptions().getMaxActivePerks() : game.getOptions().getMaxPassivePerks();
    }
    
    public void register(AbstractPerk perk) {
        if (!perks.contains(perk)) {
            perks.add(perk);
        }
    }
    
    public void onStart() {
        for (GamePlayer pu : game.getPlayers()) {
            pu.activePerks();
        }
    }
    
    @Override
    public void run() {
        if (game.isIngame()) {
            for (GamePlayer pu : game.getPlayers()) {
                pu.updatePerks();
            }
            for (AbstractPerk ap : getPerks()) {
                ap.onTick();
            }
        }
    }
    
    public List<AbstractPerk> getPerks(final Player p) {
        List<AbstractPerk> clone = new ArrayList<>(perks.size());
        for (AbstractPerk ap : perks) {
            clone.add(ap);
        }
        clone.sort((o1, o2) -> {
            String a = o1.getDisplayName(p);
            String b = o2.getDisplayName(p);
            a = ChatColor.stripColor(a);
            b = ChatColor.stripColor(b);
            return a.compareToIgnoreCase(b);
        });
        return clone;
    }
}
