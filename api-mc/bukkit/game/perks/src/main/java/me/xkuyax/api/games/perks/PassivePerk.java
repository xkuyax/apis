package me.xkuyax.api.games.perks;

import lombok.ToString;

@ToString(callSuper = true)
public abstract class PassivePerk extends AbstractPerk {
    
    public PassivePerk(String name, ManageableGame game) {
        super(name, PerkType.PASSIVE, game);
    }
    
    public PassivePerk(String name) {
        this(name, GameApi.getCurrent());
    }
}
