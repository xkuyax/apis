package me.xkuyax.api.games.perks;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public class SelectPerkInventory extends SingleInventory {
    
    private PerkManager perkManager;
    private PerkType type;
    private PerkUser user;
    private int id;
    
    public SelectPerkInventory(Player player, PerkManager manager, int id, PerkType type) {
        super(player, manager.getPerkConfig().getInt("Inventorys.selectPerkInventory.size", 36), manager.getPerkConfig()
                .getTranslated("Inventorys.selectPerkInventory.title", player, "&4Select your perk"), true, true);
        this.perkManager = manager;
        this.user = manager.getGame().getPlayer(player);
        this.type = type;
        this.id = id;
        setDynamic(true);
        updateInventory();
    }
    
    @Override
    public void updateInventory() {
        Player p = getOwner();
        clear();
        User user = ViceviceApi.getOnlineUser(p);
        int amount = GameApi.getCurrent().getOptions().getTryPerkAmount();
        List<String> a = GameApi.getCurrent().getPerkManager().getPerkConfig().getTranslatedList("Usesleft.lore", p, true, "&eVerbleibende Benutzungen: &b%uses%");
        for (AbstractPerk ap : perkManager.getPerks(p)) {
            if (ap.getType().equals(type)) {
                ItemStack selItemStack = ap.getSelectItem(p);
                if (ap.isVisible()) {
                    ItemBuilder ib = new ItemBuilder(selItemStack);
                    ib.formatBoth(ap.getFormattingReplace(), ap.getFormattingObjects());
                    if (ap.getAvailability() == Availability.STORE && !ap.hasPurchased(user)) {
                        ib.getLore().addAll(a);
                        ib.formatBoth(A.a("%uses%"), A.a(amount - ap.getUsesLeft(user) + ""));
                    }
                    ItemStack is = ib.build();
                    add(contains(ap) ? ItemUtils.addGlow(is) : is, new SelectPerkItem(ap));
                }
            }
        }
        set(perkManager.getPerkConfig().getInt("Inventorys.selectPerkInventory.size", 36) - 1, perkManager.getPerkConfig().getItemStack("Items.back", p), new BackItem());
    }
    
    public boolean contains(AbstractPerk ap) {
        if (user.getSelected().containsKey(type)) {
            List<AbstractPerk> a = user.getSelected().get(type);
            if (a.size() > id) {
                return a.get(id).equals(ap);
            }
        }
        return false;
    }
    
    public boolean hasSelected(AbstractPerk ap) {
        for (List<AbstractPerk> a : user.getSelected().values()) {
            for (AbstractPerk cp : a) {
                if (cp.getName().equals(ap.getName())) {
                    return true;
                }
            }
        }
        return false;
    }
    
    public class BackItem implements PageForwardItem {
        
        @Override
        public void onClick(InventoryClickEvent e) {
        }
        
        @Override
        public SingleInventory getForward() {
            return new SelectPerkEditInventory(getOwner(), perkManager);
        }
    }
    
    @Data
    @AllArgsConstructor
    public class SelectPerkItem implements PageItem {
        
        private AbstractPerk perk;
        
        @Override
        public void onClick(InventoryClickEvent e) {
            if (canBuy((Player) e.getWhoClicked(), perk)) {
                if (!hasSelected(perk)) {
                    user.setActive(perk, id);
                    e.getWhoClicked().openInventory(new SelectPerkEditInventory(getOwner(), perkManager).build());
                    Message.send(getOwner(), "Du hast das perk %perk% ausgewählt", "Messages.perkinfo", A.a("%perk%"), A.a(perk.getDisplayName(getOwner())));
                } else {
                    Message.send(getOwner(), "Du hast das perk %perk% beretis ausgewählt", "Messages.perkalready", A.a("%perk%"), A.a(perk.getDisplayName(getOwner())));
                }
            }
        }
        
        public boolean canBuy(Player p, AbstractPerk perk) {
            if (perk.getAvailability() == Availability.FREE) {
                return true;
            }
            User user = ViceviceApi.getOnlineUser(p);
            if (perk.getAvailability() == Availability.STORE && !perk.hasPurchased(user) && !perk.hasFreeUses(user)) {
                Message.send(p, "Du musst dir das Perk %perk% im lobby shop kaufen!", "Messages.notStorePurchased", A.a("%perk%"), A.a(perk.getDisplayName(p)));
                return false;
            }
            if (perk.getAvailability() == Availability.VIP && !p.hasPermission("general.vip")) {
                Message.send(p, "Dieses Perk ist nur für Vips!", "Messages.notVipForPerk");
                return false;
            }
            return true;
        }
    }
}
