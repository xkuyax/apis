package me.xkuyax.api.games.perks;

import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.inventory.ItemStack;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class ArrowPerk extends PassivePerk {
    
    private Map<String, Integer> arrowPeriod = new HashMap<>();
    
    public ArrowPerk(String name, ManageableGame game) {
        super(name, game);
    }
    
    public ArrowPerk(String name) {
        super(name, GameApi.getCurrent());
    }
    
    @EventHandler
    public void onEntityLaunchEvent(EntityDamageByEntityEvent e) {
        if (e.getEntity() instanceof Player && e.getDamager() instanceof Arrow && ((Projectile) e.getDamager()).getShooter() instanceof Player) {
            Arrow a = (Arrow) e.getDamager();
            Player p = (Player) e.getEntity();
            Player c = (Player) a.getShooter();
            if (isActive(c)) {
                Integer i = arrowPeriod.getOrDefault(c.getName(), 0);
                if (i == getArrowCount()) {
                    onArrowHitEffect(a, p, c, e);
                }
            }
        }
    }
    
    @EventHandler
    public void onProjectileLaunchEvent(ProjectileLaunchEvent e) {
        if (!e.isCancelled()) {
            if (e.getEntity() instanceof Arrow && e.getEntity().getShooter() instanceof Player) {
                Player p = (Player) e.getEntity().getShooter();
                if (isActive(p)) {
                    updateCount(p);
                }
                //	checkForGlow(arrowPeriod.getOrDefault(p.getName(), 0), p);
            }
        }
    }
    
    private void updateCount(Player p) {
        Integer old = arrowPeriod.getOrDefault(p.getName(), 0);
        int slot = getSlot(p);
        ItemStack is = p.getInventory().getItem(slot);
        arrowPeriod.put(p.getName(), old + 1);
        if (old == getArrowCount() - 2) {
            is = ItemUtils.addGlow(is);
        } else {
            is = getNonCooldownItem(p);
            if (old >= getArrowCount()) {
                arrowPeriod.put(p.getName(), 0);
            }
        }
        p.getInventory().setItem(slot, is);
        updateSlot(slot + 36, is, p);
    }
    
    /**
     * Überprüft ob der Bogen einen unsichtbaren enchantment efffekt haben soll
     * wenn der nächste pfeil den jeweiligen effekt auslöst
     */
    public void checkForGlow(Integer i, Player p) {
        int slot = getSlot(p);
        ItemStack is = p.getInventory().getItem(slot);
        if (i == getArrowCount() + 1) {
            is = ItemUtils.addGlow(is);
        } else {
            is = ItemUtils.removeGlow(is);
            if (i > getArrowCount()) {
                arrowPeriod.put(p.getName(), 0);
            } else {
                arrowPeriod.put(p.getName(), i + 1);
            }
        }
        p.getInventory().setItem(slot, is);
        updateSlot(slot + 36, is, p);
    }
    
    public int getArrowCount() {
        return getOptionConfig().getInt(path + ".arrowCount", 3);
    }
    
    @Override
    public List<String> addFormatingObjects() {
        List<String> a = super.addFormatingObjects();
        a.addAll(Collections.singletonList(getArrowCount() + ""));
        return a;
    }
    
    @Override
    public List<String> addFormatingReplace() {
        List<String> a = super.addFormatingObjects();
        a.addAll(Collections.singletonList("%arrows%"));
        return a;
    }
    
    /**
     * @param a pfeil
     * @param p getroffen
     * @param c geschossen
     * @param e event
     */
    public abstract void onArrowHitEffect(Arrow a, Player p, Player c, EntityDamageByEntityEvent e);
    
    public abstract Arrowtype getArrowType();
    
    public enum Arrowtype {
        FIRED_ARROWS
    }
}
