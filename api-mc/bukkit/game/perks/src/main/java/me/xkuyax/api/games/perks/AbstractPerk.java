package me.xkuyax.api.games.perks;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import java.util.*;

@Getter(value = AccessLevel.PUBLIC)
@Setter(value = AccessLevel.PUBLIC)
@EqualsAndHashCode(of = "name", callSuper = false)
public abstract class AbstractPerk extends PerkUtils implements Listener {
    
    @Getter(value = AccessLevel.PUBLIC)
    public static Config optionConfig;
    @Getter(value = AccessLevel.PUBLIC)
    public Config perksConfig;
    public ManageableGame game;
    public int cooldown;
    public HashMap<String, Long> cooldowns;
    public String name;
    @Getter(value = AccessLevel.PUBLIC)
    public HashSet<String> players;
    public PerkType type;
    public int cost;
    public String path;
    
    public AbstractPerk(String name, int cost, int cooldown, PerkType type, ManageableGame game) {
        setName(name);
        setType(type);
        setCost(cost);
        setCooldown(cooldown * 1000);
        setCooldowns(new HashMap<>());
        setPlayers(new HashSet<>());
        Bukkit.getPluginManager().registerEvents(this, General.a());
        game.getPerkManager().register(this);
        path = "Perks." + getName() + ".";
        //	PerkManager.a().registerPerk(this);
        this.game = game;
        perksConfig = new Config(game, "perks.yml", true);
        if (optionConfig == null) {
            optionConfig = new Config(game, "perkoptions.yml", true);
        }
        saveMetaData();
    }
    
    private void saveMetaData() {
        perksConfig.set("perks." + getName() + ".type", getType().toString());
        perksConfig.set("perks." + getName() + ".visible", isVisible());
        perksConfig.set("perks." + getName() + ".formatreplace", getFormattingReplace());
        perksConfig.set("perks." + getName() + ".formatobjects", getFormattingObjects());
    }
    
    public AbstractPerk(String name, PerkType type) {
        //this(name, getOptionConfig().getInt("Perk." + name + ".woolcost", 8), getOptionConfig().getInt("Perk." + name + ".cooldown", 5), type);
        optionConfig = new Config(game, "perkoptions.yml", true);
        int cost = getOptionConfig().getInt("Perk." + name + ".woolcost", 8);
        int cooldown = getOptionConfig().getInt("Perk." + name + ".cooldown", 5);
        setup(name, cost, cooldown, type, GameApi.getCurrent());
    }
    
    public AbstractPerk(String name, PerkType type, ManageableGame game) {
        optionConfig = new Config(game, "perkoptions.yml", true);
        int cost = getOptionConfig().getInt("Perk." + name + ".woolcost", 8);
        int cooldown = getOptionConfig().getInt("Perk." + name + ".cooldown", 5);
        setup(name, cost, cooldown, type, game);
    }
    
    private void setup(String name, int cost, int cooldown, PerkType type, ManageableGame game) {
        setName(name);
        setType(type);
        setCost(cost);
        setCooldown(cooldown * 1000);
        setCooldowns(new HashMap<>());
        setPlayers(new HashSet<>());
        Bukkit.getPluginManager().registerEvents(this, General.a());
        game.getPerkManager().register(this);
        path = "Perks." + getName() + ".";
        //	PerkManager.a().registerPerk(this);
        this.game = game;
        perksConfig = new Config(game, "perks.yml", true);
        if (optionConfig == null) {
            optionConfig = new Config(game, "perkoptions.yml", true);
        }
        saveMetaData();
    }
    
    public String getDisplayName(Player p) {
        return perksConfig.getTranslated("perks." + getName() + ".displayname", p, getName());
    }
    
    public String getDisplayName() {
        return perksConfig.getString("perks." + getName() + ".displayname.de", getName());
    }
    
    public void activate(Player p) {
        if (!getPlayers().contains(p.getName())) {
            getPlayers().add(p.getName());
        }
    }
    
    public void applyNonCooldown(Player p) {
        PerkUser pu = getUser(p);
        pu.getCooldown().put(this, System.currentTimeMillis());
        pu.updatePerks();
    }
    
    public boolean canUse(Player p) {
        return isActive(p) && !hasCooldown(p) && hasEnoughWool(p) && hasPerkSelected(p);
    }
    
    public void changeCooldown(Player p) {
        long current = System.currentTimeMillis();
        getCooldowns().put(p.getName(), current);
    }
    
    public ItemStack getCooldownItem(Player p) {
        return setHiddenName(perksConfig.getItemStack("perks." + getName() + ".ingame.cooldown", p));
    }
    
    public ItemStack getNonCooldownItem(Player p) {
        return setHiddenName(perksConfig.getItemStack("perks." + getName() + ".ingame.nocooldown", p));
    }
    
    public boolean isVisible() {
        return true;
    }
    
    public ItemStack getSelectItem(Player p) {
        ItemBuilder ib = perksConfig.getItemBuilder("perks." + getName() + ".lobby.select", p);
        ib.formatBoth(A.a("%cooldown%"), A.a(getCooldown() / 1000 + ""));
        return ib.build();
    }
    
    public ItemStack setHiddenName(ItemStack is) {
        return InventoryModule.setHiddenString(is, getName());
    }
    
    public void giveCoolDownItem(Player p) {
        if (game.isIngame() && !game.getPlayer(p).isSpectator()) {
            ItemStack is = getCooldownItem(p);
            int slot = getSlot(p);
            p.getInventory().setItem(slot, is);
            //	p.updateInventory();
            updateSlot(slot + 36, is, p);
        }
    }
    
    public void giveNonCoolDownItem(Player p) {
        if (game.isIngame() && !game.getPlayer(p).isSpectator()) {
            ItemStack is = getNonCooldownItem(p);
            int slot = getSlot(p);
            p.getInventory().setItem(slot, is);
            //	p.updateInventory();
            updateSlot(slot + 36, is, p);
        }
    }
    
    public int getSlot(Player p) {
        ItemStack[] a = p.getInventory().getContents();
        for (int i = 0; i < a.length; i++) {
            ItemStack is = a[i];
            if (is != null && is.hasItemMeta()) {
                String hidden = InventoryModule.getHiddenString(is);
                if (hidden != null && hidden.equalsIgnoreCase(getName())) {
                    return i;
                }
            }
        }
        if (getType() == PerkType.PASSIVE) {
            if (isEmpty(a, 8)) {
                return 8;
            }
            //SO MUCH DIRTY
            if (isEmpty(a, 7)) {
                return 7;
            }
        }
        for (int i = 0; i < a.length; i++) {
            if (a[i] == null || a[i].getType().equals(Material.AIR)) {
                return i;
            }
        }
        return 0;
    }
    
    private boolean isEmpty(ItemStack[] a, int slot) {
        return (a[slot] == null || a[slot].getType() == Material.AIR);
    }
    
    public boolean hasCooldown(Player p) {
        long current = System.currentTimeMillis();
        if (getCooldowns().containsKey(p.getName())) {
            long time = current - getCooldowns().get(p.getName());
            return (time <= getCooldown());
        } else {
            return false;
        }
    }
    
    public boolean hasEnoughWool(Player p) {
        return !game.getOptions().isPerksCosts() || (getWool(p) >= cost);
    }
    
    public boolean hasPerkSelected(Player p) {
        ItemStack hand = p.getItemInHand();
        if (hand != null) {
            String s = InventoryModule.getHiddenString(hand);
            return s != null && s.equals(name);
        }
        return false;
    }
    
    public boolean isActive(Player p) {
        return game.isInGame(p) && getPlayers().contains(p.getName());
    }
    
    public void remove(Player p) {
        getPlayers().remove(p.getName());
    }
    
    public void removeWool(Player p) {
        removeWool(p, cost);
    }
    
    public void deactivate(Player p) {
        ItemStack is = p.getItemInHand();
        if (is.getAmount() <= 1) {
            is.setType(Material.AIR);
            remove(p);
        } else {
            is.setAmount(is.getAmount() - 1);
        }
        p.setItemInHand(is);
    }
    
    public void setCooldownAndUpdate(Player p) {
        changeCooldown(p);
        giveCoolDownItem(p);
        applyNonCooldown(p);
    }
    
    public PerkUser getUser(Player p) {
        return game.getPlayer(p);
    }
    
    public void onTick() {
    }
    
    public Availability getAvailability() {
        return Availability.FREE;
    }
    
    public boolean isUpdate() {
        return true;
    }
    
    public List<String> getFormattingReplace() {
        List<String> a = new ArrayList<>(Arrays.asList("%dname%", "%name%", "%cooldown%", "%price%"));
        a.addAll(addFormatingReplace());
        return a;
    }
    
    public List<String> getFormattingObjects() {
        List<String> a = new ArrayList<>(Arrays.asList(getDisplayName(), getName(), getCooldown() / 1000 + "", getCost() + ""));
        a.addAll(addFormatingObjects());
        return a;
    }
    
    public List<String> addFormatingReplace() {
        return new ArrayList<>();
    }
    
    public List<String> addFormatingObjects() {
        return new ArrayList<>();
    }
    
    @EventHandler
    public void onInventoryClickEvent(InventoryClickEvent e) {
        if (e.getWhoClicked() instanceof Player) {
            Player p = (Player) e.getWhoClicked();
            if (game.isIngame() && game.isInGame(p) && hasCooldown(p)) {
                e.setCancelled(true);
                p.updateInventory();
                me.vicevice.general.api.games.utils.Message.send(p, "Move your items when they are noncooldown!", "Messages.moveInfo");
            }
        }
    }
    
    public boolean hasPurchased(User user) {
        return user.getPlayer().hasPermission("bedwars.perks") || user.getBuyedAmount(game.getName().toLowerCase() + "_" + getName().toLowerCase()) > 0;
    }
    
    public int getUsesLeft(User user) {
        if (game.getOptions().isTryPerks()) {
            return Utils.paste(user.getKey("uses_" + game.getName().toLowerCase() + "_" + getName().toLowerCase()), 0);
        }
        return 0;
    }
    
    public boolean hasFreeUses(User user) {
        return getUsesLeft(user) < game.getOptions().getTryPerkAmount();
    }
    
    @EventHandler
    public void onStartEvent(GameStartedEvent e) {
        if (players.size() == 0 && game.getOptions().isDisablePerksZeroPlayers()) {
            HandlerList.unregisterAll(this);
        }
        if (game.getOptions().isTryPerks() && getAvailability() == Availability.STORE) {
            for (String s : getPlayers()) {
                User user = ApiHandler.a().getUser(s, false);
                if (user != null) {
                    if (user.getBuyedAmount(game.getName() + "_" + getName().toLowerCase()) <= 0) {
                        user.setKey("uses_" + game.getName().toLowerCase() + "_" + getName().toLowerCase(), (getUsesLeft(user) + 1) + "");
                        user.saveKeys();
                    }
                }
            }
        }
    }
}
