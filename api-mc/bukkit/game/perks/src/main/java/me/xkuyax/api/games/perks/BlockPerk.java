package me.xkuyax.api.games.perks;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockPlaceEvent;

public abstract class BlockPerk extends PassivePerk {
    
    public BlockPerk(String name, ManageableGame game) {
        super(name, game);
    }
    
    public BlockPerk(String name) {
        super(name);
    }
    
    @EventHandler
    public void onBlockPlaceEvent(BlockPlaceEvent e) {
        Block block = e.getBlock();
        Player p = e.getPlayer();
        if (hasPerkSelected(p)) {
            boolean result = onBlockPlace(block);
            if (result) {
                e.setCancelled(true);
            } else {
                deactivate(p);
            }
        }
    }
    
    private void test() {
    
    }
    
    public abstract boolean onBlockBreak();
    
    public abstract boolean onBlockPlace(Block block);
}
