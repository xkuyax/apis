package me.xkuyax.api.games.perks;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public abstract class ActivePerk extends AbstractPerk {
    
    protected boolean cancelInteract;
    
    public ActivePerk(String name, int woolcost, int cooldown, boolean cancelInteract, ManageableGame game) {
        super(name, woolcost, cooldown, PerkType.ACTIVE, game);
        this.cancelInteract = cancelInteract;
    }
    
    public ActivePerk(String name, int woolcost, int cooldown) {
        this(name, woolcost, cooldown, true, GameApi.getCurrent());
    }
    
    public ActivePerk(String name, ManageableGame game) {
        super(name, PerkType.ACTIVE, game);
        cancelInteract = true;
    }
    
    public ActivePerk(String name) {
        this(name, GameApi.getCurrent());
    }
    
    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }
    
    @Override
    public int hashCode() {
        return super.hashCode();
    }
    
    public abstract void onCooldownInteract(PlayerInteractEvent e);
    
    public abstract void onNonCooldownInteract(PlayerInteractEvent e);
    
    @EventHandler
    public void playerInteractEvent(PlayerInteractEvent e) {
        if (game.isIngame() && e.getAction() != Action.PHYSICAL) {
            Player p = e.getPlayer();
            if (hasPerkSelected(p)) {
                if (isActive(p)) {
                    if (hasEnoughWool(p)) {
                        if (hasCooldown(p)) {
                            onCooldownInteract(e);
                            e.setCancelled(cancelInteract);
                            if (cancelInteract) {
                                e.getPlayer().updateInventory();
                            }
                        } else {
                            onNonCooldownInteract(e);
                            e.setCancelled(cancelInteract);
                            if (cancelInteract) {
                                e.getPlayer().updateInventory();
                            }
                        }
                    } else {
                        playNoSound(p);
                        e.setCancelled(cancelInteract);
                        if (cancelInteract) e.getPlayer().updateInventory();
                    }
                }
            }
        }
    }
}
