package me.xkuyax.api.games.perks;

import lombok.Getter;

@Getter
public enum PerkType {
    ACTIVE,
    PASSIVE
}
