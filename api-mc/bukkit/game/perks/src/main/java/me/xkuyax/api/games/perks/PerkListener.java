package me.xkuyax.api.games.perks;

import lombok.Getter;
import org.bukkit.event.Listener;

public class PerkListener implements Listener {
    
    @Getter
    private PerkManager manager;
    
    public PerkListener(PerkManager a) {
        this.manager = a;
    }
    /*
	private PerkType getType(ItemStack is, Player p) {
		ItemStack ac1 = Utils.generateItemStackFromConfig("Lobby.activeperk1", Utils.getConfigFile("perks.yml"), "perks.yml", p);
		ItemStack ac2 = Utils.generateItemStackFromConfig("Lobby.activeperk2", Utils.getConfigFile("perks.yml"), "perks.yml", p);
		ItemStack pas = Utils.generateItemStackFromConfig("Lobby.passiveperk", Utils.getConfigFile("perks.yml"), "perks.yml", p);
		if (is.equals(ac1)) {
			return PerkType.ACTIVE1;
		}
		if (is.equals(ac2)) {
			return PerkType.ACTIVE2;
		}
		if (is.equals(pas)) {
			return PerkType.PASSIVE;
		}
		return null;
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onInventoryClickEvent(InventoryClickEvent e) {
		if (e.getSlot() == e.getRawSlot()) {
			e.setCancelled(true);
			final Player p = (Player) e.getWhoClicked();
			PerkUser pu = manager.getUser(p);
			p.updateInventory();
			if (e.getCurrentItem() != null) {
				ItemStack is = e.getCurrentItem();
				AbstractPerk perk = manager.getPerk(is, p);
				PerkType type = pu.getCurrentlyOpen();
				if (perk != null && type != null) {
					pu.setPerk(type, perk);
					Utils.sendMessage(p, "perkSelected", "You have selected the perk %perk%", Arrays.asList("%perk%"), Arrays.asList(perk
							.getDisplayName(p)));
					Bukkit.getScheduler().runTaskLater(Main.a(), new Runnable() {
						
						@Override
						public void run() {
							p.closeInventory();
						}
					}, 1);
				}
			}
		}
	}
	
	/*
	@SuppressWarnings("deprecation")
	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerDropItemEvent(PlayerDropItemEvent e) {
		ItemStack is = e.getItemDrop().getItemStack();
		Player p = e.getPlayer();
		p.setItemInHand(null);
		p.setItemInHand(is);
		Utils.log(is.getAmount());
		e.getItemDrop().remove();
		p.updateInventory();
		Utils.log(p.getItemInHand().getAmount());
	}
	
	@EventHandler
	public void onPlayerInteractEvent(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		if (e.getItem() != null) {
			ItemStack is = e.getItem();
			PerkType type = getType(is, p);
			if (type != null) {
				manager.getUser(p).setCurrentlyOpen(type);
				p.openInventory(getManager().getSelectPerkInventory(type, p));
			}
		}
	}*/
}
