package me.xkuyax.api.games.perks;

import org.bukkit.entity.Player;

public class SelectPerkEditInventory extends SingleInventory {
    
    private PerkManager perkManager;
    
    public SelectPerkEditInventory(Player player, PerkManager pm) {
        //	super(player,pm.getPerkConfig().getTranslated("Inventory.selectEditPerk", player, "Select your perk you want to edit"), true, true);
        super(player, pm.getPerkConfig(), "Inventory.selectEditPerk");
        this.perkManager = pm;
        setDynamic(true);
        updateInventory();
    }
    
    @Override
    public void updateInventory() {
        Player p = getOwner();
        clear();
        for (int i = 0; i < perkManager.getMaximumPerks(PerkType.ACTIVE); i++) {
            ItemBuilder ib = perkManager.getPerkConfig().getItemBuilder("Items.editActivePerk" + i, p);
            ib.formatBoth(A.a("%number%"), A.a(i + ""));
            set(perkManager.getPerkConfig().getInt("Items.editActivePerk." + i + ".slot", i), ib
                    .build(), new PageForward(new SelectPerkInventory(p, perkManager, i, PerkType.ACTIVE)));
        }
        for (int i = 0; i < perkManager.getMaximumPerks(PerkType.PASSIVE); i++) {
            ItemBuilder ib = perkManager.getPerkConfig().getItemBuilder("Items.editPassivePerk" + i, p);
            ib.formatBoth(A.a("%number%"), A.a(i + ""));
            set(perkManager.getPerkConfig().getInt("Items.editPassivePerk." + i + ".slot", i), ib
                    .build(), new PageForward(new SelectPerkInventory(p, perkManager, i, PerkType.PASSIVE)));
        }
    }
}
