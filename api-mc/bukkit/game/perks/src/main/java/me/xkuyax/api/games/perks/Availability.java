package me.xkuyax.api.games.perks;

public enum Availability {
    VIP,
    STORE,
    FREE
}
