package me.xkuyax.api.games.utils.chat;

import com.google.common.base.Preconditions;
import org.bukkit.entity.Player;

import java.util.*;

public class ChatTable {
    
    private String[][] values;
    private int columns, rows;
    private Integer[] tabs;
    private Set<Integer> written = new HashSet<>();
    
    /**
     * Creates a new chattable
     *
     * @param columns - amount of columns the table should have (werte)
     * @param rows    - amount of rows the table should have (zeileninhalte)
     * @param tabs    - is the amount of whitespaces you want
     */
    public ChatTable(int rows, int columns, Integer... tabs) {
        this.columns = columns;
        this.rows = rows;
        this.tabs = tabs;
        values = new String[rows][columns];
    }
    
    public ChatTable(int rows, int columns, config, String path) {
        this(rows, columns, config.getIntegerList(path, true, 20, 30, 40).toArray(new Integer[0]));
    }
    
    /**
     * Adds a Strin value to the table
     *
     * @param row - x
     * @param col - y
     * @param val
     */
    public void addValue(int row, int col, String val) {
        Preconditions.checkElementIndex(col, columns, "Out of range!");
        Preconditions.checkElementIndex(row, rows, "Out of range!");
        values[row][col] = val;
        written.add(row);
    }
    
    public void addValue(int row, Player p, Config config, String path, List<String> r, List<String> o) {
        String msg = config.getTranslated(path + ".msg", p, "ChatTable");
        msg = Utils.formatString(msg, r, o);
        addValue(row, config.getInt(path + ".y", 0), msg);
    }
    
    public void sortByY(final int index) {
        sortByY(index, Comparator.comparingInt(o -> Utils.paste(o, 0)));
    }
    
    public void sortByY(final int index, Comparator<String> c) {
        values = Arrays.copyOf(values, written.size());
        Arrays.sort(values, (o1, o2) -> {
            String s1 = o1[index];
            String s2 = o2[index];
            return c.compare(s1, s2);
        });
    }
    
    public void sendTo(Player p) {
        String sb = "";
        for (int x = 0; x < written.size(); x++) {
            for (int y = 0; y < columns; y++) {
                sb += values[x][y] + (y == (columns - 1) ? "" : "`");
            }
            if (x < (rows - 1)) {
                sb += "\n";
            }
        }
        TabText tt = new TabText(sb);
        tt.setTabs(tabs);
        p.sendMessage(tt.getPage(0, false));
    }
}
