package me.xkuyax.api.games.utils.velocity;

import org.bukkit.entity.Entity;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

public class VelocityTracker extends BukkitRunnable {
    
    public static final double MAX_VELOCITY_JUMPING = 0.41999998688697815;
    public static final double MAX_VELOCITY_JUMPING_NEGATIVE = -0.37663049823865513;
    public static final double MAX_VELOCITY_JUMPING_ONE_BLOCK_NEGATIVE = -0.5169479491049732;
    public static final double ON_GROUND = -0.0784000015258789d;
    public static final Consumer<Vector> EMPTY_CONSUMER = v -> {
    };
    public static final VelocityCondition NOT_ON_GROUND = (v, l) -> v.getY() != ON_GROUND && l.getY() != ON_GROUND;
    private List<Vector> tracked = new ArrayList<>();
    private Entity player;
    private int timeOut, count;
    private Consumer<Vector> onTrack, onLast;
    private VelocityCondition velocityCondition;
    
    public VelocityTracker(Entity player, int delay, int timeOut, Consumer<Vector> onTrack, VelocityCondition condition, Consumer<Vector> onLast) {
        runTaskTimer(General.a(), delay, 1);
        this.player = player;
        this.timeOut = timeOut;
        this.onTrack = onTrack;
        this.onLast = onLast;
        this.velocityCondition = condition;
    }
    
    @Override
    public void run() {
        if (player.isValid()) {
            Vector v = player.getVelocity();
            if (velocityCondition.next(v, tracked.size() == 0 ? v : tracked.get(tracked.size() - 1))) {
                tracked.add(v);
                onTrack.accept(v);
            } else {
                onLast.accept(v);
                cancel();
            }
            count++;
            if (count > timeOut) {
                cancel();
            }
        } else {
            cancel();
        }
    }
    
    public interface VelocityCondition {
        
        boolean next(Vector v, Vector l);
    }
}
