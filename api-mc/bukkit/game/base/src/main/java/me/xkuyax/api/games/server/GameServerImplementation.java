package me.xkuyax.api.games.server;

import org.bukkit.entity.Player;

public interface GameServerImplementation {
    
    void sendToServer(String server, Player player);
    
}
