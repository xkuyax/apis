package me.xkuyax.api.games.events;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
public class LobbyTickEvent extends Event {
    
    private static final HandlerList handlerList = new HandlerList();
    private ManageableGame game;
    private int secondsLeft;
    
    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }
    
    public static HandlerList getHandlerList() {
        return handlerList;
    }
}
