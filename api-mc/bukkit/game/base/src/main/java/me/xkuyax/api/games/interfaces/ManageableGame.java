package me.xkuyax.api.games.interfaces;

import me.xkuyax.api.games.GameState;
import me.xkuyax.api.games.utils.SelfRegisteringListener;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.util.List;

public interface ManageableGame extends Game, Plugin {
    
    void handleJoin(Player player);
    
    void handleQuit(Player player);
    
    void addMap(String name);
    
    int getRemainingLobbyTime();
    
    int getMinimumPlayers();
    
    int getMaximumPlayers();
    
    int getMaximumLobbyTime();
    
    int getMinAlivePlayersPerTeam();
    
    int getMinimiumPlayersIngame();
    
    int getGracePeriod();
    
    void setGracePeriod(int graceperiod);
    
    void start();
    
    void stop();
    
    void endGracePeriod();
    
    GameOptions getOptions();
    
    void setCurrent(GameMap map);
    
    void createWorld(GameMap map);
    
    MapManager getMapManager();
    
    boolean checkIfEnd();
    
    boolean isSoloGame();
    
    boolean isInGame(Player p);
    
    boolean isInGame(String p);
    
    boolean isDuels();
    
    boolean isForcedMap();
    
    void onQuit(Player p);
    
    void onJoin(Player p);
    
    void onLobbyTick(int secondsLeft);
    
    void onLobbyReset();
    
    GameState getState();
    
    void registerListener(SelfRegisteringListener listener);
    
    List<SelfRegisteringListener> getRegistredListeners();
    
    String getMatchHistoryName();
    
    void tryEnd();
    
    void addResetBlock(Block b);
    
    GameQuests getQuests();
    
    void unnickPlayers();
    
    void setForcedMap(GameMap map);
    
    default int getAlivePlayers() {
        return 0;
    }
    
    default AchievableManager getAchievableManager() {
        return null;
    }
    
    default void onGracePeriodTick(int gracePeriod) {
    }
    
    default int getGracePeriodTimer() {
        return this.getGameConfig().getInt("Settings.graceperiodTime", 30);
    }
}
