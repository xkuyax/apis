package me.xkuyax.api.games.commands;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;

public class BuildCommand extends GCommand {
    
    public BuildCommand() {
        super("build");
    }
    
    @CmdHandler(ignoreargs = true, sender = CSender.PLAYER_ONLY)
    public void onIgnoreArgs(Player p, String[] args) {
        GameCommandPermissionEvent event = new GameCommandPermissionEvent(p.hasPermission("general.games.build"), p, GameCommandPermissionEvent.PermissionType.BUILD);
        Bukkit.getServer().getPluginManager().callEvent(event);
        if (event.isHasPermission()) {
            GamePlayer gp = GameApi.getCurrent().getGamePlayer(p.getName());
            gp.setBuild(!gp.canBuild());
            gp.getPlayer().setGameMode(GameMode.CREATIVE);
            p.sendMessage("§bBuild: " + gp.canBuild());
        }
    }
}
