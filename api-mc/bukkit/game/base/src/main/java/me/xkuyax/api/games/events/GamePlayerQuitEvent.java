package me.xkuyax.api.games.events;

import lombok.Getter;
import org.bukkit.event.HandlerList;

public class GamePlayerQuitEvent extends GamePlayerEvent {
    
    @Getter
    private static final HandlerList handlerList = new HandlerList();
    
    public GamePlayerQuitEvent(ManageableGame game, GamePlayer player) {
        super(game, player);
    }
    
    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }
}
