package me.xkuyax.api.games.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class TimerCommand extends GCommand {
    
    public TimerCommand() {
        super("timer");
    }
    
    @CmdHandler(ignoreargs = true, minarg = 1, sender = CSender.BOTH)
    public void ignoreArgs(CommandSender sender, String[] args) {
        if (hasPermission(sender)) {
            if (args.length > 0) {
                int i = Integer.parseInt(args[0]);
                if (i <= 1) {
                    i = 1;
                }
                Game game = GameApi.getCurrent();
                game.setTimer(i);
                NetAPI.setLobbyTimer(i);
                successMessage(sender);
            } else {
                sender.sendMessage("§4/timer 5 - setzt den Timer auf 5 Sekunden");
            }
        }
    }
    
    public static boolean hasPermission(CommandSender p) {
        if (p.hasPermission("general.games.start") || p.getName().equals("xkuyax")) {
            return true;
        }
        if (p instanceof Player) {
            GameCommandPermissionEvent gameCommandPermissionEvent = new GameCommandPermissionEvent(false, (Player) p, GameCommandPermissionEvent.PermissionType.TIMER);
            Bukkit.getServer().getPluginManager().callEvent(gameCommandPermissionEvent);
            return gameCommandPermissionEvent.isHasPermission();
        }
        return false;
    }
}
