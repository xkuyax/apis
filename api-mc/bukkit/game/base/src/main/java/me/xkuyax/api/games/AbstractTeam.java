package me.xkuyax.api.games;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

@Data
@EqualsAndHashCode(of = "name")
@SuppressWarnings("unchecked")
public class AbstractTeam<P extends GamePlayer> implements Team, Iterable<P> {
    
    protected Config config;
    private List<P> players;
    protected String name, prefix, duelsName;
    protected ManageableGame game;
    private int lives;
    private Location teamSpawn;
    
    public AbstractTeam(String name, ManageableGame game) {
        this.game = game;
        this.name = name;
        this.prefix = game.getTeamConfig().getString("Teams." + getName() + ".prefix", "&b");
        players = new ArrayList<>();
        config = game.getTeamConfig();
        //	teamSpawn = game.getTeamConfig().getLocation("Teams." + getName() + ".location." + game.getCurrent().getName() + ".spawnLocation");
    }
    
    public String getColoredName(Player p) {
        return getPrefix() + getDisplayname(p);
    }
    
    public String getDisplayname(Player p) {
        return game.getTeamConfig().getTranslated("Teams." + getName() + ".displayname", p, "&b" + getName());
    }
    
    public Location getTeamSpawn() {
        if (teamSpawn == null || teamSpawn.getWorld() == null) {
            teamSpawn = game.getTeamConfig().getLocation("Teams." + getName() + ".location." + game.getCurrent().getName() + ".spawnLocation");
        }
        return teamSpawn;
    }
    
    public void teleportToTeamSpawn(GamePlayer p) {
        p.getPlayer().teleport(getTeamSpawn());
    }
    
    @Override
    public int getAlivePlayers() {
        int a = 0;
        for (P p : players) {
            if (p.isAlive()) {
                a++;
            }
        }
        return a;
    }
    
    @Override
    public Iterator<P> iterator() {
        return players.iterator();
    }
    
    @Override
    public void decrementLives() {
        lives--;
    }
    
    @Override
    public void add(GamePlayer user) {
        if (!players.contains(user)) {
            players.add((P) user);
        }
    }
    
    @Override
    public GamePlayer get(int index) {
        return players.get(index);
    }
    
    @Override
    public void remove(GamePlayer user) {
        players.remove(user);
    }
    
    @Override
    public List<P> getAlivePlayersList() {
        return players.stream().filter(GamePlayer::isAlive).collect(Collectors.toList());
    }
    
    public int size() {
        return players.size();
    }
}
