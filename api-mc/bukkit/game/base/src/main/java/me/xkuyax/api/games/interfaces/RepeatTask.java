package me.xkuyax.api.games.interfaces;

import lombok.Getter;
import me.xkuyax.api.games.GameState;

public interface RepeatTask extends Runnable {
    
    enum Interval {
        MINUTES(1200),
        SECONDS(20),
        TICK(1);
        
        @Getter
        public int ticks;
        
        Interval(int ticks) {
            this.ticks = ticks;
        }
        
        public long toMillis() {
            return getTicks() * 50;
        }
    }
    
    enum RequiredState {
        DONT_CARE,
        END,
        FORCE_INGAME,
        GRACEPERIOD,
        INGAME,
        LOBBY;
        
        public boolean matchRequired(GameState state) {
            switch (this) {
                case DONT_CARE:
                    return true;
                case END:
                    return state == GameState.END;
                case GRACEPERIOD:
                    return state == GameState.GRACEPERIOD;
                case FORCE_INGAME:
                    return state == GameState.INGAME;
                case LOBBY:
                    return state.isLobby();
                case INGAME:
                    return state.isIngame();
                default:
                    return false;
            }
        }
    }
    
    Interval getInverval();
    
    RequiredState getRequiredGameState();
    
    int getTime();
}
