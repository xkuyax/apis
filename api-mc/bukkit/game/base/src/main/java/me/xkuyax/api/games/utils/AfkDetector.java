package me.xkuyax.api.games.utils;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class AfkDetector implements Listener {
    
    public static int ONE_MINUTE = 60000;
    private Map<String, Long> players = new ConcurrentHashMap<>();
    
    public AfkDetector() {
        Bukkit.getPluginManager().registerEvents(this, General.a());
    }
    
    @EventHandler
    public void onPlayerMoveEvent(PlayerMoveEvent e) {
        updateAfkTimer(e.getPlayer());
    }
    
    @EventHandler
    public void onPlayerChatEvent(AsyncPlayerChatEvent e) {
        updateAfkTimer(e.getPlayer());
    }
    
    @EventHandler
    public void onPlayerQuitEvent(PlayerQuitEvent e) {
        players.remove(e.getPlayer().getName());
    }
    
    public void updateAfkTimer(Player p) {
        players.put(p.getName(), System.currentTimeMillis());
    }
    
    public boolean isAfk(Player p, int ms) {
        Long l = players.get(p.getName());
        if (l != null) {
            return System.currentTimeMillis() - l >= ms;
        }
        ManageableGame game = GameApi.getCurrent();
        if (game != null) {
            GamePlayer gp = game.getPlayer(p);
            if (gp.isPlayed() || gp.isSpectator()) {
                return true;
            }
        }
        return false;
    }
    
    public boolean isAfk(Player p) {
        return isAfk(p, ONE_MINUTE);
    }
}
