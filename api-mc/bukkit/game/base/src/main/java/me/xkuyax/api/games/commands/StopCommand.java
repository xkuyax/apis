package me.xkuyax.api.games.commands;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class StopCommand extends GCommand {
    
    public StopCommand() {
        super("gstop");
    }
    
    @CmdHandler(ignoreargs = true, sender = CSender.BOTH)
    public void ignoreArgs(CommandSender p, String[] args) {
        if (hasPermission(p)) {
            ManageableGame cur = GameApi.getCurrent();
            if (!cur.isIngame()) {
                p.sendMessage("§4Sorry, but the game is not ingame...");
                return;
            }
            cur.stop();
        }
    }
    
    private boolean hasPermission(CommandSender commandSender) {
        if (commandSender.hasPermission("general.games.stop")) {
            return true;
        }
        if (commandSender instanceof Player) {
            Player p = (Player) commandSender;
            GameCommandPermissionEvent gameCommandPermissionEvent = new GameCommandPermissionEvent(false, p, GameCommandPermissionEvent.PermissionType.STOP_GAME);
            Bukkit.getServer().getPluginManager().callEvent(gameCommandPermissionEvent);
            return gameCommandPermissionEvent.isHasPermission();
        }
        return false;
    }
}
