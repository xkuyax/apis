package me.xkuyax.api.games.utils;

import org.bukkit.Location;
import org.bukkit.World;

import java.util.Iterator;

public class LineIterator implements Iterator<Location> {
    
    private Location a, b;
    private World world;
    private int x, z, y, x2, z2, longest, shortest;
    private int dx1 = 0, dz1 = 0, dx2 = 0, dz2 = 0, i = 0;
    private int numerator, heigthgain;
    
    public LineIterator(Location a, Location b, int heigthgain) {
        if (b.getBlockY() > a.getBlockY() && heigthgain > 0) {
            throw new IllegalStateException("Cant gain heigth if b.y > a.y!");
        }
        this.a = a;
        this.b = b;
        this.y = a.getBlockY();
        this.heigthgain = a.getBlockY() + heigthgain;
        this.world = a.getWorld();
        reset();
    }
    
    @Override
    public boolean hasNext() {
        return i <= longest && y < heigthgain;
    }
    
    @Override
    public Location next() {
        if (hasNext()) {
            Location loc = new Location(world, x, y, z);
            update();
            if (i > longest) {
                reset();
                y++;
            }
            return loc;
        }
        throw new IllegalStateException("Out of blocks");
    }
    
    private void reset() {
        this.x = a.getBlockX();
        this.x2 = b.getBlockX();
        this.z = a.getBlockZ();
        this.z2 = b.getBlockZ();
        preCalc();
    }
    
    private void update() {
        numerator += shortest;
        if (!(numerator < longest)) {
            numerator -= longest;
            x += dx1;
            z += dz1;
        } else {
            x += dx2;
            z += dz2;
        }
        i++;
    }
    
    private void preCalc() {
        int w = x2 - x;
        int h = z2 - z;
        if (w < 0) dx1 = -1;
        else if (w > 0) dx1 = 1;
        if (h < 0) dz1 = -1;
        else if (h > 0) dz1 = 1;
        if (w < 0) dx2 = -1;
        else if (w > 0) dx2 = 1;
        longest = Math.abs(w);
        shortest = Math.abs(h);
        if (!(longest > shortest)) {
            longest = Math.abs(h);
            shortest = Math.abs(w);
            if (h < 0) dz2 = -1;
            else if (h > 0) dz2 = 1;
            dx2 = 0;
        }
        numerator = longest >> 1;
        i = 0;
    }
}
