package me.xkuyax.api.games;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

@Data
@EqualsAndHashCode(of = "name")
public abstract class AbstractMap implements Map {
    
    protected String world;
    protected Config config;
    protected String name;
    protected String path;
    protected ManageableGame game;
    private MapMobOptions mobOptions = MapMobOptions.NONE;
    
    public AbstractMap(String name, ManageableGame g) {
        this.game = g;
        setName(name);
        setConfig(game.getMapConfig());
        setPath("Maps." + name + ".");
        setWorld(g.getMapConfig().getString("Maps." + getName() + ".world", "orient"));
    }
    
    @Override
    public World getBukkitWorld() {
        return Bukkit.getWorld(getWorld());
    }
    
    @Override
    public String getDisplayname(Player p) {
        return getGame().getMapConfig().getTranslated("Maps." + getName() + ".displayname", p, getName());
    }
    
    @Override
    public String getEngDisplayname() {
        return getGame().getMapConfig().getString("Maps." + getName() + ".displayname.eng", getName());
    }
    
    @Override
    public String getBuilders() {
        return getGame().getMapConfig().getString("Maps." + getName() + ".builders", "&aThevace - Team");
    }
    
    public int getRespawnSafeTime() {
        return getGame().getMapConfig().getInt("Maps." + getName() + ".respawnSafeTime", 3000);
    }
    
    @Override
    public void teleportPlayers() {
        if (getSpawnOptions().equals(MapSpawnOptions.SPAWN_PER_PLAYER)) {
            List<? extends GamePlayer> players = getGame().getGamePlayers();
            List<Location> locations = new ArrayList<>();
            for (String s : game.getMapConfig().getKeys("Maps." + getName() + ".location", "1", "2", "3")) {
                locations.add(game.getMapConfig().getLocation("Maps." + getName() + ".location." + s));
            }
            Collections.shuffle(players);
            Collections.shuffle(locations);
            for (int i = 0; i < players.size(); i++) {
                GamePlayer gp = players.get(i);
                Location loc = locations.get(i);
                if (loc == null || loc.getWorld() == null) {
                    System.out.println("Der Spawn der map " + getName() + " mit der id ist ungültig " + loc);
                } else {
                    gp.getPlayer().teleport(locations.get(i));
                }
            }
        } else {
            if (getSpawnOptions().equals(MapSpawnOptions.TEAM_SPAWNS)) {
                for (GamePlayer gp : game.getPlayers()) {
                    gp.getTeam().teleportToTeamSpawn(gp);
                }
            }
            onTeleport();
        }
    }
    
    public Location getRandomLocation() {
        List<? extends GamePlayer> players = getGame().getGamePlayers();
        List<Location> locations = new ArrayList<>();
        for (int i = 0; i < players.size(); i++) {
            locations.add(game.getMapConfig().getLocation("Maps." + getName() + ".location." + i));
        }
        Collections.shuffle(locations);
        return locations.get(new Random().nextInt(players.size()));
    }
    
    @Override
    public boolean isVisibleAtVoting() {
        return getGame().getMapConfig().getBoolean("Maps." + getName() + ".isVisibleAtVoting", true);
    }
    
    @Override
    public Location getSpectatorLocation() {
        return game.getMapConfig().getLocation("Maps." + getName() + ".spectatorLocation");
    }
    
    public void create() {
        getGame().createWorld(this);
    }
    
    /**
     * This method has to be implemented, if you dont use MapSpawnOptions.SPAWN_PER_PLAYER)
     */
    public void onTeleport() {
    }
    
    public void onLoad() {
    }
    
    public List<EntityType> getAllowSpawns() {
        return new ArrayList<>();
    }
}
