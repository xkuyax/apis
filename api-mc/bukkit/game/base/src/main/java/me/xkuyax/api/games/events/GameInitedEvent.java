package me.xkuyax.api.games.events;

import org.bukkit.event.HandlerList;

public class GameInitedEvent extends GameEvent {
    
    private static final HandlerList handerList = new HandlerList();
    
    public GameInitedEvent(ManageableGame game) {
        super(game);
    }
    
    @Override
    public HandlerList getHandlers() {
        return handerList;
    }
    
    public static HandlerList getHandlerList() {
        return handerList;
    }
}
