package me.xkuyax.api.games.interfaces;

import me.xkuyax.api.games.GameState;
import me.xkuyax.utils.config.BukkitConfig;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import java.util.List;
import java.util.stream.Collectors;

public interface Game extends Listener {
    
    @Deprecated
    GamePlayer getGamePlayer(String name);
    
    @Deprecated
    List<? extends GamePlayer> getGamePlayers();
    
    List<? extends GamePlayer> getPlayers();
    
    default List<? extends GamePlayer> getAlivePlayersList() {
        return getPlayers().stream().filter(GamePlayer::isAlive).collect(Collectors.toList());
    }
    
    GamePlayer getPlayer(String name);
    
    GamePlayer getPlayer(Player p);
    
    GamePlayer getPlayer(String name, boolean create);
    
    GamePlayer getPlayer(Player p, boolean create);
    
    Team getTeam(String name);
    
    Team getTeam(String name, boolean create);
    
    Team getTeam(Player p);
    
    Team getTeam(GamePlayer p);
    
    void setGameState(GameState set);
    
    GameState getGameState();
    
    boolean isIngame();
    
    boolean isLobby();
    
    boolean hasGraceperiod();
    
    GameMap getCurrent();
    
    GameMap getVotedMap();
    
    GameMap getMap(String name);
    
    GameMap getMap(String name, boolean create);
    
    List<? extends GameMap> getMaps();
    
    Location getLobbyLocation();
    
    BukkitConfig getGameConfig();
    
    BukkitConfig getMapConfig();
    
    BukkitConfig getItemConfig();
    
    BukkitConfig getTeamConfig();
    
    BukkitConfig getMessagesConfig();
    
    BukkitConfig getSettingsConfig();
    
    PerkManager getPerkManager();
    
    void register(RepeatTask task);
    
    void register(AbstractPerk perk);
    
    <T extends AbstractPerk & RepeatTask> void registerBoth(T t);
    
    void unregister(RepeatTask task);
    
    void setLobbyTask(LobbyTask task);
    
    long getGameTime();
    
    LobbyTask getLobbyTask();
    
    String getDisplayname(Player p);
    
    void setTimer(int n);
    
    List<? extends Team> getTeams();
    
    Multimap<PerkType, String> getDefaultPerks();
    
    MysqlStats getStats();
}
