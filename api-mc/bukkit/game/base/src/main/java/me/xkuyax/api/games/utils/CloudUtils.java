package me.xkuyax.api.games.utils;

import me.Bodoo.Load.NetAPI.Main.NetAPI;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class CloudUtils {
    
    public static ServerDataManager manager = ViceviceApi.getServerDataManager();
    
    public static boolean isLobby() {
        String game;
        return (game = getThisGamemode()) != null && (game.startsWith("lobby") || game.equalsIgnoreCase("lobby"));
    }
    
    public static GameExecutor findByThisSubType() {
        return findBySubType(getThisSubType());
    }
    
    public static GameExecutor findBySubType(String subtype) {
        return findBest(ge -> getManager().getGamemode(ge.getGamemode()).getSubtype().equalsIgnoreCase(subtype));
    }
    
    public static GameExecutor findByGamemode(String gamemode) {
        return findBest(ge -> ge.getGamemode().equalsIgnoreCase(gamemode));
    }
    
    public static GameExecutor findByThisGamemode() {
        return findByGamemode(getThisGamemode());
    }
    
    public static GameExecutor findBest(Predicate<GameExecutor> p) {
        getManager().getFetcher().fetchOnce();
        List<GameExecutor> possible = new ArrayList<>();
        for (ServerData sd : getManager().getServers().values()) {
            if (sd instanceof GameExecutor && sd.getBungeename().startsWith("ge")) {
                GameExecutor ge = (GameExecutor) sd;
                if (p.test(ge)) {
                    if (ge.getExecutorState() == GameExecutorState.Waiting) {
                        possible.add(ge);
                    }
                }
            }
        }
        GameExecutor best = null;
        if (possible.size() > 0) {
            for (GameExecutor ge : possible) {
                if (isFreeSlots(ge)) {
                    if (ge.getOnlineplayers() > 0) {
                        if (best == null) {
                            best = ge;
                        } else {
                            if (best.getOnlineplayers() < ge.getOnlineplayers()) {
                                best = ge;
                            }
                        }
                    }
                }
            }
        }
        if (best == null) {
            for (GameExecutor ge : possible) {
                if (isFreeSlots(ge)) {
                    return ge;
                }
            }
        }
        return best;
    }
    
    public static String getThisGroup() {
        return getManager().getGamemode(getThisGamemode()).getGroup();
    }
    
    public static String getThisSubType() {
        return getManager().getGamemode(getThisGamemode()).getSubtype();
    }
    
    public static String getThisGamemode() {
        if (NetAPI.config != null) {
            String s = NetAPI.config.getValue("gametype");
            return s;
        }
        return "unknown";
    }
    
    public static String getLobbyID() {
        return NetAPI.getServerName();
    }
    
    public static String getThisID() {
        if (NetAPI.ID != -1) {
            return "ge" + NetAPI.ID;
        }
        if (General.a().getConfig().isSet("Settings.serverid")) {
            return Utils.getString("Settings.serverid", "skyblock");
        }
        return null;
    }
    
    private static boolean isFreeSlots(GameExecutor ge) {
        return ge.getOnlineplayers() < ge.getMaxplayers();
    }
    
    private static ServerDataManager getManager() {
        if (manager == null) {
            manager = ViceviceApi.getServerDataManager();
        }
        return manager;
    }
}
