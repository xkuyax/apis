package me.xkuyax.api.games.utils;

public class EmptyException extends Exception {
    
    private static final long serialVersionUID = 663925598197936152L;
    
    @Override
    public synchronized Throwable fillInStackTrace() {
        return this;
    }
}
