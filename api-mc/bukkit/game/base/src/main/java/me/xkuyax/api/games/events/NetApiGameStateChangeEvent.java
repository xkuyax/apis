package me.xkuyax.api.games.events;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * Created by Felix on 22.05.2016.
 */
public class NetApiGameStateChangeEvent extends Event {
    
    private static final HandlerList handlerList = new HandlerList();
    @Getter
    @Setter
    private me.Bodoo.Load.NetAPI.Main.GameState gameState;
    
    public NetApiGameStateChangeEvent(me.Bodoo.Load.NetAPI.Main.GameState state) {
        this.gameState = state;
    }
    
    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }
    
    public static HandlerList getHandlerList() {
        return handlerList;
    }
}
