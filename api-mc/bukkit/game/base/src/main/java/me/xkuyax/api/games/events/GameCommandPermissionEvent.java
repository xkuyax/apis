package me.xkuyax.api.games.events;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

@AllArgsConstructor
@Getter
public class GameCommandPermissionEvent extends Event {
    
    private static final HandlerList handerList = new HandlerList();
    @Setter
    private boolean hasPermission;
    private Player player;
    private PermissionType type;
    
    @Override
    public HandlerList getHandlers() {
        return handerList;
    }
    
    public static HandlerList getHandlerList() {
        return handerList;
    }
    
    public enum PermissionType {
        TIMER,
        TROLL,
        BUILD,
        FORCE_MAP,
        STOP_GAME
    }
}
