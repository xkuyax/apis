package me.xkuyax.api.games.events;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.event.HandlerList;

public class GameGetMinimumPlayersEvent extends GameEvent {
    
    @Getter
    private static final HandlerList handlerList = new HandlerList();
    @Getter
    @Setter
    private int value;
    
    public GameGetMinimumPlayersEvent(ManageableGame game) {
        super(game);
        this.value = game.getGameConfig().getInt("Settings.minimumPlayers", 2);
    }
    
    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }
}
