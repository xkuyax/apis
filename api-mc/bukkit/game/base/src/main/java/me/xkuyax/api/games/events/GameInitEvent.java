package me.xkuyax.api.games.events;

import lombok.Getter;
import org.bukkit.event.HandlerList;

public class GameInitEvent extends GameEvent {
    
    @Getter
    private static final HandlerList handlerList = new HandlerList();
    
    public GameInitEvent(ManageableGame game) {
        super(game);
    }
    
    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }
}
