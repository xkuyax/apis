package me.xkuyax.api.games.utils;

import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public abstract class DamageListener extends SelfRegisteringListener {
    
    private ManageableGame game;
    
    public DamageListener() {
        this(GameApi.getCurrent());
    }
    
    public DamageListener(ManageableGame game) {
        super(game);
        this.game = game;
    }
    
    @EventHandler
    public final void onEntityDamageByEntityEvent(EntityDamageByEntityEvent e) {
        if (game.isIngame()) {
            if (e.getEntity() instanceof Player) {
                Player p = (Player) e.getEntity();
                Player c = null;
                Arrow w = null;
                Projectile o = null;
                if (e.getDamager() instanceof Arrow) {
                    w = (Arrow) e.getDamager();
                    if (w.getShooter() instanceof Player) {
                        c = (Player) w.getShooter();
                    }
                }
                if (e.getDamager() instanceof Projectile) {
                    o = (Projectile) e.getDamager();
                    if (o.getShooter() instanceof Player) {
                        c = (Player) o.getShooter();
                    }
                }
                if (e.getDamager() instanceof Player) {
                    c = (Player) e.getDamager();
                }
                if (c != null) {
                    if (game.isInGame(p) && game.isInGame(c)) {
                        if (!block(p, c)) {
                            game.getPlayer(p).setCooldown("lastDamage");
                            if (o != null) {
                                onPlayerProjectile(p, c, o, e);
                            }
                            if (w != null) {
                                onPlayerArrowPlayer(p, c, w, e);
                            } else {
                                onPlayerHitPlayer(p, c, e);
                            }
                        } else {
                            e.setCancelled(true);
                        }
                    }
                }
            }
        }
    }
    
    public boolean block(Player p, Player c) {
        if (!game.isSoloGame()) {
            if (game.getOptions().isBlockTeamDamage()) {
                Team pt = game.getTeam(p);
                Team ct = game.getTeam(c);
                if (pt.equals(ct)) {
                    return true;
                }
            }
        }
        if (game.getOptions().isRespawnSafeTimer()) {
            GamePlayer gp = game.getPlayer(p);
            if (gp.isRespawnSafeActive()) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Damage Event im direkten PVP
     *
     * @param p
     * @param c
     * @param e
     */
    public abstract void onPlayerHitPlayer(Player p, Player c, EntityDamageByEntityEvent e);
    
    /**
     * DamageEvent wenn p von c mit Arrow w getroffen wurde
     *
     * @param p
     * @param c
     * @param w
     * @param e
     */
    public abstract void onPlayerArrowPlayer(Player p, Player c, Arrow w, EntityDamageByEntityEvent e);
    
    public abstract void onPlayerProjectile(Player p, Player c, Projectile o, EntityDamageByEntityEvent e);
}
