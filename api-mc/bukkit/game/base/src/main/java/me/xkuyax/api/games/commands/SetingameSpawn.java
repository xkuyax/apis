package me.xkuyax.api.games.commands;

import org.bukkit.entity.Player;

public class SetingameSpawn extends GCommand {
    
    public SetingameSpawn(String name) {
        super("setingamespawn");
    }
    
    @CmdHandler(ignoreargs = true, permission = "general.games.setspawn", sender = CSender.PLAYER_ONLY)
    public void onIgnoreArgs(Player p, String[] args) {
        Game cur = GameApi.getCurrent();
        if (args.length > 1) {
            String map = args[0];
            int i = Integer.parseInt(args[1]);
            cur.getMapConfig().saveLocation(p.getLocation(), "Maps." + map + ".location." + i);
            successMessage(p);
        } else {
            p.sendMessage("§4/setingamespawn <map> <id>");
        }
    }
}
