package me.xkuyax.api.games.commands;

import org.bukkit.command.CommandSender;

public class StartCommand extends GCommand {
    
    public StartCommand() {
        super("start");
    }
    
    @CmdHandler(ignoreargs = true, sender = CSender.BOTH)
    public void onIgnoreArgs(CommandSender p, String[] args) {
        if (TimerCommand.hasPermission(p)) {
            ManageableGame curr = GameApi.getCurrent();
            if (curr.getGameState() != GameState.LOBBY) {
                p.sendMessage("§4Sorry, but the game is already ingame...");
                return;
            }
            curr.start();
            NetAPI.setLobbyTimer(0);
            successMessage(p);
        }
    }
}
