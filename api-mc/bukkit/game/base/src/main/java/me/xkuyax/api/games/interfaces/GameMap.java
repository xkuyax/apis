package me.xkuyax.api.games.interfaces;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import java.util.List;

public interface GameMap {
    
    enum MapMobOptions {
        BOTH,
        NONE,
        SPAWN_FRIENDLY,
        SPAWN_MONSTER
    }
    
    enum MapSpawnOptions {
        CUSTOM,
        SPAWN_PER_PLAYER,
        TEAM_SPAWNS
    }
    
    void setWorld(String world);
    
    String getBuilders();
    
    World getBukkitWorld();
    
    String getDisplayname(Player player);
    
    String getEngDisplayname();
    
    MapMobOptions getMobOptions();
    
    String getName();
    
    MapSpawnOptions getSpawnOptions();
    
    Location getSpectatorLocation();
    
    String getWorld();
    
    boolean isVisibleAtVoting();
    
    void setMobOptions(MapMobOptions options);
    
    void teleportPlayers();
    
    int getRespawnSafeTime();
    
    List<EntityType> getAllowSpawns();
}
