package me.xkuyax.api.games;

@SuppressWarnings("rawtypes")
public abstract class Sologame<P extends GamePlayer, M extends Map> extends GenericGame<P, M, AbstractTeam> {
    
    @Override
    public void onEnable() {
        super.onEnable();
    }
    
    @Override
    void setupGame() {
    }
    
    @Override
    public boolean checkIfEnd() {
        int alive = 0;
        for (GamePlayer gp : getGamePlayers()) {
            if (gp.isAlive()) {
                gp.updateItems();
                alive++;
            }
        }
        return alive < getMinimiumPlayersIngame();
    }
    
    @Override
    public Class<AbstractTeam> getTeamClass() {
        return AbstractTeam.class;
    }
    
    @Override
    public Config getTeamConfig() {
        throw new UnsupportedOperationException();
    }
    
    @Override
    public boolean isSoloGame() {
        return true;
    }
}
