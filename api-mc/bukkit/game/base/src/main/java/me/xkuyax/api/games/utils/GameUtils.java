package me.xkuyax.api.games.utils;

import code.aterstones.protocol.ProtocolHelper;
import lombok.AllArgsConstructor;
import lombok.Data;
import net.minecraft.server.v1_8_R3.PacketPlayOutGameStateChange;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.*;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.List;

public class GameUtils {
    
    public static void sendDeathMessage(Player p, ManageableGame game) {
        GamePlayer gp = game.getGamePlayer(p.getName());
        if (p.getKiller() == null) {
            Message.sendAll("Player %player% died in a tragic way!", "Messages.death.natural", A.a("%player%"), A.a(gp.getColoredName()), game);
        } else {
            GamePlayer gc = game.getGamePlayer(p.getKiller().getName());
            Message.sendAll("Player %player% was murderd by %killer%", "Messages.death.murder", A.a("%player%", "%killer%"), A
                    .a(gp.getColoredName(), gc.getColoredName()), game);
        }
    }
    
    public static void resetDamager(Player p) {
        p.setLastDamageCause(null);
        ((CraftPlayer) p).getHandle().killer = null;
    }
    
    public static DamageResult getLastDamager(Player p) {
        return getLastDamager(p, GameApi.getCurrent());
    }
    
    public static DamageResult getLastDamager(Player p, ManageableGame game) {
        return getLastDamager(p, game, false);
    }
    
    public static DamageResult getLastDamager2(Player p) {
        EntityDamageEvent e = p.getLastDamageCause();
        if (e != null && e.getEntity() instanceof Player) {
            if (e instanceof EntityDamageByEntityEvent) {
                EntityDamageByEntityEvent ed = (EntityDamageByEntityEvent) e;
                Entity damager = ed.getDamager();
                if (damager instanceof Player) {
                    return new DamageResult(((Player) damager), damager, false, false, false, e);
                }
                if (damager instanceof Projectile) {
                    Projectile pro = (Projectile) damager;
                    if (pro.getShooter() instanceof Player) {
                        if (damager instanceof Arrow) {
                            return new DamageResult((Player) pro.getShooter(), pro, true, true, false, e);
                        } else {
                            return new DamageResult((Player) pro.getShooter(), pro, false, true, false, e);
                        }
                    }
                }
                if (damager instanceof TNTPrimed) {
                    TNTPrimed tp = (TNTPrimed) damager;
                    if (tp.getSource() instanceof Player) {
                        return new DamageResult((Player) tp.getSource(), tp, false, false, true, e);
                    }
                }
            } else {
                if (p.getKiller() != null) {
                    return new DamageResult(p.getKiller(), p.getKiller(), false, false, false, e);
                }
            }
        }
        return null;
    }
    
    public static DamageResult getLastDamager(Player p, ManageableGame game, boolean ignore) {
        EntityDamageEvent e = p.getLastDamageCause();
        GamePlayer gp = game.getPlayer(p);
        if (e != null && e.getEntity() instanceof Player) {
            if (e instanceof EntityDamageByEntityEvent) {
                EntityDamageByEntityEvent ed = (EntityDamageByEntityEvent) e;
                Entity damager = ed.getDamager();
                if (ignore || gp.isCooldownActive("lastDamage", 10000)) {
                    if (damager instanceof Player) {
                        return new DamageResult(((Player) damager), damager, false, false, false, e);
                    }
                    if (damager instanceof Projectile) {
                        Projectile pro = (Projectile) damager;
                        if (pro.getShooter() instanceof Player) {
                            if (damager instanceof Arrow) {
                                return new DamageResult((Player) pro.getShooter(), pro, true, true, false, e);
                            } else {
                                return new DamageResult((Player) pro.getShooter(), pro, false, true, false, e);
                            }
                        }
                    }
                    if (damager instanceof TNTPrimed) {
                        TNTPrimed tp = (TNTPrimed) damager;
                        if (tp.getSource() instanceof Player) {
                            return new DamageResult((Player) tp.getSource(), tp, false, false, true, e);
                        }
                    }
                }
            } else {
                if (ignore || gp.isCooldownActive("lastDamage", 10000)) {
                    if (p.getKiller() != null) {
                        return new DamageResult(p.getKiller(), p.getKiller(), false, false, false, e);
                    }
                }
            }
        }
        return null;
    }
    
    public static void setUnmoveAble(Player p, int ticks) {
        p.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, ticks, 20));
        p.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, ticks, 200));
        p.setFoodLevel(1);
    }
    
    public static boolean isUnmoveAble(Player p) {
        return p.hasPotionEffect(PotionEffectType.SLOW) && p.hasPotionEffect(PotionEffectType.JUMP);
    }
    
    public static void dropInventory(Player p) {
        Location loc = p.getLocation();
        World world = loc.getWorld();
        List<ItemStack> a = new ArrayList<>(A.a(p.getEquipment().getArmorContents()));
        a.addAll(A.a(p.getInventory().getContents()));
        for (ItemStack is : a) {
            if (is != null && !is.getType().equals(Material.AIR)) {
                world.dropItemNaturally(loc, is);
            }
        }
    }
    
    public static void setGamemode3(Player p) {
        PacketPlayOutGameStateChange gs = new PacketPlayOutGameStateChange(2, 3);
        ProtocolHelper.sendPacket(p, gs);
    }
    
    public static void bypassHacks(Player p, int seconds) {
        //	PermissionsEx.getPermissionManager().getUser(UResolver.getUUIDFromPlayer(p)).addTimedPermission(, arg1, arg2);
    }
    
    @Data
    @AllArgsConstructor
    public static class DamageResult {
        
        private Player damager;
        private Entity source;
        private boolean arrow, projectile, tnt;
        private EntityDamageEvent event;
        
        public DamageResult(Player p, Entity s, boolean a, boolean b, boolean c) {
            this.damager = p;
            this.source = s;
            this.arrow = a;
            this.projectile = b;
            this.tnt = c;
        }
    }
}
