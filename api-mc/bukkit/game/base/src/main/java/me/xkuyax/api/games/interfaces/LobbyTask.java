package me.xkuyax.api.games.interfaces;

public interface LobbyTask extends RepeatTask {
    
    int getRemainingTime();
    
    void setRemainingTime(int n);
}
