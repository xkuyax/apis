package me.xkuyax.api.games.events;

import lombok.Getter;

public abstract class GamePlayerEvent extends GameEvent {
    
    @Getter
    private GamePlayer player;
    
    public GamePlayerEvent(ManageableGame game, GamePlayer player) {
        super(game);
        this.player = player;
    }
}
