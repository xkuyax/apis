package me.xkuyax.api.games.events;

import org.bukkit.event.HandlerList;

public class GameStopEvent extends GameEvent {
    
    private static final HandlerList handlerList = new HandlerList();
    
    public GameStopEvent(ManageableGame game) {
        super(game);
    }
    
    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }
    
    public static HandlerList getHandlerList() {
        return handlerList;
    }
}
