package me.xkuyax.api.games.commands;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class ForceMapCommand extends GCommand {
    
    public ForceMapCommand() {
        super("forcemap", new String[]{"fm"});
    }
    
    @CmdHandler(ignoreargs = true, sender = CSender.PLAYER_ONLY, runAsyc = true)
    public void forceMapCommand(Player p, String[] args) {
        GameCommandPermissionEvent event = new GameCommandPermissionEvent(p
                .hasPermission("general.games.forcemap"), p, GameCommandPermissionEvent.PermissionType.FORCE_MAP);
        Bukkit.getServer().getPluginManager().callEvent(event);
        if (event.isHasPermission()) {
            ManageableGame game = GameApi.getCurrent();
            if (game != null) {
                if (game.isLobby()) {
                    if (args.length > 0) {
                        Map map = game.getMap(args[0]);
                        if (map != null) {
                            if (!game.isForcedMap()) {
                                game.setForcedMap(map);
                                Message.send(p, "&aDu hast erfolgreich folgende Map %map% gewählt", "Messages.forceMap.success", A.a("%map%"), A
                                        .a(map.getEngDisplayname()));
                            } else {
                                if (args.length > 1) {
                                    if (args[1].equalsIgnoreCase("adminforce") && p.hasPermission("general.adminMapForce")) {
                                        game.setForcedMap(map);
                                        Message.send(p, "&aDu hast erfolgreich folgende Map %map% gewählt", "Messages.forceMap.success", A.a("%map%"), A
                                                .a(map.getEngDisplayname()));
                                    } else {
                                        Message.send(p, "&cIn der dieser Runde wurde bereits die Map bestimmt!", "Messages.forcemap.errormapalreadyforced");
                                    }
                                } else {
                                    Message.send(p, "&cIn der dieser Runde wurde bereits die Map bestimmt!", "Messages.forcemap.errormapalreadyforced");
                                }
                            }
                        } else {
                            Message.send(p, "&cLeider ist in diesem Spielmodi diese Map nicht verfügbar", "Messages.forcemap.errormapnotfound1");
                            StringBuilder sb = new StringBuilder();
                            for (Map m : game.getMaps()) {
                                sb.append("&6").append(m.getName()).append(" &8(&7").append(m.getEngDisplayname()).append("&8)&c, ");
                            }
                            String maps = sb.toString().trim();
                            maps = maps.substring(0, sb.length() - 2);
                            Message.send(p, "&cFolgende Maps sind verfügbar: %maps%", "Messages.forcemap.errormapnotfound2", A.a("%maps%"), A.a(maps));
                        }
                    } else {
                        Message.send(p, "&c/forcemap <MapName>.", "Messages.forcemap.syntax");
                    }
                } else {
                    Message.send(p, "&cDu kannst nur in der LobbyPhase die Map bestimmen!", "Messages.forcemap.erroronlylobby");
                }
            } else {
                Message.send(p, "&cLeider ist in diesem Spielmodi diese Option nicht verfügbar.", "Messages.forcemap.error");
            }
        }
        
    }
}
