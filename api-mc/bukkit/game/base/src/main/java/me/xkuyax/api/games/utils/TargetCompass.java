package me.xkuyax.api.games.utils;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@EqualsAndHashCode(of = {"target"})
@Data
public class TargetCompass {
    
    private String player, target;
    
    public TargetCompass(Player p, Player target) {
        this.player = p.getName();
        this.target = target.getName();
    }
    
    public Player getPlayer() {
        return Bukkit.getPlayer(player);
    }
    
    public Player getTarget() {
        return Bukkit.getPlayer(target);
    }
    
    public void setTarget(Player target) {
        this.target = target.getName();
        unregister();
        register();
    }
    
    private void register() {
        TargetCompassModule.a().register(this);
    }
    
    public void unregister() {
        TargetCompassModule.a().compasses.remove(this);
    }
    
    public static class TargetCompassModule implements RepeatTask {
        
        private static TargetCompassModule instance;
        private List<TargetCompass> compasses;
        private Game game;
        
        public TargetCompassModule() {
            compasses = new ArrayList<>();
            game = GameApi.getCurrent();
            GameApi.getCurrent().register(this);
        }
        
        @Override
        public void run() {
            for (Iterator<TargetCompass> iterator = compasses.iterator(); iterator.hasNext(); ) {
                TargetCompass tc = iterator.next();
                Player p = tc.getPlayer();
                Player t = tc.getTarget();
                if (p == null || t == null) {
                    iterator.remove();
                    continue;
                }
                run(tc);
            }
        }
        
        private void run(TargetCompass tc) {
            Player p = tc.getPlayer();
            Player t = tc.getTarget();
            int slot = ItemUtils.getFirstSlot(p.getInventory(), "compass");
            if (slot < 8) {
                if (!p.getLocation().getWorld().equals(t.getLocation().getWorld())) {
                    return;
                }
                double d = p.getLocation().distance(t.getLocation());
                List<String> r = A.a("%target%", "%blocks%");
                List<String> o = A.a(game.getGamePlayer(tc.target).getColoredName(), Math.round(d) + "");
                if (slot == -1) {
                    ItemBuilder compass = game.getItemConfig().getItemBuilder("Items.compass", p);
                    compass.setType(Material.COMPASS);
                    compass.format(r, o).formatDisplayname(r, o);
                    ItemStack is = InventoryModule.setHiddenString(compass.build(), "compass");
                    p.getInventory().setItem(7, is);
                    ItemUtils.updateSlot(7, is, p);
                } else {
                    String dname = game.getItemConfig().getTranslated("Items.compass.dname", p, "");
                    List<String> lore = game.getItemConfig().getTranslatedList("Items.compass.lore", p, "");
                    ItemStack is = ItemUtils.setLoreAndDisplay(p.getInventory().getItem(slot), lore, dname);
                    ItemUtils.reformat(InventoryModule.setHiddenString(is, "compass"), r, o);
                    p.getInventory().setItem(slot, is);
                    ItemUtils.updateSlot(slot, is, p);
                }
                String s = Utils.formatString(null, game.getMessagesConfig().getTranslated("Messages.targetActionbar", p, "Your target %target% is %blocks% away"), r, o);
                Utils.sendActionBar(p, s);
                p.setCompassTarget(t.getLocation());
            }
        }
        
        public void register(TargetCompass t) {
            compasses.add(t);
            run(t);
        }
        
        @Override
        public Interval getInverval() {
            return Interval.SECONDS;
        }
        
        @Override
        public RequiredState getRequiredGameState() {
            return RequiredState.INGAME;
        }
        
        @Override
        public int getTime() {
            return 1;
        }
        
        public static TargetCompassModule a() {
            if (instance == null) {
                instance = new TargetCompassModule();
            }
            return instance;
        }
    }
}
