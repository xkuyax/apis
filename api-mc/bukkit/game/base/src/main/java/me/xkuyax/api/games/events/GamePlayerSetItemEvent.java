package me.xkuyax.api.games.events;

import lombok.Getter;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

@Getter
public class GamePlayerSetItemEvent extends PlayerEvent {
    
    @Getter
    private static final HandlerList handlerList = new HandlerList();
    private ManageableGame game;
    private GamePlayer gamePlayer;
    private ItemType type;
    
    public GamePlayerSetItemEvent(GamePlayer gp, ItemType type) {
        super(gp.getPlayer());
        this.game = gp.getGame();
        this.gamePlayer = gp;
        this.type = type;
    }
    
    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }
    
    public enum ItemType {
        LOBBY,
        INGAME,
        SPECTATOR,
        ENDITEMS
    }
}
