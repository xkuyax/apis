package me.xkuyax.api.games;

import lombok.Getter;
import lombok.Setter;
import me.Bodoo.Load.NetAPI.Main.NetAPI;
import me.xkuyax.api.games.events.LobbyTickEvent;
import me.xkuyax.api.games.interfaces.LobbyTask;
import me.xkuyax.api.games.interfaces.ManageableGame;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

@Getter
@Setter
public class AbstractLobbyTask implements LobbyTask {
    
    private ManageableGame game;
    private int remainingTime, lastPush;
    
    public AbstractLobbyTask(ManageableGame game) {
        this.game = game;
        remainingTime = game.getMaximumLobbyTime();
        NetAPI.setLobbyTimer(lastPush = 0);
    }
    
    @Override
    public void run() {
        if (enoughPlayersOnline()) {
            if (remainingTime == game.getMaximumLobbyTime()) {
                NetAPI.setLobbyTimer(lastPush = remainingTime);
            }
            remainingTime--;
            if (remainingTime < 0) {
                game.start();
            } else {
                game.onLobbyTick(remainingTime);
            }
        } else {
            remainingTime = game.getMaximumLobbyTime();
            game.onLobbyReset();
            if (lastPush != 0) {
                NetAPI.setLobbyTimer(lastPush = 0);
            }
        }
        Bukkit.getPluginManager().callEvent(new LobbyTickEvent(game, remainingTime));
        updatePlayers();
    }
    
    @Override
    public Interval getInverval() {
        return Interval.SECONDS;
    }
    
    @Override
    public RequiredState getRequiredGameState() {
        return RequiredState.LOBBY;
    }
    
    @Override
    public int getTime() {
        return 1;
    }
    
    private void updatePlayers() {
        float exp = (float) ((double) remainingTime / (double) game.getMaximumLobbyTime());
        for (Player p : Bukkit.getOnlinePlayers()) {
            p.setExp(exp > 1f ? 1f : exp);
            p.setLevel(remainingTime);
            game.getPlayer(p).updateScoreboard();
        }
    }
    
    private boolean enoughPlayersOnline() {
        return Bukkit.getOnlinePlayers().size() >= game.getMinimumPlayers();
    }
}
