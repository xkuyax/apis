package me.xkuyax.api.games.utils;

import com.comphenix.packetwrapper.WrapperPlayServerSetSlot;
import lombok.Data;
import net.minecraft.server.v1_8_R3.NBTBase;
import net.minecraft.server.v1_8_R3.NBTTagByte;
import net.minecraft.server.v1_8_R3.NBTTagCompound;
import net.minecraft.server.v1_8_R3.NBTTagList;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class ItemUtils {
    
    public static String getHiddenString(ItemStack is) {
        return InventoryModule.getHiddenString(is);
    }
    
    public static ItemStack setHiddenString(ItemStack is, String name) {
        return InventoryModule.setHiddenString(is, name);
    }
    
    public static int getFreeSlots(Inventory inv) {
        ItemStack[] a = inv.getContents();
        int c = 0;
        for (ItemStack ic : a) {
            if (ic == null || ic.getType() == Material.AIR) {
                c++;
            }
        }
        return c;
    }
    
    public static int getFirstSlot(Inventory inv, ItemStack is) {
        ItemStack[] a = inv.getContents();
        for (int i = 0; i < a.length; i++) {
            ItemStack ic = a[i];
            if (ic != null && is.equals(ic)) {
                return i;
            }
        }
        return -1;
    }
    
    public static int getFirstSlot(Inventory inv, String unique) {
        ItemStack[] a = inv.getContents();
        for (int i = 0; i < a.length; i++) {
            ItemStack ic = a[i];
            if (ic != null) {
                String n = InventoryModule.getHiddenString(ic);
                if (n != null && n.equalsIgnoreCase(unique)) {
                    return i;
                }
            }
        }
        return -1;
    }
    
    public static ItemStack setLoreAndDisplay(ItemStack is, List<String> lore, String dname) {
        ItemMeta im = is.getItemMeta();
        im.setLore(lore);
        im.setDisplayName(dname);
        is.setItemMeta(im);
        return is;
    }
    
    public static ItemStack addToLore(ItemStack is, String s, List<String> r, List<String> o) {
        ItemMeta im = is.getItemMeta();
        List<String> lore = im.hasLore() ? im.getLore() : new ArrayList<>();
        lore.add(Utils.formatString(s, r, o));
        im.setLore(lore);
        is.setItemMeta(im);
        return is;
    }
    
    public static ItemStack removeLore(ItemStack is) {
        ItemMeta im = is.getItemMeta();
        im.setLore(new ArrayList<>());
        is.setItemMeta(im);
        return is;
    }
    
    public static ItemStack reformat(ItemStack is, List<String> r, List<String> o) {
        ItemMeta im = is.getItemMeta();
        List<String> lore = new ArrayList<>();
        for (String s : im.getLore()) {
            lore.add(Utils.formatString(s, r, o));
        }
        im.setLore(lore);
        im.setDisplayName(Utils.formatString(im.getDisplayName(), r, o));
        is.setItemMeta(im);
        return is;
    }
    
    public static ItemStack setSkullandName(ItemStack is, String name) {
        is.setType(Material.SKULL_ITEM);
        is.setDurability((short) 3);
        SkullMeta sm = (SkullMeta) is.getItemMeta();
        sm.setOwner(name);
        is.setItemMeta(sm);
        return is;
    }
    
    public static int count(Inventory inv, String hidden) {
        int r = 0;
        for (ItemStack is : inv.getContents()) {
            if (is != null) if (hidden.equalsIgnoreCase(InventoryModule.getHiddenString(is))) r += is.getAmount();
        }
        return r;
    }
    
    public static int count(Player p, Material material) {
        int r = 0;
        Inventory inv = p.getInventory();
        for (ItemStack is : inv.getContents()) {
            if (is == null && material == Material.AIR) {
                r += 1;
            }
            if (is != null && is.getType().equals(material)) r += is.getAmount();
        }
        return r;
    }
    
    public static int count(Player p, Predicate<ItemStack> match) {
        int r = 0;
        Inventory inv = p.getInventory();
        for (ItemStack is : inv.getContents()) {
            if (is != null && match.test(is)) {
                r += is.getAmount();
            }
        }
        return r;
    }
    
    public static int count(Player p, String hidden) {
        return count(p.getInventory(), hidden);
    }
    
    public static void remove(Player p, int toRemove, Material material) {
        Inventory inv = p.getInventory();
        int removed = 0;
        for (int i = 0; i < inv.getSize(); i++) {
            ItemStack is = inv.getItem(i);
            if (is == null || !is.getType().equals(material)) {
                continue;
            }
            int a = is.getAmount();
            while (a > 0 && removed < toRemove) {
                removed++;
                a--;
            }
            if (a == 0) {
                inv.clear(i);
            } else {
                is.setAmount(a);
            }
        }
        p.updateInventory();
    }
    
    public static void remove(Player p, int toRemove, Predicate<ItemStack> match) {
        Inventory inv = p.getInventory();
        int removed = 0;
        for (int i = 0; i < inv.getSize(); i++) {
            ItemStack is = inv.getItem(i);
            if (is == null || !match.test(is)) {
                continue;
            }
            int a = is.getAmount();
            while (a > 0 && removed < toRemove) {
                removed++;
                a--;
            }
            if (a == 0) {
                inv.clear(i);
            } else {
                is.setAmount(a);
            }
        }
        p.updateInventory();
    }
    
    public static void remove(Player p, String hidden) {
        Inventory inv = p.getInventory();
        for (ItemStack is : inv) {
            String s = is != null && is.getType() != Material.AIR ? InventoryModule.getHiddenString(is) : null;
            if (s != null && s.equalsIgnoreCase(hidden)) {
                inv.remove(is);
            }
        }
    }
    
    public static void remove(Player p, String hidden, int toRemove) {
        Inventory inv = p.getInventory();
        int removed = 0;
        for (int i = 0; i < inv.getSize(); i++) {
            ItemStack is = inv.getItem(i);
            if (is == null) {
                continue;
            }
            String h = getHiddenString(is);
            if (hidden.equals(h)) {
                int a = is.getAmount();
                while (a > 0 && removed < toRemove) {
                    removed++;
                    a--;
                }
                if (a == 0) {
                    inv.clear(i);
                } else {
                    is.setAmount(a);
                }
            }
        }
        p.updateInventory();
    }
    
    public static void updateSlot(int i, ItemStack is, Player p) {
        WrapperPlayServerSetSlot wps = new WrapperPlayServerSetSlot();
        wps.setSlot((short) (i + 36));
        wps.setSlotData(is);
        wps.setWindowId((byte) 0);
        wps.sendPacket(p);
    }
    
    public static ItemStack setNBTTags(ItemStack item, NBTTagPair... values) {
        net.minecraft.server.v1_8_R3.ItemStack nmsStack = CraftItemStack.asNMSCopy(item);
        NBTTagCompound tag = null;
        if (!nmsStack.hasTag()) {
            tag = new NBTTagCompound();
            nmsStack.setTag(tag);
        }
        if (tag == null) {
            tag = nmsStack.getTag();
        }
        NBTTagCompound finalTag = tag;
        Arrays.stream(values).forEach(nbtTagPair -> finalTag.set(nbtTagPair.getKey(), nbtTagPair.getValue()));
        nmsStack.setTag(tag);
        return CraftItemStack.asCraftMirror(nmsStack);
    }
    
    public static ItemStack addGlow(ItemStack item) {
        return setNBTTags(item, new NBTTagPair("ench", new NBTTagList()));
    }
    
    public static ItemStack unbreakable(ItemStack is) {
        return setNBTTags(is, new NBTTagPair("Unbreakable", new NBTTagByte((byte) 1)));
    }
    
    public static ItemStack removeGlow(ItemStack is) {
        net.minecraft.server.v1_8_R3.ItemStack nmsStack = CraftItemStack.asNMSCopy(is);
        NBTTagCompound tag = null;
        if (!nmsStack.hasTag()) {
            tag = new NBTTagCompound();
            nmsStack.setTag(tag);
        }
        if (tag == null) tag = nmsStack.getTag();
        tag.remove("Unbreakable");
        nmsStack.setTag(tag);
        return CraftItemStack.asCraftMirror(nmsStack);
    }
    
    public static ItemStack first(Inventory inv, Predicate<ItemStack> match) {
        for (ItemStack is : inv.getContents()) {
            if (is != null && match.test(is)) {
                return is;
            }
        }
        return null;
    }
    
    public static ItemStack first(Inventory inv, Material mat) {
        int slot = inv.first(mat);
        if (slot == -1) {
            return null;
        }
        return inv.getItem(slot);
    }
    
    @Data
    public static class NBTTagPair {
        
        private final String key;
        private final NBTBase value;
        
    }
    
}
