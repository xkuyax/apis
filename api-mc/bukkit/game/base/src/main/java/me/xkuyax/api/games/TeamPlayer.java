package me.xkuyax.api.games;

public class TeamPlayer<T extends Team> extends AbstractPlayer {
    
    public TeamPlayer(String name, ManageableGame game) {
        super(name, game);
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public T getTeam() {
        return (T) super.getTeam();
    }
}
