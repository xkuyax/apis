package me.xkuyax.api.games.utils;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.bukkit.Location;

import java.util.Iterator;

@NoArgsConstructor
public class StairIterator implements Iterator<Location> {
    
    private Location start;
    private int heigth;
    private Side side;
    private int c;
    @Getter
    private int i;
    private int y;
    
    public StairIterator(Location start, int heigth, Side side) {
        this.start = start;
        this.heigth = heigth;
        this.side = side;
        i = heigth;
        y = start.getBlockY();
    }
    
    @Override
    public boolean hasNext() {
        return c < heigth;
    }
    
    @Override
    public Location next() {
        Location b = BlockUtils.add(side, i, start);
        b.setY(y);
        update();
        return b;
    }
    
    private void update() {
        i--;
        if (i == c) {
            i = heigth;
            c++;
            y++;
        }
    }
}
