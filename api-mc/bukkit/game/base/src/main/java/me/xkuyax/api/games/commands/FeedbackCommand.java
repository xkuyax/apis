package me.xkuyax.api.games.commands;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ClickEvent.Action;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerEditBookEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.BookMeta;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public class FeedbackCommand extends GCommand implements Listener {

    private static Gson gson = new GsonBuilder().create();
    private static Config mysqlConfig = new Config(General.a(), "me.xkuyax.utils.mysql-achievable.yml");
    private static Config config = new Config(General.a(), "feedback.yml");
    private AchievSQLBackend achievSQLBackend;
    private PooledMysqlConnection mysql;
    private Map<String, AtomicLong> timeOut = new HashMap<>(0);
    private Map<String, FeedbackData> feedbackDataMap = new HashMap<>();

    public FeedbackCommand() {
        super("feedback", new String[]{"fb"});
        mysql = PooledMysqlConnection.get(mysqlConfig, "Mysql");
        achievSQLBackend = new AchievSQLBackend(mysql);
        Bukkit.getPluginManager().registerEvents(this, a);
    }

    @CmdHandler(ignoreargs = true, sender = CSender.PLAYER_ONLY, runAsyc = true)
    public void onIgnoreArgs(Player p, String[] args) {
        ManageableGame game = GameApi.getCurrent(p);
        if (game != null) {
            GamePlayer gp = game.getPlayer(p);
            if (gp != null && (gp.isSpectator() || game.getState() == GameState.END) && gp.isPlayed()) {
                me.vicevice.general.api.games.interfaces.Map map = game.getCurrent();
                if (map != null) {
                    int gameId = achievSQLBackend.getGameID(game.getName());
                    int mapid = achievSQLBackend.getMapID(gameId, map.getName(), map.getEngDisplayname());
                    int playerid = achievSQLBackend.getPlayerID(UResolver.getStringUUIDFromPlayer(p.getName()));
                    if (!hasSubmitted(playerid, gameId, mapid)) {
                        Bukkit.getScheduler().runTask(General.a(), () -> p.openInventory(new FeedbackInventory(p).build()));
                    } else {
                        Message.send0(p, "Du hast bereits abgestimmt!", "Feedback.alreadySubmitted", false, config, null, null);
                    }
                } else {
                    Message.send0(p, "Es ist derzeit keine Map ausgewählt!", "Feedback.noCurrentMap", false, config, null, null);
                }
            } else {
                Message.send0(p, "Du kannst nur nachdem du eine Runde gespielt hast als spectator Feedback abgeben!", "Feedback.notSpectator", false, config, null, null);
            }
        } else {
            Message.send0(p, "Du kannst in diesem Spielmodus kein Feedback abgeben", "Feedback.notGameApi", false, config, null, null);
        }
    }

    private void submit(Player p, FeedbackData feedbackData) {
        ManageableGame game = GameApi.getCurrent(p);
        me.vicevice.general.api.games.interfaces.Map map = game.getCurrent();
        int gameId = achievSQLBackend.getGameID(game.getName());
        int mapid = achievSQLBackend.getMapID(gameId, map.getName(), map.getEngDisplayname());
        int playerid = achievSQLBackend.getPlayerID(UResolver.getStringUUIDFromPlayer(p.getName()));
        mysql.prepare("insert into `feedback` (`playerid`,`gameid`,`mapid`,`overall`,`look`,`pvp`,`creativity`,`userFeedback`) values (?,?,?,?,?,?,?,?)", ps -> {
            ps.setInt(1, playerid);
            ps.setInt(2, gameId);
            ps.setInt(3, mapid);
            ps.setInt(4, feedbackData.getOverall());
            ps.setInt(5, feedbackData.getLook());
            ps.setInt(6, feedbackData.getPvp());
            ps.setInt(7, feedbackData.getCreativity());
            ps.setString(8, feedbackData.getUserFeedback());
        });
        Bukkit.getScheduler().runTask(General.a(), () -> submitSuccess(p));
    }

    private void submitSuccess(Player p) {
        Message.send0(p, "Feedback erfolgreich abgegeben, danke :)", "Feedback.submitSucess1", false, config, null, null);
        ViceviceApi.addOnlineSimpleCookies(p, 20);
        Message.send0(p, "Da du feedback abgegeben hast erhälst du 20 cookies ", "Feedback.submitSucess", false, config, null, null);
    }

    private boolean hasSubmitted(Player p) {
        ManageableGame game = GameApi.getCurrent(p);
        me.vicevice.general.api.games.interfaces.Map map = game.getCurrent();
        int gameId = achievSQLBackend.getGameID(game.getName());
        int mapid = achievSQLBackend.getMapID(gameId, map.getName(), map.getEngDisplayname());
        int playerid = achievSQLBackend.getPlayerID(UResolver.getStringUUIDFromPlayer(p.getName()));
        return hasSubmitted(playerid, gameId, mapid);
    }

    private boolean hasSubmitted(int playerid, int gameid, int mapid) {
        return mysql.hasEntries("select `overall` from `feedback` where `gameid` = ? and `mapid` = ? and `playerid` = ?", ps -> {
            ps.setInt(1, gameid);
            ps.setInt(2, mapid);
            ps.setInt(3, playerid);
        });
    }

    @EventHandler
    public void onGameStoppedEvent(GameStoppedEvent e) {
        e.getGame().getPlayers().forEach(this::test);
    }

    @EventHandler
    public void onGamePlayerSetItemEvent(GamePlayerSetItemEvent e) {
        if (e.getType() == ItemType.SPECTATOR) {
            if (e.getGame().getState() != GameState.END) {
                test(e.getGamePlayer());
            }
        }
    }

    private void test(GamePlayer gp) {
        if (gp.isPlayed()) {
            Player p = gp.getPlayer();
            Bukkit.getScheduler().runTaskAsynchronously(General.a(), () -> {
                if (!hasSubmitted(p)) {
                    Message.send0(p, "NEU: Gib feedback zur gespielten Map ab!", "Feedback.info", false, config, null, null, new JsonMessage2()
                            .setClick(new ClickEvent(Action.RUN_COMMAND, "/feedback")).setHover(config.getHoverEvent("Feedback.hover", "Feedback abgeben")));
                }
            });
        }
    }

    @EventHandler
    public void onGamePlayerShouldKickEvent(GamePlayerShouldKickEvent e) {
        GamePlayer gp = e.getPlayer();
        Player p = gp.getPlayer();
        InventoryView inventoryView = p.getOpenInventory();
        if (inventoryView.getTopInventory() != null) {
            Inventory inventory = inventoryView.getTopInventory();
            if (inventory.getType() == InventoryType.CHEST) {
                e.setKick(false);
            }
        }
        if (timeOut.containsKey(p.getName())) {
            e.setKick(SysTimeUtils.isCooldownExpired(timeOut.get(p.getName()).get(), 600000));
        }
        System.out.println(e.isKick());
    }

    @EventHandler
    public void onBookEditEvent(PlayerEditBookEvent e) {
        BookMeta bm = e.getNewBookMeta();
        Player p = e.getPlayer();
        FeedbackData fd = feedbackDataMap.get(p.getName());
        if (fd != null) {
            fd.setUserFeedback(gson.toJson(new BookData(bm.getAuthor(), bm.getTitle(), bm.getPages())));
            p.setItemInHand(null);
            Message.send0(p, "Erfolgreich dein Feedback hinzugefügt, /Feedback zum abgeben", "Feedback.advancedSuccess", false, config, null, null);
        }
    }

    @Data
    @AllArgsConstructor
    private static class BookData {

        private String author;
        private String title;
        private List<String> pages;

    }

    private class FeedbackInventory extends SingleInventory {

        private FeedbackData feedbackData;

        public FeedbackInventory(Player player) {
            super(player, config, "Inventory");
            setDynamic(true);
            feedbackData = feedbackDataMap.computeIfAbsent(player.getName(), (p) -> new FeedbackData());
            updateInventory();
        }

        @Override
        public void updateInventory() {
            clear();
            Player p = getOwner();
            System.out.println(feedbackData);
            for (FeedbackType feedbackType : FeedbackType.values()) {
                String s = feedbackType.name();
                set(config, "Items." + s + ".display", p, new PageItemStack());
                int slot = config.getInt("Slots." + s, 0);
                for (int i = 1; i <= 5; i++) {
                    final int c = i;
                    ItemStack is = config.getItemStack("Items." + s + ".vote" + i);
                    is = c == feedbackType.getGetter().get(feedbackData) ? ItemUtils.addGlow(is) : is;
                    set(slot + i, is, e -> {
                        feedbackType.getSetter().set(feedbackData, c);
                        refresh();
                    });
                }
            }
            set(config, "Items.extended", p, e -> {
                timeOut.put(p.getName(), new AtomicLong(System.currentTimeMillis()));
                p.closeInventory();
                if (ItemUtils.count(p, Material.BOOK_AND_QUILL) <= 0) {
                    ItemStack book = new ItemStack(Material.BOOK_AND_QUILL);
                    p.getInventory().addItem(book);
                    Message.send0(p, "Schreibe dein Feedback in das Book and quill!!", "Feedback.writeInfo", false, config, null, null);
                }
            });
            set(config, "Items.submit", p, e -> {
                if (feedbackData.getLook() != -1 && feedbackData.getPvp() != -1 && feedbackData.getCreativity() != -1 && feedbackData.getOverall() != -1) {
                    p.closeInventory();
                    Bukkit.getScheduler().runTaskAsynchronously(General.a(), () -> submit(p, feedbackData));
                } else {
                    Message.send0(p, "Du musst alle 4 Sachen bewerten!", "Feedback.notAll", false, config, null, null);
                }
            });
            fill(config, "Items.fill");
        }
    }

    @RequiredArgsConstructor
    @Getter
    private enum FeedbackType {

        LOOK(FeedbackData::setLook, FeedbackData::getLook),
        PVP(FeedbackData::setPvp, FeedbackData::getPvp),
        CREATIVITY(FeedbackData::setCreativity, FeedbackData::getCreativity),
        OVERALL(FeedbackData::setOverall, FeedbackData::getOverall);

        private final Setter setter;
        private final FeedBackGetter getter;

    }

    private interface FeedBackGetter {

        int get(FeedbackData fd);

    }

    private interface Setter {

        void set(FeedbackData fd, int i);

    }

    @Data
    private static class FeedbackData {

        private int look = -1;
        private int pvp = -1;
        private int creativity = -1;
        private int overall = -1;
        private String userFeedback = "NULL";
    }
}
