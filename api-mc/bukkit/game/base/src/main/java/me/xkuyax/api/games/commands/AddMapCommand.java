package me.xkuyax.api.games.commands;

import org.bukkit.entity.Player;

public class AddMapCommand extends GCommand {
    
    public AddMapCommand() {
        super("addmap");
    }
    
    @CmdHandler(ignoreargs = true, permission = "general.games.setup", sender = CSender.PLAYER_ONLY)
    public void onIgnoreArgs(Player p, String[] args) {
        if (args.length == 0) {
            p.sendMessage("§b/setup mapname worldname");
            return;
        }
        ManageableGame game = GameApi.getCurrent();
        game.getMapConfig().set("Maps." + args[0] + ".world", args[1]);
        successMessage(p);
    }
}
