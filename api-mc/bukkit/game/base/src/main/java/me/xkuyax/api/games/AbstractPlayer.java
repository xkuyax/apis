package me.xkuyax.api.games;

import code.aterstones.nickapi.NickAPI;
import lombok.*;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.potion.PotionEffect;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Data
@EqualsAndHashCode(of = "name", callSuper = false)
@ToString(callSuper = true, exclude = "team")
public class AbstractPlayer extends AbstractPerkUser implements GamePlayer {

    private static HotbarItems hotbar = HotbarItems.a();
    private boolean alive = true, build = false, played = false, firstSpec = true;
    private Map<String, Long> cooldowns = new HashMap<>();
    protected ManageableGame game;
    private BScoreboard lobbyScoreboard;
    @Setter(value = AccessLevel.PRIVATE)
    private String name;
    private String nickName;
    private Player player;
    private Team team;

    public AbstractPlayer(String name, ManageableGame game) {
        super(name, game);
        setName(name);
        this.game = game;
        this.player = Bukkit.getPlayer(name);
        this.nickName = getPlayer().getDisplayName();
    }

    @Override
    public void autoRespawn(int seconds) {
        Bukkit.getScheduler().runTaskLater(General.a(), () -> getPlayer().spigot().respawn(), 20 * seconds);
    }

    @Override
    public boolean canBuild() {
        return build;
    }

    @Override
    public String getColoredName() {
        return StringFormatting.simpleColorFormat(getPrefix() + getCuttedDisplayName());
    }

    public String getCuttedDisplayName() {
        String dname = nickName == null ? nickName = getPlayer().getDisplayName() : nickName;
        return dname.length() > 16 ? dname.substring(0, 16) : dname;
    }

    @Override
    public int getPoints() {
        return 0;
    }

    @Override
    public String getPrefix() {
        return isSpectator() ? game.getGameConfig().getString("Settings.specprefix", "§7") : game.isSoloGame() ? Utils.getPrefix(getUser()) : getTeam().getPrefix();
    }

    @Override
    public String getSelfPrefix() {
        return getPrefix();
    }

    @Override
    public String getSuffix() {
        return "";
    }

    @Override
    public User getUser() {
        return ApiHandler.a().getUser(name, false);
    }

    @Override
    public boolean isCooldownActive(String cooldown, long time) {
        return System.currentTimeMillis() - getCooldowns().getOrDefault(cooldown, 0L) <= time;
    }

    @Override
    public boolean isCooldownExpired(String cooldown, long time) {
        return !isCooldownActive(cooldown, time);
    }

    @Override
    public boolean isSpectator() {
        return !isAlive();
    }

    public void onLoad() {
    }

    public void onSetIngameItems() {
    }

    public void onSetLobbyItems() {
    }

    public void onSetSpectator() {
    }

    public void onSetStopItems() {
    }

    public void onSetGracePeriodEndItems() {
    }

    @Override
    public void removeCooldown(String cooldown) {
        setCooldown(cooldown, 0);
    }

    @Override
    public void setCooldown(String cooldown) {
        setCooldown(cooldown, System.currentTimeMillis());
    }

    @Override
    public void setCooldown(String cooldown, long time) {
        getCooldowns().put(cooldown, time);
    }

    @Override
    public void setIngameItems() {
        Player p = getPlayer();
        p.spigot().setCollidesWithEntities(true);
        clearEffects(p);
        NickAPI.refreshPlayer(p);
        if (game.getOptions().isClearInventoryOnStart()) {
            p.getInventory().clear();
            p.updateInventory();
            p.getEquipment().setArmorContents(null);
        }
        if (game.getOptions().isSetNewScoreboardOnStart()) {
            p.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
        }
        p.setLevel(0);
        p.setExp(0f);
        p.setGameMode(game.getOptions().getIngameMode());
        onSetIngameItems();
        Bukkit.getPluginManager().callEvent(new GamePlayerSetItemEvent(this, ItemType.INGAME));
    }

    @Override
    public void setLobbyItems() {
        final Player p = getPlayer();
        clearEffects(p);
        p.spigot().setCollidesWithEntities(true);
        NickAPI.refreshPlayer(p);
        p.getInventory().clear();
        p.updateInventory();
        p.setFoodLevel(20);
        p.setHealth(20.0);
        p.setHealthScale(20.0);
        p.setHealthScaled(false);
        p.getEquipment().setArmorContents(null);
        if (!game.isDuels()) {
            p.teleport(GameApi.getCurrent().getLobbyLocation());
        }
        p.setGameMode(GameMode.ADVENTURE);
        if (game.getGameConfig().getBoolean("Settings.mapVote", true)) hotbar.add(player, new MapVoteItem(), game.getItemConfig(), "Items.lobby.vote");
        if (game.getOptions().isPerks()) {
            hotbar.add(player, new PerkSelector(), game.getItemConfig(), "Items.lobby.perk");
        }
        if (!game.isSoloGame() && !game.isDuels()) {
            hotbar.add(player, new TeamChange(), game.getItemConfig(), "Items.lobby.teamChange");
        }
        for (GamePlayer gc : game.getPlayers()) {
            Player c = gc.getPlayer();
            c.showPlayer(p);
            p.showPlayer(c);
        }
        updateScoreboard();
        lobbyScoreboard.setBoard(p);
        if (game.getGameConfig().getBoolean("Settings.enableachievement", false)) {
            hotbar.add(p, (HotBarClick) e -> p.openInventory(new QuestInventory(p).build()), game.getItemConfig(), "Items.achievements");
        }
        onSetLobbyItems();
        Bukkit.getPluginManager().callEvent(new GamePlayerSetItemEvent(this, ItemType.LOBBY));
    }

    public void setGracePeriodEndItems() {
        onSetGracePeriodEndItems();
    }

    @Override
    public void setRespawnSafeActive() {
        setCooldown("respawnTimer");
    }

    public boolean isRespawnSafeActive() {
        return isCooldownActive("respawnTimer", game.getCurrent().getRespawnSafeTime());
    }

    @Override
    public void setSpectator() {
        this.alive = false;
        if (!game.isDuels()) {
            final Player p = getPlayer();
            clearEffects(p);
            if (firstSpec) {
                Message.send(p, "You are now spectator!", "Messages.youAreSpectator");
                firstSpec = false;
            }
            p.setAllowFlight(true);
            p.teleport(game.getCurrent().getSpectatorLocation());
            p.spigot().setCollidesWithEntities(false);
            p.getInventory().clear();
            HotbarItems hotbar = HotbarItems.a();
            hotbar.add(p, new ViewAlivePlayers(), game.getItemConfig(), "Items.spectatorCompass");
            for (GamePlayer ac : game.getPlayers()) {
                ac.getPlayer().hidePlayer(p);
                if (ac.isSpectator()) {
                    p.hidePlayer(ac.getPlayer());
                } else {
                    p.showPlayer(ac.getPlayer());
                }
            }
            if (team != null) {
                team.remove(this);
            }
            onSetSpectator();
        }
        Bukkit.getPluginManager().callEvent(new GamePlayerSetItemEvent(this, ItemType.SPECTATOR));
    }

    @Override
    public void setStopItems() {
        Player p = getPlayer();
        NickAPI.refreshPlayer(p);
        p.getInventory().clear();
        p.updateInventory();
        p.teleport(GameApi.getCurrent().getLobbyLocation());
        p.setGameMode(GameMode.ADVENTURE);
        onSetStopItems();
        Bukkit.getPluginManager().callEvent(new GamePlayerSetItemEvent(this, ItemType.ENDITEMS));
    }

    public void setTeam(Team team) {
        if (this.team != null) {
            this.team.remove(this);
        }
        this.team = team;
        this.team.add(this);
    }

    @Override
    public void unload() {
    }

    @Override
    public void updateItems() {
    }

    @Override
    public void updateScoreboard() {
        Config config = game.getMessagesConfig();
        Player p = getPlayer();
        if (lobbyScoreboard == null) {
            lobbyScoreboard = new BScoreboard("lobby", Utils
                    .formatString(p, config.getTranslated("Scoreboard.objectiveDisplay", p, "%gamemode%"), A.a("%gamemode%"), A.a(game.getDisplayname(p))));
        }
        lobbyScoreboard.setText(1, Utils.formatString(config.getTranslated("Scoreboard.text.1", p, "Spieler auf dem Server: %online%"), A.a("%online%"), A
                .a(Bukkit.getOnlinePlayers().size() + "")));
        lobbyScoreboard.setText(2, Utils.formatString(config.getTranslated("Scoreboard.text.2", p, "Benötigt: %max%"), A.a("%max%"), A.a(game.getMinimumPlayers() + "")));
        lobbyScoreboard.setText(3, Utils.formatString(config.getTranslated("Scoreboard.text.3", p, "Zurzeitige Map: %map%"), A.a("%map%"), A
                .a(game.getVotedMap() == null ? game.getGameConfig().getString("Settings.noMapAvailable", "NoMapAvailable") : game.getVotedMap().getDisplayname(p))));
        lobbyScoreboard
                .setText(4, Utils.formatString(config.getTranslated("Scoreboard.text.4", p, "Timer: %timer%"), A.a("%timer%"), A.a(game.getRemainingLobbyTime() + "")));
    }

    private void clearEffects(Player p) {
        for (PotionEffect pe : new ArrayList<>(p.getActivePotionEffects())) {
            p.removePotionEffect(pe.getType());
        }
    }

    public void updateSpectatorScoreboard() {
    }

    private class MapVoteItem implements PageItem {

        @Override
        public void onClick(InventoryClickEvent e) {
            game.getMapManager().openVoteInventory(getPlayer());
        }
    }

    private class PerkSelector implements PageItem {

        @Override
        public void onClick(InventoryClickEvent e) {
            getPlayer().openInventory(new SelectPerkEditInventory(getPlayer(), game.getPerkManager()).build());
        }
    }

    private class TeamChange implements PageItem {

        @Override
        public void onClick(InventoryClickEvent e) {
            getPlayer().openInventory(new TeamChangeInventory(getPlayer(), new Config(game, "teams.yml"), game).build());
        }
    }

    private class ViewAlivePlayers implements PageItem {

        @Override
        public void onClick(InventoryClickEvent e) {
            getPlayer().openInventory(new AlivePlayerInventory(getPlayer(), game).build());
        }
    }
}
