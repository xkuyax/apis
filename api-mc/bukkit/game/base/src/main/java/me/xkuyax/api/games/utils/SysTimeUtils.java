package me.xkuyax.api.games.utils;

public class SysTimeUtils {
    
    /**
     * Überprüft ob die vergange Zeit zwischen ms und jetzt noch kleiner als test ist
     *
     * @param ms
     * @param test
     * @return
     */
    public static boolean isCooldownActive(long ms, long test) {
        return System.currentTimeMillis() - ms < test;
    }
    
    /**
     * ÜBerprüft ob die vergange Zeit zwischen ms und jetzt noch größer ist
     *
     * @param ms
     * @param test
     * @return
     */
    public static boolean isCooldownExpired(long ms, long test) {
        return System.currentTimeMillis() - ms > test;
    }
}
