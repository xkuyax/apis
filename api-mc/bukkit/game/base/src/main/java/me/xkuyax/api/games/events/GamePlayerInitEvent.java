package me.xkuyax.api.games.events;

import org.bukkit.event.HandlerList;

public class GamePlayerInitEvent extends GamePlayerEvent {
    
    private static final HandlerList handerList = new HandlerList();
    
    public GamePlayerInitEvent(ManageableGame game, GamePlayer player) {
        super(game, player);
    }
    
    @Override
    public HandlerList getHandlers() {
        return handerList;
    }
    
    public static HandlerList getHandlerList() {
        return handerList;
    }
}
