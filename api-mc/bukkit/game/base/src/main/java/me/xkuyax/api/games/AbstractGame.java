package me.xkuyax.api.games;

import com.google.common.base.Preconditions;
import com.google.common.collect.Multimap;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import me.xkuyax.api.games.GameTasks.GameTask;
import me.xkuyax.api.games.events.*;
import me.xkuyax.api.games.interfaces.*;
import me.xkuyax.api.games.server.CloudImplementation;
import me.xkuyax.api.games.server.GameServerImplementation;
import me.xkuyax.api.games.server.NickHandler;
import me.xkuyax.api.games.utils.Message;
import me.xkuyax.api.games.utils.SelfRegisteringListener;
import me.xkuyax.api.games.utils.TargetCompass.TargetCompassModule;
import me.xkuyax.utils.config.BukkitConfig;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Server;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.*;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

@Getter(value = AccessLevel.PUBLIC)
public abstract class AbstractGame extends JavaPlugin implements ManageableGame {
    
    private GameState state = GameState.LOBBY;
    private GameManager manager;
    private LobbyTask lobbyTask;
    private BukkitConfig gameConfig;
    private BukkitConfig mapConfig;
    private BukkitConfig itemConfig;
    private BukkitConfig messagesConfig;
    private BukkitConfig settingsConfig;
    protected MapManager mapManager;
    private GameTasks.GameTask ingameTask;
    @Getter
    private PerkManager perkManager;
    @Setter
    private long startTime;
    @Setter
    private int gracePeriod;
    private List<SelfRegisteringListener> registredListeners = new ArrayList<>();
    private AchievableManager achievableManager;
    private GameMap forcedMap;
    private NickHandler nickHandler;
    private GameServerImplementation serverImplementation;
    private CloudImplementation cloudImplementation;
    
    /**
     * Lädt die GameAPI
     * Nicht überschreiben!
     */
    @Override
    public void onEnable() {
        GameApi.activate(this);
        loadBefore();
        Preconditions.checkNotNull(getOptions(), "You have to set the gameoptions in the method loadBefore()!");
        gameConfig = new BukkitConfig(this, "config.yml");
        mapConfig = new BukkitConfig(this, "maps.yml");
        itemConfig = new BukkitConfig(this, "items.yml");
        messagesConfig = new BukkitConfig(this, "messages.yml");
        settingsConfig = new BukkitConfig(this, "settings.yml");
        lobbyTask = new AbstractLobbyTask(this);
        manager = new GameManager(this);
        ingameTask = new GameTask(this);
        register(ingameTask);
        perkManager = new PerkManager(this);
        achievableManager = new AchievableManager(this);
        setLobbyTask(new AbstractLobbyTask(this));
        Bukkit.getPluginManager().registerEvents(this, this);
        TargetCompassModule.a();
        mapManager = new MapManager(this);
        Bukkit.getPluginManager().callEvent(new GameInitEvent(this));
        setupGame();
        for (Player player : Bukkit.getOnlinePlayers()) {
            handleJoin(player);
        }
        prepareWorlds();
        loadAfter();
        if (getOptions().isLoadPerks()) {
            for (GamePlayer gp : getPlayers()) {
                gp.loadPerks();
            }
        }
        onStartup();
        setNetApi();
        Bukkit.getPluginManager().callEvent(new GameInitedEvent(this));
    }
    
    @Override
    public void onDisable() {
        Server s = Bukkit.getServer();
        for (World w : s.getWorlds()) {
            s.unloadWorld(w, false);
        }
    }
    
    /**
     * Updatet die netApI States, ist eigentlich nicht relevant für andere Devs.
     */
    public void setNetApi() {
        NetAPI.setGameState(getGameState().getNetApiState());
        if (isIngame() && getCurrent() != null) {
            NetAPI.setMap(getCurrent().getEngDisplayname());
        }
        if (isLobby() && getVotedMap() != null) {
            NetAPI.setMap(getVotedMap().getEngDisplayname());
        }
        NetAPI.setMaxPlayers(getMaximumPlayers());
        NetAPI.pushInformation();
    }
    
    public void prepareWorlds() {
        if (getOptions().isPrepareWorlds()) {
            for (World w : Bukkit.getWorlds()) {
                w.setTime(0);
                w.setStorm(false);
                w.setThundering(false);
                w.setGameRuleValue("doDaylightCycle", "false");
                w.setGameRuleValue("keepInventory", "false");
                w.setGameRuleValue("doTileDrops", "true");
                w.setAutoSave(false);
                for (Entity e : w.getEntities()) {
                    if ((e instanceof LivingEntity || e instanceof Slime || e instanceof Item) && !(e instanceof Player)) {
                        e.remove();
                    }
                }
            }
        }
    }

	/*
     * @Override
	 */
    
    /**
     * Returnt die GameMap für die gevotet wurde
     */
    @Override
    public GameMap getVotedMap() {
        return isForcedMap() ? forcedMap : mapManager.getHighestVote();
    }
    
    /**
     * Startet das Spiel und führt doStart() aus wenn man eine andere start routine haben will
     * als die gameapi
     */
    @Override
    public final void start() {
        List<Player> a = new ArrayList<>(Bukkit.getOnlinePlayers());
        while (a.size() > getMaximumPlayers()) {
            Player p = a.remove(a.size() - 1);
            p.kickPlayer("§4Die Runde ist leider voll!");
        }
        doStart();
        String cur = getCurrent().getName();
        for (GameMap m : getMaps()) {
            if (m.isVisibleAtVoting() && !m.getName().equals(cur)) {
                Bukkit.unloadWorld(m.getBukkitWorld(), false);
            }
        }
        Bukkit.getPluginManager().callEvent(new GameStartedEvent(this));
    }
    
    /**
     * Die Standart doStart() Methode setzt die GracePeriod wenn eines gibt
     * und fï¿½hrt bei den Usern setIngameItems und setPlayed auf true
     */
    public void doStart() {
        if (hasGraceperiod()) {
            setGracePeriod(getGameConfig().getInt("Settings.graceperiodTime", 30));
        }
        setGameState(hasGraceperiod() ? GameState.GRACEPERIOD : GameState.INGAME);
        setCurrent(getVotedMap());
        mapManager.createWorld(getCurrent());
        if (getOptions().isPrepareWorlds()) {
            prepareWorlds();
        }
        onStart();
        getCurrent().teleportPlayers();
        for (GamePlayer p : getUsers().values()) {
            if (getOptions().isLoadPerks()) {
                p.savePerks();
            }
            p.setIngameItems();
            p.setPlayed(true);
        }
        startTime = System.currentTimeMillis();
        perkManager.onStart();
    }
    
    /**
     * Führt nur doStop() aus
     * überschreiben für andere Stop Routine
     */
    @Override
    public final void stop() {
        doStop();
    }
    
    /**
     * Stoppt das Spiel standartmï¿½ssig
     */
    public void doStop() {
        Bukkit.getServer().getPluginManager().callEvent(new GameStopEvent(this));
        setGameState(GameState.END);
        if (nickHandler != null) {
            nickHandler.handleNickOnStop();
        }
        Message.sendAll("The game has stopped", "Messages.gamestopped", this);
        GameSendPlayersEvent gameSendPlayersEvent = new GameSendPlayersEvent(this, "lobby");
        Bukkit.getServer().getPluginManager().callEvent(gameSendPlayersEvent);
        String targetServer = gameSendPlayersEvent.getServer();
        int ticks = this.getGameConfig().getInt("Settings.stopTimer", getOptions().isDebug() ? 1 : 200);
        Bukkit.getScheduler().runTaskTimer(this, () -> {
            AtomicBoolean none = new AtomicBoolean(true);
            Bukkit.getOnlinePlayers().forEach(p -> {
                GamePlayer gamePlayer = getPlayer(p);
                GamePlayerShouldKickEvent gamePlayerShouldKickEvent = new GamePlayerShouldKickEvent(this, gamePlayer);
                Bukkit.getPluginManager().callEvent(gamePlayerShouldKickEvent);
                boolean kick = gamePlayerShouldKickEvent.isKick();
                if (kick) {
                    serverImplementation.sendToServer(targetServer, p);
                } else {
                    none.set(false);
                }
            });
            if (none.get()) {
                if (!getOptions().isDebug()) {
                    Bukkit.getOnlinePlayers().forEach(p -> serverImplementation.sendToServer(targetServer, p));
                }
                Bukkit.getScheduler().runTaskLater(AbstractGame.this, () -> {
                    if (getOptions().isDebug()) {
                        Bukkit.reload();
                    } else {
                        for (Player p : Bukkit.getOnlinePlayers()) {
                            p.kickPlayer("§4Error at connecting you to the hub server!");
                        }
                        Bukkit.shutdown();
                    }
                }, 20);
            }
        }, ticks, ticks);
        Bukkit.getPluginManager().callEvent(new GameStoppedEvent(this));
        onStop();
    }
    
    public Multimap<PerkType, String> getDefaultPerks() {
        return null;
    }
    
    /**
     * Setzt den Lobby Timer auf den Wert n
     */
    @Override
    public void setTimer(int n) {
        lobbyTask.setRemainingTime(n);
        if (cloudImplementation != null) {
            cloudImplementation.setLobbyTimer(n);
        }
    }
    
    /**
     * Returnt die Zeit in ms wie lange das Game ingame ist
     */
    @Override
    public long getGameTime() {
        return System.currentTimeMillis() - startTime;
    }
    
    /**
     * Erstellt eine Bukkit welt von der GameMap map
     */
    @Override
    public void createWorld(GameMap map) {
        mapManager.createWorld(map);
    }
    
    /**
     * Registriert einen RepeatTask
     */
    @Override
    public void register(RepeatTask task) {
        manager.register(task);
    }
    
    /**
     * Registriert ein Perk
     */
    @Override
    public void register(AbstractPerk perk) {
        perkManager.register(perk);
    }
    
    /**
     * Registiert ein Perk und Repeattask
     *
     * @param t
     */
    public <T extends AbstractPerk & RepeatTask> void registerBoth(T t) {
        register((AbstractPerk) t);
        register((RepeatTask) t);
    }
    
    /**
     * Entfernen einen RepeatTask
     */
    @Override
    public void unregister(RepeatTask task) {
        manager.getTaskManager().unregister(task);
    }
    
    /**
     * Eigenen Lobbytask setzen
     */
    @Override
    public void setLobbyTask(LobbyTask task) {
        unregister(getLobbyTask());
        this.lobbyTask = task;
        register(task);
    }
    
    /**
     * GameState ändern, updatet den TaskManager für die RepeatTasks
     */
    @Override
    public void setGameState(GameState set) {
        this.state = set;
        manager.getTaskManager().run();
        setNetApi();
    }
    
    @Override
    public GameState getGameState() {
        return state;
    }
    
    @Override
    public boolean isLobby() {
        return state.isLobby();
    }
    
    @Override
    public boolean isIngame() {
        return state.isIngame();
    }
    
    @Override
    public int getMinimumPlayers() {
        GameGetMinimumPlayersEvent event = new GameGetMinimumPlayersEvent(this);
        Bukkit.getPluginManager().callEvent(event);
        return event.getValue();
    }
    
    @Override
    public int getMaximumPlayers() {
        return getGameConfig().getInt("Settings.maximumPlayers", 2);
    }
    
    @Override
    public int getRemainingLobbyTime() {
        return getLobbyTask().getRemainingTime();
    }
    
    @Override
    public int getMaximumLobbyTime() {
        return getGameConfig().getInt("Settings.maximumLobbyTimer", 60);
    }
    
    @Override
    public Location getLobbyLocation() {
        return getGameConfig().getLocation("Settings.lobbyspawn");
    }
    
    @Override
    public MysqlStats getStats() {
        return null;
    }
    
    @Override
    public GameQuests getQuests() {
        return null;
    }
    
    @Override
    public void registerListener(SelfRegisteringListener listener) {
        registredListeners.add(listener);
    }
    
    /**
     * Wenn die Anzahl an spielern kleiner ist als dieser wert wird das game gestoppt
     */
    @Override
    public int getMinimiumPlayersIngame() {
        return getGameConfig().getInt("Settings.minIngamePlayers", 2);
    }
    
    @Override
    public boolean hasGraceperiod() {
        return getOptions().isGraceperiod();
    }
    
    @Override
    public boolean isInGame(Player p) {
        return true;
    }
    
    @Override
    public boolean isInGame(String p) {
        return true;
    }
    
    @Override
    public boolean isDuels() {
        return false;
    }
    
    @Override
    public boolean isForcedMap() {
        return forcedMap != null;
    }
    
    @Override
    public String getMatchHistoryName() {
        return getName();
    }
    
    @Override
    public void tryEnd() {
    }
    
    @Override
    public void addResetBlock(Block b) {
    }
    
    /**
     * Returnt den Displayname des Spielers
     */
    @Override
    public String getDisplayname(Player p) {
        return getGameConfig().getTranslated("Settings.displayname", p, getName());
    }
    
    /**
     * Wenn die Anzahl der Spieler in einem Team kleiner ist als dieser Wert und weniger Teams leben als minTeams wird das game gestoppt
     */
    @Override
    public int getMinAlivePlayersPerTeam() {
        return getGameConfig().getInt("Settings.minAlivePlayersPerTeam", 1);
    }
    
    /**
     * Beendet die Graceperiod
     */
    @Override
    public void endGracePeriod() {
        for (GamePlayer gp : getPlayers()) {
            gp.setGracePeriodEndItems();
        }
        Bukkit.getPluginManager().callEvent(new GracePeriodEndEvent(this));
        onGracePeriodEnd();
    }
    
    @Override
    public void unnickPlayers() {
        NickHandler handler = ApiHandler.a().getNickHandler();
        for (GamePlayer gp : getUsers().values()) {
            User u = gp.getUser();
            handleNick(handler, gp, u);
        }
    }
    
    private void handleNick(NickHandler handler, GamePlayer gp, User u) {
        if (handler.hasNick(u.getUuid())) {
            handler.changeNick(u, u.getName());
            handler.removeNick(u.getUuid());
            Utils.sendMessage(gp.getPlayer(), "Messages.nickremoved", "Dein Nick wurde entfernt", null, null);
        }
    }
    
    @Override
    public int getAlivePlayers() {
        return (int) getPlayers().stream().filter(GamePlayer::isAlive).count();
    }
    
    @Override
    public void setForcedMap(Map map) {
        this.forcedMap = map;
    }
    
    /**
     * Wird ausgefï¿½hrt wenn die Graceperiod zu ende geht
     */
    public void onGracePeriodEnd() {
    }
    
    /**
     * Wenn ein Spieler joint
     *
     * @param p
     */
    public void onJoin(Player p) {
    }
    
    /**
     * Wenn jemand leftet
     *
     * @param p
     */
    public void onQuit(Player p) {
    }
    
    /**
     * Wenn der LobbyTimer auf 60 zurï¿½ck gesetzt wird
     */
    public void onLobbyReset() {
    }
    
    public void onLobbyTick(int secondsLeft) {
    }
    
    /**
     * Wenn das Spiel startet
     */
    public void onStart() {
    }
    
    /**
     * Wenn es stoppt
     */
    public void onStop() {
    }
    
    @Deprecated
    public void onStartup() {
    }
    
    /**
     * GameOptions sollten hier erstellt werden, und diese Methode wird vor allem anderen ausgeführt
     */
    public void loadBefore() {
    }
    
    /**
     * Nach dem die GameAPI User geladen hat wird dies ausgeführt
     */
    public void loadAfter() {
    }
    
    /**
     * Returnt die User
     *
     * @return
     */
    public abstract HashMap<String, ? extends GamePlayer> getUsers();
    
    abstract void setupGame();
}
