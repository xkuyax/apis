package me.xkuyax.api.games.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.bukkit.ChatColor;

@Data
@AllArgsConstructor
public class SortedQuest implements Comparable<SortedQuest> {
    
    private Quest quest;
    
    @Override
    public int compareTo(SortedQuest q) {
        return ChatColor.stripColor(quest.getName()).compareTo(ChatColor.stripColor(q.getQuest().getName()));
    }
}
