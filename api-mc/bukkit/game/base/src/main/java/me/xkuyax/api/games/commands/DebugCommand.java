package me.xkuyax.api.games.commands;

import org.bukkit.command.CommandSender;

import java.lang.reflect.Field;

public class DebugCommand extends GCommand {
    
    public DebugCommand() {
        super("debug");
    }
    
    @CmdHandler(ignoreargs = true, permission = "general.games.debug", sender = CSender.BOTH, runAsyc = true)
    public void onIgnoreArgs(CommandSender p, String[] args) {
        Game game = GameApi.getCurrent();
        try {
            Class<?> s = game.getClass();
            printFields(p, s, game);
            while (!s.getSuperclass().equals(Object.class)) {
                printFields(p, s.getSuperclass(), game);
                s = s.getSuperclass();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void printFields(CommandSender p, Class<?> c, Game game) throws IllegalArgumentException, IllegalAccessException {
        Utils.sendMessage(p, "----Debug of " + c.getSimpleName() + "----");
        for (Field field : c.getDeclaredFields()) {
            field.setAccessible(true);
            Utils.sendMessage(p, "Field " + field.getName() + " value " + field.get(game));
            field.setAccessible(false);
        }
    }
}
