package me.xkuyax.api.games;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;

import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Random;

public class MapVoteInventory extends SingleInventory {
    
    private static Config config = GameApi.getCurrent().getItemConfig();
    private Game game;
    private MapManager manager;
    
    public MapVoteInventory(Player player, Game game, MapManager manager) {
        super(player, config, "Inventorys.mapVoteItem");
        this.game = game;
        this.manager = manager;
        setDynamic(true);
        updateInventory();
    }
    
    @Override
    public void updateInventory() {
        clear();
        HashMap<Map, Integer> a = getMapVoteValues();
        Player p = getOwner();
        for (Map m : game.getMaps()) {
            if (m.isVisibleAtVoting()) {
                ItemBuilder ib = game.getItemConfig().getItemBuilder("Items.mapVote", p);
                Integer in = a.get(m);
                int i = 0;
                if (in != null) {
                    i = in;
                }
                ib.formatBoth(A.a("%map%", "%voted%"), A.a(m.getDisplayname(p), i + ""));
                add(ib.build(), new MapClickVote(m));
            }
        }
        set(game.getItemConfig().getInt("Items.mapVote.slot", 28), game.getItemConfig().getItemStack("Items.randomMap", p), new RandomVoteItem());
    }
    
    private HashMap<Map, Integer> getMapVoteValues() {
        HashMap<Map, Integer> b = new HashMap<>();
        for (Entry<String, Map> e : manager.getVote().entrySet()) {
            Map i = e.getValue();
            Player p = game.getPlayer(e.getKey()).getPlayer();
            if (i.isVisibleAtVoting()) {
                if (b.containsKey(i)) {
                    int c = b.get(i);
                    if (p.hasPermission("youtuber")) {
                        c = c + 5;
                    } else if (p.hasPermission("general.vip")) {
                        c = c + 3;
                    } else {
                        c = c + 1;
                    }
                } else {
                    int c = 1;
                    if (p.hasPermission("youtuber")) {
                        c = 5;
                    } else if (p.hasPermission("general.vip")) {
                        c = 3;
                    }
                    b.put(i, c);
                }
            }
        }
        return b;
    }
    
    public class RandomVoteItem implements PageItem {
        
        @Override
        public void onClick(InventoryClickEvent e) {
            Random random = new Random();
            List<? extends Map> a = game.getMaps();
            int max = 10, i = 0;
            Map map = a.get(random.nextInt(a.size()));
            while (!map.isVisibleAtVoting()) {
                map = a.get(random.nextInt(a.size()));
                i++;
                if (i >= max) {
                    break;
                }
            }
            if (map != null) {
                manager.vote(getOwner(), map);
            }
        }
    }
    
    @Data
    @AllArgsConstructor
    public class MapClickVote implements PageItem {
        
        private Map map;
        
        @Override
        public void onClick(InventoryClickEvent e) {
            manager.vote((Player) e.getWhoClicked(), map);
        }
    }
}
