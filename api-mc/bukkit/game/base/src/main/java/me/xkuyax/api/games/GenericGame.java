package me.xkuyax.api.games;

import com.google.common.base.Preconditions;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.List;

@Getter
public abstract class GenericGame<P extends GamePlayer, M extends Map, T extends Team> extends AbstractGame implements IGenericGame<P, M, T> {
    
    private M current;
    private GenericHelper<P, T, M> generic;
    
    @Override
    public void addMap(String name) {
        Preconditions.checkNotNull(name, "Name cannot be null!");
        generic.getMap().create(name);
    }
    
    @Override
    public P getGamePlayer(String name) {
        return getPlayer(name);
    }
    
    @Override
    public List<P> getGamePlayers() {
        return generic.getPlayers();
    }
    
    @Override
    public M getMap(String name) {
        return generic.getMap().get(name);
    }
    
    @Override
    public M getMap(String name, boolean create) {
        Preconditions.checkNotNull(name, "Name cannot be null!");
        M map = generic.getMap().get(name);
        if (map == null && create) {
            map = generic.getMap().create(name);
        }
        return map;
    }
    
    public abstract Class<M> getMapClass();
    
    @Override
    public List<M> getMaps() {
        return generic.getMaps();
    }
    
    @Override
    public P getPlayer(Player p) {
        return getPlayer(p.getName());
    }
    
    @Override
    public P getPlayer(Player p, boolean create) {
        return getPlayer(p.getName(), create);
    }
    
    @Override
    public P getPlayer(String name) {
        Preconditions.checkNotNull(name, "Name cannot be null!");
        return generic.getPlayer().get(name);
    }
    
    @Override
    public P getPlayer(String name, boolean create) {
        Preconditions.checkNotNull(name, "Name cannot be null!");
        P p = generic.getPlayer().get(name);
        if (p == null && create) {
            p = generic.getPlayer().create(name);
        }
        return p;
    }
    
    public abstract Class<P> getPlayerClass();
    
    @Override
    public List<P> getPlayers() {
        return generic.getPlayers();
    }
    
    public abstract Class<T> getTeamClass();
    
    public T getTeam(Player player) {
        return getTeam(getPlayer(player));
    }
    
    @Override
    public T getTeam(GamePlayer p) {
        Preconditions.checkNotNull(p, "We cant get the team from a null player!");
        for (T t : generic.getTeams()) {
            if (t.getPlayers().contains(p)) {
                return t;
            }
        }
        return null;
    }
    
    public T getTeam(String name) {
        return getTeam(name, false);
    }
    
    public T getTeam(String name, boolean create) {
        Preconditions.checkNotNull(name, "Name cannot be null!");
        for (T t : generic.getTeams()) {
            if (t.getName().equals(name)) {
                return t;
            }
        }
        if (create) {
            T t = generic.getTeam().create(name);
            generic.getTeam().getMap().put(name, t);
            return t;
        }
        return null;
    }
    
    public List<T> getTeams() {
        return generic.getTeams();
    }
    
    @Override
    public HashMap<String, P> getUsers() {
        return generic.getPlayer().getMap();
    }
    
    @Override
    public void onEnable() {
        generic = new GenericHelper<>(this, getPlayerClass(), getTeamClass(), getMapClass());
        super.onEnable();
    }
    
    @Override
    public void handleJoin(Player player) {
        GamePlayer p = getPlayer(player.getName(), true);
        onJoin(player);
        Bukkit.getPluginManager().callEvent(new GamePlayerInitEvent(this, p));
        if (isLobby()) {
            p.setLobbyItems();
        } else {
            p.setSpectator();
        }
        if (!p.isSpectator()) {
            Message.sendAll("The player %player% has joined the game", "Messages.playerJoin", A.a("%player%"), A.a(p.getColoredName()), this);
        }
        if (getOptions().isLoadPerks()) {
            p.loadPerks();
        }
        if (getOptions().isReduceLobbyTimer() && isLobby()) {
            if (Bukkit.getServer().getOnlinePlayers().size() >= this.getMaximumPlayers() && this.getRemainingLobbyTime() > getGameConfig()
                    .getInt("Settings.reducelobbytimerto", 10)) {
                setTimer(getGameConfig().getInt("Settings.reducelobbytimerto", 10));
            }
        }
        p.onLoad();
    }
    
    @Override
    public void handleQuit(Player player) {
        try {
            if (isLobby()) {
                mapManager.getVote().remove(player.getName());
            }
            GamePlayer p = getPlayer(player.getName());
            Preconditions.checkNotNull(getState(), "State cant be null Bodoo.");
            Preconditions.checkNotNull(p, "Player cant be null " + player.getName());
            if (!p.isSpectator() && !getState().equals(GameState.END)) {
                Message.sendAll("The player %player% has left the game", "Messages.playerQuit", A.a("%player%"), A.a(p.getColoredName()), this);
            }
            if (getPerkManager() != null && getPerkManager().getPerks() != null) {
                for (AbstractPerk ap : getPerkManager().getPerks()) {
                    ap.getPlayers().remove(p.getName());
                }
            }
            p.unload();
            onQuit(player);
            Bukkit.getPluginManager().callEvent(new GamePlayerQuitEvent(this, p));
        } catch (Throwable t) {
            t.printStackTrace();
        } finally {
            generic.getPlayer().getMap().remove(player.getName());
        }
    }
    
    @SuppressWarnings("unchecked")
    @Override
    public void setCurrent(Map map) {
        this.current = (M) map;
    }
}
