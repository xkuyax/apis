package me.xkuyax.api.games.commands;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class AllCommand extends GCommand {

    public AllCommand() {
        super("all", new String[]{"a"});
    }

    @CmdHandler(ignoreargs = true, sender = CSender.BOTH)
    public void onignoreargs(Player p, String[] args) {
        if (args.length == 0) {
            Message.send(p, "/a <Message>", "Messages.allInfo");
            return;
        }
        ManageableGame game = GameApi.getCurrent();
        GamePlayer gu = game.getPlayer(p);
        if (gu.isSpectator() || game.isLobby()) {
            return;
        }
        String middle = game.getGameConfig().getTranslated("Templates.allMiddle", p, "All &r: ");
        String front = game.getGameConfig().getTranslated("Templates.allFront", p, " &r: ");
        String msg = MsgCommand.getMessage(args, 0);
        String prefix = gu.getTeam().getPrefix();
        prefix = prefix.length() > 2 ? prefix.substring(0, 2) : prefix;
        String finalmsg = gu.getColoredName() + front + msg; //Utils.simpleColorFormat(prefix + p.getDisplayName() + " &r: " + msg);
        for (Player c : Bukkit.getOnlinePlayers()) {
            c.sendMessage((StringFormatting.simpleColorFormat(gu.getTeam().getPrefix() + middle) + finalmsg));
        }
    }
}
