package me.xkuyax.api.games.interfaces;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.bukkit.Bukkit;
import org.bukkit.Location;

@Data
@AllArgsConstructor
public class GameLocation {
    
    private double x, y, z;
    private float yaw, pitch;
    private String world;
    
    public Location get() {
        return new Location(Bukkit.getWorld(world), x, y, z, yaw, pitch);
    }
}
