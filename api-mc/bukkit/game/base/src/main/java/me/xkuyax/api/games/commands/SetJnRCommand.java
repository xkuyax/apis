package me.xkuyax.api.games.commands;

import org.bukkit.Location;
import org.bukkit.entity.Player;

public class SetJnRCommand extends GCommand {
    
    private Location pos1, pos2;
    
    public SetJnRCommand() {
        super("setjnr");
    }
    
    @CmdHandler(args = "pos1", permission = "general.games.setjnr", sender = CSender.PLAYER_ONLY)
    public void pos1(Player p, String[] args) {
        this.pos1 = p.getLocation();
        this.successMessage(p);
    }
    
    @CmdHandler(args = "pos2", permission = "general.games.setjnr", sender = CSender.PLAYER_ONLY)
    public void pos2(Player p, String[] args) {
        this.pos2 = p.getLocation();
        this.successMessage(p);
    }
    
    @CmdHandler(args = "save", permission = "general.games.setjnr", sender = CSender.PLAYER_ONLY)
    public void save(Player p, String[] args) {
        if (pos1 != null && pos2 != null) {
            int minx = pos1.getBlockX() < pos2.getBlockX() ? pos1.getBlockX() : pos2.getBlockX();
            int miny = pos1.getBlockY() < pos2.getBlockY() ? pos1.getBlockY() : pos2.getBlockY();
            int minz = pos1.getBlockZ() < pos2.getBlockZ() ? pos1.getBlockZ() : pos2.getBlockZ();
            int maxx = pos1.getBlockX() > pos2.getBlockX() ? pos1.getBlockX() : pos2.getBlockX();
            int maxy = pos1.getBlockY() > pos2.getBlockY() ? pos1.getBlockY() : pos2.getBlockY();
            int maxz = pos1.getBlockZ() > pos2.getBlockZ() ? pos1.getBlockZ() : pos2.getBlockZ();
            ManageableGame game = GameApi.getCurrent(p);
            if (game != null) {
                Config config = game.getGameConfig();
                config.set("Settings.lobbyjnr.minx", minx);
                config.set("Settings.lobbyjnr.miny", miny);
                config.set("Settings.lobbyjnr.minz", minz);
                config.set("Settings.lobbyjnr.maxx", maxx);
                config.set("Settings.lobbyjnr.maxy", maxy);
                config.set("Settings.lobbyjnr.maxz", maxz);
                if (config.save()) {
                    this.successMessage(p);
                } else {
                    p.sendMessage("§4Fehler beim speichern der Config!");
                }
            } else {
                p.sendMessage("§4Kein Game gefunden :(!");
            }
        } else {
            p.sendMessage("§4Pos1 oder/und Pos2 ist nicht gesetzt :(");
        }
    }
}
