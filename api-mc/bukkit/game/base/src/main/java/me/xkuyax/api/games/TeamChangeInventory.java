package me.xkuyax.api.games;

import code.aterstones.nickapi.NickAPI;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

public class TeamChangeInventory extends SingleInventory {
    
    private Game game;
    private Config config;
    
    public TeamChangeInventory(Player player, Config config, Game game) {
        super(player, config, "Inventorys.teamChange");
        this.game = game;
        this.config = config;
        setDynamic(true);
        setForce(true);
        updateInventory();
    }
    
    @Override
    public void updateInventory() {
        Player p = getOwner();
        GamePlayer gp = game.getPlayer(p);
        for (Team t : game.getTeams()) {
            ItemStack is = config.getItemStack("Teams." + t.getName() + ".itemStack", p);
            List<String> lore = new ArrayList<>();
            for (GamePlayer c : t.getPlayers()) {
                lore.add(c.getColoredName());
            }
            is = ItemUtils.setLoreAndDisplay(is, lore, is.getItemMeta().getDisplayName());
            int slot = config.getInt("Teams." + t.getName() + ".slot", 6);
            set(slot, is, new ChangeTeam(gp, t));
            //set(config, "Teams." + t.getName(), p, new ChangeTeam(gp, t));
        }
    }
    
    @Data
    @AllArgsConstructor
    class ChangeTeam implements PageItem {
        
        private GamePlayer player;
        private Team team;
        
        @Override
        public void onClick(InventoryClickEvent e) {
            int size = team.size();
            if (size < (Bukkit.getMaxPlayers() - 2) / game.getTeams().size()) {
                player.setTeam(team);
                NickAPI.refreshPlayer((Player) e.getWhoClicked());
                Message.send(player.getPlayer(), "Du bist nun in team %team%", "Messages.yourTeam", A.a("%team%"), A.a(team.getColoredName(player.getPlayer())));
                refresh();
            } else {
                Message.send(player.getPlayer(), "&4Das Team %team%&4 ist voll!", "Messages.teamFull", A.a("%team%"), A.a(team.getColoredName(player.getPlayer())));
            }
        }
    }
}
