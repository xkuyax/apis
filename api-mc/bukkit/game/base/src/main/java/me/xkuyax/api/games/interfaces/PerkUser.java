package me.xkuyax.api.games.interfaces;

public interface PerkUser {
    
    void updatePerks();
    
    HashMap<PerkType, List<AbstractPerk>> getSelected();
    
    ConcurrentHashMap<AbstractPerk, Long> getCooldown();
    
    void activePerks();
    
    /**
     * Lädt und setzt sie ausgewählt
     */
    void loadPerks();
    
    void savePerks();
    
    void setActive(AbstractPerk ap, int id);
}
