package me.xkuyax.api.games.interfaces;

import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public interface IGenericGame<P extends GamePlayer, M extends GameMap, T extends Team> extends ManageableGame {
    
    @Override
    void addMap(String name);
    
    @Override
    P getGamePlayer(String name);
    
    @Override
    default List<P> getAlivePlayersList() {
        return getPlayers().stream().filter(GamePlayer::isAlive).collect(Collectors.toList());
    }
    
    @Override
    List<P> getGamePlayers();
    
    @Override
    M getMap(String name);
    
    @Override
    M getMap(String name, boolean create);
    
    M getCurrent();
    
    Class<M> getMapClass();
    
    @Override
    List<M> getMaps();
    
    @Override
    P getPlayer(Player p);
    
    @Override
    P getPlayer(Player p, boolean create);
    
    @Override
    P getPlayer(String name);
    
    @Override
    P getPlayer(String name, boolean create);
    
    Class<P> getPlayerClass();
    
    @Override
    List<P> getPlayers();
    
    Class<T> getTeamClass();
    
    T getTeam(Player player);
    
    @Override
    T getTeam(GamePlayer p);
    
    T getTeam(String name);
    
    T getTeam(String name, boolean create);
    
    List<T> getTeams();
    
    HashMap<String, P> getUsers();
}
