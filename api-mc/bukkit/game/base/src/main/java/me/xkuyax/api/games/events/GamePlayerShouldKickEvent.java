package me.xkuyax.api.games.events;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.event.HandlerList;

public class GamePlayerShouldKickEvent extends GamePlayerEvent {
    
    private static final HandlerList handlerList = new HandlerList();
    @Setter
    @Getter
    private boolean kick = true;
    
    public GamePlayerShouldKickEvent(ManageableGame game, GamePlayer player) {
        super(game, player);
    }
    
    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }
    
    public static HandlerList getHandlerList() {
        return handlerList;
    }
}
