package me.xkuyax.api.games.interfaces;

import org.bukkit.entity.Player;

public interface GamePlayer extends PerkUser {
    
    String getName();
    
    boolean isAlive();
    
    boolean isSpectator();
    
    String getPrefix();
    
    String getSuffix();
    
    String getSelfPrefix();
    
    String getColoredName();
    
    Player getPlayer();
    
    User getUser();
    
    void setIngameItems();
    
    void setLobbyItems();
    
    void setStopItems();
    
    void setSpectator();
    
    void setGracePeriodEndItems();
    
    void updateItems();
    
    void autoRespawn(int seconds);
    
    void setBuild(boolean b);
    
    boolean canBuild();
    
    void setNickName(String newname);
    
    String getNickName();
    
    void onLoad();
    
    void unload();
    
    void updateScoreboard();
    
    boolean isPlayed();
    
    void setPlayed(boolean played);
    
    int getPoints();
    
    void setTeam(Team team);
    
    Team getTeam();
    
    /**
     * Return true wenn der spieler in <=time den cooldown gesetzt bekommen hat
     *
     * @param cooldown
     * @param time
     * @return
     */
    boolean isCooldownActive(String cooldown, long time);
    
    /**
     * Return true wenn der Spieler länger als time nicht den cooldown gesetzt hat
     *
     * @param cooldown
     * @param time
     * @return
     */
    boolean isCooldownExpired(String cooldown, long time);
    
    /**
     * Setzt die Zeit des cooldowns auf die jeweilige Zeit
     *
     * @param cooldown
     * @param time
     */
    void setCooldown(String cooldown, long time);
    
    /**
     * Setzt den Cooldown auf die System.currentTime
     *
     * @param cooldown
     */
    void setCooldown(String cooldown);
    
    /**
     * Setzt den Cooldown auf 0
     *
     * @param cooldown
     */
    void removeCooldown(String cooldown);
    
    void setRespawnSafeActive();
    
    boolean isRespawnSafeActive();
    
    void updateSpectatorScoreboard();
    
    ManageableGame getGame();
}
