package me.xkuyax.api.games;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

@Getter
public class GenericHelper<P extends GamePlayer, T extends Team, M extends Map> {
    
    private GenericInformation<P> player;
    private GenericInformation<T> team;
    private GenericInformation<M> map;
    private ManageableGame game;
    
    public GenericHelper(ManageableGame game, Class<P> p, Class<T> t, Class<M> m) {
        try {
            this.game = game;
            player = new GenericInformation<>(p, p.getDeclaredConstructor(String.class, ManageableGame.class), new HashMap<>());
            map = new GenericInformation<>(m, m.getDeclaredConstructor(String.class, ManageableGame.class), new LinkedHashMap<>());
            team = new GenericInformation<>(t, t.getDeclaredConstructor(String.class, ManageableGame.class), new HashMap<>());
        } catch (NoSuchMethodException | SecurityException e) {
            e.printStackTrace();
        }
    }
    
    public List<P> getPlayers() {
        return player.getList();
    }
    
    public List<M> getMaps() {
        return map.getList();
    }
    
    public List<T> getTeams() {
        return team.getList();
    }
    
    @AllArgsConstructor
    @Data
    public class GenericInformation<V> {
        
        private Class<V> clazz;
        private Constructor<V> constructor;
        private HashMap<String, V> map;
        
        public List<V> getList() {
            return new ArrayList<>(map.values());
        }
        
        public V create(String name) {
            try {
                V v = constructor.newInstance(name, game);
                map.put(name, v);
                return v;
            } catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                e.printStackTrace();
                return null;
            }
        }
        
        public V get(String name) {
            return map.get(name);
        }
    }
}
