package me.xkuyax.api.games.utils;

import org.bukkit.Bukkit;
import org.bukkit.event.Listener;

public class SelfRegisteringListener implements Listener {
    
    public SelfRegisteringListener(ManageableGame game) {
        Bukkit.getPluginManager().registerEvents(this, General.a());
        game.registerListener(this);
    }
    
    public SelfRegisteringListener() {
        this(GameApi.getCurrent());
    }
}
