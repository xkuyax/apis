package me.xkuyax.api.games.utils;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

public class TeamBalancer {
    
    @Getter
    @Setter
    private ManageableGame game;
    private List<? extends Team> teams;
    
    public TeamBalancer() {
        setGame(GameApi.getCurrent());
        teams = game.getTeams();
        if (!isSorted()) {
            sort();
        }
    }
    
    private boolean isSorted() {
        //alle teams mit mehr als 1 spieler
        List<Team> playerTeams = new ArrayList<>();
        for (Team t : teams) {
            if (t.size() > 0) {
                playerTeams.add(t);
            }
        }
        //wenn nur 1 team leute hat müssen wir balancen
        if (playerTeams.size() <= 1) {
            return false;
        }
        Team lowest = null;
        Team highest = null;
        for (Team t : playerTeams) {
            lowest = lowest == null || lowest.size() > t.size() ? t : lowest;
            highest = highest == null || highest.size() < t.size() ? t : highest;
        }
        if (highest != null && lowest != null) {
            return Math.abs(highest.size() - lowest.size()) <= 1;
        }
        return false;
    }
    
    private void sort() {
        int i = 0;
        while (!isSorted() && i <= 1000) {
            Team lowest = getLowest();
            Team highest = getHighest();
            highest.get(highest.size() - 1).setTeam(lowest);
            i++;
        }
    }
    
    private Team getLowest() {
        Team lowest = null;
        for (Team t : getPlayerTeams()) {
            lowest = lowest == null || lowest.size() > t.size() ? t : lowest;
        }
        return lowest;
    }
    
    private Team getHighest() {
        Team highest = null;
        for (Team t : getPlayerTeams()) {
            highest = highest == null || highest.size() < t.size() ? t : highest;
        }
        return highest;
    }
    
    private List<Team> getPlayerTeams() {
        List<Team> playerTeams = new ArrayList<>();
        for (Team t : teams) {
            if (t.size() > 0) {
                playerTeams.add(t);
            }
        }
        return playerTeams;
    }
}
