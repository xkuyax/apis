package me.xkuyax.api.games.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public class QuestUtils {
    
    public static List<SortedQuest> sort(Set<Quest> set) {
        List<SortedQuest> sorted = new ArrayList<>();
        for (Quest q : set) {
            sorted.add(new SortedQuest(q));
        }
        Collections.sort(sorted);
        return sorted;
    }
    
    public static List<SortedQuest> cut(List<SortedQuest> quests, int page, int perpage) {
        List<SortedQuest> sorted = new ArrayList<>();
        int o = (page + 1) * perpage;
        o = o > quests.size() ? quests.size() - 1 : 0;
        int last = page <= 0 ? 0 : ((page - 1) * perpage) + 1;
        for (int b = last; b <= o; b++) {
            sorted.add(quests.get(b));
        }
        return sorted;
    }
}
