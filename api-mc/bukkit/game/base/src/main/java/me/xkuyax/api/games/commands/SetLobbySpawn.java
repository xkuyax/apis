package me.xkuyax.api.games.commands;

import org.bukkit.entity.Player;

public class SetLobbySpawn extends GCommand {
    
    public SetLobbySpawn() {
        super("setlobbyspawn");
    }
    
    @CmdHandler(ignoreargs = true, permission = "general.games.setspawn", sender = CSender.BOTH)
    public void ignoreArgs(Player p, String[] args) {
        GameApi.getCurrent().getGameConfig().saveLocation(p.getLocation(), "Settings.lobbyspawn");
        successMessage(p);
    }
}
