package me.xkuyax.api.games;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(of = "task")
@AllArgsConstructor
public class RepeatTaskMeta {
    
    private RepeatTask task;
    private int last = Integer.MAX_VALUE;
}
