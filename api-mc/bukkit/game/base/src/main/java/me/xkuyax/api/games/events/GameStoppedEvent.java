package me.xkuyax.api.games.events;

import org.bukkit.event.HandlerList;

public class GameStoppedEvent extends GameEvent {
    
    private static final HandlerList handlerList = new HandlerList();
    
    public GameStoppedEvent(ManageableGame game) {
        super(game);
    }
    
    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }
    
    public static HandlerList getHandlerList() {
        return handlerList;
    }
}
