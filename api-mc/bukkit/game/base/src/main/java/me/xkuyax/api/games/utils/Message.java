package me.xkuyax.api.games.utils;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import me.xkuyax.api.games.interfaces.GamePlayer;
import me.xkuyax.api.games.interfaces.ManageableGame;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import sun.reflect.Reflection;

import java.util.ArrayList;
import java.util.List;

public class Message {
    
    public static boolean tryExpensiveLookup = false;
    private static List<ConfigProvider> registredPlugins = new ArrayList<>();
    
    public static void send(Player p, String defaultmsg, String path) {
        send0(p, defaultmsg, path, true, getConfig(p), null, null);
    }
    
    public static void send(Player p, String defaultmsg, String path, boolean prefix) {
        send0(p, defaultmsg, path, prefix, getConfig(p), null, null);
    }
    
    public static void send(Player p, String defaultmsg, String path, List<String> r, List<String> o) {
        send0(p, defaultmsg, path, true, getConfig(p), r, o);
    }
    
    public static void send(Player p, String defaultmsg, String path, List<String> r, List<String> o, ClickEvent click, HoverEvent hover) {
        send0(p, defaultmsg, path, true, getConfig(p), r, o, wrap(click, hover));
    }
    
    public static void send(Player p, String defaultmsg, String path, boolean prefix, List<String> r, List<String> o) {
        send0(p, defaultmsg, path, prefix, getConfig(p), r, o);
    }
    
    public static void sendAll(String defaultmsg, String path) {
        for (Player p : Bukkit.getOnlinePlayers()) {
            send0(p, defaultmsg, path, true, getConfig(p), null, null);
        }
    }
    
    public static void sendAll(String defaultmsg, String path, boolean prefix) {
        for (Player p : Bukkit.getOnlinePlayers()) {
            send0(p, defaultmsg, path, prefix, getConfig(p), null, null);
        }
    }
    
    public static void sendAll(String defaultmsg, String path, List<String> r, List<String> o) {
        for (Player p : Bukkit.getOnlinePlayers()) {
            send0(p, defaultmsg, path, true, getConfig(p), r, o);
        }
    }
    
    public static void sendAll(String defaultmsg, String path, boolean prefix, List<String> r, List<String> o) {
        for (Player p : Bukkit.getOnlinePlayers()) {
            send0(p, defaultmsg, path, prefix, getConfig(p), r, o);
        }
    }
    
    public static void sendAll(String defaultmsg, String path, ManageableGame game) {
        sendAll(defaultmsg, path, true, null, null, game);
    }
    
    public static void sendAll(String defaultmsg, String path, boolean prefix, ManageableGame game) {
        sendAll(defaultmsg, path, prefix, null, null, game);
    }
    
    public static void sendAll(String defaultmsg, String path, List<String> r, List<String> o, ManageableGame game) {
        sendAll(defaultmsg, path, true, r, o, game);
    }
    
    public static void sendAll(String defaultmsg, String path, boolean prefix, List<String> r, List<String> o, ManageableGame game) {
        for (GamePlayer gp : game.getPlayers()) {
            send(gp.getPlayer(), defaultmsg, path, prefix, r, o);
        }
    }
    
    public static void send0(Player p, String d, String path, boolean prefix, Config config, List<String> r, List<String> o) {
        send0(p, d, path, prefix, config, r, o, null);
    }
    
    public static void send0(Player p, String d, String path, boolean prefix, Config config, List<String> r, List<String> o, JsonMessage2 json) {
        if (config == null) {
            return;
        }
        String prefi = config.getTranslated("Messages.prefix", p, "&c[" + config.getPlugin() + "]&r");
        String pre = "";
        if (!config.getBoolean("Messages.noPrefix", false) && prefix) {
            pre = prefi;
        }
        String msg = config.getTranslated(path, p, d);
        if (msg == null || msg.equalsIgnoreCase("idontwanttoseethismessage")) return;
        msg = msg.replaceAll("%prefix%", prefi);
        msg = r != null && o != null ? Utils.formatString(msg, r, o) : msg;
        msg = msg.replaceAll("/n", "\n");
        String fmsg = msg.contains("%noprefix%") ? msg.replaceAll("%noprefix%", "") : pre + msg;
        FormatMessageEvent fme = new FormatMessageEvent(path, fmsg, config, p);
        if (json != null) {
            fme.setJson(json);
        }
        Bukkit.getPluginManager().callEvent(fme);
        if (!fme.isCancelled()) {
            BaseComponent[] bc = TextComponent.fromLegacyText(fme.getMessage());
            if (json != null) {
                if (json.getClick() != null) {
                    Messenger.addClickEvent(bc, json.getClick());
                }
                if (json.getHover() != null) {
                    Messenger.addHoverEvent(bc, json.getHover());
                }
            }
            Messenger.sendToPlayer(p, bc);
        }
    }
    
    public static void register(ConfigProvider pov) {
        if (!registredPlugins.contains(pov)) {
            registredPlugins.add(pov);
        }
        tryExpensiveLookup = true;
    }
    
    @Deprecated
    public static void register(String s) {
        GameProvider pov = new GameProvider(s, s);
        add(pov);
    }
    
    public static void register(String pack, String game) {
        add(new GameProvider(pack, game));
    }
    
    private static void add(ConfigProvider pov) {
        if (!registredPlugins.contains(pov)) {
            registredPlugins.add(pov);
        }
        tryExpensiveLookup = true;
    }
    
    private static JsonMessage2 wrap(ClickEvent click, HoverEvent hover) {
        JsonMessage2 jsm = new JsonMessage2("", "");
        jsm.setClick(click);
        jsm.setHover(hover);
        return jsm;
    }
    
    public static Config getConfig(Player pp) {
        if (tryExpensiveLookup) {
            Config exp = getExpensive();
            if (exp != null) {
                return exp;
            }
        }
        ManageableGame game = GameApi.getCurrent();
        if (game == null) {
            game = GameApi.getCurrent(pp);
            if (game != null) {
                return game.getMessagesConfig();
            }
            return new Config("messages.yml", true);
        }
        return game.getMessagesConfig();
    }
    
    private static Config getExpensive() {
        for (int i = 2; i < 15; i++) {
            Class<?> a = Reflection.getCallerClass(i);
            Config config = getConfigFromClass(a);
            if (config != null) {
                return config;
            }
        }
        return null;
    }
    
    public static Config getConfigFromClass(Class<?> a) {
        if (a != null) {
            for (ConfigProvider s : registredPlugins) {
                String pack = s.getPack().toLowerCase();
                Package pa = a.getPackage();
                if (pack != null && pa != null) {
                    String pan = pa.getName();
                    if (pan.contains(pack)) {
                        return s.getConfig();
                    }
                }
            }
        }
        return null;
    }
    
    public interface ConfigProvider {
        
        Config getConfig();
        
        String getPack();
    }
    
    @Getter
    @RequiredArgsConstructor
    @EqualsAndHashCode(of = "pack", callSuper = false)
    public static class GameProvider implements ConfigProvider {
        
        private final String pack;
        private final String game;
        
        @Override
        public Config getConfig() {
            return new Config(game, "messages.yml", true);
        }
    }
    
    public static class GeneralConfigProvider implements ConfigProvider {
        
        private String file;
        @Getter
        private Config config;
        
        public GeneralConfigProvider(String pack, String config) {
            this.file = pack;
            this.config = new Config(config);
        }
        
        @Override
        public String getPack() {
            return file;
        }
    }
    
    public static void g(String reciever, String path, List<String> r, List<String> o) {
        Config config = getExpensive();
        Validate.notNull(config, "Could not get config, add configprovider for your package");
        JsonMessage2 jsm = new JsonMessage2(reciever, path, path, r, o);
        jsm.setPlugin(config.getPlugin());
        jsm.setConfigYaml(config.getYaml());
        jsm.sendGlobal();
    }
    
    public static void g(String reciever, String path, List<String> r, List<String> o, ClickEvent click, HoverEvent hover) {
        Config config = getExpensive();
        Validate.notNull(config, "Could not get config, add configprovider for your package");
        JsonMessage2 jsm = new JsonMessage2(reciever, path, path, r, o);
        jsm.setPlugin(config.getPlugin());
        jsm.setConfigYaml(config.getYaml());
        jsm.setClick(click);
        jsm.setHover(hover);
        jsm.sendGlobal();
    }
}
