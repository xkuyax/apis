package me.xkuyax.api.games;

import com.google.common.base.Preconditions;
import lombok.Getter;
import org.bukkit.Bukkit;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class TaskManager implements Runnable {
    
    @Getter
    private List<RepeatTaskMeta> tasks;
    private ManageableGame game;
    
    public TaskManager(ManageableGame game) {
        this.tasks = Collections.synchronizedList(new ArrayList<>());
        this.game = game;
        Bukkit.getScheduler().runTaskTimer(General.a(), this, 0, 1);
    }
    
    public void unregister(RepeatTask task) {
        tasks.removeIf(type -> type.getTask().equals(task));
    }
    
    public void register(RepeatTask task) {
        Preconditions.checkArgument(task != null, "You can not set an null task!");
        Preconditions.checkNotNull(task.getInverval(), "Interval cannot be null");
        Preconditions.checkNotNull(task.getRequiredGameState(), "Required state cant be null!");
        Preconditions.checkNotNull(task.getTime() < 0, "Time cannot be smaller than 0");
        getTasks().add(new RepeatTaskMeta(task, 0));
    }
    
    @Override
    public void run() {
        for (int in = 0; in < tasks.size(); in++) {
            RepeatTaskMeta rt = tasks.get(in);
            RepeatTask task = rt.getTask();
            if (task.getRequiredGameState().matchRequired(game.getGameState())) {
                Interval i = task.getInverval();
                if (i.getTicks() * task.getTime() <= rt.getLast() || (i == Interval.TICK && rt.getTask().getTime() == 1)) {
                    try {
                        rt.getTask().run();
                    } catch (Throwable t) {
                        t.printStackTrace();
                    }
                    rt.setLast(1);
                } else {
                    rt.setLast(rt.getLast() + 1);
                }
            } else {
                rt.setLast(Integer.MAX_VALUE);
            }
        }
    }
}
