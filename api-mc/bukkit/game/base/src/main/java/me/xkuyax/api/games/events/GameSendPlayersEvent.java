package me.xkuyax.api.games.events;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.event.HandlerList;

/**
 * Created by Felix on 18.05.2016.
 */
public class GameSendPlayersEvent extends GameEvent {
    
    private static final HandlerList handlerList = new HandlerList();
    @Getter
    @Setter
    private String server;
    
    public GameSendPlayersEvent(ManageableGame game, String serverToSend) {
        super(game);
        this.server = serverToSend;
    }
    
    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }
    
    public static HandlerList getHandlerList() {
        return handlerList;
    }
}
