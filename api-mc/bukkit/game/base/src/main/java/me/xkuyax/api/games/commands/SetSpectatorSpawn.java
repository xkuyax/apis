package me.xkuyax.api.games.commands;

import org.bukkit.entity.Player;

public class SetSpectatorSpawn extends GCommand {
    
    public SetSpectatorSpawn() {
        super("setspectatorspawn");
    }
    
    @CmdHandler(ignoreargs = true, permission = "general.games.setspawn", sender = CSender.BOTH, minarg = 1)
    public void ignoreArgs(Player p, String[] args) {
        GameApi.getCurrent().getMapConfig().saveLocation(p.getLocation(), "Maps." + args[0] + ".spectatorLocation");
        successMessage(p);
    }
}
