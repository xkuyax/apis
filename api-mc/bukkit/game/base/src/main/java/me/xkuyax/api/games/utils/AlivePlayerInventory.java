package me.xkuyax.api.games.utils;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Comparator;
import java.util.List;

public class AlivePlayerInventory extends SingleInventory {
    
    private ManageableGame game;
    private int old = Integer.MAX_VALUE;
    
    public AlivePlayerInventory(Player p, ManageableGame game) {
        super(p, game.getItemConfig().getInt("Inventorys.alivePlayers.slot", 36), game.getItemConfig()
                .getTranslated("Inventorys.alivePlayers.title", p, "&cAlive players"), true, false);
        this.game = game;
        setDynamic(true);
        updateInventory();
    }
    
    @Override
    public void updateInventory() {
        List<? extends GamePlayer> a = game.getPlayers();
        CollectionUtils.sort(a, ((g1, g2) -> g1.getTeam() == null || g2.getTeam() == null ? 0 : g1.getTeam().getName().compareTo(g2.getTeam().getName())), Comparator
                .comparing(GamePlayer::getColoredName));
        if (old == Integer.MAX_VALUE || a.size() != old) {
            clear();
            for (int i = 0; i < (a.size() > getSize() ? getSize() : a.size()); i++) {
                GamePlayer gp = a.get(i);
                if (gp.isAlive()) {
                    ItemBuilder ib = game.getItemConfig().getItemBuilder("Items.alivePlayer", gp.getPlayer());
                    ib.formatBoth(A.a("%player%", "%points%"), A.a(gp.getColoredName(), gp.getPoints() + ""));
                    ItemStack is = ItemUtils.setSkullandName(ib.build(), gp.getNickName());
                    add(is, (e) -> e.getWhoClicked().teleport(gp.getPlayer()));
                }
            }
            old = a.size();
        }
    }
}
