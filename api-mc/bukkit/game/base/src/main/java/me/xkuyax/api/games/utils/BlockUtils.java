package me.xkuyax.api.games.utils;

import lombok.RequiredArgsConstructor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.material.Bed;
import org.bukkit.util.Vector;

import java.util.function.Consumer;

public class BlockUtils {

    public static void line(Location loc, int x, int y, int x2, int y2, int yb, BlockAction action) {
        int w = x2 - x;
        int h = y2 - y;
        int dx1 = 0, dy1 = 0, dx2 = 0, dy2 = 0;
        if (w < 0) dx1 = -1;
        else if (w > 0) dx1 = 1;
        if (h < 0) dy1 = -1;
        else if (h > 0) dy1 = 1;
        if (w < 0) dx2 = -1;
        else if (w > 0) dx2 = 1;
        int longest = Math.abs(w);
        int shortest = Math.abs(h);
        if (!(longest > shortest)) {
            longest = Math.abs(h);
            shortest = Math.abs(w);
            if (h < 0) dy2 = -1;
            else if (h > 0) dy2 = 1;
            dx2 = 0;
        }
        int numerator = longest >> 1;
        for (int i = 0; i <= longest; i++) {
            Location clone = new Location(loc.getWorld(), x, yb, y);
            action.run(clone);
            numerator += shortest;
            if (!(numerator < longest)) {
                numerator -= longest;
                x += dx1;
                y += dy1;
            } else {
                x += dx2;
                y += dy2;
            }
        }
    }

    public static void line(Location loc, int x, int y, int x2, int y2, int yb, int yg, BlockAction action) {
        for (int ya = yg; ya >= yb; ya--) {
            line(loc, x, y, x2, y2, ya, action);
        }
    }

    public static void square(Location middle, int radius, Material set) {
        World world = middle.getWorld();
        int xmin = middle.getBlockX() - radius;
        int xmax = middle.getBlockX() + radius;
        int ymin = middle.getBlockY() - radius;
        int ymax = middle.getBlockY() + radius;
        int zmin = middle.getBlockZ() - radius;
        int zmax = middle.getBlockZ() + radius;
        for (int x = xmin; x < xmax; x++) {
            for (int y = ymin; y < ymax; y++) {
                for (int z = zmin; z < zmax; z++) {
                    world.getBlockAt(x, y, z).setType(set);
                }
            }
        }
    }

    public static void drawSquare(Location middle, int radius, BlockAction action) {
        World world = middle.getWorld();
        int xmin = middle.getBlockX() - radius;
        int xmax = middle.getBlockX() + radius;
        int y = middle.getBlockY();
        int zmin = middle.getBlockZ() - radius;
        int zmax = middle.getBlockZ() + radius;
        for (int x = xmin; x <= xmax; x++) {
            for (int z = zmin; z <= zmax; z++) {
                action.run(new Location(world, x, y, z));
            }
        }
    }

    public static void drawCube(Location middle, int radius, BlockAction action) {
        World world = middle.getWorld();
        int xmin = middle.getBlockX() - radius;
        int xmax = middle.getBlockX() + radius;
        int ymin = middle.getBlockY() - radius;
        int ymax = middle.getBlockY() + radius;
        int zmin = middle.getBlockZ() - radius;
        int zmax = middle.getBlockZ() + radius;
        for (int x = xmin; x <= xmax; x++) {
            for (int y = ymin; y <= ymax; y++) {
                for (int z = zmin; z <= zmax; z++) {
                    action.run(new Location(world, x, y, z));
                }
            }
        }
    }

    @Deprecated
    public static void square(Location middle, int radius, BlockAction action) {
        World world = middle.getWorld();
        int xmin = middle.getBlockX() - radius;
        int xmax = middle.getBlockX() + radius;
        int ymin = middle.getBlockY() - radius;
        int ymax = middle.getBlockY() + radius;
        int zmin = middle.getBlockZ() - radius;
        int zmax = middle.getBlockZ() + radius;
        for (int x = xmin; x < xmax; x++) {
            for (int y = ymin; y < ymax; y++) {
                for (int z = zmin; z < zmax; z++) {
                    action.run(new Location(world, x, y, z));
                }
            }
        }
    }

    public static boolean isInSquare(Location a, Location b, Location check) {
        int xmin = a.getBlockX() < b.getBlockX() ? a.getBlockX() : b.getBlockX();
        int xmax = a.getBlockX() > b.getBlockX() ? a.getBlockX() : b.getBlockX();
        int zmin = a.getBlockZ() < b.getBlockZ() ? a.getBlockZ() : b.getBlockZ();
        int zmax = a.getBlockZ() > b.getBlockZ() ? a.getBlockZ() : b.getBlockZ();
        return check.getBlockX() >= xmin && check.getBlockX() <= xmax && check.getBlockZ() >= zmin && check.getBlockZ() <= zmax;
    }

    public static boolean isInSquareXYZ(Location a, Location b, Location check) {
        int xmin = a.getBlockX() < b.getBlockX() ? a.getBlockX() : b.getBlockX();
        int xmax = a.getBlockX() > b.getBlockX() ? a.getBlockX() : b.getBlockX();
        int ymin = a.getBlockY() < b.getBlockY() ? a.getBlockY() : b.getBlockY();
        int ymax = a.getBlockY() > b.getBlockY() ? a.getBlockY() : b.getBlockY();
        int zmin = a.getBlockZ() < b.getBlockZ() ? a.getBlockZ() : b.getBlockZ();
        int zmax = a.getBlockZ() > b.getBlockZ() ? a.getBlockZ() : b.getBlockZ();
        return check.getBlockX() >= xmin && check.getBlockX() <= xmax && check.getBlockY() >= ymin && check.getBlockY() <= ymax && check.getBlockZ() >= zmin && check
                .getBlockZ() <= zmax;
    }

    public static void forEachInRectangle(Location a, Location b, boolean inclusive, Consumer<Location> consumer) {
        World world = a.getWorld();
        int xmin = a.getBlockX() < b.getBlockX() ? a.getBlockX() : b.getBlockX();
        int xmax = a.getBlockX() > b.getBlockX() ? a.getBlockX() : b.getBlockX();
        int ymin = a.getBlockY() < b.getBlockY() ? a.getBlockY() : b.getBlockY();
        int ymax = a.getBlockY() > b.getBlockY() ? a.getBlockY() : b.getBlockY();
        int zmin = a.getBlockZ() < b.getBlockZ() ? a.getBlockZ() : b.getBlockZ();
        int zmax = a.getBlockZ() > b.getBlockZ() ? a.getBlockZ() : b.getBlockZ();
        if (inclusive) {
            for (int x = xmin; x <= xmax; x++) {
                for (int y = ymin; y <= ymax; y++) {
                    for (int z = zmin; z <= zmax; z++) {
                        Location location = new Location(world, x, y, z);
                        consumer.accept(location);
                    }
                }
            }
        } else {
            for (int x = xmin; x < xmax; x++) {
                for (int y = ymin; y < ymax; y++) {
                    for (int z = zmin; z < zmax; z++) {
                        Location location = new Location(world, x, y, z);
                        consumer.accept(location);
                    }
                }
            }
        }
    }

    public static Location add(Side side, int distance, Location loc) {
        Vector v = loc.toVector();
        v.add(new Vector(side.getX() * distance, 0, side.getZ() * distance));
        return v.toLocation(loc.getWorld());
    }

    public static Location getFromSide(Player p, Side side, int distance) {
        Vector v = p.getLocation().toVector();
        v.add(new Vector(side.getX() * distance, 0, side.getZ() * distance));
        return v.toLocation(p.getWorld());
    }

    public static Location getInFront(Player p, int distance) {
        return getFromSide(p, PerkUtils.yawToSide(p.getLocation().getYaw()), distance);
    }

    public static double distanceSquared(Location a, Location b) {
        if (a.getWorld() != null && b.getWorld() != null) {
            if (a.getWorld() == b.getWorld()) {
                return a.distanceSquared(b);
            }
        }
        return Double.MAX_VALUE;
    }

    public static Block getOtherBedBlock(Block b) {
        if (b.getState().getData() instanceof Bed) {
            Bed bed = (Bed) b.getState().getData();
            if (bed.isHeadOfBed()) {
                return b.getRelative(bed.getFacing());
            } else {
                return b.getRelative(bed.getFacing());
            }
        }
        return null;
    }

    public interface BlockAction {

        void run(Location loc);
    }

    @RequiredArgsConstructor
    public static class SetBlock implements BlockAction {

        private final Material type;

        @Override
        public void run(Location loc) {
            Block b = loc.getBlock();
            if (b.getType() == Material.AIR) {
                b.setType(type);
            }
        }
    }
}
