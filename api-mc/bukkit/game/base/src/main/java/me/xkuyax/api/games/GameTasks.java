package me.xkuyax.api.games;

public class GameTasks {
    
    private GameTasks() {
    }
    
    public static class GameTask implements RepeatTask {
        
        private ManageableGame game;
        
        public GameTask(ManageableGame game) {
            this.game = game;
        }
        
        @Override
        public void run() {
            if (game.getGameState() == GameState.GRACEPERIOD) {
                if (game.getGracePeriod() <= 0) {
                    game.setGameState(GameState.INGAME);
                    game.endGracePeriod();
                } else {
                    game.setGracePeriod(game.getGracePeriod() - 1);
                    game.onGracePeriodTick(game.getGracePeriod());
                }
            }
            for (GamePlayer gp : game.getPlayers()) {
                if (gp.isAlive()) {
                    gp.updateItems();
                } else {
                    gp.updateSpectatorScoreboard();
                }
            }
            if (game.checkIfEnd()) {
                if (game.isDuels()) {
                    game.tryEnd();
                } else {
                    game.stop();
                }
            }
        }
        
        @Override
        public Interval getInverval() {
            return Interval.SECONDS;
        }
        
        @Override
        public RequiredState getRequiredGameState() {
            return RequiredState.INGAME;
        }
        
        @Override
        public int getTime() {
            return 1;
        }
    }
}
