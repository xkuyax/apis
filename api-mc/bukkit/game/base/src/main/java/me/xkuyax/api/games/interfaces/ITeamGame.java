package me.xkuyax.api.games.interfaces;

import java.util.List;

public interface ITeamGame<T extends Team> {
    
    List<T> getAliveTeams();
}
