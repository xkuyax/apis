package me.xkuyax.api.games.interfaces;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.bukkit.Difficulty;
import org.bukkit.GameMode;

@Getter
@Accessors(chain = true)
@Setter
public class GameOptions {
    
    private boolean blockBuild = true, blockFood = true, blockBreak = true, blockWeather = true, graceperiod = false, worldPeaceful = true;
    private boolean blockRespawnItems = true, clearInventoryOnStart = true, voteMaps = true, handleChat = true, alivePlayersInventory = true;
    private boolean blockTeamDamage = false, respawnSafeTimer = false, balanceTeams = false, disablePerksZeroPlayers = false;
    private boolean teleportSpectator = true, blockDurability = true, perks = false, perksCosts = false, debug = true;
    private boolean blockDropping = true, blockPickup = true, blockInteract = false, bukkitDisplayName = false;
    private boolean loadPerks = false, blockUnloadChunks = true, tryPerks = false, prepareWorlds = true;
    private boolean setNewScoreboardOnStart = false, blockFire = false, perkSlots = false, customNickRemove = false, handleEntitySpawn = true, reduceLobbyTimer = true;
    private boolean dailyModule = false;
    private GameMode ingameMode = GameMode.SURVIVAL;
    private Difficulty mapDifficulty = Difficulty.NORMAL;
    private int maxActivePerks = 1, maxPassivePerks = 1, activeSlot = 1, passiveSlot = 6, tryPerkAmount = 3;
}
