package me.xkuyax.api.games.commands;

import org.bukkit.entity.Player;

public class SetTeamSpawn extends GCommand {
    
    public SetTeamSpawn() {
        super("teamspawn");
    }
    
    @CmdHandler(ignoreargs = true, permission = "general.games.setteamspawn", sender = CSender.PLAYER_ONLY)
    public void onIgnoreArgs(Player p, String[] args) {
        GameApi.getCurrent().getTeamConfig().saveLocation(p.getLocation(), "Teams." + args[1] + ".location." + args[0] + ".spawnLocation");
        successMessage(p);
    }
}
