package me.xkuyax.api.games;

import org.bukkit.Bukkit;

public enum GameState {
    LOBBY(me.Bodoo.Load.NetAPI.Main.GameState.Waiting),
    GRACEPERIOD(me.Bodoo.Load.NetAPI.Main.GameState.Running),
    INGAME(me.Bodoo.Load.NetAPI.Main.GameState.Running),
    END(me.Bodoo.Load.NetAPI.Main.GameState.EndGame);
    
    private me.Bodoo.Load.NetAPI.Main.GameState netApiState;
    
    GameState(me.Bodoo.Load.NetAPI.Main.GameState netApiState) {
        this.netApiState = netApiState;
    }
    
    public boolean isIngame() {
        return this == GRACEPERIOD || this == INGAME;
    }
    
    public boolean isLobby() {
        return this == LOBBY;
    }
    
    public me.Bodoo.Load.NetAPI.Main.GameState getNetApiState() {
        NetApiGameStateChangeEvent netApiGameStateChangeEvent = new NetApiGameStateChangeEvent(netApiState);
        Bukkit.getServer().getPluginManager().callEvent(netApiGameStateChangeEvent);
        return netApiGameStateChangeEvent.getGameState();
    }
}
