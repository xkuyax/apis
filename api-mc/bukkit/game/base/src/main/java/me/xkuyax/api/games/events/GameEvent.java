package me.xkuyax.api.games.events;

import me.xkuyax.api.games.interfaces.ManageableGame;
import org.bukkit.event.Event;

public abstract class GameEvent extends Event {
    
    private ManageableGame game;
    
    public GameEvent(ManageableGame game) {
        this.game = game;
    }
    
    public ManageableGame getGame() {
        return game;
    }
}
