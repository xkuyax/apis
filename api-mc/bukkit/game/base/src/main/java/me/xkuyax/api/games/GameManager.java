package me.xkuyax.api.games;

import code.aterstones.nickapi.NickAPI;
import code.aterstones.nickapi.events.PlayerNickForPlayerEvent;
import code.aterstones.nickapi.events.PlayerSelfColorEvent;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import lombok.Getter;
import me.vagdedes.spartan.api.API;
import me.vagdedes.spartan.api.PlayerViolationEvent;
import me.vagdedes.spartan.system.Enums.HackType;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.*;
import org.bukkit.event.entity.*;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.event.player.*;
import org.bukkit.event.vehicle.VehicleDestroyEvent;
import org.bukkit.event.weather.WeatherChangeEvent;
import org.bukkit.event.world.ChunkUnloadEvent;
import org.bukkit.inventory.ItemStack;

import java.util.Iterator;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * Diese Klasse ist zuständig für den TaskManager und die Events.
 */
@Getter
public class GameManager implements Listener {

    private static final UUID NO_PARTY = UUID.randomUUID();
    private LoadingCache<UUID, UUID> partyCache = CacheBuilder.newBuilder().expireAfterWrite(10, TimeUnit.MINUTES).build(new CacheLoader<UUID, UUID>() {

        @Override
        public UUID load(UUID uuid) throws Exception {
            Party party = partyManager.getPartyFromPlayer(uuid);
            return party == null ? NO_PARTY : party.getPartyId();
        }
    });
    private final ManageableGame game;
    private final TaskManager taskManager;
    private PartyManager partyManager;

    public GameManager(ManageableGame game) {
        this.game = game;
        this.taskManager = new TaskManager(game);
        SocialModule sm = SocialModule.a();
        if (sm.isEnabled()) {
            partyManager = sm.getParty();
        }
        Bukkit.getPluginManager().registerEvents(this, General.a());
    }

    public void register(RepeatTask task) {
        taskManager.register(task);
    }

    @EventHandler
    public void onPlayerJoinEvent(PlayerJoinEvent e) {
        e.setJoinMessage(null);
        game.handleJoin(e.getPlayer());
    }

    @EventHandler
    public void onPlayerQuitEvent(PlayerQuitEvent e) {
        e.setQuitMessage(null);
        game.handleQuit(e.getPlayer());
    }

    @EventHandler
    public void onPlayerKickEvent(PlayerKickEvent e) {
        e.setLeaveMessage(null);
        game.handleQuit(e.getPlayer());
    }

    @EventHandler
    public void onPlayerInteractEntityEvent(PlayerArmorStandManipulateEvent e) {
        e.setCancelled(true);
    }

    @EventHandler
    public void onPlayerFoodChangeEvent(FoodLevelChangeEvent e) {
        if (game.isLobby() || game.getOptions().isBlockFood() || (e.getEntityType() == EntityType.PLAYER) && game.getPlayer((Player) e.getEntity()).isSpectator()) {
            if (!GameUtils.isUnmoveAble((Player) e.getEntity())) {
                e.setFoodLevel(20);
            } else {
                e.setFoodLevel(1);
            }
        }
    }

    @EventHandler
    public void onEntityDamgeEvent(EntityDamageEvent e) {
        if (e.getEntity() instanceof Player) {
            Player p = (Player) e.getEntity();
            if (!p.hasMetadata("NPC")) {
                if (game.getPlayer(p).isSpectator() || game.isLobby()) {
                    e.setCancelled(true);
                }
            }
        } else if (e instanceof EntityDamageByEntityEvent) {
            EntityDamageByEntityEvent ed = (EntityDamageByEntityEvent) e;
            if (ed.getDamager() instanceof Player) {
                Player p = (Player) ed.getDamager();
                if (game.getPlayer(p).isSpectator()) {
                    e.setCancelled(true);
                }
            }
        }
    }

    @EventHandler
    public void onPlayerInteractEvent(PlayerInteractEvent e) {
        if (game.isLobby() || e.getPlayer().hasMetadata("NPC") || game.getPlayer(e.getPlayer()).isSpectator() || (e.getAction() == Action.PHYSICAL && game.getOptions()
                .isBlockInteract())) {
            if (!build(e.getPlayer())) {
                if (!(e.getItem() != null && e.getItem().getType() == Material.WRITTEN_BOOK)) {
                    e.setCancelled(true);
                    e.getPlayer().updateInventory();
                }
            }
        }
    }

    @EventHandler
    public void onPlayerPickupItemEvent(PlayerPickupItemEvent e) {
        if (game.isLobby() || game.getOptions().isBlockPickup()) {
            e.setCancelled(true);
            e.getItem().remove();
        }
    }

    @EventHandler
    public void onBlockBreakEvent(BlockBreakEvent e) {
        if (build(e.getPlayer())) return;
        if (game.isLobby() || game.getGameState() == GameState.END || game.getOptions().isBlockBreak()) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onBlockPlaceEvent(BlockPlaceEvent e) {
        if (build(e.getPlayer())) return;
        if (game.isLobby() || game.getGameState() == GameState.END || game.getOptions().isBlockBuild()) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onWeatherChangeEvent(WeatherChangeEvent e) {
        if (game.isLobby() || game.getOptions().isBlockWeather()) {
            if (e.toWeatherState()) {
                e.setCancelled(true);
            }
        }
    }

    @EventHandler
    public void onBlockBurnEvent(BlockBurnEvent e) {
        if (game.getOptions().isBlockFire() || e.getBlock().getWorld().equals(game.getLobbyLocation().getWorld())) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onBlockIgniteEvent(BlockIgniteEvent e) {
        if (game.getOptions().isBlockFire() || e.getBlock().getWorld().equals(game.getLobbyLocation().getWorld())) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onBlockSpreadEvent(BlockSpreadEvent e) {
        if (game.getOptions().isBlockFire() || e.getBlock().getWorld().equals(game.getLobbyLocation().getWorld())) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onPlayerMoveEvent(PlayerMoveEvent e) {
        Location loc = e.getTo();
        Player p = e.getPlayer();
        if (game.isLobby()) {
            if (loc.getBlockY() <= game.getGameConfig().getInt("Settings.lobbyTeleport", 30)) {
                p.teleport(game.getLobbyLocation());
                API.setVL(p, HackType.Fly, 0);
                return;
            }
        } else {
            if (game.isInGame(p)) {
                GamePlayer gu = game.getPlayer(p);
                if (loc.getBlockY() <= 0 && gu.isSpectator()) {
                    p.teleport(game.getCurrent().getSpectatorLocation());
                    API.setVL(p, HackType.Fly, 0);
                }
            }
        }
    }

    @EventHandler
    public void onPlayerRespawnEvent(PlayerRespawnEvent e) {
        if (game.getOptions().isTeleportSpectator()) {
            if (game.isIngame()) {
                GamePlayer gp = game.getGamePlayer(e.getPlayer().getName());
                if (!gp.isAlive()) {
                    e.setRespawnLocation(game.getCurrent().getSpectatorLocation());
                }
            }
        }
    }

    @EventHandler
    public void onPotionSplash(PotionSplashEvent e) {
        Iterator<LivingEntity> iterator = e.getAffectedEntities().iterator();
        while (iterator.hasNext()) {
            LivingEntity en = iterator.next();
            if (en instanceof Player) {
                Player po = (Player) en;
                GamePlayer gp = game.getPlayer(po);
                if (gp.isSpectator()) {
                    e.setIntensity(en, 0);
                }
            }
        }
    }

    @EventHandler
    public void onAntiCheat(PlayerViolationEvent e) {
        Player p = e.getPlayer();
        GamePlayer gp = game.getPlayer(p);
        if (gp.isSpectator()) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onTraget(EntityTargetEvent e) {
        if (e.getTarget() instanceof Player && !(e.getEntity() instanceof Player)) {
            Player target = (Player) e.getTarget();
            GamePlayer gp = game.getPlayer(target);
            if (gp.isSpectator()) {
                e.setCancelled(true);
                e.setTarget(null);
            }
        }
    }

    @EventHandler
    public void onTraget(EntityTargetLivingEntityEvent e) {
        if (e.getTarget() instanceof Player && !(e.getEntity() instanceof Player)) {
            Player target = (Player) e.getTarget();
            GamePlayer gp = game.getPlayer(target);
            if (gp.isSpectator()) {
                e.setCancelled(true);
                e.setTarget(null);
            }
        }
    }

    @EventHandler
    public void onPlayerDropItemEvent(PlayerDropItemEvent e) {
        if (game.getPlayer(e.getPlayer()).isSpectator() || game.getOptions().isBlockDropping()) {
            /*ItemStack is = e.getItemDrop().getItemStack().clone();
            int slot = e.getPlayer().getInventory().getHeldItemSlot();
            e.getPlayer().getInventory().setItem(slot, is);
            e.getItemDrop().remove();*/
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onChunkUnloadEvent(ChunkUnloadEvent e) {
        if (game.getOptions().isBlockUnloadChunks()) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    public void onSelfEdit(PlayerSelfColorEvent e) {
        Player player = e.getPlayer();
        GamePlayer gp = game.getPlayer(player);
        e.setPrefix(StringFormatting.simpleColorFormat(gp.getSelfPrefix()));
        e.setSuffix(StringFormatting.simpleColorFormat(gp.getSuffix()));
        if (gp.getTeam() != null) {
            e.setTeamName(gp.getTeam().getName());
        }
    }

    @EventHandler
    public void onPlayerDeathEvent(PlayerDeathEvent e) {
        if (game.getOptions().isBlockRespawnItems()) {
            e.getDrops().clear();
            e.setDroppedExp(0);
            e.setNewExp(0);
            e.setNewTotalExp(0);
            e.setDeathMessage(null);
        }
    }

    @EventHandler
    public void onEntitySpawnEvent(CreatureSpawnEvent e) {
        World world = e.getLocation().getWorld();
        Entity entity = e.getEntity();
        if (game.getOptions().isHandleEntitySpawn()) {
            if (entity instanceof Creature || entity instanceof Slime) {
                if (game.isLobby() || entity instanceof Slime) {
                    e.setCancelled(true);
                    return;
                }
                if (e.getSpawnReason() != SpawnReason.CUSTOM && e.getSpawnReason() != SpawnReason.SPAWNER_EGG) {
                    for (Map gm : game.getMaps()) {
                        if (gm.getWorld().equals(world.getName())) {
                            if (!gm.getAllowSpawns().contains(e.getEntityType())) {
                                if (gm.getMobOptions().equals(MapMobOptions.NONE)) {
                                    e.setCancelled(true);
                                }
                                if (gm.getMobOptions().equals(MapMobOptions.SPAWN_FRIENDLY) && (entity instanceof Monster || entity instanceof Slime)) {
                                    e.setCancelled(true);
                                }
                                if (gm.getMobOptions().equals(MapMobOptions.SPAWN_MONSTER) && entity instanceof Animals) {
                                    e.setCancelled(true);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onEntityDamageEvent(EntityDamageByEntityEvent e) {
        if (e.getEntity() instanceof Player) {
            Player p = (Player) e.getEntity();
            GamePlayer gp = game.getPlayer(p.getName());
            if (gp.isSpectator()) {
                e.setCancelled(true);
                return;
            }
            if (e.getDamager() instanceof Player) {
                Player c = (Player) e.getDamager();
                if (p.hasMetadata("NPC")) return;
                GamePlayer gc = game.getPlayer(c.getName());
                if (gc.isSpectator()) {
                    e.setCancelled(true);
                    return;
                }
                if (game.getOptions().isBlockDurability()) {
                    ItemStack is = c.getItemInHand();
                    if (is != null && !is.getType().equals(Material.AIR)) {
                        is.setDurability((short) 0);
                        c.setItemInHand(is);
                        c.updateInventory();
                    }
                }
            }
        }
    }

    @EventHandler
    public void onPlayerChatEvent(AsyncPlayerChatEvent e) {
        if (game.getOptions().isHandleChat()) {
            if (e.isCancelled()) {
                return;
            }
            Player p = e.getPlayer();
            e.setCancelled(true);
            String msg = e.getMessage();
            if (!game.isSoloGame()) {
                if ((msg.startsWith("@all") || msg.startsWith("@a")) && game.isIngame()) {
                    Bukkit.dispatchCommand(p, "all" + msg.replaceFirst("@all", "").replaceFirst("@a", ""));
                    return;
                }
            }
            GamePlayer gp = game.getPlayer(p.getName());
            String name = "";
            if (game.getOptions().isBukkitDisplayName()) {
                name = gp.getPrefix() + gp.getPlayer().getDisplayName();
            } else {
                name = gp.getColoredName();
            }
            name = StringFormatting.simpleColorFormat(name);
            if (game.getState().equals(GameState.END)) {
                for (GamePlayer gc : game.getPlayers()) {
                    gc.getPlayer().sendMessage(name + "§r: " + msg);
                }
            } else if (gp.isSpectator() || (!game.isSoloGame() && gp.getTeam() == null)) {
                for (GamePlayer gc : game.getPlayers()) {
                    if (gc.isSpectator()) {
                        gc.getPlayer().sendMessage(name + "§r: " + msg);
                    }
                }
            } else if (game.isSoloGame() || game.isLobby()) {
                for (GamePlayer gc : game.getPlayers()) {
                    if (gc.isAlive() && gp.isAlive()) {
                        gc.getPlayer().sendMessage(name + "§r: " + msg);
                    }
                }
            } else {
                for (GamePlayer gc : game.getPlayers()) {
                    if (gc.getTeam() != null && gp.getTeam().equals(gc.getTeam())) {
                        gc.getPlayer().sendMessage(name + "§r: " + msg);
                    }
                }
            }
        }
    }

    @EventHandler
    public void onUserEditEvent(UserEditEvent e) {
        if (e.getType().equals(EditEventType.NICK)) {
            Player p = e.getPlayer();
            GamePlayer gp = game.getPlayer(p);
            gp.setNickName(gp.getUser().getNick());
            NickAPI.refreshPlayer(p);
        }
    }

    @EventHandler
    public void AsyncPlayerPreLoginEvent(AsyncPlayerPreLoginEvent e) {
        UUID uuid = e.getUniqueId();
        User u = ViceviceApi.getOfflineUser(uuid);
        if (u.isAutoNick()) {
            NickHandler handler = ApiHandler.a().getNickHandler();
            if (handler.hasNick(uuid)) {
                handler.removeNick(u.getUuid());
                change(u, u.getName());
            }
            String name = handler.getNick(u.getUuid());
            change(u, name);
        }
    }

    @EventHandler
    public void onPlayerNickForPlayerEvent(PlayerNickForPlayerEvent e) {
        Player player = e.getNicking();
        GamePlayer gp = game.getPlayer(player);
        if (gp == null) {
            return;
        }
        e.setNickTo(gp.getNickName() == null ? gp.getName() : gp.getNickName());
        e.setPrefix(StringFormatting.simpleColorFormat(gp.getPrefix()));
        e.setSuffix(StringFormatting.simpleColorFormat(gp.getSuffix()));
        if (partyManager != null) {
            Player nicking = e.getNicking();
            Player rec = e.getReceiving();
            UUID pnick = partyCache.getUnchecked(nicking.getUniqueId());
            UUID prec = partyCache.getUnchecked(rec.getUniqueId());
            if (!pnick.equals(NO_PARTY) && !prec.equals(NO_PARTY)) {
                if (pnick.equals(prec)) {
                    e.setNickTo(nicking.getName());
                }
            }
        }
        if (gp.getTeam() != null) {
            e.setTeamName(gp.getTeam().getName());
        }
    }

    @EventHandler
    public void onVehicleDestroyEvent(VehicleDestroyEvent e) {
        if (e.getAttacker() != null) {
            if (e.getAttacker() instanceof Player) {
                Player player = (Player) e.getAttacker();
                if (game.getPlayer(player).isSpectator()) {
                    e.setCancelled(true);
                }
            }
        }
    }

    private void change(User u, String n) {
        u.setNick(n);
        u.setKey(NickHandler.keyField, "NULL");
        u.saveUser();
    }

    public boolean build(Player p) {
        return game.getPlayer(p).canBuild();
    }
}
