package me.xkuyax.api.games.interfaces;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.stream.Collectors;

public interface Team {
    
    String getName();
    
    String getPrefix();
    
    List<? extends GamePlayer> getPlayers();
    
    default List<? extends GamePlayer> getAlivePlayersList() {
        return getPlayers().stream().filter(GamePlayer::isAlive).collect(Collectors.toList());
    }
    
    int getAlivePlayers();
    
    int getLives();
    
    void setLives(int i);
    
    int size();
    
    GamePlayer get(int index);
    
    /**
     * Entfernt ein Leben (decrement == --)
     */
    void decrementLives();
    
    void add(GamePlayer user);
    
    void remove(GamePlayer user);
    
    String getColoredName(Player p);
    
    String getDisplayname(Player p);
    
    void teleportToTeamSpawn(GamePlayer p);
    
    void setTeamSpawn(Location loc);
    
    Location getTeamSpawn();
    
}
