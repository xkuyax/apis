package me.xkuyax.api.games.utils;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public interface MultiLanguageItem {
    
    ItemStack getItemStack(Player p);
    
    String getName();
}
