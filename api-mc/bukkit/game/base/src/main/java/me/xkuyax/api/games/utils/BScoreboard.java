package me.xkuyax.api.games.utils;

import lombok.Data;
import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class BScoreboard {

    private Map<Integer, String> send;
    private Scoreboard wrapper;
    private Objective objective;
    private Config config;
    private String path;

    public BScoreboard(String objective, String objectiveDisplayname) {
        this.wrapper = Bukkit.getScoreboardManager().getNewScoreboard();
        this.objective = wrapper.registerNewObjective(objective, "dummy");
        this.objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        this.objective.setDisplayName(StringFormatting.simpleColorFormat(objectiveDisplayname));
        send = new HashMap<>();
    }

    public BScoreboard(Config config, String path) {
        this("test", config.getString("Scoreboard." + path + ".objective.de", path));
        this.config = config;
        this.path = path;
    }

    public void setTextFromConfig(String path, List<String> r, List<String> o, String... defaultMessages) {
        List<String> a = config.getStringList("Scoreboard." + path + ".text", true, defaultMessages);
        for (int i = 0; i < a.size(); i++) {
            String s = Utils.formatString(a.get(i), r, o);
            setText(i, s);
        }
    }

    public void setText(int index, String text) {
        if (!send.containsKey(index) || !send.get(index).equalsIgnoreCase(text)) {
            deleteWithScore(index, true);
            setText(ChatColor.translateAlternateColorCodes('&', text), index);
        }
    }

    public void setText(int index, List<String> r, List<String> o) {
        Validate.notNull(config, "You have to use the constructor which sets the config!");
        String msg = config.getString("Scoreboard." + path + ".index." + index, index + "");
        msg = Utils.formatString(msg, r, o);
        setText(index, msg);
    }

    public void setText(int index, String sub, List<String> r, List<String> o) {
        Validate.notNull(config, "You have to use the constructor which sets the config!");
        String msg = config.getString("Scoreboard." + sub + ".index." + index, index + "");
        msg = Utils.formatString(msg, r, o);
        setText(index, msg);
    }

    public void changeObjectiveName(String objective, String displayname) {
        this.objective.unregister();
        this.objective = wrapper.registerNewObjective(objective, displayname);
        this.objective.setDisplaySlot(DisplaySlot.SIDEBAR);
        this.objective.setDisplayName(StringFormatting.simpleColorFormat(displayname));
        for (Map.Entry<Integer, String> e : send.entrySet()) {
            setText(e.getKey(), e.getValue());
        }
    }

    public void changeObjectiveName(String displayname) {
        this.objective.setDisplayName(StringFormatting.simpleColorFormat(displayname));
    }

    public void setBoard(Player p) {
        p.setScoreboard(wrapper);
    }

    //text = &4Das ist voll der lange Scoreboardtext
    //teamName = &4Das ist voll d teamname
    //prefix = teamName.length-1 = &4Das ist voll d teamnam
    //Aufbau im scoreboard
    //
    private void setText(String text, int index) {
        Team team = registerTeam(index + "");
        String prefix = "";
        String suffix = "";
        String entry = "";
        if (text.length() <= 16) {
            entry = text;
        } else if (text.length() <= 32) {
            prefix = text.substring(0, 16);
            entry = text.substring(16, text.length());
        } else {
            prefix = text.substring(0, 16);
            entry = text.substring(16, 32);
            suffix = text.substring(32, text.length() > 48 ? 48 : text.length());
        }
        team.setPrefix(prefix);
        team.addEntry(entry);
        team.setSuffix(suffix);
        objective.getScore(entry).setScore(index);
        send.put(index, text);
    }

    public void clear() {
        send.forEach((i, s) -> deleteWithScore(i, false));
        send.clear();
    }

    public void removeIndex(int i) {
        deleteWithScore(i, true);
    }

    public int getFreeIndex() {
        return send.size();
    }

    private Team registerTeam(String teamName) {
        Team team = null;
        if (wrapper.getTeam(teamName) == null) {
            team = wrapper.registerNewTeam(teamName);
        } else {
            team = wrapper.getTeam(teamName);
        }
        return team;
    }

    private void deleteWithScore(int score, boolean removeLocal) {
        List<String> remove = new ArrayList<>();
        for (String s : wrapper.getEntries()) {
            for (Score i : wrapper.getScores(s)) {
                if (i.isScoreSet()) {
                    if (i.getScore() == score) {
                        remove.add(s);
                    }
                }
            }
        }
        for (String s : remove) {
            wrapper.resetScores(s);
            if (removeLocal) {
                send.remove(score);
            }
        }
    }
}
