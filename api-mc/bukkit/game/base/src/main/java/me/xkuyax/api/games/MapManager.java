package me.xkuyax.api.games;

import lombok.Getter;
import org.bukkit.World;
import org.bukkit.World.Environment;
import org.bukkit.WorldCreator;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

@Getter
public class MapManager {
    
    private HashMap<String, Map> vote = new HashMap<>();
    private Map current;
    private Map votecache;
    private ManageableGame game;
    
    public MapManager(ManageableGame game) {
        this.game = game;
        setup();
    }
    
    private void setup() {
        Config config = game.getMapConfig();
        for (String s : config.getKeys("Maps", Collections.singletonList("orient"))) {
            game.addMap(s);
        }
        /*	for (Map map : game.getMaps()) {
				//createWorld(map);
			}*/
    }
    
    public void createWorld(Map map) {
        WorldCreator wc = new WorldCreator(map.getWorld());
        wc.generator(new EmptyWorldGenerator());
        wc.environment(Environment.NORMAL);
        World world = wc.createWorld();
        if (game.getOptions() != null) {
            if (game.getOptions().getMapDifficulty() != null) {
                world.setDifficulty(game.getOptions().getMapDifficulty());
            }
        }
        ((AbstractMap) map).onLoad();
    }
    
    public void openVoteInventory(Player p) {
        p.openInventory(new MapVoteInventory(p, getGame(), this).build());
    }
    
    public void vote(Player p, Map m) {
        if (vote.containsKey(p.getName())) {
            Message.send(p, "Already voted!", "alreadyvoted");
            return;
        }
        vote.put(p.getName(), m);
        Message.send(p, "Successfully voted for the map %map%!", "votesuccess", A.a("%map%"), A.a(m.getDisplayname(p)));
        Message.send(p, "The next map is %map%", "nextmap", A.a("%map%"), A.a(getHighestVote().getDisplayname(p)));
    }
    
    public Map getHighestVote() {
        if (getVote().isEmpty()) {
            if (votecache == null) {
                List<Map> a = new ArrayList<>();
                
                for (Map map : game.getMaps()) {
                    if (map.isVisibleAtVoting()) {
                        a.add(map);
                    }
                }
                if (!a.isEmpty()) {
                    Collections.shuffle(a);
                    this.votecache = a.get(0);
                } else {
                    // DO SOME CRAZY SHIT
                }
            }
            if (votecache == null) {
                NetAPI.setMap(game.getGameConfig().getString("Settings.noMapAvailable", "NoMapAvailable"));
            } else {
                NetAPI.setMap(votecache.getEngDisplayname());
            }
            return votecache;
        }
        HashMap<Map, Integer> b = new HashMap<>();
        for (Entry<String, Map> e : getVote().entrySet()) {
            Map i = e.getValue();
            GamePlayer gp = game.getPlayer(e.getKey());
            Player p = gp == null ? null : gp.getPlayer();
            if (i.isVisibleAtVoting()) {
                if (b.containsKey(i)) {
                    int c = b.get(i);
                    if (p == null) {
                        c = c + 1;
                    } else if (p.hasPermission("youtuber")) {
                        c = c + 5;
                    } else if (p.hasPermission("general.vip")) {
                        c = c + 3;
                    } else {
                        c = c + 1;
                    }
                    b.put(i, c);
                } else {
                    int c = 1;
                    if (p == null) {
                        c = c + 1;
                    } else if (p.hasPermission("youtuber")) {
                        c = 5;
                    } else if (p.hasPermission("general.vip")) {
                        c = 3;
                    }
                    b.put(i, c);
                }
            }
        }
        int high = 0;
        Map select = null;
        for (Entry<Map, Integer> c : b.entrySet()) {
            Map d = c.getKey();
            int e = c.getValue();
            if (e > high) {
                select = d;
                high = e;
            }
        }
        if (select == null) {
            select = game.getMaps().get(0);
            votecache = select;
        }
        NetAPI.setMap(votecache.getEngDisplayname());
        return select;
    }
}
