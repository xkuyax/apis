package me.xkuyax.api.games;

import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public abstract class TeamGame<P extends GamePlayer, M extends Map, T extends AbstractTeam<P>> extends GenericGame<P, M, T> implements Iterable<T>, ITeamGame<T> {
    
    @Getter
    private PartyManager partyManager;
    private Config teamConfig = new Config(this, "teams.yml");
    
    @Override
    void setupGame() {
        for (String t : teamConfig.getKeys("Teams", "red", "green", "blue", "yellow")) {
            getTeam(t, true);
        }
        SocialModule sm = SocialModule.a();
        if (sm.isEnabled() && sm.getParty() != null) {
            partyManager = sm.getParty();
        }
        new AllCommand();
        new SetTeamSpawn();
    }
    
    @Override
    public void handleQuit(Player player) {
        GamePlayer gc = getPlayer(player);
        for (Team t : getTeams()) {
            if (t.getPlayers().contains(gc)) {
                t.remove(gc);
            }
        }
        super.handleQuit(player);
    }
    
    @Override
    public void handleJoin(Player player) {
        if (isLobby()) {
            findTeam(getPlayer(player, true));
        }
        super.handleJoin(player);
    }
    
    public void findTeam(GamePlayer user) {
        T t = findBestTeam(user);
        user.setTeam(t);
        t.add(user);
    }
    
    private T findBestTeam(GamePlayer user) {
        if (partyManager != null) {
            Party party = partyManager.getPartyFromPlayer(user.getUser().getUuid());
            if (party != null) {
                T other = getTeams().stream().filter(t -> t.getPlayers().size() < (Bukkit.getMaxPlayers() - 2) / getTeams().size() && t.getPlayers().stream()
                        .anyMatch(p -> party.getUUIDs().contains(p.getUser().getUuid()))).findFirst().orElse(null);
                if (other != null) {
                    return other;
                }
            }
        }
        List<T> a = new ArrayList<>(getTeams());
        a.sort(Comparator.comparingInt(o -> o.getPlayers().size()));
        return a.get(0);
    }
    
    @Override
    public boolean checkIfEnd() {
        return (getAliveTeams().size() < getMinAliveTeams());
    }
    
    public List<T> getAliveTeams() {
        ArrayList<T> a = new ArrayList<>();
        for (T t : this) {
            int alive = 0;
            for (P gu : t) {
                if (gu.isAlive()) {
                    alive++;
                }
            }
            if (alive >= getMinAlivePlayersPerTeam()) {
                a.add(t);
            }
        }
        return a;
    }
    
    @Override
    public void doStart() {
        setCurrent(getVotedMap());
        setTeamPoints();
        if (getOptions().isBalanceTeams()) {
            new TeamBalancer();
        }
        for (int i = 0; i < 5; i++) {
            Message.sendAll("idontwanttoseethismessage", "Messages.noAccrossTeamingAllowed." + i);
        }
        super.doStart();
    }
    
    public void setTeamPoints() {
        for (Team t : getTeams()) {
            t.setLives(getGameConfig().getInt("Settings.livesPerTeam", 5));
        }
    }
    
    @Override
    public Iterator<T> iterator() {
        return getTeams().iterator();
    }
    
    public int getMinAliveTeams() {
        return getGameConfig().getInt("Settings.minAliveTeams", 1);
    }
    
    public Config getTeamConfig() {
        return teamConfig;
    }
    
    @Override
    public boolean isSoloGame() {
        return false;
    }
}
