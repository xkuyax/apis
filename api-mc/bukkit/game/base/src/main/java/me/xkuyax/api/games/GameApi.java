package me.xkuyax.api.games;

import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.HashMap;
import java.util.Map;

public class GameApi extends Module {
    
    private Map<String, ManageableGame> games = new HashMap<>();
    private static GameApi instance;
    @Getter
    private static StatsCommand statsCommand;
    private ManageableGame activeGame;
    
    @Override
    public boolean load() {
        new DebugCommand();
        new SetLobbySpawn();
        new StopCommand();
        new StartCommand();
        new BuildCommand();
        new TimerCommand();
        new SetingameSpawn("setingamespawn");
        new SetSpectatorSpawn();
        new AddMapCommand();
        new LoadWorld();
        new SetJnRCommand();
        new ForceMapCommand();
        Bukkit.getScheduler().runTaskLater(getGeneral(), () -> {
            if (getCurrent() != null) {
                if (getCurrent().getStats() != null) {
                    statsCommand = new StatsCommand(getCurrent().getStats(), "stats", false);
                    new FeedbackCommand();
                    //new StatsCommand(getCurrent().getStats(), "cstats", true);
                }
            }
        }, 1L);
        Bukkit.getPluginManager().registerEvents(new Listener() {
            
            @EventHandler
            public void onPlayerQuitEvent(PlayerQuitEvent e) {
                games.remove(e.getPlayer().getName());
            }
        }, getGeneral());
        return true;
    }
    
    @Override
    public boolean disable() {
        return true;
    }
    
    @Override
    public String getName() {
        return "gameApiModule";
    }
    
    public static ManageableGame getCurrent() {
        return a().activeGame;
    }
    
    public static void activate(ManageableGame game) {
        if (a().activeGame != null) {
            throw new IllegalArgumentException("Another game is already running!");
        }
        a().activeGame = game;
    }
    
    public static void activate(ManageableGame game, Player p) {
        a().games.put(p.getName(), game);
    }
    
    public static ManageableGame getCurrent(Player p) {
        if (a().activeGame != null) {
            return a().activeGame;
        }
        if (p != null) {
            return a().games.get(p.getName());
        }
        return null;
    }
    
    public static GameApi a() {
        if (instance == null) {
            instance = new GameApi();
        }
        return instance;
    }
}
