package me.xkuyax.api.games.addons;

import lombok.Data;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.metadata.MetadataValue;

import java.util.HashMap;
import java.util.Map;

public class LobbyJumpAndRun extends Module {
    
    private Map<String, JumpUser> users = new HashMap<>();
    @Setter
    private ManageableGame game;
    private int miny;
    private Location jumpAndRun;
    
    @Override
    public boolean load() {
        Bukkit.getPluginManager().registerEvents(new JumpAndRunListener(), getGeneral());
        return true;
    }
    
    @Override
    public boolean disable() {
        return true;
    }
    
    @Override
    public String getName() {
        return "GameLobbyJumpAndRun";
    }
    
    public int[] getJumpAndRunValues() {
        Config config = game.getGameConfig();
        int minx = config.getInt("Settings.lobbyjnr.minx", 1000);
        int maxx = config.getInt("Settings.lobbyjnr.maxx", 2000);
        int minz = config.getInt("Settings.lobbyjnr.minz", 1000);
        int maxz = config.getInt("Settings.lobbyjnr.maxz", 2000);
        int miny = config.getInt("Settings.lobbyjnr.miny", 1000);
        int maxy = config.getInt("Settings.lobbyjnr.maxy", 2000);
        return new int[]{minx,
                miny,
                minz,
                maxx,
                maxy,
                maxz};
    }
    
    class JumpAndRunListener implements Listener {
        
        @EventHandler
        public void onGameInitEvent(GameInitEvent e) {
            setGame(e.getGame());
            game = e.getGame();
            miny = getJumpAndRunValues()[1];
            int[] where = getJumpAndRunValues();
            World world = game.getLobbyLocation().getWorld();
            for (int x = where[0]; x <= where[3]; x++) {
                for (int y = where[1]; y <= where[4]; y++) {
                    for (int z = where[2]; z <= where[5]; z++) {
                        world.getBlockAt(x, y, z).setType(Material.AIR);
                    }
                }
            }
            Generator generator = new Generator();
            jumpAndRun = generator
                    .perform(game.getLobbyLocation().getWorld(), new Generator.Box(where[0], where[1], where[2], where[3], where[4], where[5]), General.a());
        }
        
        @EventHandler
        public void onPlayerSetItemEvent(GamePlayerSetItemEvent event) {
            if (game != null) {
                if (event.getType() == ItemType.LOBBY) {
                    Player p = event.getPlayer();
                    HotbarItems.a().add(p, (HotBarClick) e -> {
                        JumpUser gu = getUser(p.getName());
                        gu.setLobbyJumpAndRun(!gu.isLobbyJumpAndRun());
                        if (gu.isLobbyJumpAndRun()) {
                            p.teleport(jumpAndRun);
                            p.getInventory().clear();
                            HotbarItems.a().add(p, (HotBarClick) ex -> {
                                gu.setLobbyJumpAndRun(false);
                                p.teleport(game.getLobbyLocation());
                                Message.send(p, "" + gu.isLobbyJumpAndRun(), "Messages.islobbyjumpandrun." + gu.isLobbyJumpAndRun());
                                game.getPlayer(p).setLobbyItems();
                            }, game.getItemConfig(), "Items.jnr.leave");
                            HotbarItems.a().add(p, (HotBarClick) ex -> {
                                p.teleport(gu.getLastCheckpoint() == null ? jumpAndRun : gu.getLastCheckpoint());
                                Message.send(p, "Du wurdest zum letzen Checkpoint teleportiert.", "Messages.checkpointtp");
                            }, game.getItemConfig(), "Items.jnr.join");
                        } else {
                            p.teleport(game.getLobbyLocation());
                        }
                        Message.send(p, "" + gu.isLobbyJumpAndRun(), "Messages.islobbyjumpandrun." + gu.isLobbyJumpAndRun());
                    }, game.getItemConfig(), "Items.lobbyjnr");
                }
            }
        }
        
        @EventHandler
        public void onPlayerMoveEvent(PlayerMoveEvent e) {
            if (game != null) {
                if (game.isLobby()) {
                    Player p = e.getPlayer();
                    if (game.isInGame(p)) {
                        Location loc = e.getTo();
                        GamePlayer gp = game.getPlayer(p);
                        JumpUser ap = getUser(gp.getName());
                        if (loc.getBlockY() <= miny) {
                            if (ap != null) {
                                if (ap.isLobbyJumpAndRun()) {
                                    Location xloc = ap.getLastCheckpoint();
                                    if (xloc != null) {
                                        p.teleport(xloc);
                                    } else {
                                        p.teleport(jumpAndRun);
                                    }
                                    return;
                                }
                            }
                        }
                        Location l = new Location(loc.getWorld(), loc.getX(), loc.getY() - 1, loc.getZ(), loc.getPitch(), loc.getYaw());
                        Block b = l.getBlock();
                        if (b != null) {
                            if (b.getType().equals(Material.GOLD_BLOCK)) {
                                if (ap != null) {
                                    if (ap.isLobbyJumpAndRun()) {
                                        int num = getCPNumber(b);
                                        if (num > ap.getLastCheckpointNumber()) {
                                            Message.send(p, "Du hast einen neuen Checkpoint erreicht", "Messages.newcheckpoint");
                                            User u = ViceviceApi.getOnlineUser(p);
                                            int points = 5;
                                            points = ViceviceApi.addOnlinePoints(p, points);
                                            int cookies = points / 5;
                                            u.addCookies(cookies);
                                            u.addPoints(points);
                                            Message.send(p, "+ %v% Cookies", "Messages.checkpoint.cookies", A.a("%v%"), A.a(String.valueOf(cookies)));
                                            Message.send(p, "+ %v% Globale Punkte", "Messages.checkpoint.points", A.a("%v%"), A.a(String.valueOf(points)));
                                            u.saveUser();
                                            ap.setLastCheckpoint(loc);
                                            ap.setLastCheckpointNumber(num);
                                            return;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    private int getCPNumber(Block b) {
        for (MetadataValue v : b.getMetadata("checkpointnumber")) {
            if (v.getOwningPlugin().equals(General.a())) {
                return v.asInt();
            }
        }
        return -1;
    }
    
    private JumpUser getUser(String name) {
        JumpUser gu = users.computeIfAbsent(name, k -> new JumpUser());
        return gu;
    }
    
    @Data
    static class JumpUser {
        
        private boolean lobbyJumpAndRun = false;
        private Location lastCheckpoint = null;
        private int lastCheckpointNumber = -1;
    }
}
