package me.xkuyax.api.games.addons;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.Listener;

public class StatsManager {
    
    public StatsManager() {
    }
    
    public interface Approver<E extends Event> {
        
        boolean approve(Player p, E e);
    }
    
    public interface StatsListener<E extends Event> extends Listener {
        
        void handleEvent(E e);
    }
}
