package me.xkuyax.api.games.addons;

import com.google.common.collect.HashMultimap;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.ArmorStand;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ArmorStandStats {
    
    private List<StatsArmorStand> armorStands = new ArrayList<>();
    private List<FetchType> fetchTypes = new ArrayList<>();
    private final StatsGetter statsGetter;
    private final MysqlStats stats;
    private final Config config;
    
    public ArmorStandStats(MysqlStats stats, Config config) {
        this.stats = stats;
        this.config = config;
        statsGetter = new StatsGetter(stats, false);
        System.out.println("RUNNING ARMOR STAND STATS");
        HashMultimap<FetchPaar, FetchType> types = HashMultimap.create();
        for (String s : config.getKeys("ArmorStands", "1", "2")) {
            Location location = config.getLocation("ArmorStands." + s + ".location");
            int position = config.getInt("ArmorStands." + s + ".position", 1);
            TimeInterval timeInterval = config.getEnum(TimeInterval.class, "ArmorStands." + s + ".timeInterval", TimeInterval.GLOBAL);
            String type = config.getString("ArmorStands." + s + ".type", "POINTS");
            ItemStack armor = config.getItemStack("ArmorStands." + s + ".armor");
            ItemStack boots = config.getItemStack("ArmorStands." + s + ".boots");
            ItemStack leggings = config.getItemStack("ArmorStands." + s + ".leggings");
            armorStands.add(new StatsArmorStand(location, type, timeInterval, position, armor, boots, leggings));
            types.put(new FetchPaar(type, timeInterval), new FetchType(type, position, timeInterval, new ArrayList<>()));
        }
        types.asMap().forEach((s, f) -> {
            FetchType max = Collections.max(f, Comparator.comparingInt(FetchType::getPosition));
            max.setPosition(max.getPosition() + 1);
            fetchTypes.add(max);
        });
        new StatsFetcher().runTaskAsynchronously(General.a());
    }
    
    private class StatsFetcher extends BukkitRunnable {
        
        @Override
        public void run() {
            for (FetchType fetchType : fetchTypes) {
                String type = fetchType.getType();
                TimeInterval ti = fetchType.getTimeInterval();
                List<PositionEntry> entries = statsGetter.getPosition(type, ti, ti.getCurrent(), 0, fetchType.getPosition());
                List<FetchedArmorStand> fetchedArmorStands = armorStands.stream().filter(s -> s.getType().equals(type) && s.getTimeInterval().equals(ti)).
                        map(s -> {
                            PositionEntry entry = entries.get(s.getPosition());
                            return new FetchedArmorStand(s, entry, ViceviceApi.getOfflineUser(entry.getUuid().toString()).getColoredName());
                        }).collect(Collectors.toList());
                new ArmorStandPlacer(fetchedArmorStands).runTask(General.a());
            }
        }
    }
    
    @RequiredArgsConstructor
    @Getter
    private class ArmorStandPlacer extends BukkitRunnable {
        
        private final List<FetchedArmorStand> fetchedArmorStands;
        
        @Override
        public void run() {
            for (FetchedArmorStand fetchedArmorStand : fetchedArmorStands) {
                PositionEntry positionEntry = fetchedArmorStand.getPositionEntry();
                StatsArmorStand statsArmorStand = fetchedArmorStand.getStatsArmorStand();
                Location location = statsArmorStand.getLocation();
                location.getBlock();
                ArmorStand armorStand = location.getWorld().spawn(location, ArmorStand.class);
                armorStand.setGravity(false);
                armorStand.setChestplate(statsArmorStand.getArmor());
                armorStand.setBoots(statsArmorStand.getBoots());
                armorStand.setLeggings(statsArmorStand.getLeggings());
                armorStand.setHelmet(ItemUtils.setSkullandName(new ItemStack(Material.SKULL_ITEM), positionEntry.getName()));
                armorStand.setArms(true);
                armorStand.setBasePlate(false);
                armorStand.setCustomName(fetchedArmorStand.getColoredName());
                armorStand.setCustomNameVisible(true);
                armorStand.setSmall(true);
                System.out.println("spawned " + location);
            }
        }
    }
    
    @Data
    @AllArgsConstructor
    private static class FetchPaar {
        
        private String type;
        private TimeInterval timeInterval;
        
    }
    
    @Data
    @AllArgsConstructor
    private static class FetchType {
        
        private String type;
        private int position;
        private TimeInterval timeInterval;
        private List<PositionEntry> entries;
    }
    
    @Data
    @AllArgsConstructor
    private static class FetchedArmorStand {
        
        private StatsArmorStand statsArmorStand;
        private PositionEntry positionEntry;
        private String coloredName;
        
    }
    
    @Data
    @AllArgsConstructor
    private static class StatsArmorStand {
        
        private Location location;
        private String type;
        private TimeInterval timeInterval;
        private int position;
        private ItemStack armor;
        private ItemStack boots;
        private ItemStack leggings;
        
    }
}
