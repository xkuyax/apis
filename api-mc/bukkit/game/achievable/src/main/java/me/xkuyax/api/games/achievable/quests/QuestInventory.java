package me.xkuyax.api.games.achievable.quests;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class QuestInventory extends SingleInventory {

    private static Config config;
    private GameQuests quests;
    private List<SortedQuest> sorted;
    @Getter
    @Setter
    private int page, maxpage = 0;
    private ItemStack has, not, nextpage, lastpage, placeholder;
    private String hass, nots, prefix;
    private List<String> abc, def, ghi;

    public QuestInventory(Player player) {
        super(player, getInvConfig(player), "Quests");
        ManageableGame game = getCurrent(player);
        quests = game.getQuests();
        int qsize = quests.getQuests().size();
        this.setSize(this.getPerfectSize(qsize));
        this.setMaxpage(getPerfectPageCount(qsize) - 1);
        this.sorted = QuestUtils.sort(quests.getQuests());
        this.has = config.getItemStack("Quests.has");
        this.not = config.getItemStack("Quests.not");
        this.hass = config.getString("Quests.hasstring", "&4%quest%");
        this.nots = config.getString("Quests.notsstring", "&e%quest%");
        this.prefix = config.getString("Quests.loreprefix", "&a");
        this.abc = config.getStringList("Quests.lorenothave", true, "", "&7???", "");
        this.def = config.getStringList("Quests.lorehaveadd1", true, "");
        this.ghi = config.getStringList("Quests.lorehaveadd2", true, "", "&aERLEDIGT!");
        this.nextpage = config.getItemStack("Quests.nextpage");
        this.lastpage = config.getItemStack("Quests.lastpage");
        this.placeholder = config.getItemStack("Quests.placeholder");
        this.updateInventory();
    }

    public void loadPage() {
        this.clear();
        GameQuests gq = quests;
        int i = 0;
        for (SortedQuest q : QuestUtils.cut(sorted, page, 54 - 9)) {
            Quest quest = q.getQuest();
            if (gq.hasFinished(getOwner(), quest, QuestType.PERMANENT, quests.getGame())) {
                ItemStack is = has.clone();
                ItemMeta meta = is.getItemMeta();
                String a = hass;
                meta.setDisplayName(Utils.formatString(a, A.a("%quest%"), A.a(quest.getName())));
                List<String> list = new ArrayList<>();
                for (String x : def) {
                    list.add(StringFormatting.simpleColorFormat(x));
                }
                for (String x : quest.getDescription().split("/n")) {
                    list.add(StringFormatting.simpleColorFormat(prefix + x));
                }
                for (String x : ghi) {
                    list.add(StringFormatting.simpleColorFormat(x));
                }
                meta.setLore(list);
                is.setItemMeta(meta);
                this.set(i, is, (HotBarClick) -> {
                });
            } else {
                ItemStack is = not.clone();
                String a = nots;
                ItemMeta meta = is.getItemMeta();
                meta.setDisplayName(Utils.formatString(a, A.a("%quest%"), A.a(quest.getName())));
                List<String> list = new ArrayList<>();
                for (String x : abc) {
                    list.add(StringFormatting.simpleColorFormat(x));
                }
                meta.setLore(list);
                is.setItemMeta(meta);
                this.set(i, is, (HotBarClick) -> {
                });
            }
            i++;
        }
    }

    public void addControls() {
        //46 47 48 | 49 50 51 | 52 53 54
        //45 46 47 | 48 49 50 | 51 52 53
        if (maxpage > 0) {
            if (page > 0) {
                this.set(45, lastpage, (HotBarClick) -> {
                    page = page - 1;
                    this.loadPage();
                });
            }
            for (int i = 46; i < 53; i++) {
                this.set(i, placeholder, (HotBarClick) -> {
                });
            }
            if (page < maxpage) {
                this.set(53, nextpage, (HotBarClick) -> {
                    page = page + 1;
                    this.loadPage();
                });
            }
        }
    }

    @Override
    public void updateInventory() {
        loadPage();
        this.addControls();
    }

    private int getPerfectSize(int a) {
        if (a % 9 != 0.0) {
            while (a % 9 != 0.0) {
                a++;
            }
        }
        return a;
    }

    private int getPerfectPageCount(int a) {
        if (a % 54 != 0.0) {
            while (a % 54 != 0.0) {
                a++;
            }
        }
        return a / 54;
    }

    private static Config getInvConfig(Player p) {
        if (config == null) {
            config = new Config(getCurrent(p), "QuestInventory.yml");
        }
        return config;
    }

    private static ManageableGame getCurrent(Player p) {
        ManageableGame game = GameApi.getCurrent(p);
        if (game == null) {
            game = GameApi.getCurrent();
        }
        return game;
    }
}
