package me.xkuyax.api.games.achievable;

import lombok.Getter;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

@Getter
public class CountAchievableIncrementEvent extends Event {
    
    private static HandlerList handlerList = new HandlerList();
    private Player gamePlayer;
    private CountAchievable achievable;
    
    public CountAchievableIncrementEvent(Player gamePlayer, CountAchievable achievable) {
        this.gamePlayer = gamePlayer;
        this.achievable = achievable;
    }
    
    public int getProgress() {
        return achievable.get(gamePlayer.getPlayer());
    }
    
    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }
    
    public static HandlerList getHandlerList() {
        return handlerList;
    }
    
}
