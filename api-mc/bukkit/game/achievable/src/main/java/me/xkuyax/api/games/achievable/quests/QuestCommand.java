package me.xkuyax.api.games.achievable.quests;

import lombok.RequiredArgsConstructor;
import org.bukkit.entity.Player;

public class QuestCommand extends GCommand {
    
    private QuestHandler handler;
    
    public QuestCommand() {
        super("quest");
        handler = new GameQuests("general", Quests.values());
    }
    
    @CmdHandler(ignoreargs = true, permission = "general.games.debug", sender = CSender.PLAYER_ONLY)
    public void onNullArgs(Player p, String[] args) {
        handler.notifyFinish(p, Quests.RUN_COMMAND);
        p.sendMessage("perma " + handler.hasFinished(p, Quests.RUN_COMMAND, QuestType.PERMANENT, "general") + "");
        p.sendMessage("quest " + handler.hasFinished(p, Quests.RUN_COMMAND, QuestType.QUEST, "general") + "");
        handler.setFinished(p, Quests.RUN_COMMAND, QuestType.QUEST, "general");
        handler.setFinished(p, Quests.RUN_COMMAND, QuestType.PERMANENT, "general");
        p.sendMessage("perma " + handler.hasFinished(p, Quests.RUN_COMMAND, QuestType.PERMANENT, "general") + "");
        p.sendMessage("quest " + handler.hasFinished(p, Quests.RUN_COMMAND, QuestType.QUEST, "general") + "");
        handler.removeFinished(p, Quests.RUN_COMMAND, QuestType.PERMANENT, "general");
        handler.removeFinished(p, Quests.RUN_COMMAND, QuestType.QUEST, "general");
        p.sendMessage("perma " + handler.hasFinished(p, Quests.RUN_COMMAND, QuestType.PERMANENT, "general") + "");
        p.sendMessage("quest " + handler.hasFinished(p, Quests.RUN_COMMAND, QuestType.QUEST, "general") + "");
    }
    
    @CmdHandler(args = "working", permission = "general.games.debug", sender = CSender.PLAYER_ONLY)
    public void working(Player p, String[] args) {
        p.sendMessage("has quest " + handler.hasQuest(p, Quests.RUN_COMMAND, QuestType.QUEST) + "");
        p.sendMessage("can work " + handler.canWork(p, Quests.RUN_COMMAND, QuestType.QUEST) + "");
        handler.startQuest(p, Quests.RUN_COMMAND, QuestType.QUEST);
        p.sendMessage("has quest " + handler.hasQuest(p, Quests.RUN_COMMAND, QuestType.QUEST) + "");
        p.sendMessage("can work " + handler.canWork(p, Quests.RUN_COMMAND, QuestType.QUEST) + "");
        handler.endQuest(p, Quests.RUN_COMMAND, QuestType.QUEST);
        p.sendMessage("has quest " + handler.hasQuest(p, Quests.RUN_COMMAND, QuestType.QUEST) + "");
        p.sendMessage("can work " + handler.canWork(p, Quests.RUN_COMMAND, QuestType.QUEST) + "");
    }
    
    @CmdHandler(args = "start", permission = "general.games.debug", sender = CSender.PLAYER_ONLY)
    public void start(Player p, String[] args) {
        handler.startQuest(p, Quests.RUN_COMMAND, QuestType.QUEST);
        p.sendMessage(handler.hasFinished(p, Quests.RUN_COMMAND, QuestType.PERMANENT, "general") + "");
    }
    
    private static final Config config = new Config("Bedwars", "quests.yml", true);
    
    @RequiredArgsConstructor
    private enum Quests implements Quest {
        RUN_COMMAND;
        
        @Override
        public String getName() {
            return toString();
        }
        
        @Override
        public String getDescription() {
            return config.getString("Quests." + getName() + ".desc.de", getName());
        }
    }
}
