package me.xkuyax.api.games.achievable;

import lombok.Getter;
import org.bukkit.event.Event;

public abstract class AchievableEvent extends Event {
    
    @Getter
    private Achievable achievable;
    @Getter
    private ManageableGame game;
    
    public AchievableEvent(Achievable achievable, ManageableGame game) {
        this.achievable = achievable;
        this.game = game;
    }
}
