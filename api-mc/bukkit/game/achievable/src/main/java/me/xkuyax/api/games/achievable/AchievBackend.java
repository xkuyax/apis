package me.xkuyax.api.games.achievable;

public interface AchievBackend {
    
    int getGameID(String game);
    
    int getAchievID(String name, int gameID);
    
    int getPlayerID(String uuid);
    
    void insertIntoHistory(String uuid, AchievableState state, int achievid, int gameid, int playerid);
    
    void insertIntoHistory(String uuid, Achievable achievable, AchievableState state, String gameName);
}
