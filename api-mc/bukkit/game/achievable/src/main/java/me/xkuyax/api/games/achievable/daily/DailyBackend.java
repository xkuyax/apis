package me.xkuyax.api.games.achievable.daily;

import lombok.RequiredArgsConstructor;
import redis.clients.jedis.JedisCluster;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Random;

@RequiredArgsConstructor
public class DailyBackend {
    
    private static final Random random = new Random();
    private Calendar calendar = new GregorianCalendar();
    private final String gameName;
    private final List<String> achievables;
    
    public DailyChallenge getDailyChallenge(User user) {
        String lastDay = user.getKey("dailyAchievable:" + gameName + ":day");
        if (lastDay == null || !lastDay.equalsIgnoreCase(getToday() + "")) {
            setToday(user);
        }
        int progress = Utils.paste(user.getKey("dailyAchievable:" + gameName + ":progress"), 0);
        String day = user.getKey("dailyAchievable:" + gameName + ":day");
        String achievable = user.getKey("dailyAchievable:" + gameName + ":achievable");
        return new DailyChallenge(achievable, day, progress, isFinished(user));
    }
    
    public boolean isFinished(User user) {
        return Boolean.parseBoolean(user.getKey("dailyAchievable:" + gameName + ":finished"));
    }
    
    public void setFinished(User user, boolean finished) {
        user.setKey("dailyAchievable:" + gameName + ":finished", finished + "");
        user.saveKeys();
    }
    
    public void setProgess(User user, int progess) {
        user.setKey("dailyAchievable:" + gameName + ":progress", progess + "");
        user.saveKeys();
    }
    
    private void setToday(User user) {
        user.setKey("dailyAchievable:" + gameName + ":progress", "0");
        user.setKey("dailyAchievable:" + gameName + ":day", getToday());
        user.setKey("dailyAchievable:" + gameName + ":achievable", getGlobalChallenge());
        setFinished(user, false);
        user.saveKeys();
    }
    
    private String getGlobalChallenge() {
        JedisCluster jedis = RedisBukkit.a().getJedis();
        String existing = jedis.hget("dailyAchievable:" + gameName, getToday());
        if (existing == null) {
            existing = getRandomAchievable();
            jedis.hset("dailyAchievable:" + gameName, getToday(), existing);
        }
        return existing;
    }
    
    private String getRandomAchievable() {
        return achievables.get(random.nextInt(achievables.size()));
    }
    
    private String getToday() {
        return calendar.get(Calendar.YEAR) + ":" + calendar.get(Calendar.DAY_OF_YEAR);
    }
    
}
