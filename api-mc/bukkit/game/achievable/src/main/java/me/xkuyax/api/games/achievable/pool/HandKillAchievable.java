package me.xkuyax.api.games.achievable.pool;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.PlayerDeathEvent;

public class HandKillAchievable extends CraftAchievable {
    
    public HandKillAchievable(ManageableGame game) {
        super(game);
    }
    
    @EventHandler
    public void onEntityDamageByEntityEvent(EntityDamageEvent ed) {
        if (ed instanceof EntityDamageByEntityEvent) {
            EntityDamageByEntityEvent e = (EntityDamageByEntityEvent) ed;
            if (e.getEntity() instanceof Player && e.getDamager() instanceof Player) {
                Player p = (Player) e.getEntity();
                Player c = (Player) e.getDamager();
                if (c.getItemInHand() == null || c.getItemInHand().getType() == Material.AIR) {
                    Utils.saveMetaData(p, "punched", true);
                    return;
                }
            }
        }
        Utils.saveMetaData(ed.getEntity(), "punched", false);
    }
    
    @EventHandler
    public void onPlayerDeathEvent(PlayerDeathEvent e) {
        if (game.isInGame(e.getEntity())) {
            DamageResult dr = GameUtils.getLastDamager(e.getEntity(), game);
            Object o = Utils.getMetaData("punched", e.getEntity());
            if (dr != null && dr.getDamager() != null && o instanceof Boolean && (boolean) o) {
                Player c = dr.getDamager();
                if (isIngame(c) && hasOpen(c)) {
                    achieve(c);
                }
            }
        }
    }
    
    @Override
    public String getName() {
        return "boxer";
    }
}
