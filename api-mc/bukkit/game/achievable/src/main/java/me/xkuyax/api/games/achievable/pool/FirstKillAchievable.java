package me.xkuyax.api.games.achievable.pool;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.PlayerDeathEvent;

public class FirstKillAchievable extends CraftAchievable {
    
    private boolean first = true;
    
    public FirstKillAchievable(ManageableGame game) {
        super(game);
    }
    
    @EventHandler
    public void onPlayerDeathEvent(PlayerDeathEvent e) {
        Player p = e.getEntity();
        if (game.isInGame(p)) {
            DamageResult dr = GameUtils.getLastDamager(p, game);
            if (dr != null && dr.getDamager() != null && first) {
                Player c = dr.getDamager();
                if (game.isInGame(c.getName()) && hasOpen(c)) {
                    achieve(c);
                }
            }
            first = false;
        }
    }
    
    @Override
    public String getName() {
        return "firstKill";
    }
}
