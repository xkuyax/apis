package me.xkuyax.api.games.achievable.quests;

import lombok.Getter;
import org.bukkit.entity.Player;
import org.bukkit.event.HandlerList;
import org.bukkit.event.player.PlayerEvent;

@Getter
public class PlayerFinishedQuestEvent extends PlayerEvent {
    
    private static final HandlerList handlerList = new HandlerList();
    private Quest quest;
    private QuestType type;
    
    public PlayerFinishedQuestEvent(Player player, Quest quest, QuestType type) {
        super(player);
        this.quest = quest;
        this.type = type;
    }
    
    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }
    
    public static HandlerList getHandlerList() {
        return handlerList;
    }
}
