package me.xkuyax.api.games.achievable.pool;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerToggleSneakEvent;

public class NoSneakAchievable extends CraftAchievable implements WinAchievable, NoFailAchievable {
    
    public NoSneakAchievable(ManageableGame game) {
        super(game);
    }
    
    @EventHandler
    public void onPlayerToggleSneakEvent(PlayerToggleSneakEvent e) {
        if (game.isInGame(e.getPlayer())) {
            Player p = e.getPlayer();
            if (hasOpen(p)) {
                if (e.isSneaking()) {
                    failed(p);
                }
            }
        }
    }
    
    @Override
    public String getName() {
        return "hunger";
    }
}
