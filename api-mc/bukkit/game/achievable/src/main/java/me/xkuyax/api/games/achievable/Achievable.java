package me.xkuyax.api.games.achievable;

import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import java.util.Map;

public interface Achievable extends Listener {
    
    boolean isIngame(Player p);
    
    boolean hasOpen(Player p);
    
    boolean isIngame(GamePlayer p);
    
    boolean hasOpen(GamePlayer p);
    
    void achieve(Player p);
    
    void achieve(GamePlayer gp);
    
    String getName();
    
    AchievableState getState(String name);
    
    void setState(String name, AchievableState state);
    
    void failed(Player p);
    
    void failed(GamePlayer gp);
    
    Config getConfig();
    
    Map<String, AchievableData> getUsers();
    
    AchievableData getData(String name);
}
