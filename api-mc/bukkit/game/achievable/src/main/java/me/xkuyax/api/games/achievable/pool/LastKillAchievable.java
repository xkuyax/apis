package me.xkuyax.api.games.achievable.pool;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.PlayerDeathEvent;

public class LastKillAchievable extends CraftAchievable implements WinAchievable, NoFailAchievable {
    
    public LastKillAchievable(ManageableGame game) {
        super(game);
    }
    
    @EventHandler
    public void onGameStoppedEvent(GameStoppedEvent e) {
    }
    
    @EventHandler
    public void onPlayerDeathEvent(PlayerDeathEvent e) {
        if (game.isInGame(e.getEntity())) {
            DamageResult dr = GameUtils.getLastDamager(e.getEntity(), game);
            long alive = game.getPlayers().stream().filter(GamePlayer::isAlive).count();
            String except = "";
            if (alive == 1) {
                if (dr != null && dr.getDamager() != null) {
                    Player c = dr.getDamager();
                    except = c.getName();
                }
                final String ex = except;
                getUsers().values().stream().filter(ad -> !ad.getName().equals(ex)).forEach(ad -> failed(Bukkit.getPlayer(ad.getName())));
            }
        }
    }
    
    @Override
    public String getName() {
        return "lastkill";
    }
}
