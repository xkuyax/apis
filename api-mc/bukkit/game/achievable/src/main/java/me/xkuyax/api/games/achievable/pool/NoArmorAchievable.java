package me.xkuyax.api.games.achievable.pool;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;

public class NoArmorAchievable extends CraftAchievable implements NoFailAchievable, WinAchievable {
    
    public NoArmorAchievable(ManageableGame game) {
        super(game);
    }
    
    @EventHandler
    public void onPlayerDeathEvent(PlayerDeathEvent e) {
        Player p = e.getEntity();
        if (game.isInGame(p.getName())) {
            DamageResult dr = GameUtils.getLastDamager(p, game);
            if (dr != null && dr.getDamager() != null) {
                Player c = dr.getDamager();
                if (hasOpen(c)) {
                    for (ItemStack is : c.getEquipment().getArmorContents()) {
                        if (is != null && is.getType() != Material.AIR) {
                            failed(c);
                            return;
                        }
                    }
                }
            }
        }
    }
    
    @Override
    public String getName() {
        return "noarmor";
    }
}
