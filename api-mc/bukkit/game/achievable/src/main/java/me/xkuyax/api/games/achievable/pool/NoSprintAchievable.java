package me.xkuyax.api.games.achievable.pool;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerMoveEvent;

public class NoSprintAchievable extends CraftAchievable implements NoFailAchievable, WinAchievable {
    
    public NoSprintAchievable(ManageableGame game) {
        super(game);
    }
    
    @EventHandler
    public void onPlayerMoveEvent(PlayerMoveEvent e) {
        Player p = e.getPlayer();
        if (isIngame(p)) {
            if (hasOpen(p)) {
                if (p.isSprinting()) {
                    failed(p);
                }
            }
        }
    }
    
    @Override
    public String getName() {
        return "nosprint";
    }
}
