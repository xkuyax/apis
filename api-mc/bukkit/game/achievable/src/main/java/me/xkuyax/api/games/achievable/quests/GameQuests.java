package me.xkuyax.api.games.achievable.quests;

import lombok.Getter;
import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Getter
public class GameQuests implements QuestHandler {
    
    public static final String KEY_FINISHED = "finished";
    public static final String KEY_WORKING = "working";
    public static final String KEY_NOTSET = "notset";
    private Set<Quest> quests = new HashSet<>();
    private String game;
    private String keyPrefix;
    
    public GameQuests(String game, Collection<Quest> quests) {
        this.game = game;
        keyPrefix = "game:" + game + ":quest:";
        quests.forEach(q -> this.quests.add(q));
        for (Quest quest : quests) {
            int fail = 0;
            for (Quest c : quests) {
                if (c.getName().equals(quest.getName())) {
                    fail++;
                }
                if (fail > 1) {
                    throw new IllegalArgumentException("Not unique names " + quest.getName());
                }
            }
        }
        Bukkit.getScheduler().runTaskAsynchronously(General.a(), () -> new GameIO.GameSaver(quests, this.game));
    }
    
    public GameQuests(String game, Quest[] quests) {
        this(game, Arrays.asList(quests));
    }
    
    @Override
    public void notifyFinish(Player p, Quest quest) {
        notNull(p, quest);
        User user = ViceviceApi.getOnlineUser(p);
        for (QuestType type : QuestType.values()) {
            if (canWork(p, quest, type)) {
                if (!hasFinished(p, quest, type, game)) {
                    Bukkit.getPluginManager().callEvent(new PlayerFinishedQuestEvent(p, quest, type));
                }
                user.setKey(keyPrefix + quest.getName() + ":" + type.toString(), System.currentTimeMillis() + "");
            }
        }
        user.saveKeys();
    }
    
    @Override
    public boolean canWork(Player p, Quest quest, QuestType type) {
        notNull(p, quest);
        User user = ViceviceApi.getOnlineUser(p);
        if (type == QuestType.PERMANENT) {
            return true;
        }
        return getStatus(user, quest, type).equals(KEY_WORKING);
    }
    
    @Override
    public boolean hasFinished(Player p, Quest quest, QuestType type, String id) {
        notNull(p, quest, type, id);
        User user = ViceviceApi.getOnlineUser(p);
        if (user.hasKey(keyPrefix + quest.getName() + ":" + type.toString() + ":" + id)) {
            return false;
        }
        return (user.hasKey(keyPrefix + quest.getName() + ":" + type.toString()));
    }
    
    public void setFinished(Player p, Quest quest, QuestType type, String id) {
        notNull(p, quest, type, id);
        User user = ViceviceApi.getOnlineUser(p);
        user.setKey(keyPrefix + quest.getName() + ":" + type.toString() + ":" + id, KEY_FINISHED);
        user.saveKeys();
    }
    
    @Override
    public void removeFinished(Player p, Quest quest, QuestType type, String id) {
        notNull(p, quest, type, id);
        User user = ViceviceApi.getOnlineUser(p);
        user.setKey(keyPrefix + quest.getName() + ":" + type.toString() + ":" + id, null);
        user.saveKeys();
    }
    
    @Override
    public boolean hasQuest(Player p, Quest quest, QuestType type) {
        notNull(p, quest, type);
        return getStatus(ViceviceApi.getOnlineUser(p), quest, type).equals(KEY_WORKING);
    }
    
    @Override
    public boolean startQuest(Player p, Quest quest, QuestType type) {
        notNull(p, quest, type);
        User user = ViceviceApi.getOnlineUser(p);
        String stat = getStatus(user, quest, type);
        if (stat.equals(KEY_NOTSET)) {
            setStatus(user, quest, type, KEY_WORKING);
            return true;
        }
        return false;
    }
    
    @Override
    public boolean endQuest(Player p, Quest quest, QuestType type) {
        notNull(p, quest, type);
        User user = ViceviceApi.getOnlineUser(p);
        String stat = getStatus(user, quest, type);
        if (stat.equals(KEY_WORKING)) {
            setStatus(user, quest, type, KEY_NOTSET);
            return true;
        }
        return false;
    }
    
    private void setStatus(User user, Quest quest, QuestType type, String n) {
        user.setKey(keyPrefix + quest.getName() + ":" + type.toString() + ":status", n);
        user.saveKeys();
    }
    
    private String getStatus(User user, Quest quest, QuestType type) {
        String s = user.getKey(keyPrefix + quest.getName() + ":" + type.toString() + ":status");
        return s == null ? KEY_NOTSET : s;
    }
    
    private void notNull(Player p, Quest quest) {
        Validate.notNull(p, "Player cant be null!");
        Validate.notNull(quest, "Quest cant be null!");
    }
    
    private void notNull(Player p, Quest quest, QuestType type) {
        Validate.notNull(p, "Player cant be null!");
        Validate.notNull(quest, "Quest cant be null!");
        Validate.notNull(type, "Type cant be null");
    }
    
    private void notNull(Player p, Quest quest, QuestType type, String id) {
        Validate.notNull(p, "Player cant be null!");
        Validate.notNull(quest, "Quest cant be null!");
        Validate.notNull(type, "Type cant be null");
        Validate.notNull(id, "Id cant be null!");
    }
}
