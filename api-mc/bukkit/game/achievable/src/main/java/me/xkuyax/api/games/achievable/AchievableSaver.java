package me.xkuyax.api.games.achievable;

import org.bukkit.Bukkit;

import java.util.UUID;

public class AchievableSaver {
    
    private PooledMysqlConnection mysql;
    private AchievableManager manager;
    private AchievSQLBackend backend;
    
    public AchievableSaver(Config config, AchievableManager manager) {
        mysql = PooledMysqlConnection.get(config, "Mysql");
        this.manager = manager;
        this.backend = new AchievSQLBackend(mysql);
    }
    
    public void saveUserAsync(String name) {
        Bukkit.getScheduler().runTaskAsynchronously(General.a(), () -> saveUser(name));
    }
    
    public void saveUser(String name) {
        for (Achievable achievable : manager.getAchievables()) {
            AchievableState state = achievable.getState(name);
            UUID uuid = UResolver.getUUIDFromPlayer(name);
            if (state == AchievableState.WORKING) {
                saveState(uuid, achievable, AchievableState.FAILED);
            } else {
                if (state != AchievableState.NOT_WORKING) {
                    saveState(uuid, achievable, state);
                }
            }
        }
    }
    
    private void saveState(UUID uuid, Achievable achievable, AchievableState state) {
        backend.insertIntoHistory(uuid.toString(), achievable, state, manager.getGame().getName());
    }
}
