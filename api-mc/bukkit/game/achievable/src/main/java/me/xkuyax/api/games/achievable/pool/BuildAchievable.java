package me.xkuyax.api.games.achievable.pool;

import lombok.Getter;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockPlaceEvent;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class BuildAchievable extends CraftAchievable implements CountAchievable {
    
    @Getter
    private Map<UUID, Integer> counts = new HashMap<>();
    
    public BuildAchievable(ManageableGame game) {
        super(game);
    }
    
    @EventHandler
    public void onBlockPlaceEvent(BlockPlaceEvent e) {
        if (isIngame(e.getPlayer())) {
            if (hasOpen(e.getPlayer())) {
                increment(e.getPlayer());
            }
        }
    }
    
    @Override
    public String getName() {
        return "baumeister";
    }
}
