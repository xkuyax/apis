package me.xkuyax.api.games.achievable.daily;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.List;

public class DailyModule extends Module implements Listener {
    
    private static int DASHES = 50;
    private DailyBackend backend;
    private ManageableGame game;
    private String gameName;
    private List<String> achievables;
    private Config config;
    
    @Override
    public boolean load() {
        Bukkit.getScheduler().runTaskLater(getGeneral(), () -> {
            if (GameApi.getCurrent() != null) {
                game = GameApi.getCurrent();
                if (game.getOptions().isDailyModule()) {
                    gameName = game.getName();
                    config = game.getAchievableManager().getConfig();
                    achievables = config.getStringList("DailyChallenges.achievables", true, "kills5", "wins5", "kills20", "wins20");
                    backend = new DailyBackend(gameName, achievables);
                    Bukkit.getPluginManager().registerEvents(this, getGeneral());
                    Bukkit.getOnlinePlayers().forEach(p -> onPlayerJoinEvent(new PlayerJoinEvent(p, null)));
                }
            }
        }, 1);
        return true;
    }
    
    @EventHandler
    public void onPlayerJoinEvent(PlayerJoinEvent e) {
        Player p = e.getPlayer();
        User user = ViceviceApi.getOnlineUser(e.getPlayer());
        DailyChallenge dailyChallenge = backend.getDailyChallenge(user);
        String s = "Heigth";
        Location location = config.getLocation("hologram.location");
        Hologram hologram = HologramsAPI.createHologram(getGeneral(), location);
        hologram.getVisibilityManager().setVisibleByDefault(false);
        hologram.getVisibilityManager().showTo(e.getPlayer());
        if (!dailyChallenge.isFinished()) {
            CountAchievable achievable = (CountAchievable) game.getAchievableManager().getAchievable(dailyChallenge.getChallenge());
            List<String> text = config.getTranslatedList("DailyChallenges.hologram." + dailyChallenge.getChallenge(), p, true, dailyChallenge.getChallenge());
            text.forEach(hologram::appendTextLine);
            if (achievable != null) {
                hologram.appendTextLine(getProgressAsString(dailyChallenge.getProgress(), achievable.getRepeat()));
            }
        } else {
            List<String> text = config.getTranslatedList("DailyChallenges.hologram.finished", p, true, "Täglich Challenge", "&4Geschafft!");
            text.forEach(hologram::appendTextLine);
        }
    }
    
    @EventHandler
    public void onGameStartEvent(GameStartedEvent e) {
        for (GamePlayer gp : game.getPlayers()) {
            DailyChallenge challenge = backend.getDailyChallenge(gp.getUser());
            CountAchievable achievable = (CountAchievable) game.getAchievableManager().getAchievable(challenge.getChallenge());
            if (challenge.getProgress() < achievable.getRepeat()) {
                achievable.set(gp.getPlayer(), challenge.getProgress());
                achievable.setState(gp.getName(), AchievableState.WORKING);
            }
        }
    }
    
    @EventHandler
    public void onAchievableFinishedEvent(AchievableFinishedEvent e) {
        Achievable achievable = e.getAchievable();
        if (this.achievables.contains(achievable.getName())) {
            e.setSendMessage(false);
            GamePlayer gp = e.getGamePlayer();
            Player p = gp.getPlayer();
            if (!backend.isFinished(ViceviceApi.getOnlineUser(p))) {
                config.sendBothTitleWithRadius(gp, "DailyChallengeTitle.sucess", AchievableManager.PLAYER_TITLE_RADIUS);
                int cookies = ViceviceApi.addOnlineSimpleCookies(p, 150);
                Message.send(p, "Achievable fertig ", "DailyChallenge.success", A.a("%c%"), A.a(cookies + ""));
                backend.setFinished(ViceviceApi.getOnlineUser(p), true);
            }
        }
    }
    
    @EventHandler
    public void onAchievableFailedEvent(AchievableFailedEvent e) {
        Achievable achievable = e.getAchievable();
        if (this.achievables.contains(achievable.getName())) {
            e.setSendMessage(false);
        }
    }
    
    @EventHandler
    public void onCountAchievable(CountAchievableIncrementEvent e) {
        CountAchievable achievable = e.getAchievable();
        Player p = e.getGamePlayer();
        if (achievables.contains(achievable.getName())) {
            if (achievable.getState(p.getName()) == AchievableState.WORKING) {
                System.out.println("saving progress");
                backend.setProgess(ViceviceApi.getOnlineUser(p), achievable.get(p));
            } else {
                System.out.println("state " + achievable.getState(p.getName()));
            }
        }
    }
    
    @EventHandler
    public void onPlayerQuitEvent(PlayerQuitEvent e) {
        Player p = e.getPlayer();
        game.getAchievableManager().getAchievables().forEach(achievable -> {
            if (achievables.contains(achievable.getName())) {
                if (achievable.getState(p.getName()) == AchievableState.WORKING) {
                    CountAchievable countAchievable = (CountAchievable) achievable;
                    System.out.println("saving progress 2");
                    backend.setProgess(ViceviceApi.getOnlineUser(p), countAchievable.get(p));
                }
            }
        });
    }
    
    private String getProgressAsString(int progress, int max) {
        int a = (int) Math.round(map(progress, 0, max, 0, DASHES));
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < DASHES; i++) {
            sb.append(i < a ? "§a:" : "§7:");
        }
        return sb.toString();
    }
    
    private double map(double x, double in_min, double in_max, double out_min, double out_max) {
        return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
    }
    
    @Override
    public boolean disable() {
        return false;
    }
    
    @Override
    public String getName() {
        return "dailyModule";
    }
    
    @Data
    @AllArgsConstructor
    public static class DailyChallenge {
        
        private String challenge;
        private String day;
        private int progress;
        private boolean finished;
        
    }
}
