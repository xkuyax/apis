package me.xkuyax.api.games.achievable.stats;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

public class StatsHandler implements AchievableHandler, Listener {
    
    private AchievableManager manager;
    
    public StatsHandler(AchievableManager manager) {
        Bukkit.getPluginManager().registerEvents(this, General.a());
        this.manager = manager;
    }
    
    @Override
    public boolean canHandle(Achievable achievable) {
        return achievable instanceof StatsAchievable;
    }
    
    @Override
    public void onStop(Achievable achievable, AchievableData ad, Player p, GamePlayer gp) {
        ManageableGame game = gp.getGame();
        if (game.getStats() == null) {
            System.out.println("You cant use Stats Achievable when your game doesn't have stats!");
            return;
        }
        StatsAchievable sa = (StatsAchievable) achievable;
        if (sa.getDepends().stream().allMatch(sd -> sd.canAchieve(gp))) {
            MysqlStats stats = game.getStats();
            StatValue statValue = stats.getValue(ad.getName(), sa.getStatsType());
            if (statValue != null && statValue.getValue() instanceof Integer) {
                int value = (int) statValue.getValue();
                if (value >= sa.getRequired()) {
                    achievable.achieve(p);
                }
            }
        }
    }
    
    @EventHandler
    public void onPlayerStatsEvent(PlayerStatsEvent event) {
        Player player = event.getPlayer();
        String statsName = event.getStatsName();
        manager.getAchievables().forEach(achievable -> {
            if (achievable instanceof StatsAchievable) {
                StatsAchievable statsAchievable = (StatsAchievable) achievable;
                if (statsAchievable.getStatsType().toString().equalsIgnoreCase(statsName)) {
                    if (achievable.getState(player.getName()).equals(AchievableState.WORKING)) {
                        statsAchievable.increment(player);
                    }
                }
            }
        });
    }
}