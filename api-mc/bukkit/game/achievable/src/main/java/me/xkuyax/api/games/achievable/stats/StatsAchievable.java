package me.xkuyax.api.games.achievable.stats;

import java.util.Collections;
import java.util.List;

public interface StatsAchievable extends Achievable, CountAchievable {
    
    StatsDependable ALIVE = GamePlayer::isAlive;
    List<StatsDependable> depends = Collections.singletonList(ALIVE);
    
    StatPointEnum getStatsType();
    
    default int getRequired() {
        return getConfig().getInt(getName() + ".required", 2);
    }
    
    default List<StatsDependable> getDepends() {
        return depends;
    }
    
    /**
     * Interface to just allow stats achievables at the end of the game when the player is alive or something else
     */
    interface StatsDependable {
        
        boolean canAchieve(GamePlayer gp);
    }
}