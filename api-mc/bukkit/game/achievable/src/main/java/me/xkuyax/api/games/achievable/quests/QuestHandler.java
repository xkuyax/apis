package me.xkuyax.api.games.achievable.quests;

import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

public interface QuestHandler extends Listener {
    
    /**
     * Meldet der API, dass der Player @param p eine Quest abgeschlossen hat
     *
     * @param p
     * @param quest
     */
    void notifyFinish(Player p, Quest quest);
    
    /**
     * @param p
     * @param quest
     * @return ob der Spieler die Quest abschliesen kann
     */
    boolean canWork(Player p, Quest quest, QuestType type);
    
    /**
     * @param p
     * @param quest
     * @param type
     * @return ob der spieler eine Quest aktiv hat
     */
    boolean hasQuest(Player p, Quest quest, QuestType type);
    
    boolean startQuest(Player p, Quest quest, QuestType type);
    
    boolean endQuest(Player p, Quest quest, QuestType type);
    
    void removeFinished(Player p, Quest quest, QuestType type, String id);
    
    void setFinished(Player p, Quest quest, QuestType type, String id);
    
    boolean hasFinished(Player p, Quest quest, QuestType type, String id);
    
    enum QuestType {
        QUEST,
        // Temporär (in einer woche/ 1 monat/ 1 jahr..)
        PERMANENT // Globales Achievment
    }
}
