package me.xkuyax.api.games.achievable.pool;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.PlayerDeathEvent;

public class KillStreakAchievable extends CraftAchievable implements RepeatAchievable {
    
    public KillStreakAchievable(ManageableGame game) {
        super(game);
    }
    
    @EventHandler
    public void onPlayerDeathEvent(PlayerDeathEvent e) {
        if (game.isInGame(e.getEntity())) {
            DamageResult dr = GameUtils.getLastDamager(e.getEntity(), game);
            if (dr != null && dr.getDamager() != null) {
                Player c = dr.getDamager();
                if (isIngame(c) && hasOpen(c)) {
                    achieve(c);
                }
            }
        }
    }
    
    @Override
    public String getName() {
        return "killstreak";
    }
}
