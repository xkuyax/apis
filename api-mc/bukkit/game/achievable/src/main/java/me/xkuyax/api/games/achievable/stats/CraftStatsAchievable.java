package me.xkuyax.api.games.achievable.stats;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Getter
public class CraftStatsAchievable extends CraftAchievable implements StatsAchievable {
    
    private final String name;
    private final StatPointEnum statsType;
    private final Map<UUID, Integer> counts = new HashMap<>();
    private final int required;
    
    public CraftStatsAchievable(ManageableGame game, String name, StatPointEnum statPointEnum, int required) {
        super(game);
        this.statsType = statPointEnum;
        this.name = name;
        this.required = required;
    }
    
    @Override
    public int getRepeat() {
        return required;
    }
}

