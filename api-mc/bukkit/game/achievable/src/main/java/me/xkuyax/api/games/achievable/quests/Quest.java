package me.xkuyax.api.games.achievable.quests;

public interface Quest {
    
    String getDescription();
    
    String getName();
}
