package me.xkuyax.api.games.achievable;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

public class Handlers {
    
    public interface AchievableHandler {
        
        boolean canHandle(Achievable achievable);
        
        default void onStop(Achievable achievable, AchievableData ad, Player p, GamePlayer gp) {
        }
        
        default boolean onAchieve(Achievable achievable, AchievableData ad, Player p, GamePlayer gp) {
            return true;
        }
        
        default void onStart(Achievable achievable, AchievableData ad, Player p, GamePlayer gp) {
        }
    }
    
    public static class TimeHandler implements AchievableHandler {
        
        @Override
        public boolean canHandle(Achievable achievable) {
            return achievable instanceof TimeAchievable;
        }
        
        @Override
        public void onStart(Achievable achievable, AchievableData ad, Player p, GamePlayer gamePlayer) {
            TimeAchievable timeAchievable = (TimeAchievable) achievable;
            ManageableGame game = gamePlayer.getGame();
            Bukkit.getScheduler().runTaskLater(General.a(), () -> game.getAlivePlayersList().forEach(gp -> {
                if (achievable.hasOpen(gp)) {
                    achievable.failed(gp);
                }
            }), timeAchievable.getTime() * 20);
        }
    }
    
    public static class DefaultHandler implements AchievableHandler {
        
        @Override
        public boolean canHandle(Achievable achievable) {
            return true;
        }
        
        @Override
        public void onStop(Achievable achievable, AchievableData ad, Player p, GamePlayer gp) {
            if (ad.getState() == AchievableState.WORKING) {
                achievable.setState(p.getName(), AchievableState.FAILED);
            }
        }
    }
    
    public static class RepeatHandler implements AchievableHandler {
        
        private Map<String, Map<Achievable, Integer>> repeatCounts = new HashMap<>();
        private boolean onStop = false;
        
        @Override
        public boolean canHandle(Achievable achievable) {
            return achievable instanceof RepeatAchievable;
        }
        
        @Override
        public boolean onAchieve(Achievable achievable, AchievableData ad, Player p, GamePlayer gp) {
            RepeatAchievable rp = (RepeatAchievable) achievable;
            Map<Achievable, Integer> a = repeatCounts.computeIfAbsent(p.getName(), s -> new HashMap<>());
            int i = a.computeIfAbsent(achievable, b -> 0) + 1;
            if (i != rp.getRepeat()) {
                a.put(achievable, i);
                repeatCounts.put(p.getName(), a);
                if (i == rp.getRepeat()) {
                    if (!(achievable instanceof WinAchievable)) {
                        rp.achieve(gp);
                    } else {
                        return false;
                    }
                } else {
                    rp.setState(p.getName(), AchievableState.WORKING);
                    return false;
                }
            } else {
                if (i >= rp.getRepeat() && !onStop) {
                    return false;
                }
            }
            return true;
        }
        
        @Override
        public void onStop(Achievable achievable, AchievableData ad, Player p, GamePlayer gp) {
            onStop = true;
        }
    }
    
    public static class WinHandler implements AchievableHandler {
        
        @Override
        public boolean canHandle(Achievable achievable) {
            return achievable.getConfig().getBoolean("Achievables." + achievable.getName() + ".winHandler", false) || achievable instanceof WinAchievable;
        }
        
        @Override
        public void onStop(Achievable achievable, AchievableData ad, Player p, GamePlayer gp) {
            if (ad.getState() == AchievableState.WORKING) {
                ad.setState(AchievableState.FAILED);
            }
            if (ad.getState() == AchievableState.FINISHED) {
                if (gp.isAlive()) {
                    ad.setState(AchievableState.FINISHED);
                } else {
                    ad.setState(AchievableState.FAILED);
                }
            }
        }
    }
    
    public static class NoFailHandler implements AchievableHandler {
        
        @Override
        public boolean canHandle(Achievable achievable) {
            return achievable instanceof NoFailAchievable;
        }
        
        @Override
        public void onStop(Achievable achievable, AchievableData ad, Player p, GamePlayer gp) {
            if (ad.getState() == AchievableState.WORKING) {
                ad.setState(AchievableState.FINISHED);
            }
            if (ad.getState() == AchievableState.FINISHED) {
                System.out.println("Achievable " + achievable.getName() + " has in onStop finished as state! ");
            }
        }
    }
}
