package me.xkuyax.api.games.achievable.quests;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import redis.clients.jedis.JedisCluster;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class GameIO {
    
    private GameIO() {
    }
    
    public static final TypeToken<ArrayList<Quest>> questArray = new TypeToken<ArrayList<Quest>>() {};
    
    @RequiredArgsConstructor
    @Getter
    public static class GameSaver {
        
        private final Collection<Quest> quests;
        private final String game;
        
        public void save() {
            RedisBukkit rb = RedisBukkit.a();
            JedisCluster jedis = rb.getJedis();
            Gson gson = new Gson();
            jedis.hset("general:quests", game, gson.toJson(new ArrayList<>(quests), questArray.getType()));
        }
    }
    
    @Getter
    @RequiredArgsConstructor
    public static class GameLoader {
        
        private final String game;
        
        public List<Quest> load() {
            RedisBukkit rb = RedisBukkit.a();
            JedisCluster jedis = rb.getJedis();
            Gson gson = new Gson();
            if (jedis.hexists("general:quests", game)) {
                String data = jedis.hget("general:quests", game);
                try {
                    return gson.fromJson(data, questArray.getType());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
    }
}
