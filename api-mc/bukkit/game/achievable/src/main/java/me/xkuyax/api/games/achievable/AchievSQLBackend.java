package me.xkuyax.api.games.achievable;

import java.sql.*;

public class AchievSQLBackend implements AchievBackend {
    
    private PooledMysqlConnection mysql;
    
    public AchievSQLBackend(PooledMysqlConnection mysql) {
        this.mysql = mysql;
    }
    
    public int getMapID(int gameId, Map map) {
        return getMapID(gameId, map.getName(), map.getEngDisplayname());
    }
    
    public int getGameTeamID(String teamName) {
        return mysql.queryKeyInsert("select `teamId` from `game_teams` where `name` = ? ", "insert into `game_teams` (`name`) values (?)", ps -> ps
                .setString(1, teamName), ps -> ps.setString(1, teamName));
    }
    
    public int getGroupID(String group) {
        String finalGroup = group == null || group.isEmpty() || group.equals(" ") ? "gast" : group.toLowerCase();
        return mysql.queryKeyInsert("select `groupid` from `groups` where `name` = ? ", "insert into `groups` (`name`) values (?)", ps -> ps
                .setString(1, finalGroup), ps -> ps.setString(1, finalGroup));
    }
    
    public int getMapID(int game, String map, String display) {
        return mysql
                .queryKeyInsert("select `mapid` from `maps` where `gameid` = ? and `name` like ?", "insert into `maps` (`gameid`,`name`,`display`) values (?,?,?)", ps -> {
                    ps.setInt(1, game);
                    ps.setString(2, map);
                }, ps -> {
                    ps.setInt(1, game);
                    ps.setString(2, map);
                    ps.setString(3, display);
                });
    }
    
    @Override
    public int getGameID(String game) {
        try (Connection connection = mysql.getConnection(); PreparedStatement ps = connection.prepareStatement("select `gameid` from `games` where `name` like ? ")) {
            ps.setString(1, game);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return rs.getInt(1);
                } else {
                    try (PreparedStatement is = connection.prepareStatement("insert into `games` (`name`) values (?)", Statement.RETURN_GENERATED_KEYS)) {
                        is.setString(1, game);
                        is.execute();
                        try (ResultSet key = is.getGeneratedKeys()) {
                            if (key.next()) {
                                return key.getInt(1);
                            }
                        }
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
    
    @Override
    public int getAchievID(String name, int gameID) {
        try (Connection connection = mysql.getConnection(); PreparedStatement ps = connection
                .prepareStatement("select `achievid` from `achievid` where `gameID` like ? and `name` like ?")) {
            ps.setInt(1, gameID);
            ps.setString(2, name);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return rs.getInt(1);
                } else {
                    try (PreparedStatement is = connection.prepareStatement("insert into `achievid` (`name`,`gameid`) values (?,?)", Statement.RETURN_GENERATED_KEYS)) {
                        is.setString(1, name);
                        is.setInt(2, gameID);
                        is.execute();
                        try (ResultSet key = is.getGeneratedKeys()) {
                            if (key.next()) {
                                return key.getInt(1);
                            }
                        }
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
    
    @Override
    public int getPlayerID(String uuid) {
        try (Connection connection = mysql.getConnection(); PreparedStatement ps = connection.prepareStatement("select `playerid` from `player` where `uuid` like ? ")) {
            ps.setString(1, uuid);
            try (ResultSet rs = ps.executeQuery()) {
                if (rs.next()) {
                    return rs.getInt(1);
                } else {
                    try (PreparedStatement is = connection.prepareStatement("insert into `player` (`uuid`,`name`) values (?,?)", Statement.RETURN_GENERATED_KEYS)) {
                        is.setString(1, uuid);
                        is.setString(2, UResolver.getPlayerFromUUID(uuid));
                        is.execute();
                        try (ResultSet key = is.getGeneratedKeys()) {
                            if (key.next()) {
                                return key.getInt(1);
                            }
                        }
                    }
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
    
    @Override
    public void insertIntoHistory(String uuid, Achievable achievable, AchievableState state, String gameName) {
        int gameID = getGameID(gameName);
        insertIntoHistory(uuid, state, getAchievID(achievable.getName(), gameID), gameID, getPlayerID(uuid));
    }
    
    @Override
    public void insertIntoHistory(String uuid, AchievableState state, int achievid, int gameid, int playerid) {
        try (Connection connection = mysql.getConnection(); PreparedStatement ps = connection
                .prepareStatement("insert into `history` (`playerid`,`gameid`,`achievid`,`state`) values (?,?,?,?) ")) {
            ps.setInt(1, playerid);
            ps.setInt(2, gameid);
            ps.setInt(3, achievid);
            ps.setString(4, state.toString());
            ps.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
