package me.xkuyax.api.games.achievable;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.Map;
import java.util.UUID;

public class Achievables {
    
    public interface TimeAchievable extends Achievable {
        
        default int getTime() {
            return getConfig().getInt("Achievables." + getName() + ".time", 300);
        }
    }
    
    public interface CountAchievable extends Achievable {
        
        Map<UUID, Integer> getCounts();
        
        default void increment(Player p) {
            //+1 since map returns the last value...
            int i = getCounts().put(p.getUniqueId(), get(p) + 1) + 1;
            if (i >= getRepeat()) {
                achieve(p);
            } else {
                Bukkit.getPluginManager().callEvent(new CountAchievableIncrementEvent(p, this));
            }
        }
        
        default void set(Player p, int i) {
            getCounts().put(p.getUniqueId(), i);
        }
        
        default int get(Player p) {
            return getCounts().computeIfAbsent(p.getUniqueId(), u -> 0);
        }
        
        default int getRepeat() {
            return getConfig().getInt("Achievables." + getName() + ".repeats", 3);
        }
    }
    
    public interface NoFailAchievable extends Achievable {}
    
    public interface RepeatAchievable extends Achievable {
        
        default int getRepeat() {
            return getConfig().getInt("Achievables." + getName() + ".repeats", 3);
        }
    }
    
    public interface WinAchievable extends Achievable {}
    
    private Achievables() {
    }
}
