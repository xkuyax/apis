package me.xkuyax.api.games.achievable;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.event.HandlerList;

@Getter
public class AchievableFailedEvent extends AchievableEvent {
    
    @Getter
    private static HandlerList handlerList = new HandlerList();
    @Setter
    private boolean sendMessage = true;
    private GamePlayer gamePlayer;
    
    public AchievableFailedEvent(GamePlayer gamePlayer, Achievable achievable, ManageableGame game) {
        super(achievable, game);
        this.gamePlayer = gamePlayer;
    }
    
    @Override
    public HandlerList getHandlers() {
        return handlerList;
    }
}
