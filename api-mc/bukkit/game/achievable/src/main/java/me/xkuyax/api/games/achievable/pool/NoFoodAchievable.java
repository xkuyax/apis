package me.xkuyax.api.games.achievable.pool;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.FoodLevelChangeEvent;

public class NoFoodAchievable extends CraftAchievable implements WinAchievable, NoFailAchievable {
    
    public NoFoodAchievable(ManageableGame game) {
        super(game);
    }
    
    @EventHandler
    public void onFoodLevelChangeEvent(FoodLevelChangeEvent e) {
        if (e.getEntity() instanceof Player) {
            Player p = (Player) e.getEntity();
            int delta = p.getFoodLevel() - e.getFoodLevel();
            if (delta < 0) {
                if (isIngame(p)) {
                    if (hasOpen(p)) {
                        failed(p);
                    }
                }
            }
        }
    }
    
    @Override
    public String getName() {
        return "hunger";
    }
}
