package me.xkuyax.api.games.achievable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Map;

public abstract class CraftAchievable extends SelfRegisteringListener implements Achievable {
    
    @Getter
    private Map<String, AchievableData> users = new HashMap<>();
    protected ManageableGame game;
    @Getter
    protected final Config config;
    private AchievableManager manager;
    
    public CraftAchievable(ManageableGame game) {
        super(game);
        this.game = game;
        this.config = new Config(game, "achievables.yml");
        this.manager = game.getAchievableManager();
    }
    
    @Override
    public void failed(Player p) {
        failed(game.getPlayer(p));
    }
    
    @Override
    public void failed(GamePlayer gp) {
        setState(gp.getName(), AchievableState.FAILED);
        manager.onFail(gp, this);
    }
    
    @Override
    public void achieve(Player p) {
        achieve(game.getPlayer(p));
    }
    
    @Override
    public void achieve(GamePlayer gp) {
        setState(gp.getName(), AchievableState.FINISHED);
        manager.onAchieve(gp, this);
    }
    
    @Override
    public boolean hasOpen(GamePlayer p) {
        return getState(p.getName()).equals(AchievableState.WORKING);
    }
    
    @Override
    public boolean hasOpen(Player p) {
        return hasOpen(game.getPlayer(p));
    }
    
    @Override
    public void setState(String name, AchievableState state) {
        getData(name).setState(state);
    }
    
    @Override
    public AchievableState getState(String name) {
        return getData(name).getState();
    }
    
    @Override
    public AchievableData getData(String name) {
        return users.computeIfAbsent(name, s -> new AchievableData(AchievableState.NOT_WORKING, name));
    }
    
    @Override
    public boolean isIngame(GamePlayer gp) {
        if (game.isInGame(gp.getName())) {
            if (!gp.isSpectator()) {
                return true;
            }
        }
        return false;
    }
    
    @Override
    public boolean isIngame(Player p) {
        return game.isIngame() && game.isInGame(p.getName()) && isIngame(game.getPlayer(p));
    }
    
    @Data
    @AllArgsConstructor
    public static class AchievableData {
        
        private AchievableState state;
        private String name;
    }
}
