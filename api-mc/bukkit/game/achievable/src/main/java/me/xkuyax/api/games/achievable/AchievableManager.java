package me.xkuyax.api.games.achievable;

import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;

public class AchievableManager extends SelfRegisteringListener {
    
    private static Config mysqlConfig = new Config(General.a(), "mysql-achievable.yml");
    @Getter
    private List<Achievable> achievables = new ArrayList<>();
    @Getter
    private ManageableGame game;
    private String keyString;
    @Getter
    private AchievableSaver saver;
    private List<AchievableHandler> handlers = Arrays
            .asList(new NoFailHandler(), new RepeatHandler(), new DefaultHandler(), new WinHandler(), new StatsHandler(this), new TimeHandler());
    @Getter
    private Config config;
    public static final int PLAYER_TITLE_RADIUS = 100;
    
    public AchievableManager(ManageableGame game) {
        super(game);
        this.game = game;
        this.saver = new AchievableSaver(mysqlConfig, this);
        this.config = new Config(game, "achievables.yml");
        keyString = game.getName() + "_activeChallenge";
    }
    
    public void register(Achievable achievable) {
        System.out.println("register " + achievable);
        System.out.println("register " + achievable.getClass());
        Objects.requireNonNull(achievable, "Achievable cant be null");
        Objects.requireNonNull(achievable.getName(), "Achievable name cant be null");
        achievables.add(achievable);
    }
    
    public void onFail(GamePlayer p, final Achievable achievable) {
        AchievableFailedEvent event = new AchievableFailedEvent(p, achievable, getGame());
        Bukkit.getPluginManager().callEvent(event);
        if (event.isSendMessage()) {
            config.sendBothTitleWithRadius(p, "Tiles.failed", PLAYER_TITLE_RADIUS);
            Message.send(p.getPlayer(), "Achievable failed ", "Achievables.failed");
        }
    }
    
    public void onAchieve(GamePlayer p, final Achievable achievable) {
        System.out.println("on achieve");
        boolean sendMessage = handlers.stream().filter(ah -> ah.canHandle(achievable))
                .allMatch(ah -> ah.onAchieve(achievable, achievable.getData(p.getName()), p.getPlayer(), p));
        AchievableFinishedEvent event = new AchievableFinishedEvent(p, achievable, game);
        Bukkit.getPluginManager().callEvent(event);
        if (sendMessage && event.isSendMessage()) {
            forEach(achievable, h -> h.onAchieve(achievable, achievable.getData(p.getName()), p.getPlayer(), p));
            config.sendBothTitleWithRadius(p, "Tiles.sucess", PLAYER_TITLE_RADIUS);
            disableAchievableForUser(p.getUser());
            Message.send(p.getPlayer(), "Achievable fertig ", "Achievables.success");
        }
    }
    
    public Achievable getAchievable(String name) {
        System.out.println(achievables);
        return achievables.stream().filter(at -> at.getName().equalsIgnoreCase(name)).findAny().orElse(null);
    }
    
    public Achievable getAchievableFromUser(User user) {
        String s = user.getKey(keyString);
        return getAchievable(s);
    }
    
    public void disableAchievableForUser(User user) {
        user.setKey(keyString, "disabled");
        user.saveKeys();
    }
    
    public Achievable getFromUser(String name) {
        for (Achievable achievable : achievables) {
            for (AchievableData ad : achievable.getUsers().values()) {
                if (ad.getState() == AchievableState.FINISHED) {
                    return achievable;
                }
            }
        }
        return null;
    }
    
    @EventHandler
    public void onGameStartEvent(GameStartedEvent e) {
        for (Player p : Bukkit.getOnlinePlayers()) {
            User user = ViceviceApi.getOnlineUser(p);
            Achievable achievable = getAchievableFromUser(user);
            if (achievable != null) {
                achievable.setState(p.getName(), AchievableState.WORKING);
                config.sendBothTitle(game.getPlayer(p), "Tiles.starting." + achievable.getName());
                forEach(achievable, h -> h.onStart(achievable, achievable.getData(p.getName()), p, game.getPlayer(p)));
            }
        }
    }
    
    @EventHandler
    public void onGameStoppedEvent(GameStoppedEvent e) {
        for (Achievable achievable : getAchievables()) {
            achievable.getUsers().forEach((s, ad) -> {
                try {
                    Player p = Bukkit.getPlayer(s);
                    if (p != null) {
                        forEach(achievable, ah -> ah.onStop(achievable, ad, p, game.getPlayer(p)));
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            });
            for (AchievableData ad : achievable.getUsers().values()) {
                try {
                    if (Bukkit.getPlayer(ad.getName()) != null) {
                        if (ad.getState() == AchievableState.FAILED || ad.getState() == AchievableState.WORKING) {
                            onFail(game.getPlayer(ad.getName()), achievable);
                        }
                        if (ad.getState() == AchievableState.FINISHED) {
                            onAchieve(game.getPlayer(ad.getName()), achievable);
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        Bukkit.getScheduler().runTaskAsynchronously(General.a(), () -> game.getPlayers().stream().filter(GamePlayer::isPlayed).forEach(gp -> {
            try {
                saver.saveUser(gp.getName());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }));
    }
    
    private void forEach(Achievable achievable, Consumer<AchievableHandler> p) {
        handlers.forEach(ah -> {
            if (ah.canHandle(achievable)) {
                p.accept(ah);
            }
        });
    }
    
    @EventHandler
    public void onGamePlayerQuitEvent(GamePlayerQuitEvent e) {
        GamePlayer gp = e.getPlayer();
        String name = gp.getName();
        for (Achievable achievable : achievables) {
            if (achievable instanceof NoFailAchievable) {
                if (achievable.getState(name) == AchievableState.WORKING) {
                    achievable.failed(gp);
                }
            }
        }
        if (gp.isPlayed() && game.getState() != me.vicevice.general.api.games.GameState.END) {
            saver.saveUserAsync(gp.getName());
        }
    }
    
    public enum AchievableState {
        FAILED,
        NOT_WORKING,
        WORKING,
        FINISHED
    }
}
