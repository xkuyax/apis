package me.xkuyax.api.games.achievable.pool;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;

public class NoChestAchievable extends CraftAchievable implements NoFailAchievable, WinAchievable {
    
    public NoChestAchievable(ManageableGame game) {
        super(game);
    }
    
    @EventHandler
    public void onInventoryOpenEvent(InventoryOpenEvent e) {
        if (e.getInventory().getType() == InventoryType.CHEST) {
            if (e.getPlayer() instanceof Player) {
                Player p = (Player) e.getPlayer();
                if (isIngame(p) && hasOpen(p)) {
                    failed(p);
                }
            }
        }
    }
    
    @Override
    public String getName() {
        return "nochest";
    }
}
