package me.xkuyax.utils;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.plugin.java.JavaPlugin;

public class APIPlugin {

    @Getter
    @Setter
    public static JavaPlugin plugin;

}
