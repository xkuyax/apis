package me.xkuyax.module;

public interface IModule {
    
    String getName();
    
    default void load() {
    
    }
    
    default void disable() {
    
    }
}
