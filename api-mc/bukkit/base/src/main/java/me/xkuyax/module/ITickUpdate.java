package me.xkuyax.module;

public interface ITickUpdate {
    
    default void tick() {
    }
    
    default void second() {
    }
    
    default void minute() {
    }
}
