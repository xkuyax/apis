package me.xkuyax.utils;

import lombok.Data;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.ItemFrame;

import java.util.ArrayList;
import java.util.List;

@Data
public class FrameResolver {

    private ItemFrame entity;
    private List<ItemFrame> frameList;
    private int vertical;
    private int horizontal;

    public FrameResolver(ItemFrame entity) {
        this.entity = entity;
    }

    public FrameResolver invoke() {
        int moveX, moveZ;
        switch (entity.getFacing()) {
            case SOUTH: {
                moveX = 1;
                moveZ = 0;
                break;
            }
            case EAST: {
                moveX = 0;
                moveZ = -1;
                break;
            }
            case NORTH: {
                moveX = -1;
                moveZ = 0;
                break;
            }
            case WEST: {
                moveX = 0;
                moveZ = 1;
                break;
            }
            default: {
                moveX = 1;
                moveZ = 0;
                break;
            }
        }

        frameList = new ArrayList<>();
        World world = entity.getWorld();
        int x = entity.getLocation().getBlockX();
        int y = entity.getLocation().getBlockY();
        int z = entity.getLocation().getBlockZ();
        ItemFrame cacheFrame = getItemFrameFromChunk(entity.getLocation().getChunk(), new Location(world, x, y, z), entity.getFacing());

        vertical = 0;
        horizontal = 1;
        while (cacheFrame != null) {
            frameList.add(cacheFrame);
            vertical++;
            x += moveX;
            z += moveZ;
            Location loc = new Location(world, x, y, z);
            cacheFrame = getItemFrameFromChunk(loc.getChunk(), loc, entity.getFacing());
        }

        boolean success = true;
        while (success) {
            //Reset Locations
            x = entity.getLocation().getBlockX();
            y--;
            z = entity.getLocation().getBlockZ();

            for (int i = 0; i < vertical; i++) {
                Location loc = new Location(world, x, y, z);
                ItemFrame cEntity = getItemFrameFromChunk(loc.getChunk(), loc, entity.getFacing());
                if (cEntity == null) {
                    success = false;
                    break;
                }
                x += moveX;
                z += moveZ;
                frameList.add(cEntity);
            }
            if (success)
                horizontal++;
        }
        return this;
    }

    public static ItemFrame getItemFrameFromChunk(Chunk chunk, Location loc, BlockFace face) {
        Entity[] var6;
        int var5 = (var6 = chunk.getEntities()).length;

        for (int var4 = 0; var4 < var5; ++var4) {
            Entity entity = var6[var4];
            if (entity.getType() == EntityType.ITEM_FRAME && isSameLocation(entity.getLocation(), loc)) {
                ItemFrame frameEntity = (ItemFrame) entity;
                if (face == null || frameEntity.getFacing() == face) {
                    return frameEntity;
                }
            }
        }

        return null;
    }

    public static boolean isSameLocation(Location loc1, Location loc2) {
        return (
                (loc1.getWorld() == loc2.getWorld()) &&
                        (loc1.getBlockX() == loc2.getBlockX()) &&
                        (loc1.getBlockY() == loc2.getBlockY()) &&
                        (loc1.getBlockZ() == loc2.getBlockZ())
        );
    }

}
