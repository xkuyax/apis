package me.xkuyax.utils;

import net.minecraft.server.v1_8_R3.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle.EnumTitleAction;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class Titles {
    
    public static String convert(String text) {
        if ((text == null) || (text.length() == 0)) {
            return "\"\"";
        }
        int len = text.length();
        StringBuilder sb = new StringBuilder(len + 4);
        sb.append('"');
        for (int i = 0; i < len; i++) {
            char c = text.charAt(i);
            switch (c) {
                case '"':
                case '\\':
                    sb.append('\\');
                    sb.append(c);
                    break;
                case '/':
                    sb.append('\\');
                    sb.append(c);
                    break;
                case '\b':
                    sb.append("\\b");
                    break;
                case '\t':
                    sb.append("\\t");
                    break;
                case '\n':
                    sb.append("\\n");
                    break;
                case '\f':
                    sb.append("\\f");
                    break;
                case '\r':
                    sb.append("\\r");
                    break;
                default:
                    if (c < ' ') {
                        String t = "000" + Integer.toHexString(c);
                        sb.append("\\u").append(t.substring(t.length() - 4));
                    } else {
                        sb.append(c);
                    }
                    break;
            }
        }
        sb.append('"');
        return sb.toString();
    }
    
    public static void sendTitle(Player p, EnumTitleAction action, int fadein, int fadeout, int stay) {
        PacketPlayOutTitle packet = new PacketPlayOutTitle(action, null, fadein, stay, fadeout);
        ((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
    }
    
    public static void sendTitle(Player p, int fadein, int fadeout, int stay) {
        PacketPlayOutTitle packet = new PacketPlayOutTitle(EnumTitleAction.TIMES, null, fadein, stay, fadeout);
        ((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
    }
    
    public static void sendTitle(Player p, EnumTitleAction action, String text) {
        PacketPlayOutTitle packet = new PacketPlayOutTitle(action, ChatSerializer.a(convert(text)));
        ((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
    }
    
    public static void sendTitle(Player p, EnumTitleAction action) {
        PacketPlayOutTitle packet = new PacketPlayOutTitle(action, null);
        ((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
    }
}

