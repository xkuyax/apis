package me.xkuyax.utils;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import lombok.Data;
import net.minecraft.server.v1_8_R3.NBTBase;
import net.minecraft.server.v1_8_R3.NBTTagByte;
import net.minecraft.server.v1_8_R3.NBTTagCompound;
import net.minecraft.server.v1_8_R3.NBTTagList;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.function.Predicate;

public class ItemUtils {

    private static Field profileField;

    static {
        try {
            final Class craftMetaSkull = Class.forName("org.bukkit.craftbukkit." + Bukkit.getServer().getClass().getPackage().getName().split("\\.")[3] + ".inventory.CraftMetaSkull");
            profileField = craftMetaSkull.getDeclaredField("profile");
            profileField.setAccessible(true);
        } catch (final ReflectiveOperationException e) {
            e.printStackTrace();
        }
    }

    public static String getHiddenString(ItemStack item) {
        net.minecraft.server.v1_8_R3.ItemStack nmsItemStack = CraftItemStack.asNMSCopy(item);
        if (nmsItemStack == null) return null;
        NBTTagCompound compound = nmsItemStack.getTag() == null ? new NBTTagCompound() : nmsItemStack.getTag();
        return compound.hasKey("hiddenString") ? compound.getString("hiddenString") : null;
    }

    public static ItemStack setHiddenString(ItemStack itemToName, String name) {
        net.minecraft.server.v1_8_R3.ItemStack nmsItemStack = CraftItemStack.asNMSCopy(itemToName);
        if (nmsItemStack == null) return itemToName;
        NBTTagCompound compound = nmsItemStack.getTag() == null ? new NBTTagCompound() : nmsItemStack.getTag();

        compound.setString("hiddenString", name);
        nmsItemStack.setTag(compound);
        return CraftItemStack.asBukkitCopy(nmsItemStack);
    }

    private static String toReadable(String string) {
        String[] names = string.split("_");
        for (int i = 0; i < names.length; i++) {
            names[i] = names[i].substring(0, 1) + names[i].substring(1).toLowerCase();
        }
        return (org.apache.commons.lang.StringUtils.join(names, " "));
    }

    public static boolean hasHiddenString(ItemStack item) {
        return getHiddenString(item) != null;
    }

    public static int getFreeSlots(Inventory inv) {
        ItemStack[] a = inv.getContents();
        int c = 0;
        for (ItemStack ic : a) {
            if (ic == null || ic.getType() == Material.AIR) {
                c++;
            }
        }
        return c;
    }

    public static int getFirstSlot(Inventory inv, ItemStack is) {
        ItemStack[] a = inv.getContents();
        for (int i = 0; i < a.length; i++) {
            ItemStack ic = a[i];
            if (ic != null && is.equals(ic)) {
                return i;
            }
        }
        return -1;
    }

    public static int getFirstSlot(Inventory inv, String unique) {
        ItemStack[] a = inv.getContents();
        for (int i = 0; i < a.length; i++) {
            ItemStack ic = a[i];
            if (ic != null) {
                String n = getHiddenString(ic);
                if (n != null && n.equalsIgnoreCase(unique)) {
                    return i;
                }
            }
        }
        return -1;
    }

    public static ItemStack setLoreAndDisplay(ItemStack is, List<String> lore, String dname) {
        ItemMeta im = is.getItemMeta();
        im.setLore(lore);
        im.setDisplayName(dname);
        is.setItemMeta(im);
        return is;
    }

    public static ItemStack addToLore(ItemStack is, String s, List<String> r, List<String> o) {
        ItemMeta im = is.getItemMeta();
        List<String> lore = im.hasLore() ? im.getLore() : new ArrayList<>();
        lore.add(StringFormatting.formatString(s, r, o));
        im.setLore(lore);
        is.setItemMeta(im);
        return is;
    }

    public static ItemStack removeLore(ItemStack is) {
        ItemMeta im = is.getItemMeta();
        im.setLore(new ArrayList<>());
        is.setItemMeta(im);
        return is;
    }

    public static ItemStack reformat(ItemStack is, List<String> r, List<String> o) {
        ItemMeta im = is.getItemMeta();
        List<String> lore = new ArrayList<>();
        for (String s : im.getLore()) {
            lore.add(StringFormatting.formatString(s, r, o));
        }
        im.setLore(lore);
        im.setDisplayName(StringFormatting.formatString(im.getDisplayName(), r, o));
        is.setItemMeta(im);
        return is;
    }

    public static ItemStack setSkullandName(ItemStack is, String name) {
        is.setType(Material.SKULL_ITEM);
        is.setDurability((short) 3);
        SkullMeta sm = (SkullMeta) is.getItemMeta();
        sm.setOwner(name);
        is.setItemMeta(sm);
        return is;
    }

    public static int count(Inventory inv, String hidden) {
        int r = 0;
        for (ItemStack is : inv.getContents()) {
            if (is != null) if (hidden.equalsIgnoreCase(getHiddenString(is))) r += is.getAmount();
        }
        return r;
    }

    public static int count(Player p, Material material) {
        int r = 0;
        Inventory inv = p.getInventory();
        for (ItemStack is : inv.getContents()) {
            if (is == null && material == Material.AIR) {
                r += 1;
            }
            if (is != null && is.getType().equals(material)) r += is.getAmount();
        }
        return r;
    }

    public static int count(Player p, Predicate<ItemStack> match) {
        int r = 0;
        Inventory inv = p.getInventory();
        for (ItemStack is : inv.getContents()) {
            if (is != null && match.test(is)) {
                r += is.getAmount();
            }
        }
        return r;
    }

    public static int count(Player p, String hidden) {
        return count(p.getInventory(), hidden);
    }

    public static void remove(Player p, int toRemove, Material material) {
        Inventory inv = p.getInventory();
        int removed = 0;
        for (int i = 0; i < inv.getSize(); i++) {
            ItemStack is = inv.getItem(i);
            if (is == null || !is.getType().equals(material)) {
                continue;
            }
            int a = is.getAmount();
            while (a > 0 && removed < toRemove) {
                removed++;
                a--;
            }
            if (a == 0) {
                inv.clear(i);
            } else {
                is.setAmount(a);
            }
        }
        p.updateInventory();
    }

    public static void remove(Player p, int toRemove, Predicate<ItemStack> match) {
        Inventory inv = p.getInventory();
        int removed = 0;
        for (int i = 0; i < inv.getSize(); i++) {
            ItemStack is = inv.getItem(i);
            if (is == null || !match.test(is)) {
                continue;
            }
            int a = is.getAmount();
            while (a > 0 && removed < toRemove) {
                removed++;
                a--;
            }
            if (a == 0) {
                inv.clear(i);
            } else {
                is.setAmount(a);
            }
        }
        p.updateInventory();
    }

    public static void remove(Player p, String hidden) {
        Inventory inv = p.getInventory();
        for (ItemStack is : inv) {
            String s = is != null && is.getType() != Material.AIR ? getHiddenString(is) : null;
            if (s != null && s.equalsIgnoreCase(hidden)) {
                inv.remove(is);
            }
        }
    }

    public static void remove(Player p, String hidden, int toRemove) {
        Inventory inv = p.getInventory();
        int removed = 0;
        for (int i = 0; i < inv.getSize(); i++) {
            ItemStack is = inv.getItem(i);
            if (is == null) {
                continue;
            }
            String h = getHiddenString(is);
            if (hidden.equals(h)) {
                int a = is.getAmount();
                while (a > 0 && removed < toRemove) {
                    removed++;
                    a--;
                }
                if (a == 0) {
                    inv.clear(i);
                } else {
                    is.setAmount(a);
                }
            }
        }
        p.updateInventory();
    }

    public static ItemStack setNBTTags(ItemStack item, NBTTagPair... values) {
        net.minecraft.server.v1_8_R3.ItemStack nmsStack = CraftItemStack.asNMSCopy(item);
        NBTTagCompound tag = null;
        if (!nmsStack.hasTag()) {
            tag = new NBTTagCompound();
            nmsStack.setTag(tag);
        }
        if (tag == null) {
            tag = nmsStack.getTag();
        }
        NBTTagCompound finalTag = tag;
        Arrays.stream(values).forEach(nbtTagPair -> finalTag.set(nbtTagPair.getKey(), nbtTagPair.getValue()));
        nmsStack.setTag(tag);
        return CraftItemStack.asCraftMirror(nmsStack);
    }

    public static Color getFireworkChargeColor(ItemStack item) {
        net.minecraft.server.v1_8_R3.ItemStack itemStack = CraftItemStack.asNMSCopy(item);
        NBTTagCompound nbtTagCompound = itemStack.hasTag() ? itemStack.getTag() : new NBTTagCompound();
        if (!nbtTagCompound.hasKey("Explosion")) {
            return Color.GRAY;
        }
        NBTTagCompound explosion = nbtTagCompound.getCompound("Explosion");
        if (!explosion.hasKey("Colors")) {
            return Color.GRAY;
        }
        int[] colors = explosion.getIntArray("Colors");
        if (colors.length == 0) {
            return Color.GRAY;
        }
        return Color.fromRGB(colors[0]);

    }

    public static ItemStack setFireworkChargeColor(ItemStack item, Color color) {
        net.minecraft.server.v1_8_R3.ItemStack itemStack = CraftItemStack.asNMSCopy(item);
        NBTTagCompound nbtTagCompound = itemStack.hasTag() ? itemStack.getTag() : new NBTTagCompound();
        NBTTagCompound explosion = new NBTTagCompound();
        explosion.setIntArray("Colors", new int[]{color.asRGB()});
        nbtTagCompound.set("Explosion", explosion);
        return CraftItemStack.asBukkitCopy(itemStack);
    }

    public static ItemStack addGlow(ItemStack item) {
        return setNBTTags(item, new NBTTagPair("ench", new NBTTagList()));
    }

    public static ItemStack unbreakable(ItemStack is) {
        return setNBTTags(is, new NBTTagPair("Unbreakable", new NBTTagByte((byte) 1)));
    }

    public static ItemStack removeGlow(ItemStack is) {
        net.minecraft.server.v1_8_R3.ItemStack nmsStack = CraftItemStack.asNMSCopy(is);
        NBTTagCompound tag = null;
        if (!nmsStack.hasTag()) {
            tag = new NBTTagCompound();
            nmsStack.setTag(tag);
        }
        if (tag == null) tag = nmsStack.getTag();
        tag.remove("Unbreakable");
        nmsStack.setTag(tag);
        return CraftItemStack.asCraftMirror(nmsStack);
    }

    public static ItemStack first(Inventory inv, Predicate<ItemStack> match) {
        for (ItemStack is : inv.getContents()) {
            if (is != null && match.test(is)) {
                return is;
            }
        }
        return null;
    }

    public static ItemStack first(Inventory inv, Material mat) {
        int slot = inv.first(mat);
        if (slot == -1) {
            return null;
        }
        return inv.getItem(slot);
    }

    public static ItemStack setSkullTexture(final ItemStack stack, final String textureHash) {
        Preconditions.checkNotNull(stack, "The stack cannot be null!");
        Preconditions.checkNotNull(textureHash, "The textureHash cannot be null!");
        Preconditions.checkArgument(!textureHash.isEmpty(), "The textureHash cannot be empty!");
        final ItemMeta meta = stack.getItemMeta();
        Preconditions.checkState(meta instanceof SkullMeta, "Meta must be a skull meta");
        final GameProfile profile = new GameProfile(UUID.randomUUID(), null);
        profile.getProperties().put("textures", new Property("textures", textureHash));
        try {
            profileField.set(meta, profile);
        } catch (final IllegalAccessException e) {
            e.printStackTrace();
        }
        stack.setItemMeta(meta);
        return stack;
    }

    @Data
    public static class NBTTagPair {

        private final String key;
        private final NBTBase value;

    }

}