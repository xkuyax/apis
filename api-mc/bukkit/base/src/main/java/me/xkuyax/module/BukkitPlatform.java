package me.xkuyax.module;

import me.xkuyax.utils.Titles;
import me.xkuyax.utils.mc.MCPlatform;
import net.md_5.bungee.api.chat.BaseComponent;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle.EnumTitleAction;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class BukkitPlatform implements MCPlatform<Player> {

    @Override
    public void sendChatComponent(Player player, BaseComponent[] components) {
        player.spigot().sendMessage(components);
    }

    @Override
    public void sendTitleTimes(Player player, int fadeIn, int fadeOut, int stay) {
        Titles.sendTitle(player, EnumTitleAction.TIMES, fadeIn, fadeOut, stay);
    }

    @Override
    public void sendTitle(Player player, String title) {
        Titles.sendTitle(player, EnumTitleAction.TITLE, title);
    }

    @Override
    public void sendSubTitle(Player player, String subtitle) {
        Titles.sendTitle(player, EnumTitleAction.SUBTITLE, subtitle);
    }

    @Override
    public void sendSound(Player player, String sound, float volume, float pitch) {
        Sound[] values = Sound.values();
        for (Sound lookup : values) {
            if (lookup.name().equalsIgnoreCase(sound)) {
                player.playSound(player.getLocation(), lookup, volume, pitch);
                return;
            }
        }
        player.playSound(player.getLocation(), sound, volume, pitch);
    }

    @Override
    public void sendActionBar(Player player, String message) {
        IChatBaseComponent chatBaseComponent = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + message + "\"}");
        PacketPlayOutChat bar = new PacketPlayOutChat(chatBaseComponent, (byte) 2);
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(bar);
    }
}
