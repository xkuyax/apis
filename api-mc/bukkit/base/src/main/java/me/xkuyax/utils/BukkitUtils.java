package me.xkuyax.utils;

import org.bukkit.scheduler.BukkitRunnable;

public class BukkitUtils {

    @Deprecated
    public static void scheduleNTimes(Runnable runnable, int repeat, int delay, int period) {
        new BukkitRunnable() {

            private int loop;

            @Override
            public void run() {
                if (loop < repeat) {
                    runnable.run();
                } else {
                    cancel();
                }
                loop++;
            }
        }.runTaskTimer(APIPlugin.getPlugin(), delay, period);
    }

}
