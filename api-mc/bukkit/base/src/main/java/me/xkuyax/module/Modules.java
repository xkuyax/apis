package me.xkuyax.module;

import me.xkuyax.utils.APIPlugin;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Modules {
    
    private static final Map<String, IModule> namedModules = new HashMap<>();
    private static final Map<Class<? extends IModule>, IModule> clazzModules = new HashMap<>();
    private static final List<ITickUpdate> tickUpdates = new ArrayList<>();
    private static JavaPlugin javaPlugin;
    
    public static <T extends IModule> void registerModule(T module, Class<T> clazz) {
        if (module instanceof ITickUpdate) {
            registerTickUpdate((ITickUpdate) module);
        }
        String name = module.getName();
        if (namedModules.containsKey(name)) {
            throw new IllegalArgumentException("Module already registered");
        }
        if (clazzModules.containsKey(clazz)) {
            throw new IllegalArgumentException("Class is already mapped!");
        }
        clazzModules.put(clazz, module);
        namedModules.put(name, module);
    }
    
    public static void registerTickUpdate(ITickUpdate update) {
        if (tickUpdates.isEmpty()) {
            startTickUpdater();
        }
        tickUpdates.add(update);
    }
    
    public static IModule get(String name) {
        return namedModules.get(name);
    }
    
    public static <T> T get(String name, Class<T> clazz) {
        return (T) namedModules.get(name);
    }
    
    public static <T> T get(Class<T> clazz) {
        return (T) clazzModules.get(clazz);
    }
    
    private static void startTickUpdater() {
        JavaPlugin plugin = getJavaPlugin();
        Bukkit.getScheduler().runTaskTimer(plugin, () -> tickUpdates.forEach(ITickUpdate::tick), 1, 1);
        Bukkit.getScheduler().runTaskTimer(plugin, () -> tickUpdates.forEach(ITickUpdate::second), 1, 20);
        Bukkit.getScheduler().runTaskTimer(plugin, () -> tickUpdates.forEach(ITickUpdate::minute), 1, 20 * 60);
    }
    
    public static JavaPlugin getJavaPlugin() {
        if (javaPlugin == null) {
            javaPlugin = APIPlugin.getPlugin();
            if (javaPlugin == null) {
                throw new RuntimeException("Could not get the api plugin!");
            }
        }
        return javaPlugin;
    }
}
