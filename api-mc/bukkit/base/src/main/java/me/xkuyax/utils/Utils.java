package me.xkuyax.utils;

import org.bukkit.ChatColor;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.function.Predicate;

@Deprecated
public class Utils {

    public static void scheduleNTimes(Runnable runnable, int repeat, int delay, int period) {
        new BukkitRunnable() {

            private int loop;

            @Override
            public void run() {
                if (loop < repeat) {
                    runnable.run();
                } else {
                    cancel();
                }
                loop++;
            }
        }.runTaskTimer(APIPlugin.getPlugin(), delay, period);
    }

    public static String formatString(String s, List<String> r, List<String> o) {
        if (r == null || o == null) {
            return simpleColorFormat(s);
        }
        return simpleColorFormat(StringUtils.formatString(s, r, o));
    }

    public static ArrayList<String> formatStringList(List<String> lore) {
        ArrayList<String> tmp = new ArrayList<>();
        for (String s : lore) {
            tmp.add(simpleColorFormat(s));
        }
        return tmp;
    }

    public static ArrayList<String> formatStringList(List<String> lore, List<String> r, List<String> o) {
        ArrayList<String> tmp = new ArrayList<>();
        for (String s : lore) {
            if (s != null) {
                tmp.add(formatString(s, r, o));
            }
        }
        return tmp;
    }

    public static String simpleColorFormat(String s) {
        return ChatColor.translateAlternateColorCodes('&', s);
    }

    public static <T> int find(T t, T[] a) {
        for (int i = 0; i < a.length; i++) {
            if (a[i].equals(t)) {
                return i;
            }
        }
        throw new IllegalArgumentException(t + " Is not in the array! " + Arrays.toString(a));
    }

    public static <T> int next(T[] a, int i) {
        return i >= a.length - 1 ? 0 : i + 1;
    }

    public static <T> T getNext(T t, T[] a) {
        return a[next(a, find(t, a))];
    }

    public static <T> int find(List<T> a, Predicate<T> predicate) {
        for (int i = 0; i < a.size(); i++) {
            if (predicate.test(a.get(i))) {
                return i;
            }
        }
        throw new IllegalArgumentException("Predicate did not find something in the array! " + a);
    }

    public static <T> int next(List<T> a, int i) {
        return i >= a.size() - 1 ? 0 : i + 1;
    }

    public static <T> int last(List<T> a, int i) {
        return i > 0 ? i - 1 : a.size() - 1;
    }

    public static <T> T getNext(List<T> a, Predicate<T> predicate) {
        return a.get(next(a, find(a, predicate)));
    }

    public static <T> T getLast(List<T> a, Predicate<T> predicate) {
        return a.get(last(a, find(a, predicate)));
    }

    public static <T> int findWithNull(List<T> a, Predicate<T> predicate) {
        if (predicate.test(null)) {
            return -1;
        }
        for (int i = 0; i < a.size(); i++) {
            if (predicate.test(a.get(i))) {
                return i;
            }
        }
        throw new IllegalArgumentException("Predicate did not find something in the array! " + a);
    }

    public static <T> int nextWithNull(List<T> a, int i) {
        return i >= a.size() - 1 ? 0 : i + 1;
    }

    public static <T> T getNextWithNull(List<T> a, Predicate<T> predicate) {
        int index = findWithNull(a, predicate);
        if (index == a.size() - 1) {
            return null;
        }
        return a.get(nextWithNull(a, index));
    }

    public static <T> T getNextWithNull(T t, List<T> a) {
        return getNextWithNull(a, other -> Objects.equals(t, other));
    }



}
