package me.xkuyax.utils;

import me.xkuyax.utils.Lambdas.ExceptionConsumer;
import me.xkuyax.utils.Lambdas.ExceptionRunnable;
import me.xkuyax.utils.Lambdas.ExceptionSupplier;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.concurrent.atomic.AtomicInteger;

public class Schedule {
    
    public static void async(ExceptionRunnable runnable) {
        Bukkit.getScheduler().runTaskAsynchronously(APIPlugin.getPlugin(), runnable);
    }
    
    public static void sync(ExceptionRunnable runnable) {
        Bukkit.getScheduler().runTask(APIPlugin.getPlugin(), runnable);
    }
    
    public static void repeat(ExceptionConsumer<Integer> tick, ExceptionConsumer<Integer> endTick, int ticks, int interval, int delay, ExceptionSupplier<Boolean> predicate) {
        AtomicInteger atomicInteger = new AtomicInteger();
        new BukkitRunnable() {
            
            @Override
            public void run() {
                int i = atomicInteger.get();
                if (i < ticks) {
                    atomicInteger.incrementAndGet();
                    try {
                        if (!predicate.getWith()) {
                            cancel();
                        }
                        tick.consume(i + 1);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        endTick.consume(ticks);
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        cancel();
                    }
                }
            }
        }.runTaskTimer(APIPlugin.getPlugin(), delay, interval);
    }
}
