package me.xkuyax.utils;

import me.xkuyax.utils.GCommand.CSender;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface CmdHandler {
    
    String args() default "";
    
    String permission() default "nopermission";
    
    CSender sender();
    
    boolean nullArgs() default false;
    
    boolean ignoreArgs() default false;
    
    boolean runAsync() default false;
    
    int minArgs() default -1;
}
