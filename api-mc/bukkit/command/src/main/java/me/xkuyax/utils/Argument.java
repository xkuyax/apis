package me.xkuyax.utils;

import java.lang.reflect.Method;

public class Argument {
    
    private Method method;
    private GCommand.CSender sender;
    private String permission;
    
    @Override
    public boolean equals(Object obj) {
        return (obj == this) || (obj instanceof Argument && method.equals(((Argument) obj).getMethod()));
    }
    
    @Override
    public String toString() {
        return "Method-Name: " + method.getName();
    }
    
    @Override
    public int hashCode() {
        return super.hashCode();
    }
    
    public Method getMethod() {
        return method;
    }
    
    public void setMethod(Method method) {
        this.method = method;
    }
    
    public GCommand.CSender getSender() {
        return sender;
    }
    
    public void setSender(GCommand.CSender sender) {
        this.sender = sender;
    }
    
    public String getPermission() {
        return permission;
    }
    
    public void setPermission(String permission) {
        this.permission = permission;
    }
}
