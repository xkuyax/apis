package me.xkuyax.utils.scoreboard;

import lombok.Data;
import me.xkuyax.utils.StringFormatting;
import me.xkuyax.utils.config.BukkitConfig;
import me.xkuyax.utils.replace.ReplaceHandler;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.Locale;
import java.util.function.Function;
import java.util.function.Supplier;

@Data
public class DynamicDataHandler<T> {

    private final String name;
    private final ReplaceHandler<T> replaceHandler;
    private final Supplier<List<T>> listSupplier;
    private Function<T, Integer> scoreFunction;
    private Function<T, Integer> stateFunction;
    private int dynamicStart = 16;

    public void render(WrappedScoreboard scoreboard, Locale locale, BukkitConfig config, Player player, String path) {
        if (config.getBoolean(path + ".dynamic." + name + ".disabled", false)) {
            return;
        }
        dynamicStart = config.getInt(path + ".dynamic." + name + ".dynamicStart", 16);
        List<T> list = listSupplier.get();
        int pos = dynamicStart;
        for (int i = 0; i < list.size(); i++) {
            T data = list.get(i);
            int state = stateFunction != null ? stateFunction.apply(data) : 0;
            String template = config.getString(path + ".dynamic." + name + "." + locale.getLanguage() + ".state." + state, "Dynamic");
            String format = StringFormatting.simpleColorFormat(replaceHandler.format(template, data));
            scoreboard.setText(format, name + i, pos--);
        }
    }

    public void init(BukkitConfig config, String path) {
    }
}
