package me.xkuyax.utils.scoreboard;

import lombok.Data;
import lombok.NonNull;
import me.xkuyax.utils.config.BukkitConfig;
import org.bukkit.entity.Player;

import javax.annotation.Nullable;
import java.util.Locale;

@Data
public class ScoreboardContext {
    
    private final Player player;
    private final int index;
    @Nullable
    private final String indexName;
    @NonNull
    private final Locale locale;
    @NonNull
    private final BukkitConfig config;
    
}
