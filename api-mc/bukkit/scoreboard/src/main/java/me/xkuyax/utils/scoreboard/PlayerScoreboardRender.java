package me.xkuyax.utils.scoreboard;

import me.xkuyax.utils.config.BukkitConfig;
import org.bukkit.entity.Player;

import java.util.Locale;

@FunctionalInterface
public interface PlayerScoreboardRender {
    
    String format(BukkitConfig config, Locale locale, ScoreboardContext context, Player player);
    
}
