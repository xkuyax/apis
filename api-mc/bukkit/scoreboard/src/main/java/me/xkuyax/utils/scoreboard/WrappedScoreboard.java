package me.xkuyax.utils.scoreboard;

import lombok.Data;
import me.xkuyax.utils.StringFormatting;
import org.bukkit.Bukkit;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class WrappedScoreboard {

    private final Scoreboard scoreboard;
    private final Objective objective;
    private Map<String, String> sent = new HashMap<>();

    public void setText(String text, String teamName, int score) {
        setTeam0(text, teamName, score);
    }

    public void clearText(String teamName) {
        deleteFromName(teamName);
    }

    private void setTeam0(String text, String teamName, int score) {
        if (sent.getOrDefault(teamName, "").equals(text)) {
            return;
        }
        Team team;
        if (scoreboard.getTeam(teamName) == null) {
            team = scoreboard.registerNewTeam(teamName);
        } else {
            team = scoreboard.getTeam(teamName);
            deleteFromName(teamName);
        }
        String prefix = "";
        String suffix = "";
        String entry = "";
        if (text.length() <= 16) {
            entry = text;
        } else if (text.length() <= 32) {
            prefix = text.substring(0, 16);
            entry = text.substring(16);
        } else {
            prefix = text.substring(0, 16);
            entry = text.substring(16, 32);
            suffix = text.substring(32, text.length() > 48 ? 48 : text.length());
        }
        team.setPrefix(prefix);
        team.addEntry(entry);
        team.setSuffix(suffix);
        objective.getScore(entry).setScore(score);
        sent.put(teamName, text);
    }

    public void changeObjectiveName(String displayname) {
        if (displayname.length() > 32) {
            objective.setDisplayName(StringFormatting.simpleColorFormat(displayname.substring(0, 32)));
        } else {
            objective.setDisplayName(StringFormatting.simpleColorFormat(displayname));
        }
    }

    private void deleteFromName(String teamName) {
        List<String> remove = new ArrayList<>();
        for (String s : scoreboard.getEntries()) {
            Team team = scoreboard.getEntryTeam(s);
            if (team != null && team.getName().equals(teamName)) {
                remove.add(s);
            }
        }
        for (String s : remove) {
            scoreboard.resetScores(s);
        }
    }

    public static WrappedScoreboard newBoard(String objective) {
        Scoreboard newScoreboard = Bukkit.getScoreboardManager().getNewScoreboard();
        String nameObjective = objective.length() > 16 ? objective.substring(0, 16) : objective;
        Objective newObjective = newScoreboard.registerNewObjective(nameObjective, "dummy");
        newObjective.setDisplaySlot(DisplaySlot.SIDEBAR);
        newObjective.setDisplayName(StringFormatting.simpleColorFormat(objective));
        return new WrappedScoreboard(newScoreboard, newObjective);
    }
}
