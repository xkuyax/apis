package me.xkuyax.utils.scoreboard;

import me.xkuyax.utils.config.BukkitConfig;

import java.util.Locale;

@FunctionalInterface
public interface LocaleScoreboardRender {
    
    String format(BukkitConfig config, ScoreboardContext context, Locale locale);
    
}
