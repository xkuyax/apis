package me.xkuyax.utils.scoreboard;

import me.xkuyax.utils.replace.ReplaceHandler;

public class ScoreboardReplaceHandler extends ReplaceHandler<ScoreboardContext> {
    
    public ScoreboardReplaceHandler with(String var, PlayerScoreboardRender scoreboardRender) {
        variables.put(toPattern(var), (context) -> {
            if (context.getPlayer() == null) {
                throw new RuntimeException("Could not call PlayerScoreboardRender because no player is given in this context!");
            }
            return scoreboardRender.format(context.getConfig(), context.getLocale(), context, context.getPlayer());
        });
        return this;
    }
    
    public ScoreboardReplaceHandler with(String var, LocaleScoreboardRender localeRender) {
        variables.put(toPattern(var), (context) -> localeRender.format(context.getConfig(), context, context.getLocale()));
        return this;
    }
}
