package me.xkuyax.utils.scoreboard;

import lombok.Getter;
import lombok.Setter;
import me.xkuyax.utils.StringFormatting;
import me.xkuyax.utils.StringUtils;
import me.xkuyax.utils.config.BukkitConfig;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Scoreboard;

import java.util.*;
import java.util.regex.Pattern;

@Getter
public class ConfigBoard {

    private final BukkitConfig config;
    private final ScoreboardReplaceHandler render;
    private final Map<Locale, WrappedScoreboard> locales = new HashMap<>();
    private final Map<String, WrappedScoreboard> players = new HashMap<>();
    private final String path;
    @Setter
    private boolean perLineRendering;
    @Setter
    private ScoreboardReplaceHandler objectiveNameRender;
    @Setter
    private StateHandler<ScoreboardContext> stateHandler;
    private final List<DynamicDataHandler<?>> dynamicHandlers = new ArrayList<>();

    public ConfigBoard(BukkitConfig config, ScoreboardReplaceHandler render) {
        this(config, render, "Scoreboard");
    }

    public ConfigBoard(BukkitConfig config, ScoreboardReplaceHandler render, String path) {
        this.config = config;
        this.render = render;
        this.path = path;
    }

    public Scoreboard renderLocale(Locale locale) {
        Map<String, Integer> scores = getConfigScores();
        return render(locale, null, scores, getLocaleScoreboard(locale));
    }

    public Scoreboard renderPlayer(Player player, Locale locale) {
        Map<String, Integer> scores = getConfigScores();
        WrappedScoreboard scoreboard = getPlayerScoreboard(player, locale);
        return render(locale, player, scores, scoreboard);
    }

    public Scoreboard render(Locale locale, Player player, Map<String, Integer> scores, WrappedScoreboard scoreboard) {
        if (perLineRendering) {
            scores.forEach((name, integer) -> {
                ScoreboardContext context = new ScoreboardContext(player, integer, name, locale, config);
                String input = getInputString(name, locale, context);
                String format = render.format(input, context);
                scoreboard.setText(StringFormatting.simpleColorFormat(format), name, integer);
            });
        } else {
            ScoreboardContext context = new ScoreboardContext(player, -1, null, locale, config);
            Map<Pattern, String> rendered = render.renderMap(context);
            scores.forEach((name, integer) -> {
                String input = getInputString(name, locale, context);
                String output = render.format(input, rendered);
                scoreboard.setText(StringFormatting.simpleColorFormat(output), name, integer);
            });
        }
        dynamicHandlers.forEach(dynamicHandler -> dynamicHandler.render(scoreboard, locale, config, player, path));
        if (objectiveNameRender != null) {
            String name = objectiveNameRender.format(getObjective(locale), new ScoreboardContext(player, -1, null, locale, config));
            if (!scoreboard.getObjective().getDisplayName().equals(name)) {
                scoreboard.changeObjectiveName(name);
            }
        }
        return scoreboard.getScoreboard();
    }

    private String getInputString(String name, Locale locale, ScoreboardContext context) {
        if (stateHandler != null && context != null) {
            String function = config.getString(path + ".line." + name + ".function", "none");
            if (!Objects.equals(function, "none")) {
                int state = stateHandler.getState(function, context);
                return config.getString(path + ".line." + name + ".state." + state + "." + locale.getLanguage(), "Line " + name);
            }
        }
        return config.getString(path + ".line." + name + "." + locale.getLanguage(), "Line " + name);
    }

    private Map<String, Integer> getConfigScores() {
        List<String> list = config.getStringList(path + ".scores", "1:1", "2:2", "3:3");
        Map<String, Integer> scores = new HashMap<>();
        list.forEach(t -> {
            String[] split = t.split(":");
            scores.put(split[0], StringUtils.parseInt(split[1], 1));
        });
        return scores;
    }

    public WrappedScoreboard getLocaleScoreboard(Locale locale) {
        return locales.computeIfAbsent(locale, (e) -> newBoard(locale));
    }

    public WrappedScoreboard getPlayerScoreboard(Player player, Locale locale) {
        return players.computeIfAbsent(player.getName(), (e) -> newBoard(locale));
    }

    private WrappedScoreboard newBoard(Locale locale) {
        String objective = getObjective(locale);
        return WrappedScoreboard.newBoard(objective);
    }

    private String getObjective(Locale locale) {
        return config.getString(path + ".objective." + locale.getLanguage(), "Objective");
    }

    public void setLocaleBoard(Player player, Locale locale) {
        player.setScoreboard(getLocaleScoreboard(locale).getScoreboard());
    }

    public void setPlayerBoard(Player player, Locale locale) {
        player.setScoreboard(getPlayerScoreboard(player, locale).getScoreboard());
    }

    public void addDynamicHandler(DynamicDataHandler<?> dataHandler) {
        this.dynamicHandlers.add(dataHandler);
        dataHandler.init(config, path);
    }
}
