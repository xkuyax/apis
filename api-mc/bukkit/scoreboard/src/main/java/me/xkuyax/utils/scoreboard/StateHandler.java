package me.xkuyax.utils.scoreboard;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class StateHandler<T> {
    
    private final Map<String, Function<T, Integer>> functions = new HashMap<>();
    
    public void with(String name, Function<T, Integer> function) {
        functions.put(name, function);
    }
    
    public int getState(String name, T context) {
        return functions.getOrDefault(name, t -> -1).apply(context);
    }
}
