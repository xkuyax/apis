package me.xkuyax.utils.inventory;

import me.xkuyax.utils.config.BukkitConfig;
import org.bukkit.entity.Player;

import java.util.Arrays;

public class MultiPageExampleInventory extends MultiPageInventory<String> {

    private static final BukkitConfig config = new BukkitConfig("TestPlugin", MultiPageExampleInventory.class);

    public MultiPageExampleInventory(Player player) {
        super(player, config, Arrays.asList("Value1", "Value2", "Value3"));
    }
}
