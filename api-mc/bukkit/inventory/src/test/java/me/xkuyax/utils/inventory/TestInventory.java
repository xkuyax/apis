package me.xkuyax.utils.inventory;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import me.xkuyax.utils.config.BukkitConfig;
import me.xkuyax.utils.config.ItemBuilder;
import org.bukkit.entity.Player;

import java.util.Arrays;

@Data
public class TestInventory extends SingleInventory {

    private static final BukkitConfig config = new BukkitConfig("TestPlugin", TestInventory.class);
    @Getter
    @Setter
    private int currentValue = 1;

    public TestInventory(Player player) {
        super(player, config);
        updateInventory();
    }

    @Override
    public void updateInventory() {
        Player player = getOwner();
        setClick("Items.simpleItem", e -> {
            if (isRightClick(e)) {
                player.sendMessage("§bDu hast mich mit rechts gedrückt!");
            }
            player.sendMessage("§Du bist hat mich gedrückt :O");
        });
        setForward("Items.forward", e -> new SecondTestInventory(player));
        setState("Items.state", Arrays.asList(1, 3, 5, 7, 9), this::getCurrentValue, this::setCurrentValue, integer -> {
            ItemBuilder itemBuilder = config.getItemBuilder("Items.stateTemplate");
            itemBuilder.formatBoth("%value%", integer + "");
            return itemBuilder.build();
        });
        setStateName("Items.state", Arrays.asList(1, 3, 5, 7, 9), this::getCurrentValue, this::setCurrentValue, integer -> integer + "");
    }
}
