package me.xkuyax.utils.inventory;

import me.xkuyax.utils.config.BukkitConfig;
import org.bukkit.entity.Player;

public class SecondTestInventory extends SingleInventory {

    private static final BukkitConfig config = new BukkitConfig("TestPlugin", TestInventory.class);

    public SecondTestInventory(Player player) {
        super(player, config);
        updateInventory();
    }

    @Override
    public void updateInventory() {
        Player player = getOwner();
        setClick("Items.anotherItem", e -> player.sendMessage("Hello from the second inventory!"));
    }
}
