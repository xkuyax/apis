package me.xkuyax.utils.inventory;

public interface IMultiPageInventory<V> extends ModifiableInventory {
    
    int getItemsPerSite();
    
    default void onNextPage(int page) {
    
    }
    
    default void onLastPage(int page) {
    
    }
    
}
