package me.xkuyax.utils.inventory;

import lombok.Getter;
import lombok.Setter;
import me.xkuyax.utils.APIPlugin;
import me.xkuyax.utils.config.BukkitConfig;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.Map.Entry;

@Deprecated
public class SinglePlayerInventory extends SingleInventory {

    private static final int PLAYER_INVENTORY_SIZE = 36;
    @Getter
    @Setter
    private boolean saveInventory = true;
    @Getter
    @Setter
    private InventorySave inventorySave;
    @Getter
    @Setter
    private boolean restored = false;
    private Inventory lastInventoryBuild;

    public SinglePlayerInventory(Player player, int size, String title, boolean cancel, boolean force, boolean dynamic) {
        super(player, size + PLAYER_INVENTORY_SIZE, title, cancel, force, dynamic);
    }

    public SinglePlayerInventory(Player player, int size, String title, boolean cancel, boolean force) {
        super(player, size + PLAYER_INVENTORY_SIZE, title, cancel, force);
    }

    public SinglePlayerInventory(Player player, int size, String title, boolean cancel) {
        super(player, size + PLAYER_INVENTORY_SIZE, title, cancel);
    }

    public SinglePlayerInventory(Player player, int size, String title) {
        super(player, size + PLAYER_INVENTORY_SIZE, title);
    }

    public SinglePlayerInventory(Player player, BukkitConfig config, String path) {
        super(player, config, path);
        setSize(getSize() + PLAYER_INVENTORY_SIZE);
    }

    @Override
    public Inventory build() {
        return build(Bukkit.createInventory(getOwner(), getSize() - PLAYER_INVENTORY_SIZE, getTitle().length() > 32 ? getTitle().substring(0, 32) : getTitle()));
    }

    @Override
    public Inventory build(Inventory inv, boolean reg) {
        Player player = getOwner();
        Inventory playerInventory = player.getInventory();
        if (inventorySave == null) {
            inventorySave = new InventorySave(player);
            player.getInventory().clear();
            player.getInventory().setArmorContents(null);
            player.updateInventory();
            Bukkit.getScheduler().runTaskLater(APIPlugin.getPlugin(), player::updateInventory, 1);
        }
        for (Entry<Integer, PageItemData> e : getData().entrySet()) {
            Integer i = e.getKey();
            PageItemData pi = e.getValue();
            if (pi.getItemStack() != null) {
                if (!pi.getUniqueName().isEmpty()) {
                    if (i < (getSize() - PLAYER_INVENTORY_SIZE)) {
                        inv.setItem(i, InventoryModule.setHiddenString(pi.getItemStack().clone(), pi.getUniqueName()));
                    } else {
                        playerInventory.setItem(i - (getSize() - PLAYER_INVENTORY_SIZE), InventoryModule.setHiddenString(pi.getItemStack().clone(), pi.getUniqueName()));
                    }
                } else {
                    if (i < (getSize() - PLAYER_INVENTORY_SIZE)) {
                        inv.setItem(i, pi.getItemStack().clone());
                    } else {
                        playerInventory.setItem(i - (getSize() - PLAYER_INVENTORY_SIZE), InventoryModule.setHiddenString(pi.getItemStack().clone(), pi.getUniqueName()));
                    }
                }
            }
        }
        setLastInventoryBuild(inv);
        if (reg) {
            InventoryModule.a().register(this, inv);
        }
        return inv;
    }

    public SingleInventory setPlayerInventory(int slot, ItemStack is, PageForwardItem pageItem) {
        return super.set(slot + (getSize() - PLAYER_INVENTORY_SIZE), is, pageItem);
    }

    public SingleInventory setPlayerInventory(int slot, ItemStack is, PageItem pageItem) {
        return super.set(slot + (getSize() - PLAYER_INVENTORY_SIZE), is, pageItem);
    }

    public SingleInventory setPlayerInventory(BukkitConfig config, String path, Player p, PageItem pageItem) {
        set(config.getInt(path + ".slot", 1) + (getSize() - PLAYER_INVENTORY_SIZE), config.getItemStack(path + ".itemStack", p), pageItem);
        return this;
    }

    public SingleInventory setPlayerInventory(BukkitConfig config, String path, Player p, PageForwardItem pageItem) {
        set(config.getInt(path + ".slot", 1) + (getSize() - PLAYER_INVENTORY_SIZE), config.getItemStack(path + ".itemStack", p), pageItem);
        return this;
    }

    public SingleInventory setPlayerInventory(int slot, String uniqueName, ItemStack is, PageItem pageItem) {
        return super.set(slot + (getSize() - PLAYER_INVENTORY_SIZE), uniqueName, is, pageItem);
    }

    @Override
    public SingleInventory fill(ItemStack is) {
        for (int i = 0; i < (getSize() - PLAYER_INVENTORY_SIZE); i++) {
            if (getData().get(i) == null) {
                add(is, PageItemStack.getInstance());
            }
        }
        return this;
    }

    public SingleInventory handlePlayerFillItems(BukkitConfig config, String path) {
        List<Integer> slots = config.getIntegerList(path + ".slots", true);
        slots.forEach(i -> setPlayerInventory(i, config.getItemStack(path + ".item"), e -> {
        }));
        return this;
    }

    @Override
    public void onClose() {
        Player player = getOwner();
        if (inventorySave != null && !restored) {
            inventorySave.applyOnPlayer(getOwner());
            restored = true;
        }
    }
}
