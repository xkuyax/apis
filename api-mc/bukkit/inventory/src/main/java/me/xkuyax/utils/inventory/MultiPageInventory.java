package me.xkuyax.utils.inventory;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import me.xkuyax.utils.config.BukkitConfig;
import me.xkuyax.utils.config.ItemBuilder;
import org.apache.commons.lang.Validate;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

@Getter
@Accessors(chain = true)
public class MultiPageInventory<V> extends SingleInventory implements IMultiPageInventory {

    @Setter
    private List<V> pageData;
    @Setter
    private int page = 0;
    @Setter
    private boolean clearOnUpdate = true;
    @Setter
    private int itemsPerSite = 45;
    @Setter
    private MultiPageDataClickItem<V> dataClickItem = (a, b, c, d) -> {
    };
    @Setter
    private ItemStack nextPageItemStack;
    @Setter
    private ItemStack lastPageItemStack;
    @Setter
    private List<Integer> nextPageItemSlots;
    @Setter
    private List<Integer> lastPageItemSlots;
    @Setter
    private boolean addControls = true;
    private List<Integer> fillSlots = new ArrayList<>();
    private ItemStack fillItem;
    private List<Integer> reservedSlots = new ArrayList<>();
    /**
     * If set to true, the next and last page button only appear if
     * there are more pages to look at
     */
    @Setter
    private boolean addSmartControls = true;
    @Setter
    private MultiPageItemStackFormatter<V> itemStackFormatter;
    private int finalSlot;

    public MultiPageInventory(Player player, int size, String title, boolean cancel, boolean force, boolean dynamic, List<V> data) {
        super(player, size, title, cancel, force, dynamic);
        setup(data, null, null);
    }

    public MultiPageInventory(Player player, int size, String title, boolean cancel, boolean force, List<V> data) {
        super(player, size, title, cancel, force);
        setup(data, null, null);
    }

    public MultiPageInventory(Player player, int size, String title, boolean cancel, List<V> data) {
        super(player, size, title, cancel);
        setup(data, null, null);
    }

    public MultiPageInventory(Player player, int size, String title, List<V> data) {
        super(player, size, title);
        setup(data, null, null);
    }

    public MultiPageInventory(Player player, BukkitConfig config, List<V> data) {
        this(player, config, "Inventory", data);
    }

    public MultiPageInventory(Player player, BukkitConfig config, String path, List<V> data) {
        super(player, config, path);
        setup(data, config, path);
    }

    private void setup(List<V> data, BukkitConfig config, String path) {
        this.pageData = data;
        if (config != null) {
            this.nextPageItemSlots = config.getIntegerList(path + ".nextPageSlots", true, 45);
            this.lastPageItemSlots = config.getIntegerList(path + ".lastPageSlots", true, 53);
            this.nextPageItemStack = config.getItemStack(path + ".nextPageItemStack");
            this.lastPageItemStack = config.getItemStack(path + ".lastPageItemStack");
            this.itemsPerSite = config.getInt(path + ".itemsPerSite", 45);
            this.fillSlots = config.getIntegerList(path + ".fill.slots", true);
            this.fillItem = config.getItemStack(path + ".fill.item");
            this.reservedSlots = config.getIntegerList(path + ".reservedSlots", 10000000);
        }
    }

    @Override
    public void updateInventory() {
        Validate.notNull(itemStackFormatter, "You have to set the itemStack Formatter!");
        if (clearOnUpdate) {
            clear();
        }
        int slot = bukkitConfig != null ? bukkitConfig.getInt("Inventory.slotStartItem", 0) : 0;
        for (int i = 0; i < itemsPerSite && (itemsPerSite * page) + i < pageData.size(); i++) {
            while (lastPageItemSlots.contains(slot) || nextPageItemSlots.contains(slot) || fillSlots.contains(slot) || reservedSlots.contains(slot)) {
                slot++;
            }
            finalSlot = slot;
            if (i >= pageData.size()) {
                break;
            }

            V data = getDataFromIndex(i);
            final int index = (itemsPerSite * page) + i;
            ItemStack dataItemStack = itemStackFormatter.format(finalSlot, index, data);
            setDataClickItem(data, index, dataItemStack);
            slot++;
        }
        fillSlots.forEach(i -> set(i, fillItem, PageItemStack.getInstance()));
        setTitle(getTitle().replace("%page%", page + ""));
        if (addControls) {
            Validate.notNull(lastPageItemStack, "You have to set the lastPageItemStack or use a constructor which provides a config to get the lastPageitemStack!");
            Validate.notNull(nextPageItemStack, "You have to set the nextPageItemStack or use a constructor which provides a config to get the nextPageitemStack!");
            if (!addSmartControls || page > 0) {
                int lastSize = page > 0 / itemsPerSite ? page : pageData.size() / itemsPerSite + 1;
                ItemBuilder lastItemStackBuilder = new ItemBuilder(lastPageItemStack);
                lastItemStackBuilder.formatBoth(("%page%"), (lastSize + ""));
                for (int lastPageItemSlot : lastPageItemSlots) {
                    set(lastPageItemSlot, lastItemStackBuilder.build(), getLastPageSlot());
                }
            }
            if (!addSmartControls || pageData.size() > itemsPerSite * (page + 1)) {
                int nextSite = page < pageData.size() / itemsPerSite ? page + 2 : 1;
                ItemBuilder builder = new ItemBuilder(nextPageItemStack);
                builder.formatBoth(("%page%"), (nextSite + ""));
                for (int nextPageItemSlot : nextPageItemSlots) {
                    set(nextPageItemSlot, builder.build(), getNextPageSlot());
                }
            }
            addControls();
        }
    }

    private PageItem getNextPageSlot() {
        return (e) -> {
            if (pageData.size() > itemsPerSite && page < pageData.size() / itemsPerSite) {
                page++;
                onNextPage(page);
                refresh();
            } else {
                if (page != 0) {
                    page = 0;
                    onLastPage(page);
                    refresh();
                }
            }
        };
    }

    private PageItem getLastPageSlot() {
        return (e) -> {
            if (page > 0) {
                page--;
                onLastPage(page);
                refresh();
            } else {
                if (page != pageData.size() / itemsPerSite) {
                    page = pageData.size() / itemsPerSite;
                    onLastPage(page);
                    refresh();
                }
            }
        };
    }

    private void setDataClickItem(V data, int index, ItemStack dataItemStack) {
        if (dataClickItem instanceof MultiPageInventory.MultiPageForwardClickItem) {
            set(finalSlot, dataItemStack, new PageForwardItem() {

                private InventoryClickEvent clickEvent;

                @Override
                public SingleInventory getForward(InventoryClickEvent e) {
                    return ((MultiPageForwardClickItem<V>) dataClickItem).onForward(finalSlot, index, data, clickEvent);
                }

                @Override
                public void onClick(InventoryClickEvent e) {
                    this.clickEvent = e;
                }
            });
        } else {
            set(finalSlot, dataItemStack, e -> dataClickItem.onClick(finalSlot, index, data, e));
        }
    }

    public V getDataFromIndex(int d) {
        return pageData.get((itemsPerSite * page) + d);
    }

    /**
     * Override this method to add your own control items
     */
    public void addControls() {

    }

    @Override
    public SingleInventory handleFillItems(BukkitConfig config, String path) {
        fillSlots = config.getIntegerList(path + ".slots", true);
        fillItem = config.getItemStack(path + ".item");
        return this;
    }

    @Data
    @AllArgsConstructor
    public static class WrappedData<V> {

        private V data;
        private ItemStack renderedItem;

    }

    public interface MultiPageForwardClickItem<V> extends MultiPageDataClickItem<V> {

        SingleInventory onForward(int slot, int index, V v, InventoryClickEvent e);

        default void onClick(int slot, int index, V v, InventoryClickEvent e) {
        }
    }

    public interface MultiPageDataClickItem<V> extends PageItem {

        void onClick(int slot, int index, V v, InventoryClickEvent e);

        @Override
        default void onClick(InventoryClickEvent e) {
        }
    }

    public interface MultiPageItemStackFormatter<V> {

        ItemStack format(int slot, int index, V v);

    }
}
