package me.xkuyax.utils.inventory;

import me.xkuyax.utils.APIPlugin;
import me.xkuyax.utils.ItemUtils;
import me.xkuyax.utils.inventory.SingleInventory.PageItemData;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class InventoryModule implements Listener {

    private static InventoryModule instance;
    private final Map<String, SingleInventory> watch = new HashMap<>();

    public InventoryModule() {
        Bukkit.getPluginManager().registerEvents(this, APIPlugin.getPlugin());
        new BukkitRunnable() {

            @Override
            public void run() {
                for (Iterator<Entry<String, SingleInventory>> it = watch.entrySet().iterator(); it.hasNext(); ) {
                    Entry<String, SingleInventory> e = it.next();
                    SingleInventory inv = e.getValue();
                    Player p = inv.getOwner();
                    if (p == null || inv.getNotRight() >= 2) {
                        it.remove();
                        inv.onClose();
                        continue;
                    }
                    String invTitle = p.getOpenInventory().getTitle();
                    String realTitle = inv.getTitle();
                    if (!invTitle.equals(realTitle)) {
                        it.remove();
                        inv.onClose();
                        continue;
                    }
                    if (inv.isDynamic()) {
                        if (inv.getChanged() == 1) {
                            overrideContent(inv);
                            inv.setChanged(0);
                        }
                    }
                }
            }
        }.runTaskTimer(APIPlugin.getPlugin(), 10, 20);
    }

    public void register(SingleInventory newInventory, Inventory inv) {
        SingleInventory old = watch.get(newInventory.getPlayer());
        if (old != null) {
            if (old.isPlayerInventory() && newInventory.isPlayerInventory()) {
                old.setRestored(true);
                newInventory.setInventorySave(old.getInventorySave());
            }
            if (old instanceof SinglePlayerInventory && newInventory instanceof SinglePlayerInventory) {
                SinglePlayerInventory spi = (SinglePlayerInventory) old;
                SinglePlayerInventory spin = (SinglePlayerInventory) newInventory;
                spi.setRestored(true);
                spin.setInventorySave(spi.getInventorySave());
            }
            old.onClose();
        }
        watch.put(newInventory.getPlayer(), newInventory);
    }

    @EventHandler
    public void onInventoryClickEvent(InventoryClickEvent e) {
        if (e.getAction() != null) {
            if (e.getClickedInventory() != null) {
                if (e.getSlot() == e.getRawSlot()) {
                    if (e.getWhoClicked() instanceof Player) {
                        if (e.getCurrentItem() != null) {
                            if (e.getCurrentItem().getType() != Material.AIR) {
                                BetterClickEvent bce = new BetterClickEvent(e);
                                Bukkit.getPluginManager().callEvent(bce);
                            }
                        }
                    }
                }
            }
        }
    }

    @EventHandler
    public void onBetterClickEvent(InventoryClickEvent e) {
        if (e.getAction() != null) {
            if (e.getClickedInventory() != null) {
                if (e.getWhoClicked() instanceof Player) {
                    if (e.getCurrentItem() != null) {
                        if (e.getCurrentItem().getType() != Material.AIR) {
                            String hidden = getHiddenString(e.getCurrentItem());
                            if (hidden != null) {
                                SingleInventory pi = watch.get(e.getWhoClicked().getName());
                                if (pi != null) {
                                    if (pi.isCancel()) {
                                        e.setCancelled(true);
                                        ((Player) e.getWhoClicked()).updateInventory();
                                    }
                                    for (PageItemData pid : pi.getData().values()) {
                                        if (pid.getUniqueName().equals(hidden)) {
                                            if (pid.getItem() instanceof PageItemStack) {
                                                continue;
                                            }
                                            pi.setRightClick(pi.isRightClick(e));
                                            pi.onClick();
                                            pid.getItem().onClick(e);
                                            if (pid.getItem() instanceof PageForwardItem) {
                                                PageForwardItem pfi = (PageForwardItem) pid.getItem();
                                                SingleInventory forward = pfi.getForward(e);
                                                if (forward != null && forward != SingleInventory.NO_FORWARD_INVENTORY) {
                                                    if (forward.isForce()) {
                                                        forward.updateInventory();
                                                        e.getWhoClicked().openInventory(forward.build());
                                                    } else {
                                                        overrideContent(forward, true);
                                                    }
                                                    return;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    @EventHandler
    public void onPlayerQuitEvent(PlayerQuitEvent e) {
        watch.remove(e.getPlayer().getName());
    }

    @EventHandler
    public void onPlayerKickEvent(PlayerKickEvent e) {
        watch.remove(e.getPlayer().getName());
    }

    public void saveOverrideContent(SingleInventory singleInventory) {
        Player p = singleInventory.getOwner();
        Inventory top = p.getOpenInventory().getTopInventory();
        if (top.getTitle().equalsIgnoreCase(singleInventory.getTitle())) {
            overrideContent(singleInventory);
        }
    }

    public void overrideContent(SingleInventory si) {
        overrideContent(si, false);
    }

    public void overrideContent(SingleInventory si, boolean b) {
        Player p = si.getOwner();
        si.updateInventory();
        Inventory top = p.getOpenInventory().getTopInventory();
        top.clear();
        si.build(top, b);
    }

    @EventHandler
    public void onClose(InventoryCloseEvent e) {
        Player p = (Player) e.getPlayer();
        SingleInventory inventory = watch.get(p.getName());
        if (inventory != null) {
            if (inventory.getLastInventoryBuild() != null) {
                if (inventory.getLastInventoryBuild().equals(e.getInventory())) {
                    inventory.onClose();
                }
            }
        }
    }

    public void cleanUpPlayer(String name) {
        watch.remove(name);
    }

    public static InventoryModule a() {
        return instance == null ? instance = new InventoryModule() : instance;
    }

    public static String getHiddenString(ItemStack item) {
        return ItemUtils.getHiddenString(item);
    }

    public static ItemStack setHiddenString(ItemStack itemToName, String name) {
        return ItemUtils.setHiddenString(itemToName, name);
    }
}
