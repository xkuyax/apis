package me.xkuyax.utils.inventory;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.inventory.InventoryClickEvent;

public class BetterClickEvent extends Event {
    
    private static final HandlerList handlers = new HandlerList();
    @Getter
    @Setter
    private InventoryClickEvent event;
    
    public BetterClickEvent(InventoryClickEvent event) {
        this.event = event;
    }
    
    @Override
    public HandlerList getHandlers() {
        return handlers;
    }
    
    public static HandlerList getHandlerList() {
        return handlers;
    }
}
