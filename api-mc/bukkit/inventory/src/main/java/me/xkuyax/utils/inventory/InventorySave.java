package me.xkuyax.utils.inventory;

import lombok.Data;
import me.xkuyax.utils.APIPlugin;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

@Data
public class InventorySave {
    
    private ItemStack[] contents = new ItemStack[36];
    
    public InventorySave(Player p) {
        int i = 0;
        for (ItemStack is : p.getInventory().getArmorContents()) {
            i++;
        }
        i = 0;
        for (ItemStack is : p.getInventory().getContents()) {
            contents[i] = (is == null ? null : is.clone());
            i++;
        }
    }
    
    public void applyOnPlayer(Player p) {
        ItemStack[] armor = new ItemStack[4];
        ItemStack[] contents = new ItemStack[36];
        int i = 0;
        for (ItemStack is : this.contents) {
            contents[i] = (is == null ? null : is.clone());
            i++;
        }
        p.getInventory().setContents(contents);
        p.updateInventory();
        Bukkit.getScheduler().runTaskLater(APIPlugin.getPlugin(), p::updateInventory, 1);
    }
}
