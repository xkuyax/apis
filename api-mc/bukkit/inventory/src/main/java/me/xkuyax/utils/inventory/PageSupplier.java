package me.xkuyax.utils.inventory;

import org.bukkit.event.inventory.InventoryClickEvent;

import java.util.function.Supplier;

public interface PageSupplier extends Supplier<SingleInventory>, PageForwardItem {
    
    @Override
    default void onClick(InventoryClickEvent e) {
    }
    
    @Override
    default SingleInventory getForward(InventoryClickEvent e) {
        return get();
    }
}
