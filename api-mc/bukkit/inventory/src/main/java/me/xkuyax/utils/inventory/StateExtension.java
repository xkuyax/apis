package me.xkuyax.utils.inventory;

import me.xkuyax.utils.RollingCollectionUtils;
import org.bukkit.inventory.ItemStack;

import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;

public interface StateExtension extends ModifiableInventory {

    default <T> void setState(String path, List<T> states, Supplier<T> stateSupplier, Consumer<T> stateSetter, Function<T, ItemStack> itemMapper) {
        T currentValue = stateSupplier.get();
        ItemStack itemStack = itemMapper.apply(currentValue);
        setClick(path, itemStack, e -> {
            T nextValue = RollingCollectionUtils.getNextWithNull(currentValue, states);
            stateSetter.accept(nextValue);
            syncRefresh();
        });
    }

    default <T> void setStateName(String path, List<T> states, Supplier<T> stateSupplier, Consumer<T> stateSetter,Function<T,String> nameFunction) {
        setState(path, states, stateSupplier, stateSetter, val -> getBukkitConfig().getItemStack(path + "." + nameFunction.apply(val)));
    }

    default <T> void setState(String path, List<T> states, Supplier<T> stateSupplier, Consumer<T> stateSetter) {
        setState(path, states, stateSupplier, stateSetter, val -> getBukkitConfig().getItemStack(path + "." + val));
    }
}
