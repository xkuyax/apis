package me.xkuyax.utils.inventory;

import org.bukkit.event.inventory.InventoryClickEvent;

public interface PageItem {
    
    void onClick(InventoryClickEvent e);
}
