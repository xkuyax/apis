package me.xkuyax.utils.inventory;

import me.xkuyax.utils.APIPlugin;
import me.xkuyax.utils.ItemUtils;
import me.xkuyax.utils.Lambdas.ExceptionRunnable;
import me.xkuyax.utils.config.BukkitConfig;
import me.xkuyax.utils.inventory.SingleInventory.PageItemData;
import me.xkuyax.utils.inventory.SingleInventory.SingleInventoryItemStackFormatter;
import me.xkuyax.utils.inventory.SingleInventory.SingleInventoryItemStackMultiConsumer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryAction;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.List;

public interface ModifiableInventory {

    SingleInventory setClick(int slot, ItemStack itemStack, PageItem pageItem);

    SingleInventory setClick(String path, PageItem pageItem);

    SingleInventory setForward(String path, BPageSupplier pageItem);

    SingleInventory setClick(String path, ItemStack itemStack, PageItem pageItem);

    SingleInventory setForward(String path, ItemStack itemStack, BPageSupplier pageItem);

    SingleInventory setForward(int slot, ItemStack itemStack, BPageSupplier pageSupplier);

    Player getOwner();

    SingleInventory add(String uniqueName, ItemStack is, PageItem pageItem);

    SingleInventory add(BukkitConfig config, String path, Player p, PageItem pageItem);

    SingleInventory add(BukkitConfig config, String path, Player p, PageSupplier pageItem);

    SingleInventory set(BukkitConfig config, String path, Player p, PageItem pageItem);

    SingleInventory set(BukkitConfig config, String path, Player p, PageSupplier pageItem);

    SingleInventory add(ItemStack is, PageItem pageItem);

    SingleInventory add(ItemStack is, PageSupplier pageItem);

    SingleInventory set(int slot, String uniqueName, ItemStack is, PageItem pageItem);

    SingleInventory set(int slot, ItemStack is, PageItem pageItem);

    SingleInventory set(int slot, ItemStack is, PageSupplier pageItem);

    <T> SingleInventory set(BukkitConfig config, String path, List<T> data, SingleInventoryItemStackFormatter<T> formatter,
                            SingleInventoryItemStackMultiConsumer<T> pageItem);

    PageItemData getItem(int slot);

    Inventory build(Inventory inv, boolean reg);

    void refresh();

    default void setGlow(BukkitConfig config, String path, Player p, PageItem item, boolean glow) {
        int slot = config.getInt(path + ".slot", this.getSize() - 1);
        ItemStack itemStack = config.getItemStack(path + ".itemStack", p);
        if (glow) {
            itemStack = ItemUtils.addGlow(itemStack);
        }
        set(slot, itemStack, item);
    }

    default void syncRefresh() {
        Bukkit.getScheduler().runTask(APIPlugin.getPlugin(), this::refresh);
    }

    default void async(ExceptionRunnable runnable) {
        Bukkit.getScheduler().runTaskAsynchronously(APIPlugin.getPlugin(), runnable);
    }

    default void sync(ExceptionRunnable runnable) {
        Bukkit.getScheduler().runTask(APIPlugin.getPlugin(), runnable);
    }

    default void addDummyItems(BukkitConfig config) {
        addDummyItems(config, "PageItemStack");
    }

    default void addDummyItems(BukkitConfig config, String path) {
        Player player = getOwner();
        config.getStringList(path + "s", "1", "2", "3").forEach(s -> set(config, path + "." + s, player, PageItemStack.getInstance()));
    }

    void updateInventory();

    Inventory build(Inventory inv);

    Inventory build();

    void clear();

    SingleInventory fill(ItemStack is);

    SingleInventory handleFillItems(BukkitConfig config, String path);

    SingleInventory fill(BukkitConfig config, String path);

    void onClick();

    void onClose();

    int getLastSlot();

    java.util.concurrent.ConcurrentHashMap<Integer, PageItemData> getData();

    String getPlayer();

    String getTitle();

    int getSize();

    boolean isCancel();

    boolean isForce();

    boolean isDynamic();

    int getChanged();

    int getNotRight();

    Inventory getLastInventoryBuild();

    void setData(java.util.concurrent.ConcurrentHashMap<Integer, PageItemData> data);

    void setPlayer(String player);

    void setTitle(String title);

    void setSize(int size);

    void setCancel(boolean cancel);

    void setForce(boolean force);

    void setDynamic(boolean dynamic);

    void setChanged(int changed);

    void setNotRight(int notRight);

    void setLastInventoryBuild(Inventory lastInventoryBuild);

    boolean isRightClick();

    default boolean isRightClick(InventoryClickEvent e) {
        return e.getAction() == InventoryAction.PICKUP_HALF;
    }

    BukkitConfig getBukkitConfig();
}
