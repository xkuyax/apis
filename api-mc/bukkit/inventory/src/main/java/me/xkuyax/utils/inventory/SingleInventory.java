package me.xkuyax.utils.inventory;

import lombok.*;
import me.xkuyax.utils.APIPlugin;
import me.xkuyax.utils.config.BukkitConfig;
import org.apache.commons.lang3.Validate;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;

@Data
@EqualsAndHashCode(of = "player")
public class SingleInventory implements ModifiableInventory, StateExtension {

    @Data
    @AllArgsConstructor
    public class PageItemData {

        private PageItem item;
        private ItemStack itemStack;
        private String uniqueName;
    }

    public static final SingleInventory NO_FORWARD_INVENTORY = new SingleInventory();
    public static final int PLAYER_INVENTORY_SIZE = 36;
    private ConcurrentHashMap<Integer, PageItemData> data;
    private String player, title;
    private int size;
    private boolean cancel, force, dynamic;
    private int changed = 1, notRight = 0;
    private Inventory lastInventoryBuild;
    private boolean rightClick;
    @Getter(value = AccessLevel.PUBLIC)
    protected BukkitConfig bukkitConfig;
    private boolean playerInventory;
    private boolean saveInventory = true;
    private InventorySave inventorySave;
    private boolean restored = false;

    public SingleInventory(Player player, int size, String title, boolean cancel, boolean force, boolean dynamic) {
        Validate.notNull(player, "Cant create an inventory for an null player");
        Validate.isTrue(size % 9 == 0, "The inventory size has to be dividable by 9!");
        Validate.notNull(title, "Cant create an inventory with an null title!");
        setPlayer(player.getName());
        setDynamic(dynamic);
        setSize(size);
        setTitle(title);
        setForce(true);
        setCancel(cancel);
        data = new ConcurrentHashMap<>();
    }

    public SingleInventory(Player player, int size, String title, boolean cancel, boolean force) {
        this(player, size, title, cancel, force, false);
    }

    public SingleInventory(Player player, int size, String title, boolean cancel) {
        this(player, size, title, cancel, false);
    }

    public SingleInventory(Player player, int size, String title) {
        this(player, size, title, true, false);
    }

    public SingleInventory(Player player, BukkitConfig config, String path) {
        this(player, config.getInt(path + ".size", 54), config.getTranslated(path + ".title", player, "&aNice Inventory"), true);
        this.bukkitConfig = config;
        if (bukkitConfig.getBoolean(path + ".playerInventory", false)) {
            setPlayerInventory();
        }
    }

    public SingleInventory(Player player, BukkitConfig config) {
        this(player, config, "Inventory");
    }

    private SingleInventory() {

    }

    @Override
    public SingleInventory setClick(int slot, ItemStack itemStack, PageItem pageItem) {
        return set(slot, itemStack, pageItem);
    }

    @Override
    public SingleInventory setClick(String path, PageItem pageItem) {
        set(bukkitConfig, path, getOwner(), pageItem);
        return this;
    }

    @Override
    public SingleInventory setForward(String path, BPageSupplier pageItem) {
        return setClick(path, pageItem);
    }

    @Override
    public SingleInventory setClick(String path, ItemStack itemStack, PageItem pageItem) {
        int slot = bukkitConfig.getInt(path + ".slot", getSize() - 3);
        set(slot, itemStack, pageItem);
        return this;
    }

    @Override
    public SingleInventory setForward(String path, ItemStack itemStack, BPageSupplier pageItem) {
        return setClick(path, itemStack, pageItem);
    }

    @Override
    public SingleInventory setForward(int slot, ItemStack itemStack, BPageSupplier pageSupplier) {
        return set(slot, itemStack, pageSupplier);
    }

    @Override
    public Player getOwner() {
        return Bukkit.getPlayer(player);
    }

    @Override
    public SingleInventory add(String uniqueName, ItemStack is, PageItem pageItem) {
        set(getLastSlot(), uniqueName, is, pageItem);
        return this;
    }

    @Override
    public SingleInventory add(BukkitConfig config, String path, Player p, PageItem pageItem) {
        add(config.getItemStack(path, p), pageItem);
        return this;
    }

    @Override
    public SingleInventory add(BukkitConfig config, String path, Player p, PageSupplier pageItem) {
        add(config.getItemStack(path, p), pageItem);
        return this;
    }

    @Override
    public SingleInventory set(BukkitConfig config, String path, Player p, PageItem pageItem) {
        set(config.getInt(path + ".slot", getSize() - 1), config.getItemStack(path + ".itemStack", p), pageItem);
        return this;
    }

    @Override
    public SingleInventory set(BukkitConfig config, String path, Player p, PageSupplier pageItem) {
        set(config.getInt(path + ".slot", getSize() - 1), config.getItemStack(path + ".itemStack", p), pageItem);
        return this;
    }

    @Override
    public SingleInventory add(ItemStack is, PageItem pageItem) {
        return add(System.nanoTime() + "", is, pageItem);
    }

    @Override
    public SingleInventory add(ItemStack is, PageSupplier pageItem) {
        return add(System.nanoTime() + "", is, pageItem);
    }

    @Override
    public SingleInventory set(int slot, String uniqueName, ItemStack is, PageItem pageItem) {
        PageItemData pd = data.get(slot);
        if (pd == null || !pd.getItemStack().isSimilar(is)) {
            data.put(slot, new PageItemData(pageItem, is, uniqueName));
            changed = 1;
        }
        return this;
    }

    @Override
    public SingleInventory set(int slot, ItemStack is, PageItem pageItem) {
        validate(slot, is, pageItem);
        set(slot, System.nanoTime() + "", is, pageItem);
        return this;
    }

    @Override
    public SingleInventory set(int slot, ItemStack is, PageSupplier pageItem) {
        validate(slot, is, pageItem);
        set(slot, System.nanoTime() + "", is, pageItem);
        return this;
    }

    private void validate(int slot, ItemStack is, PageItem pageItem) {
        Validate.notNull(is, "You cant set an null itemstack");
        Validate.notNull(pageItem, "Cant set an null pageitem");
    }

    @Override
    public <T> SingleInventory set(BukkitConfig config, String path, List<T> data, SingleInventoryItemStackFormatter<T> formatter,
                                   SingleInventoryItemStackMultiConsumer<T> pageItem) {
        for (int i = 0; i < data.size(); i++) {
            T t = data.get(i);
            ItemStack itemStack = formatter.format(t);
            int slot = config.getInt(path + ".slots." + i, i);
            set(slot, itemStack, e -> pageItem.onClick(t, e));
        }
        return this;
    }

    @Override
    public PageItemData getItem(int slot) {
        return data.get(slot);
    }

    @Override
    public Inventory build(Inventory inv, boolean reg) {
        if (playerInventory) {
            return buildPlayerInventory(inv, reg);
        }
        for (Entry<Integer, PageItemData> e : data.entrySet()) {
            Integer i = e.getKey();
            PageItemData pi = e.getValue();
            ItemStack itemStack = pi.getItemStack();
            if (itemStack != null && i < inv.getSize()) {
                if (!pi.getUniqueName().isEmpty()) {
                    setItem(inv, i, InventoryModule.setHiddenString(itemStack.clone(), pi.getUniqueName()));
                } else {
                    setItem(inv, i, itemStack.clone());
                }
            }
        }
        setLastInventoryBuild(inv);
        if (reg) {
            InventoryModule.a().register(this, inv);
        }
        return inv;
    }

    private void setItem(Inventory inv, Integer i, ItemStack itemStack) {
        if (i < inv.getSize()) {
            inv.setItem(i, itemStack);
        } else {
            System.out.printf("The itemstack %s is set to slot %d which is greater or equal than the inventory %d%n", itemStack, i, inv.getSize());
        }
    }

    @Override
    public void refresh() {
        InventoryModule.a().saveOverrideContent(this);
    }

    @Override
    public void updateInventory() {
    }

    @Override
    public Inventory build(Inventory inv) {
        return build(inv, true);
    }

    @Override
    public Inventory build() {
        return build(Bukkit.createInventory(getOwner(), size, title.length() > 32 ? title.substring(0, 32) : title));
    }

    @Override
    public void clear() {
        data.clear();
        changed = 1;
    }

    @Override
    public SingleInventory fill(ItemStack is) {
        for (int i = 0; i < size; i++) {
            if (data.get(i) == null) {
                add(is, PageItemStack.getInstance());
            }
        }
        return this;
    }

    @Override
    public SingleInventory handleFillItems(BukkitConfig config, String path) {
        List<Integer> slots = config.getIntegerList(path + ".slots", 1, 2, 3);
        slots.forEach(i -> {
            if (data.get(i) == null) {
                set(i, config.getItemStack(path + ".item"), e -> {
                });
            }
        });
        return this;
    }

    @Override
    public SingleInventory fill(BukkitConfig config, String path) {
        return fill(config.getItemStack(path, getOwner()));
    }

    @Override
    public void onClick() {
    }

    @Override
    public void onClose() {
        if (isPlayerInventory()) {
            checkForInventorySave();
        }
    }

    @Override
    public int getLastSlot() {
        for (int i = 0; i < size; i++) {
            if (data.get(i) == null) {
                return i;
            }
        }
        throw new IllegalStateException("There is no free slot in this inventory!");
    }

    public void setPlayerInventory() {
        this.playerInventory = true;
    }

    public Inventory buildPlayerInventory(Inventory inventory, boolean register) {
        Player player = getOwner();
        PlayerInventory playerInventory = player.getInventory();
        if (inventorySave == null) {
            inventorySave = new InventorySave(player);
            playerInventory.clear();
            // playerInventory.setArmorContents(null);
            player.updateInventory();
            Bukkit.getScheduler().runTaskLater(APIPlugin.getPlugin(), player::updateInventory, 1);
        }
        playerInventory.clear();
        for (Entry<Integer, PageItemData> entry : getData().entrySet()) {
            Integer slot = entry.getKey();
            PageItemData pageItemData = entry.getValue();
            if (pageItemData.getItemStack() != null) {
                int size = getSize();
                String uniqueName = pageItemData.getUniqueName();
                ItemStack clone = pageItemData.getItemStack().clone();
                ItemStack itemWithHiddenString = InventoryModule.setHiddenString(clone, uniqueName);
                if (!uniqueName.isEmpty()) {
                    if (slot < size) {
                        setItem(inventory, slot, itemWithHiddenString);
                    } else {
                        setItem(playerInventory, slot - size, itemWithHiddenString);
                    }
                } else {
                    if (slot < size) {
                        setItem(inventory, slot, clone);
                    } else {
                        setItem(playerInventory, slot - size, itemWithHiddenString);
                    }
                }
            }
        }
        setLastInventoryBuild(inventory);
        if (register) {
            InventoryModule.a().register(this, inventory);
        }
        return inventory;
    }

    public void checkForInventorySave() {
        if (inventorySave != null && !restored) {
            inventorySave.applyOnPlayer(getOwner());
            restored = true;
        }
    }

    public interface SingleInventoryItemStackMultiConsumer<T> {

        void onClick(T t, InventoryClickEvent e);

    }

    public interface SingleInventoryItemStackFormatter<T> {

        ItemStack format(T t);

    }
}
