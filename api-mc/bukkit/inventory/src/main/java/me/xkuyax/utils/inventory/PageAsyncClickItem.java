package me.xkuyax.utils.inventory;

import me.xkuyax.utils.APIPlugin;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.scheduler.BukkitRunnable;

public abstract class PageAsyncClickItem implements PageItem {
    
    @Override
    public void onClick(InventoryClickEvent e) {
        new BukkitRunnable() {
            
            @Override
            public void run() {
                onClick();
            }
        }.runTaskAsynchronously(APIPlugin.getPlugin());
    }
    
    public abstract void onClick();
}
