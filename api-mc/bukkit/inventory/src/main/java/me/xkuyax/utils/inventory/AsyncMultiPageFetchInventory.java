package me.xkuyax.utils.inventory;

import lombok.Data;
import lombok.EqualsAndHashCode;
import me.xkuyax.utils.ListHelper;
import me.xkuyax.utils.config.BukkitConfig;
import me.xkuyax.utils.inventory.MultiPageInventory.WrappedData;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

@Data
public abstract class AsyncMultiPageFetchInventory<V> extends MultiPageInventory<WrappedData<V>> {

    private final BukkitConfig config;
    private int from;
    private int to;

    public AsyncMultiPageFetchInventory(Player player, BukkitConfig config) {
        super(player, config, "Inventory", new ArrayList<>());
        this.config = config;
        from = 0;
        to = preLoadSize();
        setItemStackFormatter((slot, index, vWrappedData) -> vWrappedData.getRenderedItem());
        fetchAndUpdate();
    }

    public abstract List<WrappedData<V>> fetch(int from, int to) throws Exception;

    @Override
    public void onNextPage(int page) {
        from = to;
        to = to + preLoadSize();
        fetchAndUpdate();
    }

    @Override
    public void updateInventory() {
        handleFillItems(config, "Fill");
        super.updateInventory();
    }

    public void fetchAndUpdate() {
        async(() -> {
            getPageData().addAll(fetch(from, to));
            syncRefresh();
        });
    }

    public void reset() {
        setPage(0);
        from = 0;
        to = preLoadSize();
        getPageData().clear();
        fetchAndUpdate();
    }

    public int preLoadSize() {
        return getItemsPerSite() * 2;
    }

    public <T> List<T> sublist(List<T> list, int fromIndex, int toIndex) {
        return ListHelper.sublist(list, fromIndex, toIndex);
    }
}
