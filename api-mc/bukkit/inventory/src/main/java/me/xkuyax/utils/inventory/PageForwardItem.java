package me.xkuyax.utils.inventory;

import org.bukkit.event.inventory.InventoryClickEvent;

public interface PageForwardItem extends PageItem {
    
    default void onClick(InventoryClickEvent e) {
    }
    
    SingleInventory getForward(InventoryClickEvent e);
}