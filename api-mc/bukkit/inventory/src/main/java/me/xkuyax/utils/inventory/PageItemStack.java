package me.xkuyax.utils.inventory;

import lombok.Getter;
import org.bukkit.event.inventory.InventoryClickEvent;

public class PageItemStack implements PageItem {
    
    @SuppressWarnings("deprecation")
    @Getter
    public static PageItemStack instance = new PageItemStack();
    
    @Deprecated
    public PageItemStack() {
    }
    
    @Override
    public void onClick(InventoryClickEvent e) {
    }
}
