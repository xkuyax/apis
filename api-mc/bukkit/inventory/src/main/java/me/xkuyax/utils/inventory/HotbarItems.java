package me.xkuyax.utils.inventory;

import com.google.common.base.Preconditions;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import me.xkuyax.utils.APIPlugin;
import me.xkuyax.utils.config.BukkitConfig;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.Event.Result;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

import java.util.*;

public class HotbarItems implements Listener {

    private final HashMap<UUID, List<HotbarItemData>> data = new HashMap<>();
    private static final Random random = new Random();
    private static HotbarItems instance;

    public HotbarItems() {
        Bukkit.getPluginManager().registerEvents(this, APIPlugin.getPlugin());
    }

    public void add(Player p, ItemStack is, int slot, HotbarItemsClickHandler handler) {
        if (p == null) return;
        Preconditions.checkArgument(slot < 9 && slot >= 0, "Sorry, but the hotbar is slot 0-8");
        String uniq = System.nanoTime() + "";
        is = InventoryModule.setHiddenString(is.clone(), uniq);
        if (!data.containsKey(p.getUniqueId())) {
            data.put(p.getUniqueId(), new ArrayList<>());
        }
        for (int i = 0; i < data.get(p.getUniqueId()).size(); i++) {
            HotbarItemData data = this.data.get(p.getUniqueId()).get(i);
            if (data.getSlot() == slot) {
                if (p.getInventory().getItem(data.getSlot()) != null) {
                    String s = InventoryModule.getHiddenString(is);
                    if (s != null) {
                        if (s.equals(data.getUniqueName())) {
                            p.getInventory().setItem(data.getSlot(), new ItemStack(Material.AIR));
                        }
                    }
                }
                this.data.get(p.getUniqueId()).remove(i);
            }
        }
        HotbarItemData data = new HotbarItemData(p.getUniqueId(), is, slot, uniq, handler);
        this.data.get(p.getUniqueId()).add(data);
        p.getInventory().setItem(data.getSlot(), data.getItem());
        p.updateInventory();
    }

    public void add(Player p, BukkitConfig config, String path, HotbarItemsClickHandler click) {
        add(p, config.getItemStack(path + ".item", p), config.getInt(path + ".slot", random.nextInt(9)), click);
    }

    @EventHandler
    public void onPlayerDropItemEvent(PlayerDropItemEvent e) {
        ItemStack is = e.getItemDrop().getItemStack();
        Player p = e.getPlayer();
        if (data.containsKey(p.getUniqueId())) {
            String s = InventoryModule.getHiddenString(is);
            if (s != null) {
                for (HotbarItemData d : data.get(p.getUniqueId())) {
                    if (d.getUniqueName().equals(s)) {
                        e.setCancelled(true);
                        return;
                    }
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerInteract(PlayerInteractEvent e) {
        Player p = e.getPlayer();
        if (e.getItem() != null) {
            if (data.containsKey(p.getUniqueId())) {
                String s = InventoryModule.getHiddenString(e.getItem());
                if (s != null) {
                    List<HotbarItemData> datas = data.get(p.getUniqueId());
                    for (HotbarItemData d : datas) {
                        if (d.getUniqueName().equals(s)) {
                            HotbarItemClick click = new HotbarItemClick(p, d.getItem(), d.getSlot(), e);
                            d.getHandler().onClick(click);
                            if (click.isUpdateHotbarItem()) {
                                add(p, click.getItem(), click.getSlot(), d.getHandler());
                            }
                            e.setCancelled(true);
                            e.setUseItemInHand(Result.DENY);
                            p.updateInventory();
                        }
                    }
                }
            }
        }
    }

    @EventHandler
    public void onInventoryClickEvent(BetterClickEvent ev) {
        InventoryClickEvent e = ev.getEvent();
        Player p = (Player) e.getWhoClicked();
        if (e.getCurrentItem() != null) {
            if (data.containsKey(p.getUniqueId())) {
                String s = InventoryModule.getHiddenString(e.getCurrentItem());
                if (s != null) {
                    List<HotbarItemData> datas = data.get(p.getUniqueId());
                    for (HotbarItemData d : datas) {
                        if (d.getUniqueName().equals(s)) {
                            e.setCancelled(true);
                            ((Player) e.getWhoClicked()).updateInventory();
                        }
                    }
                }
            }
        }
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        Player p = e.getPlayer();
        data.remove(p.getUniqueId());
    }

    public static HotbarItems a() {
        return instance == null ? (instance = new HotbarItems()) : instance;
    }

    @Data
    @RequiredArgsConstructor
    public static class HotbarItemClick {

        @NonNull
        private Player player;
        @NonNull
        private ItemStack item;
        @NonNull
        private int slot;
        private boolean updateHotbarItem = false;
        @NonNull
        private PlayerInteractEvent event;
    }

    @Data
    @AllArgsConstructor
    public static class HotbarItemData {

        private final UUID player;
        private final ItemStack item;
        private final int slot;
        private final String uniqueName;
        private final HotbarItemsClickHandler handler;
    }

    public interface HotbarItemsClickHandler {

        void onClick(HotbarItemClick e);
    }
}
