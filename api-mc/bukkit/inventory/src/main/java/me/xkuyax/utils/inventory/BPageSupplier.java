package me.xkuyax.utils.inventory;

import org.bukkit.event.inventory.InventoryClickEvent;

@FunctionalInterface
public interface BPageSupplier extends PageSupplier {
    
    SingleInventory get(InventoryClickEvent e);
    
    @Override
    default SingleInventory getForward(InventoryClickEvent e) {
        return get(e);
    }
    
    @Override
    default SingleInventory get() {
        return null;
    }
}
