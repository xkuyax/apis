package me.xkuyax.api.bukkit.hologram;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class TextLine implements HologramLine {

    private String currentLine;

    public TextLine(String currentLine) {
        this.currentLine = currentLine;
    }
}
