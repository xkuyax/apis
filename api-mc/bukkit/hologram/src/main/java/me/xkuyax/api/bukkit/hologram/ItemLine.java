package me.xkuyax.api.bukkit.hologram;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.bukkit.inventory.ItemStack;

@Data
@NoArgsConstructor
public class ItemLine implements HologramLine {

    private ItemStack itemStack;

    public ItemLine(ItemStack itemStack) {
        this.itemStack = itemStack;
    }
}
