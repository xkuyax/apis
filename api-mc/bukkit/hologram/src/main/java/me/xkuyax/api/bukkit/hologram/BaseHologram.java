package me.xkuyax.api.bukkit.hologram;

import lombok.Data;
import me.xkuyax.api.bukkit.hologram.components.SentHologramPlayerData;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

@Data
public class BaseHologram {

    private final Player player;
    private Location location;
    private List<HologramLine> lines = new ArrayList<>();
    private SentHologramPlayerData playerData = new SentHologramPlayerData();
    //lololol
    private boolean forceUpdate;

    public BaseHologram(Player player, Location location) {
        this.player = player;
        this.location = location;
    }

    public HologramLine getLine(int index) {
        return lines.get(index);
    }

    public void setText(int index, String line) {
        lines.set(index, new TextLine(line));
    }

    public void addText(String line) {
        lines.add(new TextLine(line));
    }

    public void setItem(int index, ItemStack itemStack) {
        lines.set(index, new ItemLine(itemStack));
    }

    public void addItem(ItemStack itemStack) {
        lines.add(new ItemLine(itemStack));
    }

    public void removeLine(int index) {
        lines.remove(index);
    }

    public void moveTo(Location location) {
        forceUpdate = true;
    }

    public void update() {

    }

    public void clear() {

    }
}
