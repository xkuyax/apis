package me.xkuyax.utils.bukkit;

import me.xkuyax.module.BukkitPlatform;
import me.xkuyax.utils.APIPlugin;
import me.xkuyax.utils.config.ConfigReloadCommand;
import me.xkuyax.utils.mc.MCPlatforms;
import org.bukkit.plugin.java.JavaPlugin;

public class ApiPluginMain extends JavaPlugin {

    @Override
    public void onEnable() {
        APIPlugin.setPlugin(this);
        MCPlatforms.setCurrentPlatform(new BukkitPlatform());
        new ConfigReloadCommand();
        System.out.println("Registered ConfigReloadCommand!");
    }
}
