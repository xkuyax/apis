package me.xkuyax.utils.message;

import me.xkuyax.utils.StringFormatting;
import me.xkuyax.utils.StringUtils;
import me.xkuyax.utils.Titles;
import me.xkuyax.utils.config.BukkitConfig;
import me.xkuyax.utils.mc.message.MessagePlaceholderHandler;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle.EnumTitleAction;
import org.apache.commons.lang.ArrayUtils;
import org.bukkit.Sound;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import sun.reflect.Reflection;

import java.util.*;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.function.Supplier;

public class Message {

    private static final Map<String, Supplier<BukkitConfig>> SUPPLIER_MAP = new HashMap<>();

    public static void registerConfig(String javaPackage, Supplier<BukkitConfig> configSupplier) {
        SUPPLIER_MAP.put(javaPackage, configSupplier);
    }

    private static boolean isConfigRegistered() {
        BukkitConfig config = getConfig();
        if (config == null) {
            System.out.println("Could not find config for current stack trace, please register with Message.registerConfig!");
            return false;
        }
        return true;
    }

    public static BukkitConfig getConfig() {
        for (int i = 2; i < 30; i++) {
            Class<?> clazz = Reflection.getCallerClass(i);
            if (clazz != null) {
                String pack = clazz.getPackage().getName();
                for (Entry<String, Supplier<BukkitConfig>> entries : SUPPLIER_MAP.entrySet()) {
                    String key = entries.getKey();
                    if (pack.startsWith(key)) {
                        return entries.getValue().get();
                    }
                }
            } else {
                break;
            }
        }
        return null;
    }

    public static void broadcast(String path, String msg, Iterable<? extends Player> players, String... replace) {
        broadcast(getConfig(), path, msg, players, replace);
    }

    public static void broadcast(BukkitConfig config, String path, String msg, Iterable<? extends Player> players, String... replace) {
        for (Player player : players) {
            send(player, config, path, msg, replace);
        }
    }

    public static void broadcast(String path, String msg, Iterable<? extends Player> players, Function<Player, String[]> replace) {
        broadcast(getConfig(), path, msg, players, replace);
    }

    public static <P> void broadcast(String path, String msg, Iterable<P> players, Function<P, String[]> replace, Function<P, ? extends Player> mapper) {
        broadcast(getConfig(), path, msg, players, replace, mapper);
    }

    public static <P> void broadcast(BukkitConfig config, String path, String msg, Iterable<P> players, Function<P, String[]> replace, Function<P, ? extends Player> mapper) {
        for (P p : players) {
            send(mapper.apply(p), config, path, msg, replace == null ? new String[0] : replace.apply(p));
        }
    }

    public static void broadcast(BukkitConfig config, String path, String msg, Iterable<? extends Player> players, Function<Player, String[]> replace) {
        for (Player player : players) {
            send(player, config, path, msg, replace == null ? new String[0] : replace.apply(player));
        }
    }

    public static void send(CommandSender sender, String path, String msg, String... replace) {
        BukkitConfig config = getConfig();
        if (config != null) {
            send(sender, config, path, msg, replace);
        } else {
            System.out.println("Could not find config for current stack trace, please register with Message.registerConfig!");
        }
    }

    public static void send(Player player, String path, String msg, String... replace) {
        send((CommandSender) player, path, msg, replace);
    }

    public static void send(CommandSender sender, BukkitConfig config, String path, String msg, String... replace) {
        int mode = config.getInt(path + ".mode", 1);
        String modes = config.getString(path + ".modes", "none");
        boolean prefix = config.getBoolean(path + ".prefix", true);
        replace = addGlobalPlaceholder(replace, sender);
        if ((mode & 0x1) > 0 || modes.contains("MESSAGES")) {
            sendMessage(sender, config, prefix, path, msg, replace);
        }
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if ((mode & 0b10) > 0 || modes.contains("TITLE")) {
                sendTitle(player, config, path, msg, replace);
            }
            if ((mode & 0b100) > 0 || modes.contains("SOUND")) {
                playSound(player, config, path);
            }
            if ((mode & 0b1000) > 0 || modes.contains("CHAT")) {
                sendChatComponent(player, config, path, replace);
            }
            if ((mode & 0b10000) > 0 || modes.contains("ACTIONBAR")) {
                sendActionBar(player, config, prefix, path, msg, replace);
            }
        }
    }

    public static void send(Player player, BukkitConfig config, String path, String msg, String... replace) {
        send((CommandSender) player, config, path, msg, replace);
    }

    public static void sendMessage(CommandSender sender, BukkitConfig config, boolean prefix, String path, String msg, String... replace) {
        final String prefixStr = prefix ? config.getString("Prefix", "[Prefix] ") : "";
        List<String> r = new ArrayList<>();
        List<String> o = new ArrayList<>();
        for (int i = 0; i < replace.length; i++) {
            if ((i & 0x1) > 0) {
                o.add(replace[i]);
            } else {
                r.add(replace[i]);
            }
        }
        if (config.getBoolean(path + ".repeat", false)) {
            if (config.getWrapper().isSet(path + ".message." + 0)) {
                List<String> newList = new ArrayList<>();
                for (int i = 0; i < config.getInt(path + ".repeats", 1); i++) {
                    newList.add(config.getString(path + ".message." + i, ""));
                }
                config.set(path + ".message", null);
                config.set(path + ".repeats", null);
                config.set(path + ".message." + Locale.GERMAN.getLanguage(), newList);
            }
            List<String> messages;
            if (sender instanceof Player) {
                messages = config.getTranslatedList(path + ".message", (Player) sender, msg);
            } else {
                messages = config.getTranslatedList(path + ".message", Locale.GERMAN, msg);
            }
            messages.forEach(message -> sendMessage0(sender, prefixStr, message, r, o));
        } else {
            String message;
            if (sender instanceof Player) {
                message = config.getTranslated(path + ".message", (Player) sender, msg);
            } else {
                message = config.getTranslated(path + ".message", Locale.GERMAN, msg);
            }
            sendMessage0(sender, prefixStr, message, r, o);
        }
    }

    public static void sendMessage(Player player, BukkitConfig config, boolean prefix, String path, String msg, String... replace) {
        sendMessage((CommandSender) player, config, prefix, path, msg, replace);
    }

    private static void sendMessage0(CommandSender sender, String prefix, String message, List<String> r, List<String> o) {
        String newMessage = prefix;
        newMessage += message;
        newMessage = StringFormatting.formatString(newMessage, r, o);
        sender.sendMessage(newMessage);
    }

    public static void sendChatComponent(Player player, BukkitConfig config, String path, String... replace) {
        boolean hover = config.getBoolean(path + ".hover", true);
        boolean click = config.getBoolean(path + ".click", true);
        String raw = config.getTranslated(path + ".textComponent", player, "Text");
        BaseComponent[] textComponents = TextComponent.fromLegacyText(StringFormatting.simpleColorFormat(StringUtils.format(raw, replace)));
        ClickEvent clickEvent = click ? config.getClickEvent(path + ".clickEvent", replace) : null;
        HoverEvent hoverEvent = hover ? config.getHoverEvent(path + ".hoverEvent", replace) : null;
        for (BaseComponent component : textComponents) {
            if (clickEvent != null) {
                component.setClickEvent(clickEvent);
            }
            if (hoverEvent != null) {
                component.setHoverEvent(hoverEvent);
            }
        }
        player.spigot().sendMessage(textComponents);
    }

    public static void sendTitle(Player p, BukkitConfig config, String path, String msg, String... replace) {
        String title = config.getTranslated(path + ".title", p, path);
        String subTitle = config.getTranslated(path + ".subtitle", p, path);
        int fadeIn = config.getInt(path + ".fadeIn", 5);
        int fadeout = config.getInt(path + ".fadeOut", 5);
        int stay = config.getInt(path + ".stay", 20);
        Titles.sendTitle(p, EnumTitleAction.TIMES, fadeIn, fadeout, stay);
        Titles.sendTitle(p, EnumTitleAction.TITLE, StringFormatting.simpleColorFormat(StringUtils.format(title, replace)));
        Titles.sendTitle(p, EnumTitleAction.SUBTITLE, StringFormatting.simpleColorFormat(StringUtils.format(subTitle, replace)));
    }

    public static void playSound(Player player, BukkitConfig config, String path) {
        Sound sound = config.getEnum(Sound.class, path + ".type", Sound.CLICK);
        float loud = config.getFloat(path + ".volume", 1);
        float pitch = config.getFloat(path + ".pitch", 1);
        player.playSound(player.getLocation(), sound, loud, pitch);
    }

    public static void sendActionBar(Player p, BukkitConfig config, boolean prefix, String path, String msg, String... replace) {
        String message = "";
        if (prefix) {
            message += config.getString("Prefix", "[Prefix] ");
        }
        message += config.getString(path + ".message", msg);
        IChatBaseComponent chatBaseComponent = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + StringUtils.format(message, replace) + "\"}");
        PacketPlayOutChat bar = new PacketPlayOutChat(chatBaseComponent, (byte) 2);
        ((CraftPlayer) p).getHandle().playerConnection.sendPacket(bar);
    }

    private static String[] addGlobalPlaceholder(String[] array, CommandSender sender) {
        String[] variables = MessagePlaceholderHandler.getVariables(sender);
        if (array == null) {
            return variables;
        }
        return (String[]) ArrayUtils.addAll(array, variables);
    }

}
