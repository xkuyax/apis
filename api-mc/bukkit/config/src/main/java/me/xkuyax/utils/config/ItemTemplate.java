package me.xkuyax.utils.config;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;
import me.xkuyax.utils.ItemUtils;
import me.xkuyax.utils.config.Config.ConfigSerializerIgnore;
import me.xkuyax.utils.config.additions.ConfigOptional;
import me.xkuyax.utils.config.additions.LanguageCEntry;
import me.xkuyax.utils.replace.ReplaceHandler;
import org.bukkit.Color;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.block.banner.Pattern;
import org.bukkit.block.banner.PatternType;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.*;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;

@Data
@Accessors(chain = true)
@NoArgsConstructor
public class ItemTemplate {

    private Material material = Material.STONE;
    private int data;
    private int amount = 1;
    private boolean glow;
    private LanguageCEntry<String> displayName = LanguageCEntry.withDefault("Default Item Displayname %path%");
    private LanguageCEntry<List<String>> lore = LanguageCEntry.withDefault(Collections.singletonList("Default Item Lore"));
    private List<ItemFlag> itemFlags;
    private List<SingleEnchantment> enchantments;
    private ConfigOptional<SkullItemConfig> skull;
    private ConfigOptional<LeatherArmorConfig> leatherArmor;
    private ConfigOptional<List<SinglePotionEffect>> potionEffects;
    private ConfigOptional<List<SinglePattern>> bannerPatterns;
    private ConfigOptional<Color> fireworkColor;
    private ConfigOptional<LanguageCEntry<BookConfig>> bookConfig;
    @ConfigSerializerIgnore
    @Setter(value = AccessLevel.PRIVATE)
    private Locale locale;
    @ConfigSerializerIgnore
    @Setter(value = AccessLevel.PRIVATE)
    private ReplaceHandler replaceHandler;
    @ConfigSerializerIgnore
    @Setter(value = AccessLevel.PRIVATE)
    private Object context;

    public ItemTemplate(ItemTemplate other) {
        this.material = other.material;
        this.data = other.data;
        this.amount = other.amount;
        this.glow = other.glow;
        this.displayName = other.displayName;
        this.lore = other.lore;
        this.itemFlags = other.itemFlags;
        this.enchantments = other.enchantments;
        this.skull = other.skull;
        this.leatherArmor = other.leatherArmor;
        this.potionEffects = other.potionEffects;
        this.bannerPatterns = other.bannerPatterns;
        this.fireworkColor = other.fireworkColor;
        this.bookConfig = other.bookConfig;
    }

    //something with replaceHandler for formatting
    /*public void replace(ReplaceHandler<Player> replaceHandler) {

    }*/

    public ItemTemplate with(Player player) {
        return with(player, new ReplaceHandler<>());
    }

    public ItemTemplate with(Locale locale) {
        return with(locale, new ReplaceHandler<>());
    }

    public ItemTemplate with(Player player, ReplaceHandler<Player> replaceHandler) {
        return with(player, replaceHandler, ConfigLocale::getLocale);
    }

    public ItemTemplate with(Locale locale, ReplaceHandler<Locale> replaceHandler) {
        return with(locale, replaceHandler, t -> locale);
    }

    public <T> ItemTemplate with(T context, ReplaceHandler<T> replaceHandler, Function<T, Locale> localeMapper) {
        return new ItemTemplate(this).setLocale(localeMapper.apply(context)).setContext(context).setReplaceHandler(replaceHandler);
    }

    public ItemStack build() {
        Objects.requireNonNull(locale, "Use .with() to specify the locale you want to render this for!");
        ItemStack itemStack = new ItemStack(material, amount, (short) data);
        String displayName = replaceHandler.format(this.displayName.get(locale), context);
        List<String> lore = replaceHandler.format(this.lore.get(locale), context);
        applyEnchantments(itemStack);
        ItemMeta itemMeta = itemStack.getItemMeta();
        applyLore(lore, itemMeta);
        applyItemFlags(itemMeta);
        applyLeatherArmor(itemMeta);
        applyBookConfig(itemMeta);
        applyPotions(itemMeta);
        applyBanner(itemMeta);
        applyDisplayName(displayName, itemMeta);
        itemStack.setItemMeta(itemMeta);
        itemStack = applyFireworkColor(itemStack);
        applySkull(itemStack, itemMeta);
        itemStack = applyGlow(itemStack);
        return itemStack;
    }

    private ItemStack applyGlow(ItemStack itemStack) {
        if (glow) {
            itemStack = ItemUtils.addGlow(itemStack);
        }
        return itemStack;
    }

    private void applySkull(ItemStack itemStack, ItemMeta itemMeta) {
        if (itemMeta instanceof SkullMeta && data == 3) {
            SkullItemConfig skullItemConfig = skull.get();
            String skullTexture = skullItemConfig.getSkullTexture();
            if (!isNone(skullTexture)) {
                if (skullTexture.length() > 16) {
                    ItemUtils.setSkullTexture(itemStack, skullTexture);
                } else {
                    ItemUtils.setSkullandName(itemStack, skullTexture);
                }
            }
        }
    }

    private ItemStack applyFireworkColor(ItemStack itemStack) {
        if (material == Material.FIREWORK_CHARGE && getFireworkColor().isPresent()) {
            Color color = getFireworkColor().get();
            itemStack = ItemUtils.setFireworkChargeColor(itemStack, color);
        }
        return itemStack;
    }

    private void applyDisplayName(String displayName, ItemMeta itemMeta) {
        if (!isNone(displayName)) {
            itemMeta.setDisplayName(displayName);
        }
    }

    private void applyBanner(ItemMeta itemMeta) {
        if (itemMeta instanceof BannerMeta) {
            BannerMeta bannerMeta = (BannerMeta) itemMeta;
            List<Pattern> patterns = getBannerPatterns().get().stream().map(SinglePattern::bukkit).collect(Collectors.toList());
            bannerMeta.setPatterns(patterns);
        }
    }

    private void applyPotions(ItemMeta itemMeta) {
        if (itemMeta instanceof PotionMeta) {
            PotionMeta potionMeta = (PotionMeta) itemMeta;
            List<SinglePotionEffect> potionEffectList = potionEffects.get();
            potionEffectList.forEach(potionEffect -> potionMeta.addCustomEffect(potionEffect.bukkit(), true));
        }
    }

    private void applyBookConfig(ItemMeta itemMeta) {
        if (itemMeta instanceof BookMeta && bookConfig.isPresent()) {
            BookMeta bookMeta = (BookMeta) itemMeta;
            BookConfig bookConfig = this.bookConfig.get().get(locale);
            String author = bookConfig.getAuthor();
            String title = bookConfig.getTitle();
            List<String> pages = bookConfig.getPages();
            if (!isNone(author)) {
                bookMeta.setAuthor(author);
            }
            if (!isNone(title)) {
                bookMeta.setTitle(title);
            }
            bookMeta.setPages(pages);
        }
    }

    private void applyLore(List<String> lore, ItemMeta itemMeta) {
        if (!lore.isEmpty()) {
            if (!isNone(lore.get(0))) {
                itemMeta.setLore(lore);
            }
        }
    }

    private boolean isNone(String s) {
        return s.equalsIgnoreCase("none");
    }

    private void applyItemFlags(ItemMeta itemMeta) {
        itemMeta.addItemFlags(itemFlags.toArray(new ItemFlag[0]));
    }

    private void applyLeatherArmor(ItemMeta itemMeta) {
        if (itemMeta instanceof LeatherArmorMeta && leatherArmor.isPresent()) {
            ((LeatherArmorMeta) itemMeta).setColor(leatherArmor.get().getColor());
        }
    }

    private void applyEnchantments(ItemStack itemStack) {
        enchantments.forEach(singleEnchantment -> {
            if (singleEnchantment.getLevel() > 0) {
                itemStack.addUnsafeEnchantment(singleEnchantment.bukkit(), singleEnchantment.getLevel());
            }
        });
    }

    @Data
    public static class SingleEnchantment {

        private String enchantment;
        private int level;

        public Enchantment bukkit() {
            return Enchantment.getByName(enchantment);
        }

    }

    @Data
    public static class SinglePattern {

        private PatternType patternType;
        private DyeColor dyeColor;

        public Pattern bukkit() {
            return new Pattern(dyeColor, patternType);
        }
    }

    @Data
    public static class SinglePotionEffect {

        private PotionEffectType potionEffectType;
        private int duration;
        private int amplifier;
        private boolean ambient;
        private boolean particles;

        public PotionEffect bukkit() {
            return new PotionEffect(potionEffectType, duration, amplifier, ambient, particles);
        }

    }

    @Data
    public static class BookConfig {

        private String author;
        private String title;
        private List<String> pages;

    }

    @Data
    public static class LeatherArmorConfig {

        private Color color;

    }

    @Data
    public static class SkullItemConfig {

        private String skullTexture;

    }
}
