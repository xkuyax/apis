package me.xkuyax.utils.config;

import me.xkuyax.utils.config.additions.WrappedObjectResolver;
import me.xkuyax.utils.config.file.FileConfiguration;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.io.File;
import java.nio.file.Path;
import java.util.List;

public class BukkitConfig extends Config implements BukkitConfigExtension {

    static {
        List<WrappedObjectResolver> singleObjectResolvers = ConfigGenericResolver.getSingleObjectResolvers();
        singleObjectResolvers.add(new WrappedObjectResolver((fieldPath, value, config) -> {
            BukkitConfig bukkitConfig = (BukkitConfig) config;
            return bukkitConfig.getLocation(fieldPath);
        }, Location.class));
        singleObjectResolvers.add(new WrappedObjectResolver((fieldPath, value, config) -> {
            BukkitConfig bukkitConfig = (BukkitConfig) config;
            return bukkitConfig.getWrappedLocation(fieldPath);
        }, WrappedLocation.class));
        singleObjectResolvers.add(new WrappedObjectResolver((fieldPath, value, config) -> {
            BukkitConfig bukkitConfig = (BukkitConfig) config;
            return bukkitConfig.getMaterial(fieldPath);
        }, Material.class));
        singleObjectResolvers.add(new WrappedObjectResolver((fieldPath, value, config) -> {
            BukkitConfig bukkitConfig = (BukkitConfig) config;
            return bukkitConfig.getItemStack(fieldPath);
        }, ItemStack.class));
        singleObjectResolvers.add(new WrappedObjectResolver((fieldPath, value, config) -> {
            BukkitConfig bukkitConfig = (BukkitConfig) config;
            return bukkitConfig.getItemBuilder(fieldPath);
        }, ItemBuilder.class));
        singleObjectResolvers.add(new WrappedObjectResolver((fieldPath, value, config) -> {
            BukkitConfig bukkitConfig = (BukkitConfig) config;
            return bukkitConfig.getClickEvent(fieldPath);
        }, ClickEvent.class));
        singleObjectResolvers.add(new WrappedObjectResolver((fieldPath, value, config) -> {
            BukkitConfig bukkitConfig = (BukkitConfig) config;
            return bukkitConfig.getHoverEvent(fieldPath);
        }, HoverEvent.class));
        singleObjectResolvers.add(new WrappedObjectResolver((fieldPath, value, config) -> {
            BukkitConfig bukkitConfig = (BukkitConfig) config;
            return bukkitConfig.getColor(fieldPath);
        }, Color.class));
    }

    public BukkitConfig(String plugin, Class<?> clazz) {
        this(plugin, clazz.getSimpleName() + ".yml");
    }

    public BukkitConfig(String plugin, String yaml) {
        super("plugins/" + plugin + "/" + yaml, true);
    }

    public BukkitConfig(String yaml, boolean saveAfterSet) {
        super(yaml, saveAfterSet);
    }

    public BukkitConfig(String yaml) {
        super(yaml);
    }

    public BukkitConfig(File file, FileConfiguration wrapper) {
        super(file, wrapper);
    }

    public BukkitConfig(Path path) {
        super(path);
    }
}
