package me.xkuyax.utils.config;

import lombok.Data;
import lombok.NoArgsConstructor;
import me.xkuyax.utils.ItemUtils;
import me.xkuyax.utils.StringFormatting;
import me.xkuyax.utils.StringUtils;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ClickEvent.Action;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.*;
import org.bukkit.block.banner.Pattern;
import org.bukkit.block.banner.PatternType;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.*;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import java.util.*;
import java.util.function.Supplier;

public interface BukkitConfigExtension extends ConfigObjectExtension, ConfigListExtension {

    default ItemBuilder getItemBuilder(String path) {
        return new ItemBuilder(getItemStack(path));
    }

    default Location getLocation(String path) {
        String raw = getString(path, pasteLocation(Bukkit.getWorlds().get(0).getSpawnLocation()));
        return parseLocation(raw);
    }

    default WrappedLocation getWrappedLocation(String path) {
        String raw = getString(path, pasteLocation(Bukkit.getWorlds().get(0).getSpawnLocation()));
        return parseWrappedLocation(raw);
    }

    static WrappedLocation parseWrappedLocation(String raw) {
        WrappedLocation location = null;
        try {
            String[] loc = raw.split(",");
            String w = loc[0];
            double x = Double.parseDouble(loc[1]);
            double y = Double.parseDouble(loc[2]);
            double z = Double.parseDouble(loc[3]);
            float yaw = Float.parseFloat(loc[4]);
            float pitch = Float.parseFloat(loc[5]);
            location = new WrappedLocation(w, x, y, z, yaw, pitch);
        } catch (NullPointerException ignored) {
        }
        return location;
    }

    static Location parseLocation(String raw) {
        return parseWrappedLocation(raw).get();
    }

    static String pasteLocation(Location loc) {
        return loc.getWorld().getName() + "," + loc.getX() + "," + loc.getY() + "," + loc.getZ() + "," + loc.getYaw() + "," + loc.getPitch();
    }

    static String pasteLocation(WrappedLocation loc) {
        return loc.getWorld() + "," + loc.getX() + "," + loc.getY() + "," + loc.getZ() + "," + loc.getYaw() + "," + loc.getPitch();
    }

    default ClickEvent getClickEvent(String path, String... replace) {
        ClickEvent.Action action = getEnum(ClickEvent.Action.class, path + ".type", Action.RUN_COMMAND);
        String value = StringUtils.format(getString(path + ".value", "/hello"), replace);
        return new ClickEvent(action, value);
    }

    default HoverEvent getHoverEvent(String path, String... replace) {
        HoverEvent.Action action = getEnum(HoverEvent.Action.class, path + ".type", HoverEvent.Action.SHOW_TEXT);
        String value = StringUtils.format(getString(path + ".value", "/hello"), replace);
        BaseComponent[] components = TextComponent.fromLegacyText(value);
        return new HoverEvent(action, components);
    }

    default void saveLocation(Location loc, String path) {
        set(path, pasteLocation(loc));
    }

    default void saveLocation(WrappedLocation loc, String path) {
        set(path, pasteLocation(loc));
    }

    default Color getColor(String path) {
        return getColor(path, Color.fromRGB(160, 101, 64)); //(R=160 G=101 B=64) = Default Leather Armor Color
    }

    default Color getColor(String path, Color d) {
        String[] s = this.getString(path, d.getRed() + "-" + d.getGreen() + "-" + d.getBlue()).split("-");
        return Color.fromRGB(Integer.parseInt(s[0]), Integer.parseInt(s[1]), Integer.parseInt(s[2]));
    }

    default Material getMaterial(String path, Material material) {
        try {
            return Material.valueOf(getString(path, material.toString()));
        } catch (Exception e) {
            set(path, material.toString());
            return material;
        }
    }

    default Material getMaterial(String path) {
        try {
            return Material.valueOf(get(path, Material.STONE.toString()).toString());
        } catch (Exception e) {
            set(path, Material.STONE.toString());
            return Material.STONE;
        }
    }

    default List<ItemFlag> getItemFlagList(String path, String... defaul) {
        return getEnumList(ItemFlag.class, getStringList(path, defaul));
    }

    default List<Material> getMaterialList(String path, String... defaul) {
        return getEnumList(Material.class, getStringList(path, defaul));
    }

    default List<PotionEffect> getPotionEffectList(String path) {
        List<String> rawPotionEffectList = getStringList(path);
        List<PotionEffect> potionEffects = new ArrayList<>();
        rawPotionEffectList.forEach(s -> {
            try {
                String[] split = s.split(" ");
                PotionEffectType effectType = PotionEffectType.getByName(split[0]);
                int duration = split.length >= 2 ? Integer.parseInt(split[1]) : 60 * 20;
                int amplifier = split.length >= 3 ? Integer.parseInt(split[2]) : 1;
                boolean ambient = split.length < 4 || Boolean.parseBoolean(split[3]);
                boolean particles = split.length < 5 || Boolean.parseBoolean(split[4]);
                potionEffects.add(new PotionEffect(effectType, duration, amplifier, ambient, particles));
            } catch (Exception ex) {
                System.out.println("The String " + s + " is not an valid option for potionEffect!");
            }
        });
        return potionEffects;
    }


    default List<Pattern> getBannerPatternList(String path) {
        List<String> bannerPattern = getStringList(path);
        List<Pattern> list = new ArrayList<>();
        bannerPattern.forEach(s -> {
            try {
                String[] split = s.split(" ");
                DyeColor dyeColor = DyeColor.valueOf(split[1].toUpperCase());
                PatternType patternType = PatternType.getByIdentifier(split[0]);
                if (patternType == null) patternType = PatternType.valueOf(split[0].toUpperCase());
                list.add(new Pattern(dyeColor, patternType));
            } catch (Exception ex) {
                System.out.println("The String " + s + " is not an valid option for banner!");
            }
        });
        return list;
    }

    default Map<Enchantment, Integer> getEnchantmentList(String path) {
        List<String> enchantments = getStringList(path);
        Map<Enchantment, Integer> map = new HashMap<>();
        enchantments.forEach(s -> {
            try {
                String[] split = s.split(" ");
                Enchantment e = Enchantment.getByName(split[0]);
                int i = Integer.parseInt(split[1]);
                if (i != -1 && e != null) {
                    map.put(e, i);
                }
            } catch (Exception e) {
                System.out.println("The String " + s + " is not an valid option for enchantments!");
            }
        });
        return map;
    }

    default ItemStack getItemStack(String configPath, Player player) {
        Locale playerLocale = Locale.GERMAN; //TODO:Replace with Language Handler
        return getItemStack(configPath, playerLocale);
    }

    default ItemStack getItemStack(String configPath, Locale locale, Locale fallbackLocale) {
        String defaultDisplayName = "Default Item (" + configPath.replace("\\.", "") + ")";
        if (isSet(configPath + ".dname.de")) {
            defaultDisplayName = getString(configPath + ".dname.de", "");
            set(configPath + ".dname", null);
        }
        if (isSet(configPath + ".skull_texture")) {
            set(configPath + ".customOption.skullMeta", getString(configPath + ".skull_texture", "none"));
            set(configPath + ".skull_texture", null);
        }


        String displayName = getTranslated(configPath + ".displayName", locale, fallbackLocale, defaultDisplayName);
        List<String> lore = getTranslatedList(configPath + ".lore", locale, fallbackLocale);
        List<ItemFlag> itemFlags = getItemFlagList(configPath + ".flags");
        String hidden = getString(configPath + ".hiddenName", "none");
        short data = (short) getInt(configPath + ".data", 0);
        boolean glow = getBoolean(configPath + ".glow", false);
        Material material = getMaterial(configPath + ".material");
        Map<Enchantment, Integer> encMap = getEnchantmentList(configPath + ".enchantments");
        int amount = getInt(configPath + ".amount", 1);
        ItemStack is = new ItemStack(material, amount, data);

        /*
            ItemStack Enchantments
         */
        if (!encMap.isEmpty()) {
            is.addUnsafeEnchantments(encMap);
        }


        ItemMeta im = is.getItemMeta();
        /*
            Leather Armor Settings
         */
        if (im instanceof LeatherArmorMeta) {
            Color color = getColor(configPath + ".customOption.color");
            ((LeatherArmorMeta) im).setColor(color);
        }
        /*
            Book Settings
         */
        if (im instanceof BookMeta) {
            BookMeta bookMeta = (BookMeta) im;
            String author = getTranslated(configPath + ".customOption.bookAuthor", locale, fallbackLocale, "none");
            String title = getTranslated(configPath + ".customOption.bookTitle", locale, fallbackLocale, "none");
            List<String> pages = getTranslatedList(configPath + ".customOption.bookPages", locale, fallbackLocale);
            if (!author.equals("none")) {
                bookMeta.setAuthor(author);
            }
            if (!title.equals("none")) {
                bookMeta.setTitle(title);
            }
            bookMeta.setPages(pages);
        }
        /*
            Potion Effect Settings
         */
        if (im instanceof PotionMeta) {
            PotionMeta potionMeta = (PotionMeta) im;
            List<PotionEffect> potionEffectList = getPotionEffectList(configPath + ".customOption.customPotionEffects");
            potionEffectList.forEach(potionEffect -> potionMeta.addCustomEffect(potionEffect, true));
        }
        /*
            Banner Settings
         */
        if (im instanceof BannerMeta) {
            BannerMeta bannerMeta = (BannerMeta) im;
            bannerMeta.setPatterns(getBannerPatternList(configPath + ".customOption.pattern"));
        }
        /*
            Item Flags
         */
        if (!itemFlags.isEmpty()) {
            im.addItemFlags(itemFlags.toArray(new ItemFlag[0]));
        }

        /*
            ItemStack Lore
         */
        if (!lore.isEmpty()) {
            if (!lore.get(0).equalsIgnoreCase("idontwanttoseethismessage") && !lore.get(0).equalsIgnoreCase("none")) {
                im.setLore(lore);
            }
        }
        /*
            DisplayName Lore
         */
        if (!displayName.equalsIgnoreCase("idontwanttoseethismessage") && !displayName.equalsIgnoreCase("none")) {
            im.setDisplayName(displayName);
        }
        is.setItemMeta(im);

        /*
            Firework Charge Color
         */
        if (material == Material.FIREWORK_CHARGE) {
            Color color = getColor(configPath + ".customOption.color", Color.GRAY);
            is = ItemUtils.setFireworkChargeColor(is, color);
        }

        /*
            Skull Settings
         */
        if (im instanceof SkullMeta && data == 3) {
            String skullMeta = getString(configPath + ".customOption.skullMeta", "none");
            if (!skullMeta.equals("none")) {
                if (skullMeta.length() > 16) {
                    ItemUtils.setSkullTexture(is, skullMeta);
                } else {
                    ItemUtils.setSkullandName(is, skullMeta);
                }
            }
        }
        /*
            ItemStack Enchantment Glow
         */
        if (glow) {
            is = ItemUtils.addGlow(is);
        }
        /*
            Hidden Text in ItemStacks
         */
        if (!hidden.equals("none")) {
            ItemUtils.setHiddenString(is, hidden);
        }
        return is;
    }

    default ItemStack getItemStack(String configPath, Locale locale) {
        return getItemStack(configPath, locale, Locale.GERMAN);
    }

    default ItemStack getItemStack(String configPath) {
        return getItemStack(configPath, Locale.GERMAN);
    }

    default String getTranslated(String path, Player p, String d) {
        Locale playerLocale = Locale.GERMAN; //TODO:Replace with Language Handler
        return this.getTranslated(path, playerLocale, d);
    }

    default String getTranslated(String path, Locale locale, String d) {
        return this.getTranslated(path, locale, Locale.GERMAN, d);
    }

    default String getTranslated(String path, Locale locale, Locale fallBack, String d) {
        if (isSet(path) && !getWrapper().isConfigurationSection(path)) {
            String temp = getString(path, "");
            set(path + "." + fallBack.getLanguage(), temp);
        }
        String fallBackStr = getString(path + "." + fallBack.getLanguage(), d);
        return StringFormatting.simpleColorFormat(getString(path + "." + locale.getLanguage(), fallBackStr));
    }

    default List<String> getTranslatedList(String path, Player p, String... d) {
        Locale playerLocale = Locale.GERMAN; //TODO:Replace with Language Handler
        return this.getTranslatedList(path, playerLocale, d);
    }

    default List<String> getTranslatedList(String path, Locale locale, String... d) {
        return this.getTranslatedList(path, locale, Locale.GERMAN, d);
    }

    default List<String> getTranslatedList(String path, Locale locale, Locale fallBack, String... d) {
        String[] fallBackStrArray = getStringList(path + "." + fallBack.getLanguage(), d).toArray(new String[0]);
        return StringFormatting.formatStringList(getStringList(path + "." + locale.getLanguage(), fallBackStrArray));
    }


    @Data
    @NoArgsConstructor
    class WrappedLocation {

        private String world;
        private double x, y, z;
        private float yaw, pitch;
        // private Location cached;
        //  private Vector cachedVector;

        public WrappedLocation(String world, double x, double y, double z, float yaw, float pitch) {
            this.world = world;
            this.x = x;
            this.y = y;
            this.z = z;
            this.yaw = yaw;
            this.pitch = pitch;
        }

        public WrappedLocation(Location loc) {
            this(loc.getWorld().getName(), loc.getX(), loc.getY(), loc.getZ(), loc.getYaw(), loc.getPitch());
        }

        public Location get() {
            return new Location(Bukkit.getWorld(world), x, y, z, yaw, pitch);
        }

        public org.bukkit.util.Vector vec() {
            return new Vector(x, y, z);
        }
    }

    class RandomMaterialSupplier implements Supplier<Material> {

        public static RandomMaterialSupplier instance = new RandomMaterialSupplier();
        private static final Random random = new Random();

        @Override
        public Material get() {
            Material[] a = Material.values();
            return a[random.nextInt(a.length)];
        }
    }
}
