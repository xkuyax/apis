package me.xkuyax.utils.config;

import com.google.common.base.Joiner;
import me.xkuyax.utils.CmdHandler;
import me.xkuyax.utils.GCommand;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class ConfigReloadCommand extends GCommand {

    public ConfigReloadCommand() {
        super("crl");
    }

    @CmdHandler(ignoreArgs = true, permission = "admin", sender = CSender.BOTH)
    public void onIgnoreArgs(CommandSender sender, String[] args) {
        final AtomicInteger count = new AtomicInteger();
        List<String> messageText = new ArrayList<>();

        FileConfigUtils.getCachedConfig().forEach(config -> {
            try {
                config.reload();
                count.getAndIncrement();
                messageText.add("§8» §a" + config.file.getPath());
            } catch (Exception ex) {
                sender.sendMessage("§8» §cFehler beim laden einer Konfiguration §8» " + config.file.getPath());
                messageText.add("§8» §c" + config.file.getPath());
                ex.printStackTrace();
            }
        });

        if (sender instanceof Player) {
            TextComponent textComponent = new TextComponent("§8» §b" + count.get() + " Konfigurationen §7wurden erfolgreich neu geladen!");
            textComponent.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(Joiner.on("\n").join(messageText)).create()));
            ((Player) sender).spigot().sendMessage(textComponent);
        } else {
            sender.sendMessage("» "+count.get() + " Konfigurationen wurden erfolgreich neu geladen!");
        }
    }
}
