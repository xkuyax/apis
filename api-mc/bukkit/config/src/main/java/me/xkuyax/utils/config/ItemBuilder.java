package me.xkuyax.utils.config;

import com.google.common.collect.Sets;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import me.xkuyax.utils.ItemUtils;
import me.xkuyax.utils.StringFormatting;
import me.xkuyax.utils.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.potion.Potion;

import java.util.*;
import java.util.Map.Entry;

/**
 * @author PaulBGD from https://gist.github.com/PaulBGD/9831d28b1c7bdba0cddd
 * <p>
 * This however was modified for my own needs
 */
@Accessors(chain = true)
public class ItemBuilder {

    private ItemStack itemStack;
    private int amount;
    private Color color;
    @Getter
    @Setter
    private short data;
    private final HashMap<Enchantment, Integer> enchants = new HashMap<>();
    private final Set<ItemFlag> itemFlags = Sets.newHashSet();
    @Getter
    private List<String> lore = new ArrayList<>();
    private Material mat;
    // private Potion potion;
    private String title = null;
    @Getter
    @Setter
    private boolean glow;

    public ItemBuilder(ItemStack item) {
        this(item.getType(), item.getDurability());
        this.itemStack = item.clone();
        this.amount = item.getAmount();
        this.enchants.putAll(item.getEnchantments());
        if (item.hasItemMeta()) {
            ItemMeta meta = item.getItemMeta();
            if (meta.hasDisplayName()) {
                this.title = meta.getDisplayName();
            }
            if (meta.hasLore()) {
                this.lore.addAll(meta.getLore());
            }
            if (meta instanceof LeatherArmorMeta) {
                this.setColor(((LeatherArmorMeta) meta).getColor());
            }
            if (meta instanceof EnchantmentStorageMeta) {
                for (Entry<Enchantment, Integer> en : ((EnchantmentStorageMeta) meta).getStoredEnchants().entrySet()) {
                    addEnchantment(en.getKey(), en.getValue());
                }
            }
            ItemFlag[] itemFlags = meta.getItemFlags().toArray(new ItemFlag[0]);
            addItemFlags(itemFlags);
        }
        if (item.getType() == Material.FIREWORK_CHARGE) {
            this.color = ItemUtils.getFireworkChargeColor(item);
        }
        net.minecraft.server.v1_8_R3.ItemStack itemStack = CraftItemStack.asNMSCopy(item);
        if (itemStack != null && itemStack.hasEnchantments()) {
            this.glow = true;
        }
    }

    public ItemBuilder(Material mat) {
        this(mat, 1);
    }

    public ItemBuilder(Material mat, int amount) {
        this(mat, amount, (short) 0);
    }

    public ItemBuilder(Material mat, int amount, short data) {
        this.mat = mat;
        this.amount = amount;
        this.data = data;
    }

    public ItemBuilder(Material mat, short data) {
        this(mat, 1, data);
    }

    public ItemBuilder(ItemBuilder other) {
        this.mat = other.mat;
        this.amount = other.amount;
        this.data = other.data;
        this.color = other.color;
        this.lore = other.lore;
        this.title = other.title;
        this.glow = other.glow;
        this.itemStack = new ItemStack(mat, amount, data);
    }

    public ItemBuilder addEnchantment(Enchantment enchant, int level) {
        enchants.remove(enchant);
        enchants.put(enchant, level);
        return this;
    }

    public ItemBuilder addLore(String lore, int maxLength) {
        this.lore.addAll(split(lore, maxLength));
        return this;
    }

    public ItemBuilder addLores(List<String> lores, int maxLength) {
        for (String lore : lores) {
            addLore(lore, maxLength);
        }
        return this;
    }

    public ItemBuilder addLore(String... lores) {
        for (String lore : lores) {
            this.lore.add(ChatColor.GRAY + lore);
        }
        return this;
    }

    private static ArrayList<String> split(String string, int maxLength) {
        String[] split = string.split(" ");
        string = "";
        ArrayList<String> newString = new ArrayList<>();
        for (String aSplit : split) {
            string += (string.length() == 0 ? "" : " ") + aSplit;
            if (ChatColor.stripColor(string).length() > maxLength) {
                newString.add((newString.size() > 0 ? ChatColor.getLastColors(newString.get(newString.size() - 1)) : "") + string);
                string = "";
            }
        }
        if (string.length() > 0)
            newString.add((newString.size() > 0 ? ChatColor.getLastColors(newString.get(newString.size() - 1)) : "") + string);
        return newString;
    }

    public ItemBuilder addLores(List<String> lores) {
        this.lore.addAll(lores);
        return this;
    }

    public ItemStack build() {
        Material mat = this.mat == null ? Material.AIR : this.mat;
        if (itemStack == null) {
            itemStack = new ItemStack(mat);
        } else {
            itemStack.setType(mat);
        }
        itemStack.setDurability(data);
        itemStack.setAmount(amount);
        ItemMeta meta = itemStack.getItemMeta();
        if (meta == null) meta = Bukkit.getItemFactory().getItemMeta(mat);
        meta.setDisplayName(title == null ? null : StringFormatting.simpleColorFormat(this.title));
        meta.setLore(lore == null ? Collections.emptyList() : StringFormatting.formatStringList(this.lore));
        if (meta instanceof LeatherArmorMeta) {
            ((LeatherArmorMeta) meta).setColor(this.color);
        }
        meta.removeItemFlags(meta.getItemFlags().toArray(new ItemFlag[0]));
        meta.addItemFlags(itemFlags.toArray(new ItemFlag[0]));
        itemStack.setItemMeta(meta);
        if (color != null && getType() == Material.FIREWORK_CHARGE) {
            itemStack = ItemUtils.setFireworkChargeColor(itemStack, color);
        }
        itemStack.getEnchantments().keySet().forEach(enchantment -> itemStack.removeEnchantment(enchantment));
        itemStack.addUnsafeEnchantments(this.enchants);
        return glow ? ItemUtils.addGlow(itemStack) : itemStack;
    }

    public ItemBuilder clone() {
        return new ItemBuilder(this);
    }

    public HashMap<Enchantment, Integer> getAllEnchantments() {
        return this.enchants;
    }

    public Color getColor() {
        return this.color;
    }

    public int getEnchantmentLevel(Enchantment enchant) {
        return this.enchants.get(enchant);
    }

    public List<String> getLore() {
        return this.lore;
    }

    public String getTitle() {
        return this.title;
    }

    public Material getType() {
        return this.mat;
    }

    public boolean hasEnchantment(Enchantment enchant) {
        return this.enchants.containsKey(enchant);
    }

    public boolean isItem(ItemStack item) {
        ItemMeta meta = item.getItemMeta();
        if (item.getType() != this.getType()) {
            return false;
        }
        if (!meta.hasDisplayName() && this.getTitle() != null) {
            return false;
        }
        if (!meta.getDisplayName().equals(this.getTitle())) {
            return false;
        }
        if (!meta.hasLore() && !this.getLore().isEmpty()) {
            return false;
        }
        if (meta.hasLore()) {
            for (String lore : meta.getLore()) {
                if (!this.getLore().contains(lore)) {
                    return false;
                }
            }
        }
        for (Enchantment enchant : item.getEnchantments().keySet()) {
            if (!this.hasEnchantment(enchant)) {
                return false;
            }
        }
        return true;
    }

    public ItemBuilder setAmount(int amount) {
        this.amount = amount;
        return this;
    }

    public int getAmount() {
        return this.amount;
    }

    public ItemBuilder setColor(Color color) {
        if (!this.mat.name().contains("LEATHER_") || this.mat == Material.FIREWORK_CHARGE) {
            throw new IllegalArgumentException("Can only dye leather armor or firework charge!");
        }
        this.color = color;
        return this;
    }

    public ItemBuilder setPotion(Potion potion) {
        if (this.mat != Material.POTION) {
            this.mat = Material.POTION;
        }
        // this.potion = potion;
        return this;
    }

    public ItemBuilder setTitle(String title) {
        this.title = (title == null ? null : (title.length() > 2 && ChatColor.getLastColors(title.substring(0, 2)).length() == 0 ? ChatColor.WHITE : "")) + title;
        return this;
    }

    public ItemBuilder setRawTitle(String title) {
        this.title = title;
        return this;
    }

    public ItemBuilder setTitle(String title, int maxLength) {
        if (title != null && ChatColor.stripColor(title).length() > maxLength) {
            ArrayList<String> lores = split(title, maxLength);
            for (int i = 1; i < lores.size(); i++) {
                this.lore.add(lores.get(i));
            }
            title = lores.get(0);
        }
        setTitle(title);
        return this;
    }

    public ItemBuilder setType(Material mat) {
        this.mat = mat;
        return this;
    }

    public ItemBuilder addLores(String[] description, int maxLength) {
        return addLores(Arrays.asList(description), maxLength);
    }

    public ItemBuilder formatBoth(String... strings) {
        List<String> r = new ArrayList<>();
        List<String> o = new ArrayList<>();
        StringUtils.fillLists(r, o, strings);
        return formatBoth(r, o);
    }

    public ItemBuilder formatBoth(List<String> r, List<String> o) {
        formatDisplayname(r, o);
        format(r, o);
        return this;
    }

    public ItemBuilder format(List<String> r, List<String> o) {
        ArrayList<String> n = new ArrayList<>();
        if (r.size() > 0 && o.size() > 0) {
            for (String s : lore) {
                for (int i = 0; i < r.size(); i++) {
                    String replace = r.get(i);
                    String object = o.get(i);
                    if (r == null || object == null) {
                        continue;
                    }
                    s = s.replace(replace, object);
                }
                n.add(s);
            }
            this.lore = n;
        }
        return this;
    }

    public ItemBuilder formatDisplayname(List<String> r, List<String> o) {
        if (r.size() > 0 && o.size() > 0) {
            for (int i = 0; i < r.size(); i++) {
                String replace = r.get(i);
                String object = o.get(i);
                if (r == null || object == null) {
                    continue;
                }
                title = title.replace(replace, object);
            }
        }
        return this;
    }

    public ItemBuilder addItemFlags(final ItemFlag... flags) {
        itemFlags.addAll(Arrays.asList(flags));
        return this;
    }
}
