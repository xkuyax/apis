package me.xkuyax.api.bukkit.protocol;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.bukkit.Location;
import org.bukkit.entity.Player;

@Data
@AllArgsConstructor
public class SimpleHologramLine {

    private int entityId;
    private String line;
    private boolean empty;

    private static final int METADATA_NAME = 2;
    private static final int METADATA_SHOW_NAME = 3;
    private static final int METADATA_GRAVITY = 5;
    private static final int METADATA_INVISIBLE = 0;
    private static final int METADATA_MARKER = 11;

    public void spawn(Player player, Location location) {
        /*PacketContainer spawn = ProtocolUtils.createPacket(Server.SPAWN_ENTITY_LIVING);
        spawn.getIntegers().write(0, entityId);
        spawn.getDoubles().write(0, location.getX());
        spawn.getDoubles().write(1, location.getY());
        spawn.getDoubles().write(2, location.getZ());

        spawn.getIntegers().write(1, Integer.valueOf(IRegistry.ENTITY_TYPE.a(EntityTypes.ARMOR_STAND)));

        WrappedDataWatcher.Serializer stringSerializer = WrappedDataWatcher.Registry.get(IChatBaseComponent.class, true);
        WrappedDataWatcher.Serializer booleanSerializer = WrappedDataWatcher.Registry.get(Boolean.class);
        WrappedDataWatcher.Serializer byteSerializer = WrappedDataWatcher.Registry.get(Byte.class);
        WrappedDataWatcher watcher = new WrappedDataWatcher();

        if (line != null) {
            watcher.setObject(new WrappedDataWatcherObject(METADATA_NAME, stringSerializer), Optional.of(WrappedChatComponent.fromText(ChatColor.translateAlternateColorCodes('&', line)).getHandle()));
            watcher.setObject(new WrappedDataWatcherObject(METADATA_SHOW_NAME, booleanSerializer), true);
        } else {
            watcher.setObject(new WrappedDataWatcherObject(METADATA_SHOW_NAME, booleanSerializer), false);
        }
        watcher.setObject(new WrappedDataWatcherObject(METADATA_GRAVITY, booleanSerializer), false);
        watcher.setObject(new WrappedDataWatcherObject(METADATA_INVISIBLE, byteSerializer), (byte) 0x20);
        if (!empty) {
            watcher.setObject(new WrappedDataWatcherObject(METADATA_MARKER, byteSerializer), (byte) 0x10);
        }

        spawn.getDataWatcherModifier().write(0, watcher);

        ProtocolUtils.sendPacket(player, spawn);*/
    }
/*
    public void despawn(Player player) {
        PacketContainer destroy = ProtocolUtils.createPacket(Server.ENTITY_DESTROY);
        destroy.getIntegerArrays().write(0, new int[]{entityId});
        ProtocolUtils.sendPacket(player, destroy);
    }*/

}
