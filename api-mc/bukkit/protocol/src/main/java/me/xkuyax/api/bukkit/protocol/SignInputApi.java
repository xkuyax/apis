package me.xkuyax.api.bukkit.protocol;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.ConnectionSide;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.wrappers.BlockPosition;
import com.comphenix.protocol.wrappers.WrappedBlockData;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import com.google.gson.JsonParser;
import lombok.AllArgsConstructor;
import lombok.Data;
import me.xkuyax.utils.APIPlugin;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class SignInputApi extends PacketAdapter implements Listener {
    
    private static SignInputApi instance;
    private final ConcurrentHashMap<String, PacketData> queue;
    
    public static SignInputApi getInstance() {
        return instance == null ? instance = new SignInputApi() : instance;
    }
    
    public SignInputApi() {
        super(new AdapterParameteters().connectionSide(ConnectionSide.CLIENT_SIDE).types(PacketType.Play.Client.UPDATE_SIGN).plugin(APIPlugin.getPlugin()));
        ProtocolLibrary.getProtocolManager().addPacketListener(this);
        queue = new ConcurrentHashMap<>();
        Bukkit.getPluginManager().registerEvents(this, getPlugin());
    }
    
    @Override
    public void onPacketReceiving(PacketEvent event) {
        if (event.getPacketType() == PacketType.Play.Client.UPDATE_SIGN) {
            Player player = event.getPlayer();
            PacketContainer packet = event.getPacket();
            WrapperPlayClientUpdateSign updateSign = new WrapperPlayClientUpdateSign(packet);
            if (player != null) {
                PacketData packetData = queue.get(player.getName());
                if (packetData != null) {
                    SignInfo signInfo = new SignInfo();
                    signInfo.setPlayer(player);
                    JsonParser parser = new JsonParser();
                    List<String> lines = Arrays.stream(updateSign.getLines()).map(wc -> parser.parse(wc.getJson()).getAsString()).collect(Collectors.toList());
                    signInfo.setLines(lines.toArray(new String[4]));
                    Bukkit.getScheduler().runTask(APIPlugin.getPlugin(), () -> packetData.getListener().onAction(signInfo));
                    queue.remove(player.getName());
                }
            }
        }
    }
    
    @EventHandler
    public void onPlayerQuitEvent(PlayerQuitEvent e) {
        queue.remove(e.getPlayer().getName());
    }
    
    @EventHandler
    public void onPlayerJoinEvent(PlayerJoinEvent e) {
        queue.remove(e.getPlayer().getName());
    }
    
    @EventHandler
    public void onPlayerKickEvent(PlayerKickEvent e) {
        queue.remove(e.getPlayer().getName());
    }
    
    public void addToQueue(Player player, InputListener listener) {
        addToQueue(player, null, listener);
    }
    
    public void addToQueue(Player player, String[] defaultText, InputListener listener) {
        if (queue.containsKey(player.getName())) {
            throw new IllegalAccessError("The player has already an open queue!");
        }
        sendPacket(player, defaultText);
        queue.put(player.getName(), new PacketData(player.getName(), listener));
    }
    
    private void sendPacket(Player player, String[] defaultText) {
        Material block = player.getLocation().getBlock().getType();
        byte blockdata = player.getLocation().getBlock().getData();
        Location loc = player.getLocation();
        BlockPosition position = new BlockPosition(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ());
        if (defaultText != null) {
            //set player location to sign block
            WrapperPlayServerBlockChange blockChange = new WrapperPlayServerBlockChange();
            blockChange.setLocation(position);
            WrappedBlockData data = WrappedBlockData.createData(Material.SIGN_POST);
            blockChange.setBlockData(data);
            blockChange.sendPacket(player);
            
            //set sign to pretext
            WrapperPlayServerUpdateSign updateSign = new WrapperPlayServerUpdateSign();
            updateSign.setLocation(position);
            WrappedChatComponent[] chatComponents = new WrappedChatComponent[4];
            for (int i = 0; i < chatComponents.length; i++) {
                String text = defaultText[i];
                text = text == null ? "" : text;
                chatComponents[i] = WrappedChatComponent.fromText(text);
            }
            updateSign.setLines(chatComponents);
            updateSign.sendPacket(player);
        }
        
        WrapperPlayServerOpenSignEntity signEntity = new WrapperPlayServerOpenSignEntity();
        signEntity.setLocation(position);
        signEntity.sendPacket(player);
        
        //restore dat block and remove dat sign
        if (defaultText != null) {
            WrapperPlayServerBlockChange blockChange = new WrapperPlayServerBlockChange();
            blockChange.setLocation(position);
            WrappedBlockData data = WrappedBlockData.createData(block, blockdata);
            blockChange.setBlockData(data);
            blockChange.sendPacket(player);
        }
    }
    
    @FunctionalInterface
    public interface InputListener {
        
        void onAction(SignInfo packet);
    }
    
    @Data
    public class SignInfo {
        
        private Player player;
        private String[] lines;
    }
    
    @Data
    @AllArgsConstructor
    public class PacketData {
        
        private String name;
        private InputListener listener;
    }
}
