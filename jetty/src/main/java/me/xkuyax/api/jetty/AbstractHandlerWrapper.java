package me.xkuyax.api.jetty;

import lombok.RequiredArgsConstructor;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.handler.AbstractHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RequiredArgsConstructor
public class AbstractHandlerWrapper extends AbstractHandler {
    
    private final AbstractHandlerWrapperExecutor executor;
    
    @Override
    public void handle(String s, Request request, HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException, ServletException {
        executor.handle(s, request, httpServletRequest, httpServletResponse, this);
    }
    
    @FunctionalInterface
    public interface AbstractHandlerWrapperExecutor {
        
        void handle(String s, Request request, HttpServletRequest httpRequest, HttpServletResponse response,
                    AbstractHandler handler) throws IOException, ServletException;
        
    }
}
