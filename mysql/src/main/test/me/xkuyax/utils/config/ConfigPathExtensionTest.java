package me.xkuyax.utils.config;

import org.junit.Test;

import java.nio.file.Paths;
import java.util.List;

public class ConfigPathExtensionTest {
    
    @Test
    public void getProcessBuilders() throws Exception {
        Config config = new Config(Paths.get("target/test.yml"));
        List<ProcessBuilder> builders = config.getProcessBuilders("ProcessBuilders", "%id%", "nv-02");
        builders.forEach(processBuilder -> {
            System.out.println(processBuilder.command());
        });
        String val = config.getString("value", "hi");
        System.out.println(val);
        config.reload();
        System.out.println();
        String val2 = config.getString("value", "hi2");
        System.out.println(val2);
    }
    
}