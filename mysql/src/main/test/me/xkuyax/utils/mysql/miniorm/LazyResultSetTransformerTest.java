package me.xkuyax.utils.mysql.miniorm;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import me.xkuyax.utils.mysql.MysqlConnection;
import org.junit.Test;

import java.util.List;

public class LazyResultSetTransformerTest {
    
    @Test
    public void test() {
        try {
            MysqlConnection mysqlConnection = MysqlConnection.single("localhost", "root", "a", "3306", "cloudsystem");
            reflection(mysqlConnection);
            description(mysqlConnection);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void description(MysqlConnection mysqlConnection) {
    }
    
    private void reflection(MysqlConnection mysqlConnection) {
        LazyResultSetTransformer<GameModeInfo> transformer = new LazyResultSetTransformer<>(GameModeInfo.class);
        long ms = System.currentTimeMillis();
        List<GameModeInfo> gameModeInfos = mysqlConnection.queryList("select * from gamemodes", ps -> {
        }, transformer);
        long took = (System.currentTimeMillis() - ms);
        System.out.println(took);
        System.out.println(gameModeInfos);
    }
    
    @Data
    @EqualsAndHashCode(of = "id")
    @AllArgsConstructor
    public static class GameModeInfo {
        
        private final int id;
        private final int cloudId;
        private final String name;
        private final String displayName;
        private final String shortName;
        private final int startRam;
        private final int maxRam;
        private final int minServer;
        private final int maxServer;
        private final int minCapacity;
        private final int priority;
        private final boolean active;
        private final boolean portMapping;
        private final boolean autoIncrease;
        private final boolean groupGameMode;
        private final String jvmArgs;
        private final String programArgs;
        private final String gameGroup;
        private final String templates;
        private final String serverFile;
        private final boolean pserver;
        
    }
}