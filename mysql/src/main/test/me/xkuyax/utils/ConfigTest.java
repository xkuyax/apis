package me.xkuyax.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import me.xkuyax.utils.config.Config;
import me.xkuyax.utils.config.ConfigGenericResolver;
import me.xkuyax.utils.config.ConfigGenericResolver.WrappedObjectResolver;
import org.junit.Assert;
import org.junit.Test;

import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ConfigTest {
    
    @Test
    public void test() {
        Config config = new Config(Paths.get("target/test.yml"));
        ConfigTestObject object = config.getGenericType("Test.xt", ConfigTestObject.class);
        System.out.println(object);
        System.out.println(object.getEnumTest() == ConfigEnumTest.OTTO);
    }
    
    @Test
    public void genericResolver() {
        ConfigGenericResolver.getSingleObjectResolvers().add(new WrappedObjectResolver((fieldPath, value, config) -> {
            System.out.println("hi");
            return new ObjectResolverTest(5);
        }, ObjectResolverTest.class));
        Config config = new Config(Paths.get("target/test.yml"));
        ObjectResolverTest resolverTest = config.getGenericType("ResolverTest", ObjectResolverTest.class);
        Assert.assertEquals(5, resolverTest.getValue());
        NestedObjectResolverTest nestedObjectResolverTest = config.getGenericType("ResolverTest2", NestedObjectResolverTest.class);
        Assert.assertEquals(5, nestedObjectResolverTest.getResolverTest().getValue());
    }
    
    @Test
    public void debug() throws Exception {
        Config config = new Config(Paths.get("target/nestedList.yml"));
        NestedListMapTest a = config.getGenericType("Hallo", NestedListMapTest.class);
    }
    
    @Data
    public static class ConfigTestObject {
        
        private List<String> testList;
        private List<Integer> testIntegerList;
        private List<Long> testLongList;
        private List<Float> testFloatList;
        private int testInteger;
        private String testString;
        private List<ConfigTestObjectSimple> testObjectList;
        private Map<String, ConfigTestObjectSimple> testObjectSimpleMap;
        private Map<ConfigTestObjectSimple, NestedObjectResolverTest> listNestedObjectResolverTestMap;
        private ConfigEnumTest enumTest;
        
    }
    
    public enum ConfigEnumTest {
        
        NAME,
        OTTO,
        ADOLF,
        JOE,
        BLOW
        
    }
    
    @Data
    public static class NestedListMapTest {
        
        private Map<List<List<List<List<Map<ConfigTestObjectSimple, Map<String, List<ConfigTestObject>>>>>>>, String> nestedListMap = new HashMap<>();
        private Map<String, String> stringStringMap;
        private Map<Integer, Integer> integerIntegerMap;
        private Map<Integer, String> integerStringMap;
    }
    
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class NestedObjectResolverTest {
        
        private ObjectResolverTest resolverTest;
        
    }
    
    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class ObjectResolverTest {
        
        private int value;
        
    }
    
    @Data
    public static class ConfigTestObjectSimple {
    
        private int health = 4;
        private String playerName;
        
    }
}
