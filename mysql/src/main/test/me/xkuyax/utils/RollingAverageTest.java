package me.xkuyax.utils;

import org.junit.Test;

public class RollingAverageTest {
    
    @Test
    public void test() {
        RollingAverage rollingAverage = new RollingAverage(2);
        rollingAverage.increment(2);
        rollingAverage.increment(4);
        System.out.println(rollingAverage.avg());
        System.out.println(rollingAverage.getData());
        rollingAverage.increment(5);
        System.out.println(rollingAverage.avg());
        System.out.println(rollingAverage.getData());
        rollingAverage.increment(6);
        System.out.println(rollingAverage.avg());
        System.out.println(rollingAverage.getData());
    }
}