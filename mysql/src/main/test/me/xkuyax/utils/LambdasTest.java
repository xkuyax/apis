package me.xkuyax.utils;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;

public class LambdasTest {

    @Test
    public void parallelForEach() throws Exception {
        List<String> a = Arrays.asList("a", "b", "c");
        List<Integer> b = Arrays.asList(1, 2, 3);
        List<Double> c = Arrays.asList(1.2, 1.3, 1.4);
        Lambdas.parallelForEach(2, a.stream(), System.out::println);
        Lambdas.parallelForEach(2, b.stream(), System.out::println);
        Lambdas.parallelForEach(2, c.stream(), System.out::println);
        Lambdas.parallelForEachDouble(2, DoubleStream.of(1.2, 1.3, 1.4), System.out::println);
        Lambdas.parallelForEachInt(2, IntStream.of(1, 2, 3), System.out::println);
    }
}