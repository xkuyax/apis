package me.xkuyax.utils.mysql;

import com.mysql.cj.jdbc.MysqlDataSource;

import javax.sql.DataSource;

public class JDBCDataSourceCreator {
    
    public static final String OPTIONS = "?jdbcCompliantTruncation=false&autoReconnect=true&serverTimezone=Europe/Berlin&zeroDateTimeBehavior=convertToNull" + "&max_allowed_packet=512M";
    
    public static DataSource createSource(String host, String port, String data) {
        MysqlDataSource source = new MysqlDataSource();
        source.setUrl("jdbc:mysql://" + host + ":" + port + "/" + data + OPTIONS);
        return source;
    }
}
