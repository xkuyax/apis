package me.xkuyax.utils.mysql;

import lombok.Getter;
import me.xkuyax.utils.mysql.MysqlConnection.ConnectionHandler;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

@Getter
public class AlwaysNewConnectionHandler implements ConnectionHandler {
    
    private MysqlConnection mysqlConnection;
    private DataSource dataSource;
    
    @Override
    public void init(MysqlConnection mysqlConnection) {
        this.mysqlConnection = mysqlConnection;
        this.dataSource = mysqlConnection.getDataSource();
    }
    
    @Override
    public void preConnect() {
    }
    
    @Override
    public Connection openConnection() throws SQLException {
        return dataSource.getConnection(mysqlConnection.getUser(), mysqlConnection.getPass());
    }
    
    @Override
    public void updateConnection() {
    }
    
    @Override
    public Connection getConnection() {
        try {
            return openConnection();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    
    @Override
    public void finishConnection(Connection connection) {
        MysqlConnection.close(connection);
    }
}
