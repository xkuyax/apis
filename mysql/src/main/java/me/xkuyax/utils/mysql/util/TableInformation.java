package me.xkuyax.utils.mysql.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import me.xkuyax.utils.mysql.MysqlConnection.ResultSetTransFormer;
import me.xkuyax.utils.mysql.miniorm.LazyResultSetTransformer;

import java.math.BigInteger;
import java.sql.Timestamp;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TableInformation {
    
    @Getter
    public static ResultSetTransFormer<TableInformation> transformer = new LazyResultSetTransformer<>(TableInformation.class);
    private String catalog;
    private String schema;
    private String name;
    private String type;
    private String engine;
    private BigInteger version;
    private String rowFormat;
    private BigInteger tableRows;
    private BigInteger avgRowLength;
    private BigInteger dataLength;
    private BigInteger maxDataLength;
    private BigInteger indexLength;
    private BigInteger dataFree;
    private BigInteger autoIncrement;
    private Timestamp createTime;
    private Timestamp updateTime;
    private Timestamp checkTime;
    private String collation;
    private String checkSum;
    private String createOptions;
    private String tableComment;
    
}
