package me.xkuyax.utils.mysql.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import me.xkuyax.utils.mysql.MysqlConnection.ResultSetTransFormer;
import me.xkuyax.utils.mysql.miniorm.LazyResultSetTransformer;

import java.math.BigInteger;

@Data
@AllArgsConstructor
public class FieldInformation {
    
    @Getter
    public static ResultSetTransFormer<FieldInformation> transformer = new LazyResultSetTransformer<>(FieldInformation.class);
    private String catalog;
    private String schema;
    private String name;
    private String columnName;
    private BigInteger ordinalPosition;
    private Object defaultValue;
    private String nullAble;
    private String dataType;
    private BigInteger maxLength;
    private BigInteger octetLength;
    private BigInteger numericPrecision;
    private BigInteger numericScale;
    private BigInteger dateTimePrecision;
    private String characterSetName;
    private String collationName;
    private String columnType;
    private String columnKey;
    private String extra;
    private String privileges;
    private String columnComment;
    
}
