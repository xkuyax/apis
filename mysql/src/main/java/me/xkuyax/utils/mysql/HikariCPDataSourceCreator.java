package me.xkuyax.utils.mysql;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import me.xkuyax.utils.config.Config;

import javax.sql.DataSource;

public class HikariCPDataSourceCreator {

    public static DataSource createSource(String host, String port, String data, String user, String pass, int timeOut) {
        HikariConfig config = new HikariConfig();
        config.setDataSource(JDBCDataSourceCreator.createSource(host, port, data));
        config.setUsername(user);
        config.setPassword(pass);
       /* config.setConnectionTimeout(timeOut);
        config.setValidationTimeout(timeOut);
        config.setInitializationFailTimeout(timeOut);*/
        return new HikariDataSource(config);
    }

    public static DataSource createSource(String host, String port, String data, String user, String pass, Config config, String path) {
        HikariConfig hikariConfig = new HikariConfig();
        hikariConfig.setDataSource(JDBCDataSourceCreator.createSource(host, port, data));
        hikariConfig.setUsername(user);
        hikariConfig.setPassword(pass);
        hikariConfig.setMinimumIdle(config.getInt(path + ".minimumIdle", 1));
        hikariConfig.setMaximumPoolSize(config.getInt(path + ".maximumPoolSize", 10));
        return new HikariDataSource(hikariConfig);
    }

}
