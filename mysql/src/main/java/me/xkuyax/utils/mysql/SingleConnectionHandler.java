package me.xkuyax.utils.mysql;

import lombok.Getter;
import me.xkuyax.utils.mysql.MysqlConnection.ConnectionHandler;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SingleConnectionHandler implements ConnectionHandler {

    private MysqlConnection mysqlConnection;
    @Getter
    private Connection connection;
    private DataSource dataSource;

    @Override
    public void init(MysqlConnection mysqlConnection) {
        try {
            this.mysqlConnection = mysqlConnection;
            this.dataSource = mysqlConnection.getDataSource();
            openConnection();
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Could not setup connection for " + mysqlConnection.getHost() + " user " + mysqlConnection.getUser());
        }
    }

    @Override
    public Connection openConnection() throws SQLException {
        return this.connection = this.dataSource.getConnection(mysqlConnection.getUser(), mysqlConnection.getPass());
    }

    @Override
    public void updateConnection() {
        try {
            if (this.connection == null || this.connection.isClosed()) {
                openConnection();
            } else {
                try (Statement statement = connection.createStatement(); ResultSet rs = statement.executeQuery("SELECT 1")) {
                } catch (SQLException e) {
                    openConnection();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void preConnect() {
        if (connection == null) {
            try {
                openConnection();
            } catch (SQLException ignored) {
            }
        }
    }

    @Override
    public void finishConnection(Connection connection) {
    }
}
