package me.xkuyax.utils.mysql.miniorm;

import lombok.Data;
import me.xkuyax.utils.mysql.MysqlConnection.ResultSetTransFormer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NonReflectionTransformer<T> implements ResultSetTransFormer<T> {
    
    private final ClassDescriber classDescriber;
    
    public NonReflectionTransformer(ClassDescriber describer) {
        classDescriber = describer;
    }
    
    @Override
    public T transform(ResultSet rs) throws SQLException {
        return null;
    }
    
    public interface ClassDescriber {
        
        Map<ClassDescriber, DescribedClass> instances = new HashMap<>();
        
        void describe();
        
        default void iInt(String field) {
            register(rs -> rs.getInt(field));
        }
        
        default void iString(String field) {
            register(rs -> rs.getString(field));
        }
        
        default void iBoolean(String field) {
            register(rs -> rs.getBoolean(field));
        }
        
        default void iDouble(String field) {
            register(rs -> rs.getDouble(field));
        }
        
        default void register(ResultSetTransFormer<?> supplier) {
            DescribedClass describedClass = instances.computeIfAbsent(this, classDescriber -> new DescribedClass());
            describedClass.getFields().add(supplier);
        }
    }
    
    @Data
    public static class DescribedClass {
        
        private List<ResultSetTransFormer<?>> fields = new ArrayList<>();
        
    }
    
}
