package me.xkuyax.utils.mysql.miniorm;

import me.xkuyax.utils.mysql.MysqlConnection.ResultSetTransFormer;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class LazyResultSetTransformer<T> implements ResultSetTransFormer<T> {
    
    private final Class<T> clazz;
    private Constructor<T> constructor;
    private int fields;
    private List<Field> fieldClasses = new ArrayList<>();
    private int[] types;
    
    public LazyResultSetTransformer(Class<T> clazz) {
        this.clazz = clazz;
        init();
    }
    
    private void init() {
        Field[] declaredFields = clazz.getDeclaredFields();
        types = new int[declaredFields.length];
        List<Class<?>> clazzes = new ArrayList<>();
        int i = 0;
        for (Field field : declaredFields) {
            int modifiers = field.getModifiers();
            if (Modifier.isStatic(modifiers)) {
                continue;
            }
            Class<?> type = field.getType();
            if (type.equals(UUID.class)) {
                types[i] = 1;
            }
            clazzes.add(type);
            fieldClasses.add(field);
            i++;
        }
        this.fields = clazzes.size();
        try {
            constructor = clazz.getDeclaredConstructor(clazzes.toArray(new Class<?>[0]));
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            throw new RuntimeException("Could not find a proper all argument constructor!");
        }
    }
    
    @Override
    public T transform(ResultSet rs) throws SQLException {
        Object[] objects = new Object[fields];
        for (int i = 0; i < fields; i++) {
            //transform to uuid
            if (types[i] == 1) {
                objects[i] = UUID.fromString(rs.getString(i + 1));
            } else {
                objects[i] = rs.getObject(i + 1);
            }
        }
        try {
            return constructor.newInstance(objects);
        } catch (InstantiationException | InvocationTargetException | IllegalAccessException | IllegalArgumentException e) {
            e.printStackTrace();
            if (e instanceof IllegalArgumentException) {
                System.out.println("Invalid schema for class " + clazz);
                for (int i = 0; i < fields; i++) {
                    Object object = rs.getObject(i + 1);
                    System.out.println("field " + i + " type = " + (object == null ? "null" : object.getClass()) + " registered: " + fieldClasses.get(i));
                }
            }
        }
        return null;
    }
}
