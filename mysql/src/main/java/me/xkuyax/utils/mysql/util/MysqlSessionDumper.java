package me.xkuyax.utils.mysql.util;

import lombok.Data;
import lombok.extern.apachecommons.CommonsLog;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;

@CommonsLog
public class MysqlSessionDumper {

    public static MysqlSession dumpSession(Connection connection, String queryCalling) {
        MysqlSession mysqlSession = new MysqlSession(queryCalling);
        mysqlSession.dump(connection, null);
        return mysqlSession;
    }

    @Data
    public static class MysqlSession {

        private final Map<String, String> values = new HashMap<>();
        private final String query;

        public void compareTo(Map<String, String> compareValues, MysqlSessionCompare compareType) {
            log.info("MysqlSession Compare for query: " + query);
            if (compareType == MysqlSessionCompare.ALL) {
                values.entrySet().stream().sorted(Comparator.comparing(Entry::getKey)).forEachOrdered(entry -> {
                    String key = entry.getKey();
                    String oldValue = entry.getValue();
                    String newValue = compareValues.get(key);
                    printValue(key, oldValue, newValue);
                });
            } else if (compareType == MysqlSessionCompare.DIFF) {
                values.entrySet().stream().sorted(Comparator.comparing(Entry::getKey)).forEachOrdered(entry -> {
                    String key = entry.getKey();
                    String oldValue = entry.getValue();
                    String newValue = compareValues.get(key);
                    if (!Objects.equals(oldValue, newValue)) {
                        printValue(key, oldValue, newValue);
                    }
                });
            } else if (compareType == MysqlSessionCompare.RELEVANT) {
                printValue("Rows_read", compareValues);
            }
        }

        public void dump(Connection connection, MysqlSessionCompare compare) {
            try (PreparedStatement preparedStatement = connection.prepareStatement("SHOW SESSION STATUS")) {
                preparedStatement.execute();
                try (ResultSet resultSet = preparedStatement.getResultSet()) {
                    Map<String, String> values = new HashMap<>();
                    while (resultSet.next()) {
                        values.put(resultSet.getString("variable_name"), resultSet.getString("Value"));
                    }
                    if (compare == null) {
                        this.values.putAll(values);
                    } else {
                        compareTo(values, compare);
                    }
                }
            } catch (SQLException e) {
                log.info("Error dumping Session! ", e);
            }
        }

        public void printValue(String key, Map<String, String> compareWith) {
            printValue(key, values.get(key), compareWith.get(key));
        }

        public void printValue(String key, String oldValue, String newValue) {
            try {
                double oldVal = Double.parseDouble(oldValue);
                double newVal = Double.parseDouble(newValue);
                log.info("Key: " + key + " diff: " + (newVal - oldVal) + " new: " + newValue + " old: " + oldValue);
            } catch (Exception e) {
                log.info("Key: " + key + " new: " + newValue + " old: " + oldValue);
            }
        }
    }

    public enum MysqlSessionCompare {

        ALL, DIFF, RELEVANT

    }
}
