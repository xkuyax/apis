package me.xkuyax.utils.mysql;

import java.sql.Connection;
import java.sql.SQLException;

public class HikariConnectionHandler extends AlwaysNewConnectionHandler {
    
    @Override
    public Connection openConnection() throws SQLException {
        return getDataSource().getConnection();
    }
}
