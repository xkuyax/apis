package me.xkuyax.utils.mysql;

import me.xkuyax.utils.config.Config;
import me.xkuyax.utils.mysql.MysqlConnection.ConnectionHandler;

import javax.sql.DataSource;
import java.util.function.Supplier;

public class MysqlConnectionUtils {

    public static MysqlConnection hikari(Config config, String path) {
        return generic(config, path, (host, user, pass, port, data) -> HikariCPDataSourceCreator
                .createSource(host, port, data, user, pass, config, path), HikariConnectionHandler::new);
    }

    public static MysqlConnection generic(Config config, String path, DataSourceSupplier sourceSupplier, Supplier<ConnectionHandler> handlerSupplier) {
        String host = config.getString(path + ".host", "localhost");
        String port = config.getString(path + ".port", "3306");
        String user = config.getString(path + ".userName", "root");
        String pass = config.getString(path + ".password", "a");
        String data = config.getString(path + ".database", "cloudsystem");
        return new MysqlConnection(sourceSupplier.get(host, user, pass, port, data), host, user, pass, port, data, handlerSupplier.get());
    }

    @FunctionalInterface
    public interface DataSourceSupplier {

        DataSource get(String host, String user, String pass, String port, String data);

    }
}
