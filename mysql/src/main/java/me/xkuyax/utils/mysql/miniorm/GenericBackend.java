package me.xkuyax.utils.mysql.miniorm;

import lombok.Getter;
import me.xkuyax.utils.mysql.MysqlConnection;
import me.xkuyax.utils.mysql.MysqlConnection.PreparedStatementFiller;
import me.xkuyax.utils.mysql.MysqlConnection.ResultSetTransFormer;

import java.util.List;

@Getter
public class GenericBackend<V> {
    
    private final ResultSetTransFormer<V> transFormer;
    private final String table;
    private final String id;
    private final MysqlConnection mysqlConnection;
    
    public GenericBackend(MysqlConnection mysqlConnection, ResultSetTransFormer<V> transFormer, String table, String id) {
        this.transFormer = transFormer;
        this.table = table;
        this.id = id;
        this.mysqlConnection = mysqlConnection;
    }
    
    public GenericBackend(MysqlConnection mysqlConnection, Class<V> clazz, String table, String id) {
        this(mysqlConnection, new LazyResultSetTransformer<>(clazz), table, id);
    }
    
    public List<V> getAll() {
        return mysqlConnection.queryList("select * from " + table, ps -> {
        }, transFormer);
    }
    
    public List<V> getAllBy(String field, PreparedStatementFiller filler) {
        return mysqlConnection.queryList("select * from " + table + " where `" + field + "`=?", filler, transFormer);
    }
    
    public V getById(int id) {
        return getBy(this.id, ps -> ps.setInt(1, id));
    }
    
    public V getBy(String field, PreparedStatementFiller filler) {
        return mysqlConnection.query("select * from " + table + " where `" + field + "`=?", filler, transFormer);
    }
    
    public V getBy(PreparedStatementFiller filler, String... fields) {
        StringBuilder stringBuilder = generateMultiParamQuery(fields);
        return mysqlConnection.query(stringBuilder.toString(), filler, transFormer);
    }
    
    public List<V> getAllBy(PreparedStatementFiller filler, String... fields) {
        StringBuilder stringBuilder = generateMultiParamQuery(fields);
        return mysqlConnection.queryList(stringBuilder.toString(), filler, transFormer);
    }
    
    private StringBuilder generateMultiParamQuery(String[] fields) {
        StringBuilder stringBuilder = new StringBuilder("SELECT * FROM ");
        stringBuilder.append(table);
        stringBuilder.append(" where ");
        for (int i = 0; i < fields.length; i++) {
            String field = fields[i];
            stringBuilder.append("`").append(field).append("`=?");
            if (i < fields.length - 1) {
                stringBuilder.append(" and ");
            }
        }
        return stringBuilder;
    }
    
    public V getByQuery(QuerySupplier supplier, PreparedStatementFiller filler) {
        return mysqlConnection.query(supplier.getQuery(table), filler, transFormer);
    }
    
    public List<V> getAllByQuery(QuerySupplier supplier, PreparedStatementFiller filler) {
        return mysqlConnection.queryList(supplier.getQuery(table), filler, transFormer);
    }
    
    public interface QuerySupplier {
        
        String getQuery(String table);
        
    }
    
}
