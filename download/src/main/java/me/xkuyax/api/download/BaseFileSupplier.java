package me.xkuyax.api.download;

import java.nio.file.Path;
import java.util.function.Supplier;

public interface BaseFileSupplier extends Supplier<Path> {}
