package me.xkuyax.utils;

import me.xkuyax.utils.config.Config;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.function.Consumer;

public class ScriptingUtils {
    
    public static void executeScriptInConfigDirectory(Config config, String path, Consumer<ProcessBuilder> consumer,
                                                      String... replace) throws IOException, InterruptedException {
        List<ProcessBuilder> processBuilders = config.getProcessBuilders(path + ".script", replace);
        processBuilders.forEach(consumer);
        File directory = config.getPath(path + ".workingDirectory", ".").toFile();
        for (ProcessBuilder processBuilder : processBuilders) {
            {
                processBuilder.directory(directory);
                processBuilder.start().waitFor();
            }
        }
    }
}
