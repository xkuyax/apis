package me.xkuyax.utils.config;

import me.xkuyax.utils.config.file.FileConfiguration;

import java.util.Set;

public interface ConfigExtension {
    
    FileConfiguration getWrapper();
    
    Object get(String path, Object d);
    
    void set(String path, Object value);
    
    Set<String> getKeys(String path, String... defaul);
    
    default boolean isSet(String path) {
        return getWrapper().isSet(path);
    }
    
    default String getPath(String path) {
        return path;
    }
}
