package me.xkuyax.utils.config.additions;

import lombok.Data;
import me.xkuyax.utils.config.ConfigGenericResolver;

import java.lang.reflect.Type;

public interface CETypeResolver {

    Object resolve(CETypeContext context);

    @Data
    class CETypeContext {

        private final String path;
        private final Type type;
        private final ConfigGenericResolver<?> resolver;
        private Type firstArgumentType;
        private Type[] actualTypeArguments;
        private Class<?> collectionClass;
        private Object defaultValue;
    }
}
