package me.xkuyax.utils.config.additions;

import lombok.Data;
import me.xkuyax.utils.config.ConfigGenericResolver;

import java.lang.reflect.Type;
import java.util.function.Supplier;

@Data
public class ConfigReference<T> {

    private final String path;
    private final Type type;
    private final ConfigGenericResolver<T> resolver;
    private T object;
    private boolean fetched;
    private String defaultValue;

    public ConfigReference(String objectPath, Type type, ConfigGenericResolver<T> resolver, String defaultValue) {
        String defaultPath = defaultValue == null ? "Reference." + objectPath : defaultValue;
        this.path = resolver.getConfig().getString(objectPath, defaultPath);
        this.type = type;
        this.resolver = resolver;
        this.defaultValue = defaultValue;
    }

    private ConfigReference(String defaultValue) {
        this.defaultValue = defaultValue;
        this.path = null;
        this.type = null;
        this.resolver = null;
    }

    public boolean isPresent() {
        return resolver.getConfig().isSet(path);
    }

    public T getIfPresentOrNull() {
        return isPresent() ? resolveFromConfig() : null;
    }

    public T getIfPresentOr(T defaultValue) {
        return isPresent() ? resolveFromConfig() : defaultValue;
    }

    public T getIfPresentOr(Supplier<T> defaultValue) {
        return isPresent() ? resolveFromConfig() : defaultValue.get();
    }

    public T getWithForce() {
        return resolveFromConfig();
    }

    public T get() {
        return getWithForce();
    }

    public T resolveFromConfig() {
        try {
            if (!fetched) {
                return object = (T) resolver.resolveType(defaultValue, type, this.path, "Reference", null);
            } else {
                return object;
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            fetched = true;
        }
    }

    public static <T> ConfigReference<T> withDefault(String defaultValue) {
        return (ConfigReference) new ConfigReference<>(defaultValue);
    }
}
