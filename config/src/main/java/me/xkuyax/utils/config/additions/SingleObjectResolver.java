package me.xkuyax.utils.config.additions;

import me.xkuyax.utils.config.Config;

public interface SingleObjectResolver {

    Object resolve(String fieldPath, Object value, Config config);

}
