package me.xkuyax.utils.config;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.function.Function;

public class ConfigLocale {

    @Getter
    private static Locale defaultLocale = Locale.GERMANY;
    private static List<Function<Object, Locale>> resolvers = new ArrayList<>();

    public static void setDefaultLocale(Locale defaultLocale) {
        Objects.requireNonNull(defaultLocale);
        ConfigLocale.defaultLocale = defaultLocale;
    }

    public static void registerResolver(Function<Object, Locale> localeFunction) {
        resolvers.add(localeFunction);
    }

    public static <T> void registerResolver(Class<T> clazz, Function<T, Locale> localeFunction) {
        resolvers.add(o -> {
            if (clazz.isAssignableFrom(o.getClass())) {
                return localeFunction.apply((T) o);
            }
            return null;
        });
    }

    public static Locale getLocale(Object resolve) {
        for (Function<Object, Locale> localeFunction : resolvers) {
            Locale locale = localeFunction.apply(resolve);
            if (locale != null) {
                return locale;
            }
        }
        return defaultLocale;
    }
}
