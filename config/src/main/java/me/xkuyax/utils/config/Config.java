package me.xkuyax.utils.config;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import me.xkuyax.utils.config.additions.DefaultResolvers;
import me.xkuyax.utils.config.additions.WrappedObjectResolver;
import me.xkuyax.utils.config.file.FileConfiguration;
import me.xkuyax.utils.config.hotreload.HotReloadStorage;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.nio.file.Path;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;
import java.util.stream.Collectors;

@EqualsAndHashCode(of = "file")
public class Config implements ConfigListExtension, ConfigObjectExtension, ConfigPathExtension {

    private static ConfigResolverHelper helper = new ConfigResolverHelper();
    @Getter
    private FileConfiguration wrapper;
    protected File file;
    @Getter
    @Setter
    private boolean saveAfterSet;
    private HotReloadStorage hotReloadStorage;
    private AtomicInteger changes = new AtomicInteger();

    static {
        ConfigGenericResolver.getSingleObjectResolvers().add(new WrappedObjectResolver((fieldPath, value, config) -> config.getPlainLocation(fieldPath), PlainLocation.class));
        ConfigGenericResolver.getSingleObjectResolvers().add(new WrappedObjectResolver((fieldPath, value, config) -> UUID.fromString(config.getString(fieldPath, new UUID(0, 0).toString())), UUID.class));
        DefaultResolvers.init();
    }

    public Config(String yaml, boolean saveAfterSet) {
        this.file = FileConfigUtils.getConfigurationFile(yaml);
        this.wrapper = FileConfigUtils.getConfig(file);
        this.saveAfterSet = saveAfterSet;
        FileConfigUtils.getCachedConfig().add(this);
        helper.onConstructor(this);
        hotReloadStorage = new HotReloadStorage();
    }

    public Config(String yaml) {
        this(yaml, true);
    }

    public Config(File file, FileConfiguration wrapper) {
        this.saveAfterSet = true;
        this.wrapper = wrapper;
        this.file = file;
        FileConfigUtils.getCachedConfig().add(this);
        helper.onConstructor(this);
        hotReloadStorage = new HotReloadStorage();
    }

    public Config(Path path) {
        this(path.toFile(), FileConfigUtils.getConfig(path.toFile()));
    }

    public Config(Path path, boolean saveAfterSet) {
        this(path.toFile(), FileConfigUtils.getConfig(path.toFile()));
        this.saveAfterSet = saveAfterSet;
    }

    //io stuff
    public void reload() {
        FileConfigUtils.getCachedConfigs().remove(file.getPath());
        this.wrapper = FileConfigUtils.getConfig(file);
        FileConfigUtils.getCachedConfigs().put(file.getPath(), wrapper);
        hotReloadStorage.reload();
    }

    public boolean save() {
        try {
            wrapper.save(file);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    //get and set

    public Object get(String path, Object d) {
        Object i = wrapper.get(getPath(path));
        if (i == null) {
            set(path, d);
            return d;
        } else {
            return i;
        }
    }

    public Object get(String path, Supplier<Object> d) {
        Object i = wrapper.get(path);
        if (i == null) {
            set(path, d.get());
            return d.get();
        } else {
            return i;
        }
    }

    public void set(String path, Object d) {
        wrapper.set(getPath(path), d);
        changes.incrementAndGet();
        if (saveAfterSet) {
            save();
        }
    }

    public <T> void setGenericType(String path, T object, Class<T> clazz) {
        new ConfigGenericSerializer<>(this, path, clazz, object).serialize();
    }

    //keys

    public Set<String> getKeys(String path, boolean deep, List<String> defaul) {
        if (wrapper.isSet(path)) {
            ConfigurationSection cs = wrapper.getConfigurationSection(getPath(path));
            Set<String> keys = cs.getKeys(deep);
            return keys == null ? new HashSet<>(defaul) : keys;
        }
        return new HashSet<>(defaul);
    }

    public Set<String> getKeys(String path, List<String> defaul) {
        return getKeys(path, false, defaul);
    }

    public Set<String> getKeys(String path, String... defaul) {
        return getKeys(path, Arrays.asList(defaul));
    }

    public List<String> getKeyList(String path, String... defaul) {
        return getKeyList(path, false, defaul);
    }

    public List<String> getKeyList(String path, boolean deep, String... defaul) {
        ConfigurationSection section = wrapper.getConfigurationSection(path);
        if (section == null) {
            return Arrays.asList(defaul);
        }
        return wrapper.getKeyList(section, deep);
    }

    //generic types with keys

    public <T> T getGenericType(String path, Class<T> clazz) {
        return getGenericType(path, clazz, null);
    }

    public <T> T getGenericType(String path, Class<T> clazz, String name) {
        int changes = this.changes.get();
        boolean saveAfterSet = isSaveAfterSet();
        try {
            if (saveAfterSet) {
                this.setSaveAfterSet(false);
            }
            ConfigGenericResolver<T> resolver = new ConfigGenericResolver<>(clazz, path, this, name, 1);
            T resolvedObject = resolver.resolve();
            hotReloadStorage.store(path, resolvedObject, resolver);
            return resolvedObject;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Could not create a new object for the clazz " + clazz, e);
        } finally {
            if (saveAfterSet) {
                setSaveAfterSet(true);
            }
            if (this.changes.get() != changes) {
                save();
            }
        }
    }

    public <T> List<T> getGenericListType(String path, Class<T> clazz, String... defaultKeys) {
        return getKeyList(path, defaultKeys).stream().map(s -> getGenericType(path + "." + s, clazz, s)).collect(Collectors.toList());
    }

    public <T> Map<String, T> getGenericNameMap(String path, Class<T> clazz, String... defaultKeys) {
        Map<String, T> map = new LinkedHashMap<>();
        for (String key : getKeyList(path, defaultKeys)) {
            map.put(key, getGenericType(path + "." + key, clazz));
        }
        return map;
    }

    public <K, V> Map<K, V> getGenericKeyMap(String path, Class<K> keyClass, Class<V> valueClass, String... defaultKeys) {
        Map<K, V> map = new LinkedHashMap<>();
        for (String key : getKeyList(path, defaultKeys)) {
            map.put(getGenericType(path + "." + key + ".key", keyClass), getGenericType(path + "." + key + ".value", valueClass));
        }
        return map;
    }

    //is set

    public boolean isSet(String path) {
        return wrapper.isSet(path);
    }

    //sections

    public void createSection(String path) {
        getWrapper().createSection(getPath(path));
    }

    public void createSection(String path, Map<?, ?> map) {
        getWrapper().createSection(getPath(path), map);
    }

    public boolean isSection(String path) {
        return getWrapper().isConfigurationSection(getPath(path));
    }

    public ConfigSection getSection(String path) {
        if (!isSection(getPath(path))) {
            createSection(getPath(path));
        }
        return new ConfigSection(this, getPath(path));
    }

    @Target(ElementType.FIELD)
    @Retention(RetentionPolicy.RUNTIME)
    public @interface ConfigSerializerIgnore {}
}
