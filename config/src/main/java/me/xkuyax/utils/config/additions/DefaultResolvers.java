package me.xkuyax.utils.config.additions;

import me.xkuyax.utils.config.ConfigGenericResolver;
import me.xkuyax.utils.config.additions.CETypeResolver.CETypeContext;

import java.nio.file.Path;
import java.util.Objects;
import java.util.function.Function;

public class DefaultResolvers {

    public static void init() {
        addLanguageItem();
        addDynamicEntry();
        ConfigGenericResolver.getSingleObjectResolvers().add(new WrappedObjectResolver((fieldPath, value, config) -> {
            return config.getPath(fieldPath, "test");
        }, Path.class));
        addConfigOptional();
        addConfigReference();
    }

    public static void addConfigReference() {
        add(ConfigReference.class, ceTypeContext -> {
            String defaultValue;
            Object configReference = ceTypeContext.getDefaultValue();
            if (configReference instanceof ConfigReference) {
                defaultValue = ((ConfigReference) configReference).getDefaultValue();
            } else {
                defaultValue = null;
            }
            return new ConfigReference(ceTypeContext.getPath(), ceTypeContext.getFirstArgumentType(), ceTypeContext.getResolver(), defaultValue);
        });
    }

    public static void addConfigOptional() {
        add(ConfigOptional.class, ceTypeContext -> {
            Object defaultValue;
            if (ceTypeContext.getDefaultValue() instanceof ConfigOptional) {
                defaultValue = ((ConfigOptional) ceTypeContext.getDefaultValue()).getDefaultValue();
            } else {
                defaultValue = null;
            }
            return new ConfigOptional(ceTypeContext.getPath(), ceTypeContext.getFirstArgumentType(), ceTypeContext.getResolver(), defaultValue);
        });
    }

    private static void addDynamicEntry() {
        add(DynamicCEntry.class, ceTypeContext -> {
            DynamicCEntry<?> entry = new DynamicCEntry<>(ceTypeContext.getPath(), ceTypeContext.getFirstArgumentType(), ceTypeContext.getResolver());
            if (ceTypeContext.getDefaultValue() instanceof DynamicCEntry) {
                DynamicCEntry<?> defaultValue = (DynamicCEntry) ceTypeContext.getDefaultValue();
                if (!ceTypeContext.getResolver().getConfig().isSet(ceTypeContext.getPath())) {
                    defaultValue.getDefaults().forEach(entry::get);
                }
                defaultValue.getRequired().forEach(entry::get);
            }
            return entry;
        });
    }

    private static void addLanguageItem() {
        add(LanguageCEntry.class, ceTypeContext -> {
            Object defaultValue;
            if (ceTypeContext.getDefaultValue() instanceof LanguageCEntry) {
                defaultValue = ((LanguageCEntry) ceTypeContext.getDefaultValue()).getDefaultValue();
            } else {
                defaultValue = null;
            }
            return new LanguageCEntry(ceTypeContext.getPath(), ceTypeContext.getFirstArgumentType(), ceTypeContext.getResolver(), defaultValue);
        });
    }

    public static <T> void add(Class<T> returnClass, Function<CETypeContext, T> function) {
        ConfigGenericResolver.getCeTypeResolvers().add(new WrappedCETypeResolver(context -> {
            if (Objects.equals(context.getCollectionClass(), returnClass)) {
                return function.apply(context);
            }
            return null;
        }));
    }
}
