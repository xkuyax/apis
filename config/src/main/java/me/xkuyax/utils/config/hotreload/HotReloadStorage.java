package me.xkuyax.utils.config.hotreload;

import lombok.Data;
import me.xkuyax.utils.config.ConfigGenericResolver;

import java.util.WeakHashMap;

@Data
public class HotReloadStorage {

    private WeakHashMap<Object, HotReloadData> data = new WeakHashMap<>();

    public <T> void store(String path, T resolved, ConfigGenericResolver<T> resolver) {
        data.put(resolved, new HotReloadData(path, resolver));
    }

    public void reload() {
        data.forEach((o, hotReloadData) -> {
            try {
                hotReloadData.getResolver().updateFields(o);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    @Data
    public static class HotReloadData {

        private final String path;
        private final ConfigGenericResolver resolver;

    }
}
