package me.xkuyax.utils.config;

import me.xkuyax.utils.StringUtils;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public interface ConfigPathExtension extends ConfigListExtension, ConfigObjectExtension {
    
    default Path getPath(String key, String d) {
        return Paths.get(getString(key, d));
    }
    
    default Path getPath(Path parent, String key, String d) {
        return parent.resolve(getString(key, d));
    }
    
    default List<ProcessBuilder> getProcessBuilders(String key, String... replace) {
        List<ProcessBuilder> processBuilders = new ArrayList<>();
        for (String k : getKeys(key + ".commands", "1")) {
            ProcessBuilder processBuilder = new ProcessBuilder();
            List<String> commands = new ArrayList<>();
            List<String> args = getStringList(key + ".commands." + k + ".args", "echo", "hi");
            for (String arg : args) {
                List<String> r = new ArrayList<>();
                List<String> o = new ArrayList<>();
                r.add("%s%");
                o.add("\"");
                StringUtils.fillLists(r, o, replace);
                commands.add(StringUtils.formatString(arg, r, o));
            }
            processBuilder.command(commands);
            processBuilders.add(processBuilder);
        }
        return processBuilders;
    }
}
