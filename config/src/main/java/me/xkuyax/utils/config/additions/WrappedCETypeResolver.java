package me.xkuyax.utils.config.additions;

import lombok.Data;

@Data
public class WrappedCETypeResolver {

    private final CETypeResolver resolver;

}
