package me.xkuyax.utils.config.additions;

import lombok.Data;

@Data
public class WrappedObjectResolver {

    private final SingleObjectResolver resolver;
    private final Class<?> clazz;

}
