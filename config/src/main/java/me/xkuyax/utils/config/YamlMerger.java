package me.xkuyax.utils.config;

import me.xkuyax.utils.config.file.YamlConfiguration;

import java.util.function.BiFunction;

public class YamlMerger {

    public static void mergeYaml(YamlConfiguration from, YamlConfiguration into) {
        merge(from.getRoot(), into.getRoot(), null);
    }

    public static void mergeYaml(YamlConfiguration from, YamlConfiguration into, BiFunction<Object, Object, Object> converter) {
        merge(from.getRoot(), into.getRoot(), converter);
    }

    public static void merge(ConfigurationSection from, ConfigurationSection into) {
        merge(from, into, null);
    }

    public static void merge(ConfigurationSection from, ConfigurationSection into, BiFunction<Object, Object, Object> converter) {
        from.getRawMap().forEach((s, o) -> {
            if (o instanceof ConfigurationSection) {
                ConfigurationSection cs = (ConfigurationSection) o;
                ConfigurationSection section = into.getConfigurationSection(s);
                if (section == null) {
                    into.createSection(s, cs.getRawMap());
                } else {
                    merge(cs, section, converter);
                }
            } else {
                if (converter != null) {
                    into.getRawMap().put(s, converter.apply(o, into.getRawMap().get(s)));
                } else {
                    into.getRawMap().put(s, o);
                }
            }
        });
    }
}
