package me.xkuyax.utils.config;

import lombok.Getter;
import me.xkuyax.utils.config.file.FileConfiguration;
import me.xkuyax.utils.config.file.YamlConfiguration;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class FileConfigUtils {

    @Getter
    private static HashMap<String, FileConfiguration> cachedConfigs = new HashMap<>();
    private static HashMap<String, File> cachedFiles = new HashMap<>();
    @Getter
    private static List<Config> cachedConfig = new ArrayList<>();

    public static FileConfiguration getConfig(String yaml) {
        return getConfig(getConfigurationFile(yaml));
    }

    public static FileConfiguration getConfig(File file) {
        if (cachedConfigs.containsKey(file.getPath())) {
            return cachedConfigs.get(file.getPath());
        }
        InputStreamReader in;
        try {
            in = new InputStreamReader(new FileInputStream(file), StandardCharsets.UTF_8);
            FileConfiguration n = YamlConfiguration.loadConfiguration(in);
            cachedConfigs.put(file.getPath(), n);
            return n;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static File getConfigurationFile(String yaml) {
        return checkAndCreateFile(getYamlPath(yaml));
    }

    public static String getYamlPath(String yaml) {
        return yaml;
    }

    public static File checkAndCreateFile(String pathName) {
        if (cachedFiles.containsKey(pathName)) {
            return cachedFiles.get(pathName);
        }
        Path path = Paths.get(pathName);
        if (!Files.exists(path)) {
            try {
                if (path.getParent() != null) {
                    Files.createDirectories(path.getParent());
                }
                Files.createFile(path);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        File file = path.toFile();
        cachedFiles.put(pathName, file);
        return file;
    }

    public static void reloadAll() {
        for (Config config : new ArrayList<>(cachedConfig)) {
            config.reload();
        }
    }

}
