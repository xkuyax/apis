package me.xkuyax.utils.config.additions;

import lombok.Data;
import me.xkuyax.utils.config.ConfigGenericResolver;
import me.xkuyax.utils.config.ConfigLocale;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@Data
public class LanguageCEntry<T> {

    private String path;
    private Type type;
    private ConfigGenericResolver<T> resolver;
    private Map<String, T> objects = new HashMap<>();
    private T defaultValue;

    private LanguageCEntry(T defaultValue) {
        this.defaultValue = defaultValue;
    }

    public LanguageCEntry(String path, Type type, ConfigGenericResolver<T> resolver, T defaultValue) {
        this.path = path;
        this.type = type;
        this.resolver = resolver;
        this.defaultValue = defaultValue;
        loadDefaults();
    }

    public LanguageCEntry(String path, Type type, ConfigGenericResolver<T> resolver) {
        this(path, type, resolver, null);
    }

    public void loadDefaults() {
        T german = get(Locale.GERMANY, defaultValue);
        T english = get(Locale.ENGLISH, german);
        get(new Locale.Builder().setLanguage("ru").setRegion("RU").build(), english);
    }

    public T get(Object o) {
        return get(ConfigLocale.getLocale(o));
    }

    public T get(Object o, T defaultValue) {
        return get(ConfigLocale.getLocale(o), defaultValue);
    }

    public T get(Locale locale) {
        return get(locale, null);
    }

    public T get(Locale locale, T defaul) {
        return objects.computeIfAbsent(locale.getLanguage(), s -> {
            try {
                T parsedDefaultValue = parseDefaultValue(defaul == null ? defaultValue : defaul);
                return (T) resolver.resolveType(parsedDefaultValue, type, path + "." + s, "languageItem", null);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    private T parseDefaultValue(T value) {
        if (value instanceof String) {
            return (T) ((String) value).replaceAll("%path%", path);
        }
        return value;
    }

    public static <T> LanguageCEntry<T> withDefault(T object) {
        return new LanguageCEntry<>(object);
    }
}
