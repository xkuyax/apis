package me.xkuyax.utils.config;

import lombok.Data;

@Data
public class ConfigGenericSerializer<T> {

    private final Config config;
    private final String path;
    private final Class<T> clazz;
    private final T object;

    public void serialize() {

    }
}
