package me.xkuyax.utils.config;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.apachecommons.CommonsLog;
import me.xkuyax.utils.PrimitiveMappings;
import me.xkuyax.utils.config.additions.CETypeResolver.CETypeContext;
import me.xkuyax.utils.config.additions.WrappedCETypeResolver;
import me.xkuyax.utils.config.additions.WrappedObjectResolver;
import org.apache.commons.lang3.ArrayUtils;
import sun.misc.Unsafe;

import java.lang.reflect.*;
import java.util.*;

@RequiredArgsConstructor
@CommonsLog
@Getter
public class ConfigGenericResolver<T> {

    private static final Unsafe unsafe = getUnsafe();
    @Getter
    private static List<WrappedObjectResolver> singleObjectResolvers = new ArrayList<>();
    @Getter
    private static List<WrappedCETypeResolver> ceTypeResolvers = new ArrayList<>();
    private final Class<T> clazz;
    private final String path;
    private final Config config;
    private final String name;
    private final int version;

    public T resolve() throws Exception {
        for (WrappedObjectResolver wrappedObjectResolver : singleObjectResolvers) {
            if (clazz.equals(wrappedObjectResolver.getClazz())) {
                return (T) wrappedObjectResolver.getResolver().resolve(path, null, config);
            }
        }
        T object = createNewInstance(clazz, name);
        updateFields(object);
        return object;
    }

    public static <T> T createNewInstance(Class clazz, String name) throws InstantiationException, IllegalAccessException, InvocationTargetException {
        Constructor<?> noArgConstructor = null;
        T object = null;
        for (Constructor<?> constructor : clazz.getDeclaredConstructors()) {
            Class<?>[] parameterTypes = constructor.getParameterTypes();
            if (parameterTypes.length > 0) {
                if (parameterTypes.length == 1 && parameterTypes[0].equals(String.class)) {
                    object = (T) constructor.newInstance(name);
                }
            } else {
                noArgConstructor = constructor;
            }
        }
        if (object == null && noArgConstructor != null) {
            object = (T) noArgConstructor.newInstance();
        } else {
            log.warn("Using unsafe to create " + clazz + "!");
            object = (T) unsafe.allocateInstance(clazz);
        }
        return object;
    }

    public void updateFields(T object) throws Exception {
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            if (Modifier.isStatic(field.getModifiers())) {
                continue;
            }
            field.setAccessible(true);
            String fieldName = field.getName();
            String fieldPath = path + "." + fieldName;
            if (fieldName.equals("name")) {
                field.set(object, name);
            } else if (!field.isAnnotationPresent(Config.ConfigSerializerIgnore.class) && !Modifier.isTransient(field.getModifiers())) {
                //resolves the value - even List<List<List<Map<List<List>>,Map<List<List>>> or some other completely nonsensical variables
                Object value = resolveType(field.get(object), field.getGenericType(), fieldPath, null, object);
                //we have to check for float values because the value could be a double which would result in a cast error
                try {
                    if (field.getType() == float.class) {
                        field.set(object, value instanceof Double ? ((Double) value).floatValue() : value);
                        continue;
                    } else if (field.getType() == String.class) {
                        if (!(value instanceof String)) {
                            log.warn("Field " + field + " is a string, but resolved type is not a string! Using .toString()!");
                            field.set(object, value.toString());
                            continue;
                        }
                    } else if (field.getType() == boolean.class) {
                        if (!(value instanceof Boolean)) {
                            log.warn("Field " + field + " is a boolean, but resolved type is not a boolean! Using Boolean.valueOf!");
                            field.set(object, Boolean.valueOf((String) value));
                            continue;
                        }
                    }
                    field.set(object, value);
                } catch (Exception e) {
                    e.printStackTrace();
                    log.warn("Error at setting " + field + " to " + value);
                }
            }
        }
    }

    public Object resolveType(Object value, Type genericType, String fieldPath, String name, T object) throws Exception {
        //get the default value for this field
        //Object value = field != null ? field.get(object) : null;
        //if we have a type which has more types like a list,map or something try this
        if (genericType instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) genericType;
            //hashmap support
            Class<?> collectionClass = (Class<?>) parameterizedType.getRawType();
            Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
            Type firstArgumentType = actualTypeArguments[0];
            if (Map.class.isAssignableFrom(collectionClass)) {
                if (version == 1) {
                    //create new hashmap
                    Map<Object, Object> valueMap = createMapFromClass(collectionClass);
                    //first type = key
                    //second type = value
                    Type typeValue = actualTypeArguments[1];
                    //get the keys for the field path
                    //and return the value by calling resolveType again
                    //if we happen to have a list inside us we resolve that type again correctly
                    //if not we use resolve and get field which resolves a simple type
                    return handleMapCreation(value, fieldPath, object, firstArgumentType, valueMap, typeValue);
                } else if (version == 2) {
                    Object o = config.getWrapper().get(fieldPath);
                    if (o == null) {
                        o = new LinkedHashMap<>();
                    }
                    config.getWrapper().set(fieldPath, o);
                }
                //list support
            } else if (List.class.isAssignableFrom(collectionClass)) {
                //fast finishing for List<Integer> or List<String>
                return getListOfType(config, fieldPath, firstArgumentType, collectionClass, object, value);
                //same as for the list
            } else if (Set.class.isAssignableFrom(collectionClass)) {
                Set<Object> returnList = createSetFromClass(collectionClass);
                for (String s : config.getKeys(fieldPath, "1")) {
                    returnList.add(resolveType(null, firstArgumentType, fieldPath + "." + s, "list", object));
                }
                return returnList;
            }
            CETypeContext context = new CETypeContext(fieldPath, genericType, this);
            context.setActualTypeArguments(actualTypeArguments);
            context.setFirstArgumentType(firstArgumentType);
            context.setCollectionClass(collectionClass);
            context.setDefaultValue(value);
            Object resolve = getWrappedObjectFromCETypeResolvers(context);
            if (resolve != null) {
                return resolve;
            }
            //general resolver method for primitive types
        } else if (genericType instanceof Class) {
            Class<?> fieldClazz = (Class<?>) genericType;
            //primitive handling -> isWrapperType to check for Boxed Types (Integer -> int)
            if (fieldClazz.isEnum()) {
                value = getEnumRaw((Class<? extends Enum<?>>) fieldClazz, fieldPath, value);
            } else if (fieldClazz.isArray()) {
                Class<?> componentType = fieldClazz.getComponentType();
                List<?> listOfType = getListOfType(config, fieldPath, componentType, List.class, object, null);
                Class<?> wrapperClazz = componentType.isPrimitive() ? PrimitiveMappings.getWrapperClass(componentType) : componentType;
                Object[] objects = listOfType.toArray((Object[]) Array.newInstance(wrapperClazz, listOfType.size()));
                return componentType.isPrimitive() ? castToPrimitiveArray(componentType, objects) : objects;
            } else if (fieldClazz.equals(Float.class)) {
                Object o = config.get(fieldPath, value == null ? PrimitiveMappings.defaultValue(fieldClazz) : value);
                if (o instanceof Number) {
                    value = ((Number) o).floatValue();
                }
            } else if (fieldClazz.isPrimitive() || PrimitiveMappings.isWrapperType(fieldClazz)) {
                value = config.get(fieldPath, value == null ? PrimitiveMappings.defaultValue(fieldClazz) : value);
                //string handling just to add "string" as default value
            } else if (fieldClazz.equals(String.class)) {
                value = config.get(fieldPath, value == null ? "string" : value);
                //handling for primitive/string types
            } else {
                //check if we have a mapping from CustomType/Interface to an Object
                for (WrappedObjectResolver wrappedObjectResolver : singleObjectResolvers) {
                    if (fieldClazz.equals(wrappedObjectResolver.getClazz())) {
                        return wrappedObjectResolver.getResolver().resolve(fieldPath, value, config);
                    }
                }
                CETypeContext context = new CETypeContext(fieldPath, genericType, this);
                context.setDefaultValue(value);
                Object resolve = getWrappedObjectFromCETypeResolvers(context);
                if (resolve != null) {
                    return resolve;
                }
                //we have no mapping so we need to check if we would need to resolve a interface
                if (!fieldClazz.isInterface()) {
                    //if we have no interface recall the generic resolver to resolve given type
                    ConfigGenericResolver configGenericResolver = new ConfigGenericResolver<>(fieldClazz, fieldPath, config, name, version);
                    value = configGenericResolver.resolve();
                } else {
                    log.error("cannot get the interface " + fieldClazz + " from a config!");
                }
            }
            return value;
        }
        return null;
    }

    private Object castToPrimitiveArray(Class<?> type, Object[] array) {
        if (type.equals(int.class))
            return ArrayUtils.toPrimitive((Integer[]) array);
        if (type.equals(double.class))
            return ArrayUtils.toPrimitive((Double[]) array);
        if (type.equals(float.class))
            return ArrayUtils.toPrimitive((Float[]) array);
        if (type.equals(Long.class))
            return ArrayUtils.toPrimitive((Long[]) array);
        if (type.equals(Boolean.class))
            return ArrayUtils.toPrimitive((Boolean[]) array);
        if (type.equals(Short.class))
            return ArrayUtils.toPrimitive((Short[]) array);
        if (type.equals(Byte.class))
            return ArrayUtils.toPrimitive((Byte[]) array);
        if (type.equals(Character.class))
            return ArrayUtils.toPrimitive((Character[]) array);
        throw new IllegalStateException("Type is not primitive");
    }

    private List<?> getListOfType(Config config, String fieldPath, Type type, Class<?> collectionClass, T object, Object value) throws Exception {
        if (type.equals(String.class))
            return config.getStringList(fieldPath, value == null ? new String[]{} : ((List<String>) value).toArray(new String[0]));
        if (type.equals(Integer.class) || type.equals(int.class))
            return config.getIntegerList(fieldPath, value == null ? new Integer[]{} : ((List<Integer>) value).toArray(new Integer[0]));
        if (type.equals(Float.class) || type.equals(float.class))
            return config.getFloatList(fieldPath, value == null ? new Float[]{} : ((List<Float>) value).toArray(new Float[0]));
        if (type.equals(Long.class) || type.equals(long.class))
            return config.getLongList(fieldPath, value == null ? new Long[]{} : ((List<Long>) value).toArray(new Long[0]));
        if (type.equals(Boolean.class) || type.equals(boolean.class))
            return config.getBooleanList(fieldPath, value == null ? new Boolean[]{} : ((List<Boolean>) value).toArray(new Boolean[0]));
        if (type.equals(Short.class) || type.equals(short.class))
            return config.getShortList(fieldPath, value == null ? new Short[]{} : ((List<Short>) value).toArray(new Short[0]));
        if (type.equals(Byte.class) || type.equals(byte.class))
            return config.getByteList(fieldPath, value == null ? new Byte[]{} : ((List<Byte>) value).toArray(new Byte[0]));
        if (type.equals(Character.class) || type.equals(char.class))
            return config.getCharacterList(fieldPath, value == null ? new Character[]{} : ((List<Character>) value).toArray(new Character[0]));
        if (type.equals(Double.class) || type.equals(double.class))
            return config.getDoubleList(fieldPath, value == null ? new Double[]{} : ((List<Double>) value).toArray(new Double[0]));

        List<Object> returnList = createListFromClass(collectionClass);
        for (String s : config.getKeys(fieldPath, "1")) {
            returnList.add(resolveType(null, type, fieldPath + "." + s, "list", object));
        }

        return returnList;
    }

    public Object handleMapCreation(Object value, String fieldPath, T object, Type firstArgumentType, Map<Object, Object> valueMap, Type typeValue) throws Exception {
        List<String> defaultValues = new ArrayList<>();
        Map defaultMap = null;
        if (value instanceof Map) {
            defaultMap = (Map) value;
            addMapDefaultValues((Map) value, defaultValues);
        }
        if (defaultValues.isEmpty()) {
            defaultValues.add("1");
        }
        for (String s : config.getKeys(fieldPath, defaultValues)) {
            String prefix = fieldPath + "." + s;
            if (defaultMap != null) {
                Object key = resolveType(null, firstArgumentType, prefix + ".key", "key", object);
                Object keyValue = resolveType(defaultMap.getOrDefault(s, null), typeValue, prefix + ".value", "value", object);
                valueMap.put(key, keyValue);
            } else {
                Object key = resolveType(null, firstArgumentType, prefix + ".key", "key", object);
                Object keyValue = resolveType(null, typeValue, prefix + ".value", "value", object);
                valueMap.put(key, keyValue);
            }
        }
        return valueMap;
    }

    public void addMapDefaultValues(Map value, List<String> defaultValues) {
        Set set = value.keySet();
        for (Object key : set) {
            if (key instanceof String) {
                defaultValues.add((String) key);
            }
        }
    }

    private Object getWrappedObjectFromCETypeResolvers(CETypeContext context) {
        for (WrappedCETypeResolver ceTypeResolver : ceTypeResolvers) {
            Object resolve = ceTypeResolver.getResolver().resolve(context);
            if (resolve != null) {
                return resolve;
            }
        }
        return null;
    }

    private Enum<?> getEnumRaw(Class<? extends Enum<?>> enumClass, String path, Object value) {
        Enum<?>[] constants = enumClass.getEnumConstants();
        Enum<?> defaul = value instanceof Enum<?> ? (Enum<?>) value : constants[0];
        String s = config.getString(path, defaul.toString());
        for (Enum<?> t : constants) {
            if (t.toString().equals(s)) {
                return t;
            }
        }
        config.set(path, defaul.toString());
        return defaul;
    }

    private static Map<Object, Object> createMapFromClass(Class<?> clazz) {
        if (LinkedHashMap.class.isAssignableFrom(clazz)) {
            return new LinkedHashMap<>();
        }
        if (TreeMap.class.isAssignableFrom(clazz)) {
            return new TreeMap<>();
        }
        return new HashMap<>();
    }

    private static List<Object> createListFromClass(Class<?> clazz) {
        if (LinkedList.class.isAssignableFrom(clazz)) {
            return new LinkedList<>();
        }
        return new ArrayList<>();
    }

    private static Set<Object> createSetFromClass(Class<?> clazz) {
        if (LinkedHashSet.class.isAssignableFrom(clazz)) {
            return new LinkedHashSet<>();
        }
        return new HashSet<>();
    }

    private static Unsafe getUnsafe() {
        try {
            Field unsafeField = Unsafe.class.getDeclaredField("theUnsafe");
            unsafeField.setAccessible(true);
            return (Unsafe) unsafeField.get(null);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
