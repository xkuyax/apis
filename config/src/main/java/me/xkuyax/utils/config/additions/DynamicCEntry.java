package me.xkuyax.utils.config.additions;

import lombok.Data;
import me.xkuyax.utils.config.ConfigGenericResolver;

import java.lang.reflect.Type;
import java.util.*;

@Data
public class DynamicCEntry<T> {

    private final String path;
    private final Type type;
    private final ConfigGenericResolver<T> resolver;
    private final T defaultValue;
    private Map<String, T> objects = new HashMap<>();
    private List<String> defaults = new ArrayList<>();
    private List<String> required = new ArrayList<>();

    public DynamicCEntry(String path, Type type, ConfigGenericResolver<T> resolver, T defaultValue) {
        this.path = path;
        this.type = type;
        this.resolver = resolver;
        this.defaultValue = defaultValue;
    }

    public DynamicCEntry(String path, Type type, ConfigGenericResolver<T> resolver) {
        this(path, type, resolver, null);
    }

    protected DynamicCEntry() {
        this.path = null;
        this.type = null;
        this.resolver = null;
        this.defaultValue = null;
    }

    public boolean isSet(String name){
        return resolver.getConfig().isSet(this.path+"."+name);
    }

    public T get(String name) {
        return objects.computeIfAbsent(name, s -> {
            try {
                return (T) resolver.resolveType(defaultValue, type, this.path + "." + s, s, null);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        });
    }

    public static <T> DynamicCEntry<T> createWithDefault(String... defaults) {
        DynamicCEntry<T> entry = new DynamicCEntry<>();
        entry.setDefaults(Arrays.asList(defaults));
        return entry;
    }

    public static <T> DynamicCEntry<T> createWithRequired(String... required) {
        DynamicCEntry<T> entry = new DynamicCEntry<>();
        entry.setRequired(Arrays.asList(required));
        return entry;
    }
}
