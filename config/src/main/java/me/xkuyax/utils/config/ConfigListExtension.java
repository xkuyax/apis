package me.xkuyax.utils.config;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public interface ConfigListExtension extends ConfigExtension {

    default List<String> getStringList(String path, String... defaul) {
        return getStringList(path, true, defaul);
    }

    default List<String> getStringList(String path, boolean empty, String... defaul) {
        return setDefault(path, getWrapper().getStringList(path), defaul);
    }

    default List<Integer> getIntegerList(String path, Integer... defaul) {
        return getIntegerList(path, true, defaul);
    }

    default List<Integer> getIntegerList(String path, boolean empty, Integer... defaul) {
        return setDefault(path, getWrapper().getIntegerList(path), defaul);
    }

    default List<Short> getShortList(String path, Short... defaul) {
        return getShortList(path, true, defaul);
    }

    default List<Short> getShortList(String path, boolean empty, Short... defaul) {
        return setDefault(path, getWrapper().getShortList(path), defaul);
    }

    default List<Byte> getByteList(String path, Byte... defaul) {
        return getByteList(path, true, defaul);
    }

    default List<Byte> getByteList(String path, boolean empty, Byte... defaul) {
        return setDefault(path, getWrapper().getByteList(path), defaul);
    }

    default List<Character> getCharacterList(String path, Character... defaul) {
        return getCharacterList(path, true, defaul);
    }

    default List<Character> getCharacterList(String path, boolean empty, Character... defaul) {
        return setDefault(path, getWrapper().getCharacterList(path), defaul);
    }

    default List<Boolean> getBooleanList(String path, Boolean... defaul) {
        return getBooleanList(path, true, defaul);
    }

    default List<Boolean> getBooleanList(String path, boolean empty, Boolean... defaul) {
        return setDefault(path, getWrapper().getBooleanList(path), defaul);
    }

    default List<Float> getFloatList(String path, Float... defaul) {
        return getFloatList(path, true, defaul);
    }

    default List<Float> getFloatList(String path, boolean empty, Float... defaul) {
        return setDefault(path, getWrapper().getFloatList(path), defaul);
    }

    default List<Long> getLongList(String path, Long... defaul) {
        return getLongList(path, true, defaul);
    }

    default List<Long> getLongList(String path, boolean empty, Long... defaul) {
        return setDefault(path, getWrapper().getLongList(path), defaul);
    }

    default List<Double> getDoubleList(String path, Double... defaul) {
        return getDoubleList(path, true, defaul);
    }

    default List<Double> getDoubleList(String path, boolean empty, Double... defaul) {
        return setDefault(path, getWrapper().getDoubleList(path), defaul);
    }

    default <T> List<T> setDefault(String path, List<T> existing, T[] values) {
        if (!isSet(path)) {
            List<T> value = Arrays.asList(values);
            set(path, value);
            return new ArrayList<>(value);
        }
        return existing;
    }
}
