package me.xkuyax.utils.config.additions;

import lombok.Getter;
import me.xkuyax.utils.config.ConfigGenericResolver;

import java.lang.reflect.Type;
import java.util.function.Supplier;

public class ConfigOptional<T> {

    private final String path;
    private final Type type;
    private final ConfigGenericResolver<T> resolver;
    private T object;
    private boolean fetched;
    @Getter
    private T defaultValue;

    public ConfigOptional(String path, Type type, ConfigGenericResolver<T> resolver, T defaultValue) {
        this.path = path;
        this.type = type;
        this.resolver = resolver;
        this.defaultValue = defaultValue;
    }

    public ConfigOptional(String path, Type type, ConfigGenericResolver<T> resolver) {
        this(path, type, resolver, null);
    }

    public boolean isPresent() {
        return resolver.getConfig().isSet(path);
    }

    public T getIfPresentOrNull() {
        return isPresent() ? resolveFromConfig() : null;
    }

    public T getIfPresentOrDefault() {
        return isPresent() ? resolveFromConfig() : defaultValue;
    }

    public T getIfPresentOr(T defaultValue) {
        return isPresent() ? resolveFromConfig() : defaultValue;
    }

    public T getIfPresentOr(Supplier<T> defaultValue) {
        return isPresent() ? resolveFromConfig() : defaultValue.get();
    }

    public T getWithForce() {
        return resolveFromConfig();
    }

    public T get() {
        return getWithForce();
    }

    public T resolveFromConfig() {
        try {
            if (!fetched) {
                return object = (T) resolver.resolveType(defaultValue, type, this.path, "Optional", null);
            } else {
                return object;
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            fetched = true;
        }
    }

    public static <T> ConfigOptional<T> withDefault(T defaultValue) {
        return (ConfigOptional) new ConfigOptional<>(null, null, null, defaultValue);
    }
}
