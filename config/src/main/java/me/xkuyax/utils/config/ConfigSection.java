package me.xkuyax.utils.config;

import lombok.Getter;

public class ConfigSection extends Config {
    
    @Getter
    private ConfigSection previous;
    private String subpath;
    
    protected ConfigSection(Config config, String subpath) {
        this(config, subpath, null);
    }
    
    protected ConfigSection(Config config, String subpath, ConfigSection previous) {
        super(config.file, config.getWrapper());
        setSaveAfterSet(config.isSaveAfterSet());
        this.previous = previous;
        this.subpath = subpath;
    }
    
    @Override
    public String getPath(String path) {
        return subpath + "." + path;
    }
    
    @Override
    public ConfigSection getSection(String path) {
        if (!isSection(getPath(path))) {
            createSection(getPath(path));
        }
        return new ConfigSection(this, getPath(path), this);
    }
}
