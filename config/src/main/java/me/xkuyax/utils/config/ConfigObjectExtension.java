package me.xkuyax.utils.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

public interface ConfigObjectExtension extends ConfigExtension {

    String DEFAULT_LOCATION = "lobby,0.0,100.0,0.0,0.0,0.0";

    default String getString(String path, String d) {
        Object o = get(path, d);
        if (o instanceof String) {
            return (String) o;
        }
        return o.toString();
    }

    default boolean getBoolean(String path, boolean d) {
        Object o = get(path, d);
        if (o instanceof Boolean) {
            return (boolean) o;
        } else {
            return o instanceof String ? Boolean.valueOf((String) o) : false;
        }
    }

    default int getInt(String path, int i) {
        return (int) get(path, i);
    }

    default long getLong(String path, long i) {
        if (isSet(path)) {
            return getWrapper().getLong(path, i);
        } else {
            set(path, i);
            return i;
        }
    }

    default float getFloat(String path, float f) {
        Object o = get(path, f);
        if (o instanceof Double) {
            return ((Double) o).floatValue();
        }
        return (float) o;
    }

    default short getShort(String path, short i) {
        return (short) getInt(path, i);
    }

    default double getDouble(String path, double i) {
        return (double) get(path, i);
    }

    default byte getByte(String path, byte b) {
        return (byte) get(path, b);
    }

    default char getChar(String path, char c) {
        return (char) get(path, c);
    }

    default <T extends Enum<T>> ArrayList<T> getEnumList(Class<T> enu, List<String> a) {
        ArrayList<T> c = new ArrayList<>();
        T[] enumConstants = enu.getEnumConstants();
        for (String s : a) {
            for (T e : enumConstants) {
                if (e.toString().equalsIgnoreCase(s)) {
                    c.add(e);
                    break;
                }
            }
        }
        return c;
    }

    default <T extends Enum<T>> T getEnum(Class<T> enu, String path, T defaul) {
        T[] enumConstants = enu.getEnumConstants();
        String s = getString(path, defaul.toString());
        for (T t : enumConstants) {
            if (t.toString().equals(s)) {
                return t;
            }
        }
        set(path, defaul.toString());
        return defaul;
    }

    default PlainLocation getPlainLocation(String path) {
        String raw = getString(path, DEFAULT_LOCATION);
        return parsePlainLocation(raw);
    }

    default void savePlainLocation(PlainLocation loc, String path) {
        set(path, pastePlainLocation(loc));
    }

    static PlainLocation parsePlainLocation(String raw) {
        PlainLocation location = null;
        try {
            String[] loc = raw.split(",");
            String w = (loc[0]);
            Double x = Double.parseDouble(loc[1]);
            Double y = Double.parseDouble(loc[2]);
            Double z = Double.parseDouble(loc[3]);
            float yaw = Float.parseFloat(loc[4]);
            float pitch = Float.parseFloat(loc[5]);
            location = new PlainLocation(w, x, y, z, yaw, pitch);
        } catch (NullPointerException e) {
        }
        return location;
    }

    static String pastePlainLocation(PlainLocation loc) {
        return loc.getWorld() + "," + loc.getX() + "," + loc.getY() + "," + loc.getZ() + "," + loc.getYaw() + "," + loc.getPitch();
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    class PlainLocation {

        private String world;
        private double x, y, z;
        private float yaw, pitch;

    }
}
