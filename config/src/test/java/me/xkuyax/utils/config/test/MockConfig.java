package me.xkuyax.utils.config.test;

import me.xkuyax.utils.config.Config;
import me.xkuyax.utils.config.InvalidConfigurationException;
import me.xkuyax.utils.config.file.YamlConfiguration;

import java.io.File;

public class MockConfig extends Config {

    public MockConfig(YamlConfiguration configuration) {
        super(new File("mock.yml"), configuration);
    }

    public static MockConfig newConfig() {
        YamlConfiguration configuration = new YamlConfiguration();
        try {
            configuration.loadFromString("{}");
            return new MockConfig(configuration);
        } catch (InvalidConfigurationException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean save() {
        return true;
    }
}
