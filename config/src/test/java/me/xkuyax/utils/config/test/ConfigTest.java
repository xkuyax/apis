package me.xkuyax.utils.config.test;

import lombok.Data;
import me.xkuyax.utils.config.Config;
import me.xkuyax.utils.config.ConfigGenericResolver;
import me.xkuyax.utils.config.additions.ConfigReference;
import me.xkuyax.utils.config.additions.DynamicCEntry;
import me.xkuyax.utils.config.additions.LanguageCEntry;
import org.junit.Test;

import java.util.*;

public class ConfigTest {

    @Test
    public void genericObjectTest() {
        // MockConfig config = MockConfig.newConfig();
        Config config = new Config("target/test.yml");
        LanguageTest test = config.getGenericType("Test", LanguageTest.class);
        String s = test.getStringLanguageItem().get(Locale.GERMANY);
        List<String> strings = test.getListLanguageItem().get(Locale.ENGLISH);
        System.out.println("s = " + s);
        System.out.println("strings = " + strings);
    }

    @Test
    public void configFormat2Test() {
        Config config = new Config("target/test.yml");
        GeneralTestObject test = config.getGenericType("Format2Test", GeneralTestObject.class);
        Map<SimpleObject, SimpleObject> blub = test.getMapDynamicCEntry().get("Blub");
        Map<SimpleObject, SimpleObject> map = test.getMapLanguageCEntry().get(Locale.GERMANY);
        System.out.println(test.getBla());
        config.setGenericType("SetTest", test, GeneralTestObject.class);
        SimpleObject object = test.getReference().get();
        System.out.println(object);
    }

    @Test
    public void test() {
        Config config = new Config("target/test2.yml");
        ConfigGenericResolver<GeneralTestObject> resolver = new ConfigGenericResolver<>(GeneralTestObject.class, "Root", config, "test", 2);
        try {
            resolver.resolve();
        } catch (Exception e) {
            e.printStackTrace();
        }
        config.save();
    }

    @Data
    public static class GeneralTestObject {

        private List<String> stringList;
        private List<Integer> integerList;
        private List<Float> floatList;
        private List<Double> doubleList;
        private List<SimpleObject> objectList;
        private List<SimpleObject> objectList2;
        private Set<String> stringSet;
        private Set<Integer> integerSet;
        private Set<Float> floatSet;
        private Set<Double> doubleSet;
        private Set<SimpleObject> objectSet;
        private Map<SimpleObject, SimpleObject> simpleObjectSimpleObjectMap;
        private Map<String, SimpleObject> stringSimpleObjectMap;
        private Map<Integer, SimpleObject> integerSimpleObjectMap;
        private Map<String, String> message = new HashMap<String, String>() {
            {
                put("test", "test");
                put("test1", "test");
                put("test2", "test");
                put("test3", "test");
            }
        };
        private DynamicCEntry<String> dynamicDefault = DynamicCEntry.createWithDefault("a", "b", "c", "d", "e");
        private DynamicCEntry<String> dynamicRequired = DynamicCEntry.createWithRequired("a", "b", "c", "d", "e");
        private DynamicCEntry<String> stringDynamicCEntry;
        private DynamicCEntry<Map<SimpleObject, SimpleObject>> mapDynamicCEntry;
        private LanguageCEntry<String> stringLanguageCEntry;
        private LanguageCEntry<Map<SimpleObject, SimpleObject>> mapLanguageCEntry;
        private Bla<String> bla = new Bla<>("defaultValue");
        private ConfigReference<SimpleObject> reference = ConfigReference.withDefault("Items.bla");
        private ConfigReference<SimpleObject> reference2;

    }

    @Data
    public static class Bla<T> {

        private final String name;

    }

    @Data
    public static class SimpleObject {

        private String playerName = "xkuyax";
        private int age = 20;

    }

    @Data
    public static class LanguageTest {

        private LanguageCEntry<String> stringLanguageItem;
        private LanguageCEntry<List<String>> listLanguageItem;

    }
}
