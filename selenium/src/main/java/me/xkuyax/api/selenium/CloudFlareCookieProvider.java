package me.xkuyax.api.selenium;

import com.google.common.base.Function;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.impl.cookie.BasicClientCookie2;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriver.Options;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.lang.reflect.Type;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Set;
import java.util.function.Predicate;

@RequiredArgsConstructor
public class CloudFlareCookieProvider {

    private static final Type COOKIE_TYPE = new TypeToken<ArrayList<BasicClientCookie2>>() {}.getType();
    private final String geckoDriverPath;
    private final Function<WebDriver, Boolean> webDriverWait;
    private final boolean verifyWeb;
    private final String url;
    @Getter
    private CookieStore cookieStore = new BasicCookieStore();
    private FirefoxDriver webDriver;
    private Path cookieCacheFilePath = Paths.get("cookies.txt");

    public RemoteWebDriver load() {
        System.getProperties().put("webdriver.gecko.driver", geckoDriverPath);
        webDriver = new FirefoxDriver();
        webDriver.get(url);
        return webDriver;
    }

    public void setCookies(Predicate<CookieStore> test) throws IOException {
        if (!loadExisting(test)) {
            loadWebDriver();
        }
    }

    //cookie cache
    public boolean loadExisting(Predicate<CookieStore> test) throws IOException {
        if (Files.exists(cookieCacheFilePath)) {
            ArrayList<BasicClientCookie2> cookies = new Gson().fromJson(new String(Files.readAllBytes(cookieCacheFilePath)), COOKIE_TYPE);
            cookies.forEach(cookie -> this.cookieStore.addCookie(cookie));
            if (verifyWeb) {
                return test.test(cookieStore);
            } else {
                System.out.println("Skipping cookie verification");
                return true;
            }
        }
        return false;
    }

    public void loadWebDriver() throws IOException {
        System.out.println("trying to fuck cloud flare");
        Wait<WebDriver> wait = new WebDriverWait(webDriver, 30);
        wait.until(webDriverWait);
        Options options = webDriver.manage();
        Set<Cookie> cookieSet = options.getCookies();
        cookieSet.forEach(cookie -> {
            System.out.println(cookie.getName() + " " + cookie.getValue());
            BasicClientCookie basicClientCookie = new BasicClientCookie(cookie.getName(), cookie.getValue());
            basicClientCookie.setExpiryDate(cookie.getExpiry());
            basicClientCookie.setDomain(cookie.getDomain());
            basicClientCookie.setPath(cookie.getPath());
            this.cookieStore.addCookie(basicClientCookie);
        });
        //cookie cache
        Files.write(cookieCacheFilePath, new Gson().toJson(cookieStore.getCookies(), COOKIE_TYPE).getBytes());
        System.out.println("Got cookies and successfully fucked cloudflare :)");
        try {
            webDriver.quit();
        } catch (Throwable ignore) {
        }
        System.out.println("Closed firefox");
    }

    @RequiredArgsConstructor
    public static class HttpCodeTester implements Predicate<CookieStore> {

        private final CloseableHttpClient httpClient;
        private final String url;
        private final int httpCode;

        @Override
        public boolean test(CookieStore cookieStore) {
            try (CloseableHttpResponse httpResponse = httpClient.execute(new HttpGet(url))) {
                if (httpResponse.getStatusLine().getStatusCode() == httpCode) {
                    System.out.println("Using cached cookies");
                    return true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }
    }
}
