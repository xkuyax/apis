package me.xkuyax.api.selenium;

import lombok.RequiredArgsConstructor;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@RequiredArgsConstructor
public class UserAgentCache {

    private final Path userAgentPath;
    public String userAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:50.0) Gecko/20100101 Firefox/50.0";

    public UserAgentCache() {
        userAgentPath = Paths.get("useragent.txt");
    }

    public String load(RemoteWebDriver webDriver) throws IOException {
        return loadExistingUserAgent() ? userAgent : loadFromWebDriver(webDriver) ? userAgent : "";
    }

    public boolean loadExistingUserAgent() throws IOException {
        Path userAgentPath = Paths.get("useragent.txt");
        if (Files.exists(userAgentPath)) {
            userAgent = new String(Files.readAllBytes(userAgentPath));
            System.out.println("Loaded user agent " + userAgent + " from cache");
            return true;
        }
        return false;
    }

    public boolean loadFromWebDriver(RemoteWebDriver webDriver) throws IOException {
        userAgent = (String) webDriver.executeScript("return navigator.userAgent", "");
        Files.write(userAgentPath, userAgent.getBytes());
        return true;
    }
}
