package me.xkuyax.utils.log;

import ch.qos.logback.core.Appender;
import ch.qos.logback.core.ConsoleAppender;
import ch.qos.logback.core.Context;
import ch.qos.logback.core.UnsynchronizedAppenderBase;
import ch.qos.logback.core.encoder.Encoder;
import lombok.Getter;
import lombok.Setter;
import org.jline.reader.LineReader;

@Getter
@Setter
public class JLine3Appender<E> extends UnsynchronizedAppenderBase<E> implements Appender<E> {

    @Setter
    public static LineReader reader;
    protected Encoder<E> encoder;
    private ConsoleAppender<E> defaultAppender;

    public JLine3Appender() {
        this.defaultAppender = new ConsoleAppender<>();
    }

    @Override
    public void setContext(Context context) {
        super.setContext(context);
        defaultAppender.setContext(context);
    }

    public void setEncoder(Encoder<E> encoder) {
        this.encoder = encoder;
        defaultAppender.setEncoder(encoder);
    }

    @Override
    public void start() {
        super.start();
        defaultAppender.start();
    }

    @Override
    protected void append(E e) {
        if (reader == null) {
            defaultAppender.doAppend(e);
            return;
        }
        byte[] encode = encoder.encode(e);
        String message = new String(encode);
        reader.printAbove(message);
    }
}
