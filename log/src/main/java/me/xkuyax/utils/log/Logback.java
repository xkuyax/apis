package me.xkuyax.utils.log;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.Appender;
import ch.qos.logback.core.Context;
import ch.qos.logback.core.FileAppender;
import ch.qos.logback.core.rolling.FixedWindowRollingPolicy;
import ch.qos.logback.core.rolling.RollingFileAppender;
import ch.qos.logback.core.rolling.SizeBasedTriggeringPolicy;
import ch.qos.logback.core.util.FileSize;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.Objects;

@Slf4j
public class Logback {

    public static String BASE_PATH = "logs";

    public static void createLatestFolder() {
        LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
        String logDirectory = context.getProperty("LOGDIR");
        log.info("logDirectory = " + logDirectory);
        if (logDirectory != null && !logDirectory.isEmpty()) {
            Path path = Paths.get(logDirectory);
            try {
                Path latestFolder = Paths.get(logDirectory, "latest");
                Files.deleteIfExists(latestFolder);
                Files.list(path).max(Comparator.comparing(t -> {
                    try {
                        return Files.getLastModifiedTime(t).toMillis();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    return 0L;
                })).ifPresent(latest -> {
                    try {
                        log.info("Creating link " + latestFolder + " -> " + latest);
                        Files.createSymbolicLink(latestFolder, latest.getFileName());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Deprecated
    public static Logger createLoggerFor(String name, boolean console) {
        return createLoggerFor(name, name + ".log", console);
    }

    @Deprecated
    public static Logger createLoggerFor(String string, String file, boolean console) {
        RollingFileAppender<ILoggingEvent> fileAppender = createAppender(file);
        Logger logger = (Logger) LoggerFactory.getLogger(string);
        setLoggerFields(console, fileAppender, logger);
        return logger;
    }

    @Deprecated
    public static Logger createLoggerFor(Class<?> clazz, String file, boolean console) {
        RollingFileAppender<ILoggingEvent> fileAppender = createAppender(file);
        Logger logger = (Logger) LoggerFactory.getLogger(clazz);
        setLoggerFields(console, fileAppender, logger);
        return logger;
    }

    private static void setLoggerFields(boolean console, RollingFileAppender<ILoggingEvent> fileAppender, Logger logger) {
        logger.addAppender(fileAppender);
        logger.setLevel(Level.INFO);
        logger.setAdditive(console); /* set to true if root should log too */
    }

    private static RollingFileAppender<ILoggingEvent> createAppender(String file) {
        Objects.requireNonNull(file, "File cannot be null!");
        LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
        String filePattern = getRootLoggerPattern();
        String basePath = BASE_PATH;
        basePath = basePath == null ? "" : basePath + File.separator;
        PatternLayoutEncoder ple = createPattern(context, filePattern);
        SizeBasedTriggeringPolicy<ILoggingEvent> triggeringPolicy = createTriggerPolicy(context);
        RollingFileAppender<ILoggingEvent> fileAppender = new RollingFileAppender<>();
        fileAppender.setContext(context);
        fileAppender.setTriggeringPolicy(triggeringPolicy);
        fileAppender.setFile(basePath + file);
        fileAppender.setEncoder(ple);
        fileAppender.setImmediateFlush(true);
        FixedWindowRollingPolicy windowRollingPolicy = createRollingPolicy(file, fileAppender, context);
        fileAppender.setRollingPolicy(windowRollingPolicy);
        fileAppender.start();
        return fileAppender;
    }

    @Deprecated
    public static String getRootLoggerPattern() {
        LoggerContext context = (LoggerContext) LoggerFactory.getILoggerFactory();
        try {
            Logger root = context.getLogger("ROOT");
            Appender<ILoggingEvent> appender = root.getAppender("FILE");
            if (!(appender instanceof FileAppender)) {
                return "";
            }
            FileAppender<?> fileAppender = (FileAppender<?>) appender;
            PatternLayoutEncoder layoutEncoder = (PatternLayoutEncoder) fileAppender.getEncoder();
            return layoutEncoder.getPattern();
        } catch (Exception ignored) {
        }
        return "";
    }

    @Deprecated
    public static PatternLayoutEncoder createPattern(LoggerContext lc, String filePattern) {
        PatternLayoutEncoder ple = new PatternLayoutEncoder();
        if (filePattern != null && !filePattern.isEmpty()) {
            ple.setPattern(filePattern);
        }
        ple.setContext(lc);
        ple.start();
        return ple;
    }

    private static FixedWindowRollingPolicy createRollingPolicy(String file, FileAppender parent, Context lc) {
        FixedWindowRollingPolicy windowRollingPolicy = new FixedWindowRollingPolicy();
        windowRollingPolicy.setFileNamePattern(BASE_PATH + File.separator + file + ".%i");
        windowRollingPolicy.setMaxIndex(20);
        windowRollingPolicy.setContext(lc);
        windowRollingPolicy.setParent(parent);
        windowRollingPolicy.start();
        return windowRollingPolicy;
    }

    private static SizeBasedTriggeringPolicy<ILoggingEvent> createTriggerPolicy(Context context) {
        SizeBasedTriggeringPolicy<ILoggingEvent> triggeringPolicy = new SizeBasedTriggeringPolicy<>();
        triggeringPolicy.setMaxFileSize(FileSize.valueOf("2MB"));
        triggeringPolicy.setContext(context);
        triggeringPolicy.start();
        return triggeringPolicy;
    }

    public static org.slf4j.Logger removeAppender(org.slf4j.Logger logger) {
        return removeAppender(logger, "CONSOLE");
    }

    public static org.slf4j.Logger getFilteredLogger(String name) {
        return removeAppender(LoggerFactory.getLogger(name));
    }

    public static org.slf4j.Logger getFilteredLogger(Class<?> clazz) {
        return removeAppender(LoggerFactory.getLogger(clazz));
    }

    public static org.slf4j.Logger removeAppender(org.slf4j.Logger logger, String appender) {
        if (logger instanceof Logger) {
            Logger logbackLogger = (Logger) logger;
            Logger rootLogger = (Logger) LoggerFactory.getLogger("ROOT");
            rootLogger.iteratorForAppenders().forEachRemaining(logbackLogger::addAppender);
            logbackLogger.setAdditive(false);
            logbackLogger.detachAppender(appender);
        }
        return logger;
    }
}
