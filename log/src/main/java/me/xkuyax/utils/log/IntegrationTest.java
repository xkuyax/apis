package me.xkuyax.utils.log;

import ch.qos.logback.classic.Logger;
import org.jline.reader.LineReader;
import org.jline.reader.LineReaderBuilder;
import org.jline.terminal.Terminal;
import org.jline.terminal.TerminalBuilder;
import org.slf4j.LoggerFactory;
import uk.org.lidalia.sysoutslf4j.context.SysOutOverSLF4J;

import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class IntegrationTest {

    public static void main(String[] args) throws IOException {
        SysOutOverSLF4J.sendSystemOutAndErrToSLF4J();
        Logger logger = Logback.createLoggerFor("test-logger", true);
        logger.info("hello");
        LoggerFactory.getILoggerFactory().getLogger(Logger.ROOT_LOGGER_NAME).info("root logger");
        Terminal terminal = TerminalBuilder.builder().build();
        LineReader lineReader = LineReaderBuilder.builder().terminal(terminal).build();
        JLine3Appender.reader = lineReader;
        System.out.println("from sysout");
        String line;
        Executors.newScheduledThreadPool(1).scheduleAtFixedRate(() -> {
            System.out.println("hello timer");
            logger.info("timer");
        }, 1, 1, TimeUnit.SECONDS);
        while ((line = lineReader.readLine("> ")) != null) {
            System.out.println("Got line over sysout: " + line);
        }
    }
}
