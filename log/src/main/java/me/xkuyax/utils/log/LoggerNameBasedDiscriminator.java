package me.xkuyax.utils.log;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.sift.Discriminator;

import java.io.File;

public class LoggerNameBasedDiscriminator implements Discriminator<ILoggingEvent> {

    private static final String KEY = "loggerName";

    private boolean started;

    @Override
    public String getDiscriminatingValue(ILoggingEvent iLoggingEvent) {
        String loggerName = iLoggingEvent.getLoggerName();
        loggerName = loggerName.replace(File.pathSeparator, "_");
        loggerName = loggerName.replace("/", "_");
        loggerName = loggerName.replace("\\", "_");
        return loggerName;
    }

    @Override
    public String getKey() {
        return KEY;
    }

    public void start() {
        started = true;
    }

    public void stop() {
        started = false;
    }

    public boolean isStarted() {
        return started;
    }
}
