package me.xkuyax.utils.log;

import ch.qos.logback.classic.Logger;
import org.junit.Test;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class LogbackTest {
    
    @Test
    public void test() throws IOException {
        Logger logger = Logback.createLoggerFor("test-logger", true);
        logger.info("hello");
        LoggerFactory.getILoggerFactory().getLogger(Logger.ROOT_LOGGER_NAME).info("root logger");
    }
    
}