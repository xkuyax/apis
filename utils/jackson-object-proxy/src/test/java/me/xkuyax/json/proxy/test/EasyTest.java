package me.xkuyax.json.proxy.test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import me.xkuyax.json.proxy.ChangeableJson;
import org.junit.Test;

public class EasyTest {

    @Test
    public void test() {
        ObjectMapper jsonMapper = new ObjectMapper();
        ChangeableJson<SimpleData> changeableJson = ChangeableJson.create(jsonMapper, SimpleData.class);
        changeableJson.update("{\"name\":\"xkuyax\",\"age\":15}");
        SimpleData simpleData = changeableJson.getObject();
        System.out.println(simpleData.getAge() + " " + simpleData.getName());
        System.out.println(simpleData);
        JsonNode jsonNode = changeableJson.getJsonNode().deepCopy();
        System.out.println(jsonNode);
        changeableJson.modify(simpleData1 -> {
            simpleData1.setAge(simpleData1.getAge() + 25);
        });
        System.out.println(changeableJson.getJsonNode());
        System.out.println(simpleData);
    }

    public interface SimpleData {

        String getName();

        void setName(String name);

        int getAge();

        void setAge(int age);

    }
}
