package me.xkuyax.json.proxy.test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import me.xkuyax.json.proxy.ChangeableJson;
import me.xkuyax.json.proxy.test.model.BaseUser;
import me.xkuyax.json.proxy.test.model.User;
import org.junit.Test;

public class UserMapTest {

    @Test
    public void test() throws JsonProcessingException {
        ObjectMapper jsonMapper = new ObjectMapper();
        BaseUser craftUser = ModelCreator.createCraftUser();
        String json = jsonMapper.writeValueAsString(craftUser);
        ChangeableJson<User> changeableJson = ChangeableJson.create(jsonMapper, User.class);
        changeableJson.setWriting(true);
        changeableJson.update(json);
        User object = changeableJson.getObject();
        System.out.println(object);
        System.out.println(object.getKeys().remove("test"));
        System.out.println(object.getLevels().get(0).remove(8));
        System.out.println(object);
    }
}
