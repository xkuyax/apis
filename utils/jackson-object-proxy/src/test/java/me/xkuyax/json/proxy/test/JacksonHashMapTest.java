package me.xkuyax.json.proxy.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import me.xkuyax.json.proxy.ChangeableJson;
import me.xkuyax.json.proxy.JacksonHashMap;
import me.xkuyax.json.proxy.test.model.Level;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class JacksonHashMapTest {

    @Test
    public void test() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode jsonNode = (ObjectNode) objectMapper.readTree("{\"key\":\"value\",\"otherKey\":\"otherValue\"}");
        ChangeableJson<?> changeableJson = ChangeableJson.create(objectMapper, String.class);
        changeableJson.setWriting(true);
        JacksonHashMap<String> hashMap = new JacksonHashMap<>(jsonNode, changeableJson, String.class, String.class);
        assertEquals(hashMap.toString(), "{key=value, otherKey=otherValue}");
        assertEquals(hashMap.get("key"), "value");
        assertTrue(hashMap.containsKey("key"));
        assertTrue(hashMap.containsValue("value"));
        assertEquals(hashMap.remove("key"), "value");
        assertEquals(1, hashMap.size());
    }

    @Test
    public void objectTest() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectNode jsonNode = (ObjectNode) objectMapper.readTree("{\"a\":{\"level\":1},\"b\":{\"level\":2}}");
        ChangeableJson<?> changeableJson = ChangeableJson.create(objectMapper, String.class);
        JacksonHashMap<Level> hashMap = new JacksonHashMap<>(jsonNode, changeableJson, Level.class, Level.class);
        Level a = hashMap.get("a");
        System.out.println(a);
        Level b = hashMap.get("b");
        System.out.println(b);
        Level c = hashMap.get("c");
        System.out.println(c);
    }

}
