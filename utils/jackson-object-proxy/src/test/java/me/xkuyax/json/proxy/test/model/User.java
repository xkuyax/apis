package me.xkuyax.json.proxy.test.model;

import java.util.List;
import java.util.Map;

public interface User {

    String getName();

    void setName(String name);

    int getAge();

    void setAge(int age);

    Player getPlayer();

    List<List<Level>> getLevels();

    Map<String, Integer> getKeys();

    List<Level> getSimpleLevels();
}
