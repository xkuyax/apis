package me.xkuyax.json.proxy.test.model;

public interface Level {

    int getLevel();

    void setLevel(int level);

}
