package me.xkuyax.json.proxy.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import me.xkuyax.json.proxy.ChangeableJson;
import me.xkuyax.json.proxy.test.model.BaseUser;
import me.xkuyax.json.proxy.test.model.Level;
import me.xkuyax.json.proxy.test.model.User;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class BaseTest {

    //    @Test
    public void test() throws IOException {
        ObjectMapper jsonMapper = new ObjectMapper();
        BaseUser craftUser = ModelCreator.createCraftUser();
        String json = jsonMapper.writeValueAsString(craftUser);
        ChangeableJson<User> changeableJson = ChangeableJson.create(jsonMapper, User.class);
        changeableJson.update(json);
        User user = changeableJson.getObject();
        assertEquals(user.getAge(), 20);
        assertEquals(user.getName(), "xkuyax");
        assertEquals(1, user.getLevels().size());
        assertEquals(9, user.getLevels().get(0).size());
        assertEquals(1, user.getKeys().size());
        changeableJson.modify(user1 -> user1.getLevels().get(0).get(0).setLevel(999));
        assertEquals(changeableJson.getJsonNode().get("levels").get(0).get(0).get("level").asInt(), 999);
        String update = "{\"name\":\"update\",\"age\":15}";
        changeableJson.update(update);
        assertEquals(user.getAge(), 15);
        assertEquals(user.getName(), "update");
        changeableJson.modify(user1 -> user1.setAge(20));
        assertEquals(user.getAge(), 20);
        assertEquals(changeableJson.getJsonNode().get("age").asInt(), 20);
        changeableJson.applyPatch(jsonMapper.readTree("[{\"op\":\"replace\",\"path\":\"/age\",\"value\":25}]"));
        assertEquals(user.getAge(), 25);
        assertEquals(changeableJson.getJsonNode().get("age").asInt(), 25);
        user.getKeys().put("adolf", 5);
        Level level = changeableJson.newFloatingInstance(Level.class);
        level.setLevel(10);
        user.getSimpleLevels().add(level);
        System.out.println("remove:" + user);
        user.getSimpleLevels().remove(level);
        User adolf = changeableJson.newProperty("adolf", User.class);
        adolf.setAge(100);
        System.out.println("removed: " + user);
    }
}
