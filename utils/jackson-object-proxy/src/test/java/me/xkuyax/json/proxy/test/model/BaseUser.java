package me.xkuyax.json.proxy.test.model;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class BaseUser implements User {

    private String name;
    private int age;
    private Player player;
    private List<List<Level>> levels;
    private Map<String, Integer> keys;
    private List<Level> simpleLevels;

}
