package me.xkuyax.json.proxy.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import me.xkuyax.json.proxy.ChangeableJson;
import me.xkuyax.json.proxy.test.model.BaseLevel;
import me.xkuyax.json.proxy.test.model.BaseUser;
import me.xkuyax.json.proxy.test.model.Level;
import me.xkuyax.json.proxy.test.model.User;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ModelCreator {

    public static void main(String[] args) throws Exception {
        ///String json = "{\"name\":\"xkuyax\",\"blub\":{\"blab\":{\"level\":15},\"name\":\"Hans\"},\"age\":1,\"blabs\":[[{\"level\":1},{\"level\":2},{\"level\":3},{\"level\":4},{\"level\":5}]]}";
        String update = "{\"name\":\"update\",\"blub\":{\"blab\":{\"level\":30},\"name\":\"Wurst\"},\"age\":15}";
        ObjectMapper jsonMapper = new ObjectMapper();
        BaseUser craftUser = createCraftUser();
        String json = jsonMapper.writeValueAsString(craftUser);
        System.out.println(json);
        ChangeableJson<User> changeableJson = ChangeableJson.create(jsonMapper, User.class);
        changeableJson.update(json);
        User user = changeableJson.getObject();
        /*changeableJson.update(update);
        printUser(user);
        try {
            user.setAge(5);
        } catch (IllegalArgumentException expected) {
            System.out.println("IllegalArgumentException from setAge works!");
        }
        changeableJson.modify(user1 -> {
            user1.setAge(5);
            user1.getPlayer().setName("test123");
        });
        System.out.println(user.getAge());
        System.out.println(user.getPlayer().getName());
        System.out.println(changeableJson.getJsonNode());
        System.out.println(user);
        System.out.println(changeableJson.getObject().equals(changeableJson.deepCopy().getObject()));*/
        System.out.println(user.getLevels());
        System.out.println(user.getAge());
        System.out.println(user.getName());
    }

    public static BaseUser createCraftUser() {
        BaseUser craftUser = new BaseUser();
        craftUser.setAge(20);
        craftUser.setName("xkuyax");
        ArrayList<List<Level>> levels = new ArrayList<>();
        levels.add(new ArrayList<>(BaseLevel.from(1, 2, 3, 4, 5, 6, 7, 8, 9)));
        craftUser.setLevels(levels);
        craftUser.setKeys(new HashMap<>());
        craftUser.getKeys().put("test", 25);
        return craftUser;
    }
}
