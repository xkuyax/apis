package me.xkuyax.json.proxy.test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import me.xkuyax.json.proxy.ChangeableJson;
import me.xkuyax.json.proxy.JacksonArrayList;
import me.xkuyax.json.proxy.test.model.BaseLevel;
import me.xkuyax.json.proxy.test.model.Level;
import me.xkuyax.json.proxy.test.model.User;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class JacksonArrayListTest {

    @Test
    public void stringTest() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readTree("{\"array\":[\"1\",\"2\",\"3\"],\"name\":\"xkuyax\"}");
        ArrayNode arrayNode = (ArrayNode) jsonNode.get("array");
        ChangeableJson<?> changeableJson = ChangeableJson.create(objectMapper, User.class);
        changeableJson.setWriting(true);
        JacksonArrayList<String> arrayList = new JacksonArrayList<>(arrayNode, changeableJson, String.class, String.class);
        assertEquals(arrayList.get(0), "1");
        assertEquals(arrayList.get(1), "2");
        assertEquals(arrayList.get(2), "3");
        arrayList.add("4");
        arrayList.remove(4);
        assertEquals(arrayList.size(), 4);
        arrayList.remove("4");
        assertEquals(arrayList.size(), 3);
        arrayList.remove("3");
        assertEquals(arrayList.size(), arrayNode.size());
        assertEquals(arrayNode.size(), 2);
    }

    @Test
    public void intTest() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode jsonNode = objectMapper.readTree("{\"array\":[1,2,3],\"name\":\"xkuyax\"}");
        ArrayNode arrayNode = (ArrayNode) jsonNode.get("array");
        ChangeableJson<?> changeableJson = ChangeableJson.create(objectMapper, User.class);
        changeableJson.setWriting(true);
        JacksonArrayList<Integer> arrayList = new JacksonArrayList<>(arrayNode, changeableJson, Integer.class, Integer.class);
        assertEquals(1, (int) arrayList.get(0));
        assertEquals(2, (int) arrayList.get(1));
        assertEquals(3, (int) arrayList.get(2));
        arrayList.add(4);
    }

    @Test
    public void objectTest() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        ArrayNode arrayNode = (ArrayNode) objectMapper.readTree("[{\"level\":1},{\"level\":2}]");
        ChangeableJson<?> changeableJson = ChangeableJson.create(objectMapper, User.class);
        changeableJson.setWriting(true);
        JacksonArrayList<Level> arrayList = new JacksonArrayList<>(arrayNode, changeableJson, Level.class, Level.class);
        assertEquals(arrayList.get(0).getLevel(), 1);
        assertEquals(arrayList.get(1).getLevel(), 2);
        arrayList.add(new BaseLevel(3));
    }
}
