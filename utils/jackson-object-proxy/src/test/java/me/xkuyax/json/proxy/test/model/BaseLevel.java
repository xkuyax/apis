package me.xkuyax.json.proxy.test.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BaseLevel implements Level {

    private int level;

    public static List<BaseLevel> from(int... ids) {
        return Arrays.stream(ids).mapToObj(BaseLevel::new).collect(Collectors.toList());
    }
}
