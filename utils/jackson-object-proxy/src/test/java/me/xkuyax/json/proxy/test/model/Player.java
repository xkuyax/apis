package me.xkuyax.json.proxy.test.model;

public interface Player {

    String getName();

    void setName(String name);

    Level getLevel();

}
