package me.xkuyax.json.proxy;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.Getter;
import lombok.Setter;

import java.lang.reflect.Type;
import java.util.AbstractMap.SimpleEntry;
import java.util.*;

@Getter
@Setter
public class JacksonHashMap<V> extends JacksonCollection<V> implements Map<String, V> {

    private final ObjectNode objectNode;

    public JacksonHashMap(ObjectNode objectNode, ChangeableJson changeableJson, Type clazz, Type genericType) {
        super(changeableJson, clazz, genericType);
        this.objectNode = objectNode;
    }

    @Override
    public int size() {
        return objectNode.size();
    }

    @Override
    public boolean isEmpty() {
        return objectNode.isEmpty(null);
    }

    @Override
    public boolean containsKey(Object key) {
        if (key instanceof String) {
            return objectNode.has((String) key);
        }
        return false;
    }

    @Override
    public boolean containsValue(Object value) {
        JsonNode jsonNode = null;
        if (value instanceof JsonNode) {
            jsonNode = (JsonNode) value;
        } else {
            jsonNode = convertElement(value);
        }
        if (jsonNode != null) {
            for (Iterator<JsonNode> it = objectNode.elements(); it.hasNext(); ) {
                JsonNode other = it.next();
                if (other.equals(jsonNode)) {
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public V get(Object key) {
        if (key instanceof String) {
            return convertJson(objectNode.get((String) key));
        }
        return null;
    }

    @Override
    public V put(String key, V value) {
        checkWriteAccess();
        JsonNode jsonNode = convertElement(value);
        V v = get(key);
        objectNode.set(key, jsonNode);
        return v;
    }

    @Override
    public V remove(Object key) {
        checkWriteAccess();
        if (key instanceof String) {
            JsonNode remove = objectNode.remove((String) key);
            return convertJson(remove);
        }
        return null;
    }

    @Override
    public void putAll(Map<? extends String, ? extends V> m) {
        m.forEach(this::put);
    }

    @Override
    public void clear() {
        objectNode.removeAll();
    }

    @Override
    public Set<String> keySet() {
        Iterator<String> iterator = objectNode.fieldNames();
        Set<String> keys = new HashSet<>();
        while (iterator.hasNext()) {
            keys.add(iterator.next());
        }
        return keys;
    }

    @Override
    public Collection<V> values() {
        Iterator<JsonNode> elements = objectNode.elements();
        List<V> list = new ArrayList<>();
        while (elements.hasNext()) {
            JsonNode next = elements.next();
            list.add(convertJson(next));
        }
        return list;
    }

    @Override
    public Set<Entry<String, V>> entrySet() {
        Iterator<Entry<String, JsonNode>> fields = objectNode.fields();
        Set<Entry<String, V>> entries = new HashSet<>();
        while (fields.hasNext()) {
            Entry<String, JsonNode> next = fields.next();
            entries.add(new SimpleEntry<>(next.getKey(), convertJson(next.getValue())));
        }
        return entries;
    }

    public String toString() {
        Iterator<Entry<String, V>> i = entrySet().iterator();
        if (!i.hasNext())
            return "{}";

        StringBuilder sb = new StringBuilder();
        sb.append('{');
        for (; ; ) {
            Entry<String, V> e = i.next();
            String key = e.getKey();
            V value = e.getValue();
            sb.append(key);
            sb.append('=');
            sb.append(value == this ? "(this Map)" : value);
            if (!i.hasNext())
                return sb.append('}').toString();
            sb.append(',').append(' ');
        }
    }

}
