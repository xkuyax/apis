package me.xkuyax.json.proxy;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import me.xkuyax.json.proxy.ChangeableJson.ResolveResponse;

import java.lang.reflect.Type;

@Data
public class JacksonCollection<E> {

    private final ChangeableJson changeableJson;
    private final Type clazz;
    private final Type genericType;
    private ObjectMapper objectMapper;

    public JacksonCollection(ChangeableJson changeableJson, Type clazz, Type genericType) {
        this.changeableJson = changeableJson;
        this.clazz = clazz;
        this.genericType = genericType;
        this.objectMapper = changeableJson.getObjectMapper();
    }

    public JsonNode convertElement(Object element) {
        return objectMapper.convertValue(element, JsonNode.class);
    }

    public E convertJson(JsonNode jsonNode) {
        ResolveResponse response = changeableJson.resolveValue(jsonNode, clazz, genericType);
        System.out.println(response);
        return (E) response.getObject();
    }

    public void checkWriteAccess() {
        if (!getChangeableJson().isWriting()) {
            throw new IllegalArgumentException("Currently not writing!");
        }
    }
}
