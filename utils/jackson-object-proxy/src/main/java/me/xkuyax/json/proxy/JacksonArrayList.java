package me.xkuyax.json.proxy;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import lombok.Getter;
import lombok.Setter;

import java.lang.reflect.Type;
import java.util.*;

@Getter
@Setter
public class JacksonArrayList<E> extends JacksonCollection<E> implements List<E> {

    private final ArrayNode arrayNode;

    public JacksonArrayList(ArrayNode arrayNode, ChangeableJson changeableJson, Type clazz, Type genericType) {
        super(changeableJson, clazz, genericType);
        this.arrayNode = arrayNode;
    }

    @Override
    public E get(int index) {
        JsonNode jsonNode = arrayNode.get(index);
        return convertJson(jsonNode);
    }

    @Override
    public boolean add(E e) {
        checkWriteAccess();
        JsonNode jsonNode = convertElement(e);
        arrayNode.add(jsonNode);
        return true;
    }


    @Override
    public E set(int index, E element) {
        checkWriteAccess();
        JsonNode jsonNode = convertElement(element);
        E e = get(index);
        arrayNode.set(index, jsonNode);
        return e;
    }

    @Override
    public void add(int index, E element) {
        throw new UnsupportedOperationException();
    }

    @Override
    public E remove(int index) {
        checkWriteAccess();
        JsonNode remove = arrayNode.remove(index);
        return convertJson(remove);
    }

    @Override
    public void clear() {
        checkWriteAccess();
        arrayNode.removeAll();
    }

    /*************************************************/
    @Override
    public int size() {
        return arrayNode.size();
    }

    @Override
    public boolean isEmpty() {
        return arrayNode.isEmpty(null);
    }

    @Override
    public boolean contains(Object o) {
        JsonNode jsonNode = convertElement(o);
        Iterator<JsonNode> elements = arrayNode.elements();
        while (elements.hasNext()) {
            JsonNode next = elements.next();
            if (jsonNode.equals(next)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator<E> iterator() {
        Iterator<JsonNode> iterator = arrayNode.iterator();
        return new Iterator<E>() {

            @Override
            public boolean hasNext() {
                return iterator.hasNext();
            }

            @Override
            public E next() {
                return convertJson(iterator.next());
            }
        };
    }

    @Override
    public Object[] toArray() {
        Iterator<JsonNode> iterator = arrayNode.iterator();
        Object[] array = new Object[arrayNode.size()];
        int i = 0;
        while (iterator.hasNext()) {
            array[i++] = convertJson(iterator.next());
        }
        return array;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        int size = arrayNode.size();
        if (a.length < size) {
            a = Arrays.copyOf(a, size);
        }
        Iterator<JsonNode> iterator = arrayNode.iterator();
        int i = 0;
        while (iterator.hasNext()) {
            a[i++] = (T) convertJson(iterator.next());
        }
        return a;
    }

    @Override
    public boolean remove(Object o) {
        arrayNode.remove(indexOf(o));
        return true;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return c.stream().allMatch(this::contains);
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        c.forEach(this::add);
        return false;
    }

    @Override
    public boolean addAll(int index, Collection<? extends E> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        c.forEach(this::remove);
        return false;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        throw new UnsupportedOperationException();
    }

    @Override
    public int indexOf(Object o) {
        JsonNode jsonNode = convertElement(o);
        int i = 0;
        for (JsonNode node : arrayNode) {
            if (jsonNode.equals(node)) {
                return i;
            }
            i++;
        }
        return -1;
    }

    @Override
    public int lastIndexOf(Object o) {
        throw new UnsupportedOperationException();
    }

    @Override
    public ListIterator<E> listIterator() {
        return listIterator(0);
    }

    @Override
    public ListIterator<E> listIterator(int index) {
        return new ListIterator<E>() {

            private int cursor = index;

            @Override
            public boolean hasNext() {
                return cursor < size();
            }

            @Override
            public E next() {
                E e = get(cursor);
                cursor++;
                return e;
            }

            @Override
            public boolean hasPrevious() {
                return cursor != 0;
            }

            @Override
            public E previous() {
                E e = get(cursor);
                cursor--;
                return e;
            }

            @Override
            public int nextIndex() {
                return cursor + 1;
            }

            @Override
            public int previousIndex() {
                return cursor - 1;
            }

            @Override
            public void remove() {
                JacksonArrayList.this.remove(cursor);
            }

            @Override
            public void set(E e) {
                JacksonArrayList.this.set(cursor, e);
            }

            @Override
            public void add(E e) {
                throw new UnsupportedOperationException();
            }
        };
    }

    @Override
    public List<E> subList(int fromIndex, int toIndex) {
        throw new UnsupportedOperationException();
    }

    @Override
    public String toString() {
        Iterator<E> it = iterator();
        if (!it.hasNext())
            return "[]";

        StringBuilder sb = new StringBuilder();
        sb.append('[');
        for (; ; ) {
            E e = it.next();
            sb.append(e == this ? "(this Collection)" : e);
            if (!it.hasNext())
                return sb.append(']').toString();
            sb.append(',').append(' ');
        }
    }
}
