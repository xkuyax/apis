package me.xkuyax.json.proxy;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.POJONode;
import com.flipkart.zjsonpatch.JsonDiff;
import com.flipkart.zjsonpatch.JsonPatch;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import lombok.extern.apachecommons.CommonsLog;
import me.xkuyax.utils.PrimitiveMappings;

import java.io.IOException;
import java.lang.reflect.*;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;
import java.util.function.Function;

@Data
@CommonsLog
@EqualsAndHashCode(of = "jsonNode")
public class ChangeableJson<T> implements InvocationHandler {

    private final ObjectMapper objectMapper;
    private final Class<T> clazz;
    private Map<String, Object> propertyCache = new HashMap<>();
    private JsonNode jsonNode;
    private T object;
    private boolean writing;
    private int timeOutTime = 1;

    public ChangeableJson(ObjectMapper objectMapper, Class<T> clazz) {
        this.objectMapper = objectMapper;
        this.clazz = clazz;
    }

    public <U> U newProperty(String property, Class<U> clazz) {
        if (jsonNode instanceof ObjectNode) {
            ObjectNode objectNode = new ObjectNode(objectMapper.getNodeFactory());
            U object = initNewObject(objectMapper, clazz, objectNode);
            ((ObjectNode) jsonNode).set(property, objectNode);
            return object;
        } else {
            throw new RuntimeException("Not a ObjectNode");
        }
    }

    public <U> U newFloatingInstance(Class<U> clazz) {
        return initNewObject(objectMapper, clazz, objectMapper.createObjectNode());
    }

    public static <U> U newEmptyObject(Class<U> clazz) {
        ObjectMapper objectMapper = new ObjectMapper();
        return initNewObject(objectMapper, clazz, objectMapper.createObjectNode());
    }

    private static <U> U initNewObject(ObjectMapper objectMapper, Class<U> clazz, ObjectNode objectNode) {
        ChangeableJson<U> copy = new ChangeableJson<>(objectMapper, clazz);
        //copy.setLock(new StampedLock());
        copy.setWriting(true);
        ResolveResponse response = copy.resolveValue(objectNode, clazz, clazz);
        return (U) response.getObject();
    }

    public void update(String update) {
        setJsonNode(readJsonNode(update));
    }

    public void modify(Consumer<T> consumer) {
        modifyCommit(t -> {
            consumer.accept(t);
            return true;
        });
    }

    public void modifyCommit(Function<T, Boolean> consumer) {
        try {
            startCommit();
            ChangeableJson<T> copy = deepCopy();
            copy.setWriting(true);
            boolean success = consumer.apply(copy.getObject());
            if (success) {
                createAndPatch(copy);
            }
        } finally {
            endCommit();
        }
    }

    public void endCommit() {
        //  lock.unlockWrite(writeLock);
    }

    public JsonNode createAndPatch(ChangeableJson<T> copy) {
        JsonNode patch = JsonDiff.asJson(jsonNode, copy.getJsonNode());
        applyPatchNonLocking(patch);
        return patch;
    }

    public void startCommit() {
        //writeLock = lock.writeLock();
    }

    public ChangeableJson<T> deepCopy() {
        ChangeableJson<T> copy = new ChangeableJson<>(objectMapper, clazz);
        //   copy.setLock(lock);
        copy.setJsonNode(jsonNode.deepCopy());
        return copy;
    }

    public void initProxy() {
        object = (T) Proxy.newProxyInstance(getClassLoader(), new Class[]{clazz, JsonProxyInterface.class}, this);
    }

    public void setJsonNode(JsonNode jsonNode) {
        //   writeLock= lock.writeLock();
        try {
            this.jsonNode = jsonNode;
            if (object == null) {
                initProxy();
            }
            this.propertyCache.clear();
        } finally {
            endCommit();
        }
    }

    public void applyPatch(JsonNode patch) {
        // lock.lock();
        try {
            applyPatchNonLocking(patch);
        } finally {
            endCommit();
        }
    }

    private void applyPatchNonLocking(JsonNode patch) {
        JsonNode apply = JsonPatch.apply(patch, jsonNode);
        setJsonNode(apply);
    }

    private JsonNode readJsonNode(String update) {
        try {
            return objectMapper.readTree(update);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        String property = getPropertyName(method);
        if (method.getName().startsWith("get") && method.getParameterTypes().length == 0) {
            return propertyCache.computeIfAbsent(property, s -> getProperty(jsonNode, s, method));
        }
        if (method.getName().startsWith("set") && method.getParameterTypes().length == 1) {
            return handleSetter(args, property);
        }
        if (method.getName().equals("toString") && method.getReturnType() == String.class) {
            return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonNode);
        }
        if (method.getName().equals("equals") && args.length == 1) {
            return handleEquals(args[0]);
        }
        if (method.getName().equals("hashCode") && args.length == 1) {
            return jsonNode.hashCode();
        }
        throw new IllegalArgumentException("Could not find property in json!");
    }

    private Object handleEquals(Object arg) {
        InvocationHandler handler = Proxy.getInvocationHandler(arg);
        if (handler instanceof ChangeableJson) {
            ChangeableJson changeableJson = (ChangeableJson) handler;
            return changeableJson.getJsonNode().equals(jsonNode)
                    && changeableJson.getClazz().equals(getClazz());
        }
        return false;
    }

    private Object handleSetter(Object[] args, String property) {
        if (writing) {
            if (jsonNode instanceof ObjectNode) {
                ObjectNode objectNode = (ObjectNode) jsonNode;
                JsonNode node = objectMapper.valueToTree(args[0]);
                objectNode.set(property, node);
                propertyCache.remove(property);
            } else {
                throw new IllegalArgumentException("You are trying to modify a no object node!");
            }
        } else {
            throw new IllegalArgumentException("Use modify and do not use direct setters!");
        }
        return null;
    }

    private String getPropertyName(Method method) {
        String property = method.getName().substring(3);
        return Character.toLowerCase(property.charAt(0)) + property.substring(1);
    }

    private Object getProperty(JsonNode data, String property, Method method) {
        JsonNode value = data.get(property);
        ResolveResponse response = resolveValue(value, method.getReturnType(), method.getGenericReturnType());
        if (response.getCreated() != null && data instanceof ObjectNode) {
            ObjectNode objectNode = (ObjectNode) data;
            objectNode.set(property, response.getCreated());
        }
        return response.getObject();
    }

    public ResolveResponse resolveValue(JsonNode value, Type type, Type generic) {
        //    Class<?> returnType = method.getReturnType();
        Class<?> returnType = type instanceof Class ? (Class<?>) type : type instanceof ParameterizedType ? (Class<?>) ((ParameterizedType) type).getRawType() : null;
        //Type genericReturnType = method.getGenericReturnType();
        if (Collection.class.isAssignableFrom(returnType)) {
            return handleCollectionCreation(value, generic, returnType);
        }
        if (Map.class.equals(returnType)) {
            return handleMapCreation(value, generic);
        }
        if (value == null || value.isNull()) {
            Object object = PrimitiveMappings.defaultValue(returnType);
            if (object != null) {
                return new ResolveResponse(object);
            }
            return new ResolveResponse(null, null);
        }
        if (Integer.class.equals(returnType) || int.class.equals(returnType)) {
            return new ResolveResponse(value.asInt());
        } else if (Double.class.equals(returnType) || double.class.equals(returnType)) {
            return new ResolveResponse(value.asDouble());
        } else if (Long.class.equals(returnType) || long.class.equals(returnType)) {
            return new ResolveResponse(value.asLong());
        } else if (Boolean.class.equals(returnType) || boolean.class.equals(returnType)) {
            return new ResolveResponse(value.asBoolean());
        } else if (String.class.equals(returnType)) {
            return new ResolveResponse(value.asText());
        } else if (returnType.isInterface()) {
            if (value.isObject()) {
                ChangeableJson<?> changeableJson = new ChangeableJson<>(objectMapper, returnType);
                // changeableJson.setLock(lock);
                changeableJson.setWriting(writing);
                changeableJson.setJsonNode(value);
                return new ResolveResponse(changeableJson.getObject());
            } else if (value.isPojo()) {
                return new ResolveResponse(((POJONode) value).getPojo());
            }
        }
        throw new IllegalArgumentException("Unknown return type: " + returnType);
    }

    private ResolveResponse handleMapCreation(JsonNode value, Type generic) {
        JsonNode created = null;
        if (value == null || value.isNull()) {
            value = created = new ObjectNode(objectMapper.getNodeFactory());
        }
        if (!value.isObject()) {
            throw new IllegalArgumentException("Wrong json, expected object and not " + value.getNodeType());
        }
        ObjectNode objectNode = (ObjectNode) value;
        if (generic instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) generic;
            Type[] typeArguments = parameterizedType.getActualTypeArguments();
            if (typeArguments.length > 1) {
                Map<String, Object> map = new JacksonHashMap<>(objectNode, this, typeArguments[1], typeArguments[1]);
                //Map<String, Object> map = new HashMap<>();
               /* objectNode.fields().forEachRemaining(stringJsonNodeEntry -> {
                    map.put(stringJsonNodeEntry.getKey(), resolveValue(stringJsonNodeEntry.getValue(), typeArguments[1], typeArguments[1]));
                });*/
                return new ResolveResponse(map, created);
            }
        }
        throw new IllegalArgumentException("Unknown Map Type! Supported: Map and please specify the generic type! Map<String,Integer> eg.");
    }

    private ResolveResponse handleCollectionCreation(JsonNode value, Type generic, Class<?> returnType) {
        JsonNode created = value == null || value.isNull() ? value = new ArrayNode(objectMapper.getNodeFactory()) : null;
        if (!value.isArray()) {
            throw new IllegalArgumentException("Wrong json, expected array and not " + value.getNodeType());
        }
        ArrayNode arrayNode = (ArrayNode) value;
        if (generic instanceof ParameterizedType) {
            ParameterizedType parameterizedType = (ParameterizedType) generic;
            Type[] typeArguments = parameterizedType.getActualTypeArguments();
            if (typeArguments.length > 0) {
                Collection<Object> collection = null;
                if (List.class.equals(returnType)) {
                    collection = new JacksonArrayList<>(arrayNode, this, typeArguments[0], typeArguments[0]);
                    //collection = new ArrayList<>();
                }
                if (collection != null) {
                    return new ResolveResponse(collection, created);
                }
            }
        }
        throw new IllegalArgumentException("Unknown Collection Type! Supported: List and please specify the generic type! List<String> eg.");
    }

    public static ClassLoader getClassLoader() {
        return Thread.currentThread().getContextClassLoader();
    }

    public static <T> ChangeableJson<T> create(ObjectMapper objectMapper, Class<T> clazz) {
        return create(objectMapper, clazz, new ReentrantLock());
    }

    public static <T> ChangeableJson<T> create(ObjectMapper objectMapper, Class<T> clazz, Lock lock) {
        ChangeableJson<T> changeableJson = new ChangeableJson<>(objectMapper, clazz);
        // changeableJson.setLock(lock);
        initJacksonModule(objectMapper);
        return changeableJson;
    }

    private static void initJacksonModule(ObjectMapper objectMapper) {
        SimpleModule module = new SimpleModule("ChangeableJson", new Version(2, 0, 0, null, null, null));
        module.addSerializer(new JsonSerializer<JsonProxyInterface>() {
            @Override
            public void serialize(JsonProxyInterface o, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
                if (Proxy.isProxyClass(o.getClass())) {
                    InvocationHandler invocationHandler = Proxy.getInvocationHandler(o);
                    if (invocationHandler instanceof ChangeableJson) {
                        ChangeableJson changeableJson = (ChangeableJson) invocationHandler;
                        JsonNode jsonNode = changeableJson.getJsonNode();
                        jsonGenerator.writeTree(jsonNode);
                    }
                }
            }

            @Override
            public Class<JsonProxyInterface> handledType() {
                return JsonProxyInterface.class;
            }
        });
        objectMapper.registerModule(module);
    }

    @Data
    @AllArgsConstructor
    @RequiredArgsConstructor
    public static class ResolveResponse {

        private final Object object;
        private JsonNode created;

    }
}
