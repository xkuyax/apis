package me.xkuyax.utils.objectstorage;

import org.junit.Assert;
import org.junit.Test;

public class ObjectStorageTest {

    @Test
    public void prefixNumber() {
        Assert.assertEquals(ObjectStorage.prefixNumber(1234, 3), "234");
        Assert.assertEquals(ObjectStorage.prefixNumber(123, 3), "123");
        Assert.assertEquals(ObjectStorage.prefixNumber(12, 3), "12");
        Assert.assertEquals(ObjectStorage.prefixNumber(1, 3), "1");
        Assert.assertEquals(ObjectStorage.prefixNumber(-1, 3), "1");
        Assert.assertEquals(ObjectStorage.prefixNumber(-1234, 3), "234");
    }

}