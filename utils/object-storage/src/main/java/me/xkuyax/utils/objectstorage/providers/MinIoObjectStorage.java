package me.xkuyax.utils.objectstorage.providers;

import io.minio.ErrorCode;
import io.minio.MinioClient;
import io.minio.PutObjectOptions;
import io.minio.errors.ErrorResponseException;
import io.minio.errors.InvalidEndpointException;
import io.minio.errors.InvalidPortException;
import lombok.extern.apachecommons.CommonsLog;
import me.xkuyax.utils.objectstorage.ObjectStorage;

import java.io.InputStream;
import java.nio.file.Path;

@CommonsLog
public class MinIoObjectStorage extends ObjectStorage {

    private MinioClient client;

    public MinIoObjectStorage(ObjectStorageConfigEntry configEntry) {
        super(configEntry);
        try {
            client = new MinioClient(configEntry.getUrl(), configEntry.getAccessKey(), configEntry.getAccessSecret());
        } catch (InvalidEndpointException | InvalidPortException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void store(String name, Path path) {
        try {
            client.putObject(getConfigEntry().getDefaultBucket(), name, path.toString(), null);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void download(String name, Path path) {
        try {
            client.getObject(getConfigEntry().getDefaultBucket(), name, path.toString());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public InputStream downloadStream(String name) {
        try {
            return client.getObject(getConfigEntry().getDefaultBucket(), name);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void store(String name, InputStream stream, long length) {
        try {
            client.putObject(getConfigEntry().getDefaultBucket(), name, stream, new PutObjectOptions(length, PutObjectOptions.MIN_MULTIPART_SIZE));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public int exists(String name) {
        try {
            client.statObject(getConfigEntry().getDefaultBucket(), name);
            return 1;
        } catch (Exception e) {
            if (e instanceof ErrorResponseException) {
                if (((ErrorResponseException) e).errorResponse().errorCode() == ErrorCode.NO_SUCH_KEY) {
                    return 0;
                }
            }
            return -1;
        }
    }

    @Override
    public void delete(String name) {
        try {
            client.removeObject(configEntry.getDefaultBucket(), name);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
