package me.xkuyax.utils.objectstorage;

import lombok.Data;
import lombok.Getter;
import me.xkuyax.utils.config.Config;

import java.io.InputStream;
import java.nio.file.Path;
import java.util.function.Function;

@Getter
public abstract class ObjectStorage {

    protected final ObjectStorageConfigEntry configEntry;

    protected ObjectStorage(ObjectStorageConfigEntry configEntry) {
        this.configEntry = configEntry;
    }

    public abstract void store(String name, Path path);

    public abstract void download(String name, Path path);

    public abstract InputStream downloadStream(String name);

    public abstract void store(String name, InputStream stream, long length);

    public abstract int exists(String name);

    public abstract void delete(String name);

    public boolean storeRetry(String name, Path path) {
        return retry(() -> store(name, path), configEntry.getRetries());
    }

    public boolean storeRetry(String name, InputStream stream, long length) {
        return retry(() -> store(name, stream, length), configEntry.getRetries());
    }

    public boolean downloadRetry(String name, Path path) {
        return retry(() -> download(name, path), configEntry.getRetries());
    }

    public boolean existsRetry(String name) {
        return existsRetry0(name, configEntry.getRetries());
    }

    private boolean existsRetry0(String name, int count) {
        int response = exists(name);
        //-1 exception from provider, retry
        if (response == -1) {
            if (count > 0) {
                return existsRetry0(name, count - 1);
            } else {
                return false;
            }
        }
        return response == 1;
    }

    public boolean retry(Runnable runnable, int count) {
        try {
            runnable.run();
            return true;
        } catch (Exception e) {
            if (count > 0) {
                return retry(runnable, count - 1);
            } else {
                e.printStackTrace();
            }
            return false;
        }
    }

    public static String numberWithPrefix(int id) {
        return numberWithPrefix(id, 3);
    }

    public static String numberWithPrefix(int id, int prefixLength) {
        return prefixNumber(id, prefixLength) + "/" + id;
    }

    public static String prefixNumber(int id, int prefixLength) {
        int abs = Math.abs(id);
        String number = abs + "";
        int length = number.length();
        StringBuilder prefix = new StringBuilder();
        for (int i = 0; i < prefixLength; i++) {
            int index = i + (length - prefixLength);
            if (index >= 0) {
                char c = number.charAt(index);
                prefix.append(c);
            }
        }
        return prefix.toString();
    }

    public static ObjectStorage create(Config config, String path) {
        ObjectStorageConfigEntry configEntry = config.getGenericType(path, ObjectStorageConfigEntry.class);
        ObjectStorageImplementation implementation = configEntry.getImplementation();
        Function<ObjectStorageConfigEntry, ObjectStorage> supplier = implementation.getInstanceSupplier();
        return supplier.apply(configEntry);
    }

    @Data
    public static class ObjectStorageConfigEntry {

        private ObjectStorageImplementation implementation;
        private String url;
        private String defaultBucket;
        private String accessKey;
        private String accessSecret;
        private String region;
        private String storageType;
        private int retries = 3;

    }
}
