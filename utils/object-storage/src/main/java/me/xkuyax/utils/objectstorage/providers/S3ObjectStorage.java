package me.xkuyax.utils.objectstorage.providers;

import lombok.Data;
import lombok.extern.apachecommons.CommonsLog;
import me.xkuyax.utils.objectstorage.ObjectStorage;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.core.async.AsyncRequestBody;
import software.amazon.awssdk.core.async.AsyncResponseTransformer;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3AsyncClient;
import software.amazon.awssdk.services.s3.model.*;
import software.amazon.awssdk.utils.IoUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;

@Data
@CommonsLog
public class S3ObjectStorage extends ObjectStorage {

    private final Region region;
    private final S3AsyncClient s3Client;

    public S3ObjectStorage(ObjectStorageConfigEntry configEntry) {
        super(configEntry);
        this.region = Region.of(configEntry.getRegion());
        this.s3Client = S3AsyncClient.builder().credentialsProvider(() -> {
            return AwsBasicCredentials.create(configEntry.getAccessKey(), configEntry.getAccessSecret());
        }).region(region).build();
        configEntry.setRetries(0);
    }

    @Override
    public void store(String name, Path path) {
        PutObjectResponse response = s3Client.putObject(PutObjectRequest.builder()
                .bucket(configEntry.getDefaultBucket())
                .storageClass(StorageClass.fromValue(configEntry.getStorageType()))
                .key(name)
                .build(), path).join();
    }

    @Override
    public void store(String name, InputStream stream, long length) {
        try {
            AsyncRequestBody requestBody;
            Path tempFile = null;
            if (length > 100 * 1024 * 1024) {
                tempFile = Files.createTempFile("tempUpload", "");
                try (OutputStream writer = Files.newOutputStream(tempFile)) {
                    IoUtils.copy(stream, writer);
                }
                requestBody = AsyncRequestBody.fromFile(tempFile);
            } else {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream((int) length);
                IoUtils.copy(stream, byteArrayOutputStream);
                requestBody = AsyncRequestBody.fromBytes(byteArrayOutputStream.toByteArray());
            }
            PutObjectResponse response = s3Client.putObject(PutObjectRequest.builder()
                    .bucket(configEntry.getDefaultBucket())
                    .storageClass(StorageClass.fromValue(configEntry.getStorageType()))
                    .key(name)
                    .build(), requestBody).join();
            if (tempFile != null) {
                Files.deleteIfExists(tempFile);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(String name) {
        s3Client.deleteObject(DeleteObjectRequest.builder()
                .key(name)
                .bucket(configEntry.getDefaultBucket())
                .build());
    }

    @Override
    public int exists(String name) {
        try {
            HeadObjectResponse response = s3Client.headObject(HeadObjectRequest.builder()
                    .key(name)
                    .bucket(configEntry.getDefaultBucket())
                    .build()).join();
            return 1;
        } catch (NoSuchKeyException e) {
            return 0;
        } catch (Exception e) {
            return -1;
        }
    }

    @Override
    public void download(String name, Path path) {
        GetObjectResponse object = s3Client.getObject(GetObjectRequest.builder()
                .key(name)
                .bucket(configEntry.getDefaultBucket())
                .build(), AsyncResponseTransformer.toFile(path)).join();
    }

    @Override
    public InputStream downloadStream(String name) {
        return s3Client.getObject(GetObjectRequest.builder()
                .key(name)
                .bucket(configEntry.getDefaultBucket())
                .build(), AsyncResponseTransformer.toBytes()).join().asInputStream();

    }
}