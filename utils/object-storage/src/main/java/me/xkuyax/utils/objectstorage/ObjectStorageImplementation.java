package me.xkuyax.utils.objectstorage;

import lombok.Getter;
import me.xkuyax.utils.objectstorage.ObjectStorage.ObjectStorageConfigEntry;
import me.xkuyax.utils.objectstorage.providers.MinIoObjectStorage;
import me.xkuyax.utils.objectstorage.providers.S3ObjectStorage;

import java.util.function.Function;

@Getter
public enum ObjectStorageImplementation {

    S3(S3ObjectStorage::new),
    MIN_IO(MinIoObjectStorage::new);
    private final Function<ObjectStorageConfigEntry, ObjectStorage> instanceSupplier;

    ObjectStorageImplementation(Function<ObjectStorageConfigEntry, ObjectStorage> instanceSupplier) {
        this.instanceSupplier = instanceSupplier;
    }
}
