package me.xkuyax.utils.objectstorage;

import me.xkuyax.utils.config.Config;

public class Bootstrap {

    public static void main(String[] args) {
        Config config = new Config("api.yml");
        ObjectStorage storage = ObjectStorage.create(config, "Test");
        boolean retry = storage.existsRetry(ObjectStorage.numberWithPrefix(2000));
        System.out.println("retry = " + retry);
    }

}
