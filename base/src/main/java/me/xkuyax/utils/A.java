package me.xkuyax.utils;

import java.util.Arrays;
import java.util.List;

/**
 * Klasse ist fürs verkürzen von Arrays.aslist()
 *
 * @author Lukas
 */
@Deprecated
public class A {

    /**
     * Verkürzt nur arrays.aslist
     * @param b
     * @return
     */
    @Deprecated
    @SafeVarargs
    public static <T> List<T> a(T... b) {
        return Arrays.asList(b);
    }
}
