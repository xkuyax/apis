package me.xkuyax.utils;

public class StopWatch {

    private final String message;
    private long timeStamp;
    private long diff;
    private int print;

    public StopWatch(String message) {
        this.message = message;
        start();
    }

    public void start() {
        timeStamp = System.nanoTime();
    }

    public void snapshot() {
        diff = System.nanoTime() - timeStamp;
    }

    public void print() {
        snapshot();
        System.out.println("StopWatch: " + message + " " + diff + " ns " + diff / 1000 + " µs " + diff / 1000000 + " ms");
    }

    public void increment() {
        snapshot();
        System.out.println("StopWatch (" + print++ + "): " + message + " " + diff + " ns " + diff / 1000 + " µs " + diff / 1000000 + " ms");
    }
}
