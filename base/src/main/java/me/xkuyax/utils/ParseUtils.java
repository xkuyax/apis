package me.xkuyax.utils;

import java.util.function.Function;

public class ParseUtils {

    public static <T> T parse(String data, Function<String, T> supplier, T defaultValue) {
        try {
            return supplier.apply(data);
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public static long parseLong(String data, long l) {
        return parse(data, t -> Long.parseLong(data), l);
    }

    public static int parseInteger(String data, int l) {
        return parse(data, t -> Integer.parseInt(data), l);
    }

    public static double parseDouble(String data, double l) {
        return parse(data, t -> Double.parseDouble(data), l);
    }

}
