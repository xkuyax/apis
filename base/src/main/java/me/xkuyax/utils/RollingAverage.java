package me.xkuyax.utils;

import lombok.Getter;

import java.util.LinkedList;

@Getter
public class RollingAverage {
    
    private LinkedList<Double> data = new LinkedList<>();
    private int count = 0;
    private int track;
    private double sum;
    private double avg;
    
    public RollingAverage(int track) {
        Preconditions.checkArgument(track > 0, "Track has to be greater than 0!");
        this.track = track;
    }
    
    public void increment(double toAdd) {
        if (count >= track) {
            sum -= data.removeFirst();
            count--;
        }
        data.addLast(toAdd);
        count++;
        sum += toAdd;
        avg = sum / count;
    }
    
    public double avg() {
        return avg;
    }
    
    public void reset() {
        this.count = 0;
        sum = 0;
        avg = 0;
        data.clear();
    }
}
