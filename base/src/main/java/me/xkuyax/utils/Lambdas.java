package me.xkuyax.utils;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.function.Consumer;
import java.util.function.DoubleConsumer;
import java.util.function.IntConsumer;
import java.util.stream.DoubleStream;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Lambdas {
    
    public static <T> void forEachIterable(Iterable<T> stream, ExceptionConsumer<T> consumer) {
        stream.forEach(consumer);
    }
    
    public static <T> void forEach(Stream<T> stream, ExceptionConsumer<T> consumer) {
        stream.forEach(consumer);
    }
    
    public static void forEachInt(IntStream stream, ExceptionIntConsumer consumer) {
        stream.forEach(consumer);
    }
    
    public static void forEachDouble(DoubleStream stream, ExceptionDoubleConsumer consumer) {
        stream.forEach(consumer);
    }
    
    public static <T> void parallelForEach(int threads, Stream<T> stream, ExceptionConsumer<T> consumer) {
        ForkJoinPool forkJoinPool = new ForkJoinPool(threads);
        try {
            forkJoinPool.submit(() -> stream.parallel().forEach(consumer)).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        forkJoinPool.shutdown();
    }
    
    public static void parallelForEachInt(int threads, IntStream stream, ExceptionIntConsumer consumer) {
        ForkJoinPool forkJoinPool = new ForkJoinPool(threads);
        try {
            forkJoinPool.submit(() -> stream.parallel().forEach(consumer)).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        forkJoinPool.shutdown();
    }
    
    public static void parallelForEachDouble(int threads, DoubleStream stream, ExceptionDoubleConsumer consumer) {
        ForkJoinPool forkJoinPool = new ForkJoinPool(threads);
        try {
            forkJoinPool.submit(() -> stream.parallel().forEach(consumer)).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        forkJoinPool.shutdown();
    }
    
    public static <T> void parallelForEachPool(ForkJoinPool forkJoinPool, Stream<T> stream, ExceptionConsumer<T> consumer) {
        try {
            forkJoinPool.submit(() -> stream.parallel().forEach(consumer)).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }
    
    public static void parallelForEachIntPool(ForkJoinPool forkJoinPool, IntStream stream, ExceptionIntConsumer consumer) {
        try {
            forkJoinPool.submit(() -> stream.parallel().forEach(consumer)).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }
    
    public static void parallelForEachDoublePool(ForkJoinPool forkJoinPool, DoubleStream stream, ExceptionDoubleConsumer consumer) {
        try {
            forkJoinPool.submit(() -> stream.parallel().forEach(consumer)).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }
    
    @FunctionalInterface
    public interface ExceptionRunnable extends Runnable {
        
        @Override
        default void run() {
            try {
                runWithException();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        
        void runWithException() throws Exception;
    }
    
    @FunctionalInterface
    public interface ExceptionConsumer<T> extends Consumer<T> {
        
        @Override
        default void accept(T t) {
            try {
                consume(t);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        
        void consume(T t) throws Exception;
    }
    
    @FunctionalInterface
    public interface ExceptionSupplier<T> {
        
        T getWith() throws Exception;
    }
    
    @FunctionalInterface
    public interface ExceptionDoubleConsumer extends DoubleConsumer {
        
        @Override
        default void accept(double t) {
            try {
                consume(t);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        
        void consume(double t) throws Exception;
    }
    
    @FunctionalInterface
    public interface ExceptionIntConsumer extends IntConsumer {
        
        @Override
        default void accept(int t) {
            try {
                consume(t);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        
        void consume(int t) throws Exception;
    }
    
}
