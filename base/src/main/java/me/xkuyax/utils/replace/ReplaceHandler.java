package me.xkuyax.utils.replace;

import lombok.Getter;

import java.util.*;
import java.util.function.Function;
import java.util.regex.Pattern;

@Getter
public class ReplaceHandler<T> {

    protected Map<Pattern, Function<T, String>> variables = new LinkedHashMap<>();

    public ReplaceHandler<T> with(String var, Function<T, String> render) {
        variables.put(toPattern(var), render);
        return this;
    }

    public List<String> format(List<String> input, T context) {
        List<String> output = new ArrayList<>(input.size());
        for (String value : input) {
            output.add(format(value, context));
        }
        return output;
    }

    public String format(String input, T context) {
        return format(input, renderMap(context, getVariables()));
    }

    public String format(String input, Map<Pattern, String> rendered) {
        String[] line = {input};
        rendered.forEach((replace, with) -> line[0] = replace.matcher(line[0]).replaceAll(with));
        return line[0];
    }

    public Map<Pattern, String> renderMap(T context) {
        return renderMap(context, getVariables());
    }

    public Map<Pattern, String> renderMap(T context, Map<Pattern, Function<T, String>> variables) {
        Map<Pattern, String> rendered = new HashMap<>();
        variables.forEach((pattern, function) -> {
            String apply = function.apply(context);
            if (apply == null) {
                apply = "null";
            }
            rendered.put(pattern, apply);
        });
        return rendered;
    }

    public Pattern toPattern(String var) {
        return Pattern.compile(var);
    }

}
