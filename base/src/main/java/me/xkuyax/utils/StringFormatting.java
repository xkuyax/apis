package me.xkuyax.utils;

import java.util.ArrayList;
import java.util.List;

public class StringFormatting {

    public static String formatString(String s, List<String> r, List<String> o) {
        if (r == null || o == null) {
            return simpleColorFormat(s);
        }
        return simpleColorFormat(StringUtils.formatString(s, r, o));
    }

    public static ArrayList<String> formatStringList(List<String> lore) {
        ArrayList<String> tmp = new ArrayList<>();
        for (String s : lore) {
            tmp.add(simpleColorFormat(s));
        }
        return tmp;
    }

    public static ArrayList<String> formatStringList(List<String> lore, List<String> r, List<String> o) {
        ArrayList<String> tmp = new ArrayList<>();
        for (String s : lore) {
            if (s != null) {
                tmp.add(formatString(s, r, o));
            }
        }
        return tmp;
    }

    public static String simpleColorFormat(String textToTranslate) {
        return simpleColorFormat('&', textToTranslate);
    }

    public static String simpleColorFormat(char altColorChar, String textToTranslate) {
        char[] b = textToTranslate.toCharArray();

        for (int i = 0; i < b.length - 1; ++i) {
            if (b[i] == altColorChar && "0123456789AaBbCcDdEeFfKkLlMmNnOoRr".indexOf(b[i + 1]) > -1) {
                b[i] = 167;
                b[i + 1] = Character.toLowerCase(b[i + 1]);
            }
        }

        return new String(b);
    }
}
