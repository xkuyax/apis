package me.xkuyax.utils;

import java.util.ArrayList;
import java.util.List;

public class ListHelper {

    public static <T> List<T> sublist(List<T> list, int fromIndex, int toIndex) {
        if (fromIndex > list.size()) {
            return new ArrayList<>();
        }
        if (list.size() < toIndex) {
            return list.subList(fromIndex, list.size());
        }
        return list.subList(fromIndex, toIndex);
    }
}
