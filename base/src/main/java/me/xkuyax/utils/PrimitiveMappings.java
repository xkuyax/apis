package me.xkuyax.utils;

import lombok.Getter;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class PrimitiveMappings {

    @Getter
    private static final Map<Class<?>, Object> DEFAULTS;
    @Getter
    private static final Map<Class<?>, Class<?>> PRIMITIVE_WRAPPER_MAP;

    static {
        // Only add to this map via put(Map, Class<T>, T)
        Map<Class<?>, Object> map = new HashMap<>();
        put(map, boolean.class, false);
        put(map, Boolean.class, false);
        put(map, char.class, '\0');
        put(map, Character.class, '\0');
        put(map, byte.class, (byte) 0);
        put(map, Byte.class, (byte) 0);
        put(map, short.class, (short) 0);
        put(map, Short.class, (short) 0);
        put(map, int.class, 0);
        put(map, Integer.class, 0);
        put(map, long.class, 0L);
        put(map, Long.class, 0L);
        put(map, float.class, 0f);
        put(map, Float.class, 0f);
        put(map, double.class, 0d);
        put(map, Double.class, 0d);
        DEFAULTS = Collections.unmodifiableMap(map);

        Map<Class<?>, Class<?>> wrapperMap = new HashMap<>();
        wrapperMap.put(boolean.class, Boolean.class);
        wrapperMap.put(byte.class, Byte.class);
        wrapperMap.put(char.class, Character.class);
        wrapperMap.put(double.class, Double.class);
        wrapperMap.put(float.class, Float.class);
        wrapperMap.put(int.class, Integer.class);
        wrapperMap.put(long.class, Long.class);
        wrapperMap.put(short.class, Short.class);
        PRIMITIVE_WRAPPER_MAP = wrapperMap;
    }

    private static <T> void put(Map<Class<?>, Object> map, Class<T> type, T value) {
        map.put(type, value);
    }

    public static <T> T defaultValue(Class<T> type) {
        // Primitives.wrap(type).cast(...) would avoid the warning, but we can't use that from here
        @SuppressWarnings("unchecked") // the put method enforces this key-value relationship
                T t = (T) DEFAULTS.get(Objects.requireNonNull(type));
        return t;
    }

    public static boolean isWrapperType(Class clazz) {
        return DEFAULTS.containsKey(clazz);
    }

    public static Class<?> getWrapperClass(Class<?> clazz) {
        return getPRIMITIVE_WRAPPER_MAP().get(clazz);
    }
}
