package me.xkuyax.utils;

import java.util.Objects;

public class Preconditions {
    
    public static void checkArgument(boolean expression, String errorMessage) {
        if (!expression) {
            throw new IllegalArgumentException(String.valueOf(errorMessage));
        }
    }
    
    public static void checkNotNull(Object path, String errorMessage) {
        Objects.requireNonNull(path, errorMessage);
    }
    
    public static void checkState(boolean expression, Object errorMessage) {
        if (!expression) {
            throw new IllegalStateException(String.valueOf(errorMessage));
        }
    }
}
