package me.xkuyax.utils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class FileUtils {

    public static String removeInvalidFileNameChars(String title) {
        return title.replaceAll("[\\\\/:*?\"<>|]", "").trim();
    }

    public static String readAllString(Path path) throws IOException {
        return new String(Files.readAllBytes(path));
    }

    public static String getFileEnding(Path path) {
        String[] split = path.getFileName().toString().split("\\.");
        return split[split.length - 1];
    }
}
